"""
app.py: Main Dash app for the data explorer.

This module contains the main Dash app for the data explorer. It is responsible for loading the data, setting up the
Dash app, and running the Dash server.
"""

from dash import Dash, get_asset_url
from dash_bootstrap_templates import load_figure_template
import logging
from argparse import Namespace

from .utils import load_data, clean_dtypes, load_value_maps
from .nav import generate_navbar
from .callbacks import *
from .profile_report.callbacks import *
from .comparison_report.callbacks import *
from .dicom_comparison.callbacks import *
from .plotting.callbacks import *

dash_path = os.path.dirname(os.path.abspath(__file__))


def run_data_explorer(
        args: Optional[Namespace] = None
) -> None:
    """
    Run the data explorer Dash app.

    :param args: Optional Namespace object containing command line arguments. If None, command line arguments will be
        parsed.
    """
    logging.basicConfig(filename=os.path.join(dash_path, 'app.log'), level=logging.DEBUG)
    pd.options.mode.chained_assignment = None
    load_figure_template("lumen")

    app = Dash(
        __name__,
        external_stylesheets=[dbc.themes.FLATLY, os.path.join(dash_path, 'assets', 'custom.css')],
        meta_tags=[{"name": "viewport", "content": "width=device-width, initial-scale=1"}, ],
        suppress_callback_exceptions=True,
        # assets_folder=os.path.join(dash_path, 'assets'),
        assets_url_path=os.path.join(dash_path, 'assets'),
    )

    data_paths = args.data

    value_maps = load_value_maps(get_asset_url("value_maps.yml") if args.value_maps is None else args.value_maps)
    data_schema = get_asset_url("data_schema.yml") if args.data_schema is None else args.data_schema
    all_dicom_headers = args.all_dicom_headers if args.all_dicom_headers is not None else False
    plotting_category_limit = args.cat_limit if args.cat_limit is not None else 15
    host = '127.0.0.1' if args.host is None else args.host
    port = 5000 if args.port is None else args.port
    debug_mode = args.debug if args.debug is not None else False
    join_on = args.join_on if args.join_on is not None else "name"

    df = load_data(data_paths, join_on, "left")
    df = clean_dtypes(df, data_schema, value_maps, all_dicom_headers)
    dtypes = df.dtypes.apply(lambda x: x.name).to_dict()

    app.layout = dbc.Container([
        dcc.Location(id='url'),
        generate_navbar(),
        dbc.Container(id="page-content"),
        dcc.Store(id='dataframe', data=df.to_json(orient='split')),
        dcc.Store(id='dtypes', data=dtypes),
        dcc.Store(id='value-maps', data=value_maps),
        dcc.Store(id='cat-limit', data=plotting_category_limit),
        dcc.Store(id='all_dicom_headers_option', data=all_dicom_headers),
        dcc.Store(id='data-schema', data=data_schema),
        dcc.Store(id='data_schema_path', data=data_schema),
    ], fluid=True)

    app._favicon = get_asset_url('favicon.ico')

    app.run_server(
        port=port,
        host=host,
        debug=debug_mode,
        dev_tools_ui=debug_mode,
    )


if __name__ == '__main__':
    run_data_explorer()
