"""
callbacks.py: Callbacks for the comparison report tool.
"""

import dash
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
from dash import dcc, no_update, html
from typing import Tuple, Any, Optional, List
import tempfile
import os
import pandas as pd

from .comparison_report import generate_comparison_report
from ..utils import load_data_schema


@dash.callback(
    [Output('comp_sample_size', 'disabled'),
     Output('comp_seed', 'disabled')],
    [Input('comp_sample_report', 'value')]
)
def activate_sample_buttons(to_sample: bool) -> Tuple[bool, bool]:
    """
    Activates/deactivates the sample size and seed inputs depending on whether the sample switch is activated.

    :param to_sample: Whether to sample the data.
    :return: Tuple of booleans indicating whether the sample size and seed inputs should be disabled.
    """
    if to_sample:
        return False, False
    return True, True


@dash.callback(
    [Output('comp-button-container', 'children'),
     Output('split_col-input', 'disabled'),
     Output('comp_settings_dropdown', 'disabled')],
    [Input('generate-comparison-report-button', 'n_clicks')]
)
def update_comparison_button_color(n_clicks: int) -> Tuple[Any, bool, bool]:
    """
    Trigger report generation and update the loading status.

    :param n_clicks: Number of button clicks.
    :return: tuple: Button style and HTML content of the report.
    """
    if n_clicks == 0:
        return no_update, False, False
    elif n_clicks == 1:
        loading_button = dbc.Button(
            [
                " Generating - Please wait... ",
                dbc.Spinner(size="sm"),
            ], id='generate-comparison-report-button', color='success', outline=True, size="lg",
            n_clicks=n_clicks, disabled=True
        )
        return loading_button, True, True
    else:
        pass


@dash.callback(
    [Output('generate-comparison-report-button', 'style'),
     Output('comp_settings_dropdown', 'style'),
     Output('comp_report', 'srcDoc'),
     Output('save-comp-report-button', 'style')],
    [Input('generate-comparison-report-button', 'n_clicks')],
    [State('split_col-input', 'value'),
     State("comp_minimal_report", 'value'),
     State("comp_explorative", 'value'),
     State("comp_focused_report", 'value'),
     State("comp_continuous_var_interactions", 'value'),
     State("comp_sample_report", 'value'),
     State("comp_sample_size", 'value'),
     State("comp_seed", 'value'),
     State("dataframe", 'data'),
     State("dtypes", 'data'),
     State("all_dicom_headers_option", 'data'),
     State("value-maps", 'data'),
     State("data_schema_path", 'data')]
)
def update_comp_output(
        n_clicks: int,
        selected_column: str,
        minimal_report: bool,
        explorative: bool,
        focused_report: bool,
        continuous_var_interactions: bool,
        sample_report: bool,
        sample_size: int,
        seed: int,
        json_data: str,
        dtypes: dict,
        all_dicom_headers: bool,
        value_maps: dict,
        data_schema_path: str
) -> Tuple:
    """
    Trigger report generation and update the loading status.

    :param n_clicks: Number of button clicks.
    :param selected_column: Column to compare.
    :param minimal_report: Whether to generate a report using the 'minimal' setting (see YDataProfiling docs).
    :param explorative: Whether to generate a report using the 'explorative' setting (see YDataProfiling docs).
    :param focused_report: Whether to generate a report using a more focused subset of columns if large amounts of data.
    :param continuous_var_interactions: Whether to generate a report with continuous variable interactions calculated
    (see YDataProfiling docs).
    :param sample_report: Whether to generate a report using a sample of the data.
    :param sample_size: Size of the sample to use.
    :param seed: Seed for the random number generator when sampling.
    :param json_data: Dataframe in JSON format.
    :param dtypes: Dataframe dtypes.
    :param all_dicom_headers: Whether to include all DICOM headers specified within the schema - note that this may
    result in large dataframes / higher memory use and slower performance.
    :param value_maps: Value maps stored in dcc.Store.
    :param data_schema_path: Path to the data schema.
    :return: tuple: Button style and HTML content of the report.
    """
    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)

    if n_clicks > 0:
        comp_report_html = generate_comparison_report(
            df,
            all_dicom_headers,
            selected_column,
            minimal_report,
            explorative,
            focused_report,
            continuous_var_interactions,
            sample_report,
            sample_size,
            seed,
            value_maps,
            data_schema_path
        )
        return ({'display': 'none'},
                {'display': 'none'},
                comp_report_html,
                {'display': 'block'})

    return ({'display': 'block'},
            {'display': 'grid'},
            None,
            {'display': 'none'})


@dash.callback(
    Output('generate-comparison-report-button', 'disabled'),
    Input('split_col-input', 'value'),
    State("dataframe", 'data')
)
def enable_comp_generate_button(
        selected_column: str,
        json_data: str
) -> bool:
    """
    Enable the generate report button if a column has been selected.

    :param selected_column:
    :param json_data:
    :return:
    """
    df = pd.read_json(json_data, orient='split')
    if selected_column and selected_column in df.columns:
        return False
    return True


@dash.callback(
    Output("download-comp-report", "data"),
    [Input("save-comp-report-button", "n_clicks"), Input('comp_report', 'srcDoc')],
    prevent_initial_call=True,
)
def generate_file(n: int, rpt: Optional[str] = ""):
    """
    Generate a file and return its path for downloading.

    :param n: Number of button clicks
    :param rpt: Path to the file to download, if any.
    """
    if n:
        # Create a temporary file
        with tempfile.NamedTemporaryFile(delete=False, suffix=".html") as temp:
            temp.write(rpt.encode("utf-8"))
            temp_file_path = temp.name

        # Check if the file exists before trying to send it
        if not os.path.exists(temp_file_path):
            return no_update

        return dcc.send_file(temp_file_path)

    else:
        return no_update


@dash.callback(
    [Output('search-column-names-col', 'children')],
    Input('comp_focused_report', 'value'),
    [State("dataframe", 'data'),
     State("dtypes", 'data'),
     State("all_dicom_headers_option", 'data'),
     State("value-maps", 'data'),
     State("data_schema_path", 'data')]
)
def update_comp_search_input_options(
        focused_report: bool,
        json_data: str,
        dtypes: dict,
        all_dicom_headers: bool,
        value_maps: dict,
        data_schema_path: str
) -> List[html.Div]:
    """
    Update the search input options based on the focused report setting.

    :param focused_report: The focused report setting.
    :param json_data: The dataframe in JSON format.
    :param dtypes: The dataframe dtypes.
    :param all_dicom_headers: The all DICOM headers setting.
    :param value_maps: The value maps stored in dcc.Store.
    :param data_schema_path: The path to the data schema.
    :return: The search input object.
    """
    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)

    search_columns = [
        col for col in df.columns.tolist() if isinstance(df[col].dtype, pd.CategoricalDtype) and
                                              col not in value_maps["DONT_PLOT_COLS"] and
                                              col in load_data_schema(
            data_schema_path, all_dicom_headers=all_dicom_headers, focused_columns=focused_report
        ).keys()
    ]

    return [html.Div([
        dbc.Input(
            type="gt_col", placeholder="Start typing here...", list="search-column-names", className="me-2",
            id="split_col-input", debounce=True, autocomplete='off', persistence=True,
            persistence_type='session',
        ),
        html.Datalist(
            id="search-column-names",
            children=[
                html.Option(value=col) for col in search_columns
                ]
        )
    ]), ]

