"""
plotting.py: Functions for the interactive plotting page of data explorer.

This file contains functions for generating the interactive plotting area of the data explorer. The plotting page
allows the user to plot the data in the dataframe in various ways. The user can plot the data as a histogram,
pie chart, box plot, or violin plot. The user can also plot the data by a categorical variable.
"""


from dash import dcc
import dash_bootstrap_components as dbc
import plotly.express as px
from dash.exceptions import PreventUpdate
import plotly.graph_objects as go
import pandas as pd
import logging
from typing import Union, Optional, Tuple, List
from plotly.subplots import make_subplots

from ..utils import limit_categories, create_plot_button_group


dist_plot_add_plot_button_group = create_plot_button_group("dist_plot_add_plot-radios", "Secondary Plot:", [
    {"label": "Rug", "value": "rug"},
    {"label": "Box", "value": "box"},
    {"label": "Violin", "value": "violin"},
    {"label": "Off", "value": None},
], None)

dist_plot_bar_mode_button_group = create_plot_button_group("dist_plot_bar-radios", "Bar Mode:", [
    {"label": "Overlay", "value": "overlay"},
    {"label": "Group", "value": "group"},
    {"label": "Stack", "value": "stack"},
], 'overlay')

violin_plot_mode_button_group = create_plot_button_group("violin_plot_mode-radios", "Mode:", [
    {"label": "Group", "value": "group"},
    {"label": "Overlay", "value": "overlay"},
], "group")

violin_plot_box_button_group = create_plot_button_group("violin_plot_box-radios", "Box Plot:", [
    {"label": "On", "value": True},
    {"label": "Off", "value": False},
], False)

violin_plot_points_button_group = create_plot_button_group("violin_plot_points-radios", "Plot Points:", [
    {"label": "All", "value": "all"},
    {"label": "Outliers", "value": 'outliers'},
    {"label": "Suspected", "value": 'suspectedoutliers'},
    {"label": "Off", "value": False},
], False)

box_plot_points_button_group = create_plot_button_group("box_plot_points-radios", "Plot Points:", [
    {"label": "All", "value": "all"},
    {"label": "Outliers", "value": 'outliers'},
    {"label": "Suspected", "value": 'suspectedoutliers'},
    {"label": "Box Off", "value": 'only'},
    {"label": "Off", "value": False},
], False)


def check_x(func):
    """
    Decorator to raise PreventUpdate if the `x` parameter is None or evaluates to False.

    :param func: Function to wrap.
    :return: Wrapped function.
    """
    def wrapper(*args, **kwargs):
        x = args[1]
        if not x:
            raise PreventUpdate
        return func(*args, **kwargs)
    return wrapper


def join_plot_button_groups(groups: List) -> dbc.Container:
    """
    Join multiple button groups into a single container.

    :param groups: List of button groups.
    :return: The button groups in a single container.
    """
    return dbc.Container([
        dbc.Row([
            dbc.Col([
                group
            ], width='auto')
            for group in groups
        ], className="d-flex", justify="end", align='center'),
    ], className='g-0 mb-3')


def create_control_dropdown(
        dd_id: str,
        label: str,
        options: List[str],
        default_value: Optional[str] = None,
        placeholder_value: Optional[str] = None
) -> dbc.Container:
    """
    Create a dropdown control.

    :param dd_id: The ID of the dropdown.
    :param label: The label for the dropdown.
    :param options: The options for the dropdown.
    :param default_value: The default value for the dropdown, if any.
    :param placeholder_value: The placeholder value for the dropdown, if any.
    :return: The dropdown control in a Container.
    """
    return dbc.Container([
        dbc.Row([
            dbc.Col([
                dbc.Label(label),
            ], width='auto', className="pt-1"),
            dbc.Col([
                dcc.Dropdown(id=dd_id, options=options, value=default_value, placeholder=placeholder_value),
            ], xl=8, lg=7, md=6, width=12),
        ], className="d-flex", justify="between", align='center'),
    ])


def get_controls(
        x_options: List[str],
        y_options: List[str],
        by_options: List[str],
        default_x: str
) -> dbc.Card:
    """
    Get the controls for the plotting page.

    :param x_options: The options for the x-axis dropdown.
    :param y_options: The options for the y-axis dropdown.
    :param by_options: The options for the by dropdown.
    :param default_x: The default value for the x-axis dropdown.
    :return: A Card containing the controls.
    """
    return dbc.Card([
        dbc.Col([
            create_control_dropdown("x-variable-dropdown", "X Variable:", x_options, default_x),
            create_control_dropdown("y-variable-dropdown", "Y Variable:", y_options, None, "..."),
            create_control_dropdown("by-variable-dropdown", "By:", by_options, None, "..."),
        ], width='auto'),
        dbc.CardFooter("N.B. Fields with caps. are from DICOMs...", className="text-muted mt-3"),
    ], body=True, className="w-100 mb-3", )


def bin_numeric_column(data: pd.DataFrame, col: str, bins: Optional[int] = 5) -> pd.DataFrame:
    """
    Bins a numeric column.

    :param data: Original DataFrame.
    :param col: Column to bin.
    :param bins: Number of bins.
    :return: DataFrame with binned column.
    """
    try:
        data.loc[:, col] = pd.cut(data[col], bins=bins).astype(str).str.strip("(").str.strip("]").fillna('Missing')
        return data
    except TypeError:
        return data
    except Exception as e:
        logging.warning(e)
        return data


def get_title_text(x: str) -> str:
    """
    Get the title text for a string and replace underscores with spaces.

    :param x:
    :return:
    """
    if x.islower():
        return x.title().replace('_', ' ')
    else:
        return x


def prep_variable(
        data: pd.DataFrame,
        col: str,
        col_type: str,
        fig_title: str,
        x_col: Optional[str] = None,
        bins: Optional[int] = 5
) -> Tuple[pd.DataFrame, str, str]:
    """
    Prepares a variable for plotting.

    :param data: The DataFrame.
    :param col: The column name to prepare.
    :param col_type: The type of column.
    :param fig_title: The title of the figure.
    :param x_col: The x-axis column, if any.
    :param bins: The number of bins to use for binning if the column is numeric, if any.
    :return: The prepared DataFrame, the figure title, and the label for the legend.
    """
    _data = data.copy()

    if pd.api.types.is_numeric_dtype(_data[col]) and x_col is None:
        _data = bin_numeric_column(_data, col, bins=bins)
    elif x_col:
        if pd.api.types.is_numeric_dtype(_data[x_col]) and pd.api.types.is_numeric_dtype(_data[col]):
            _data = bin_numeric_column(_data, col, bins=bins)

    if isinstance(_data[col].dtype, pd.CategoricalDtype):
        _data[col] = _data[col].cat.remove_unused_categories()

    title_prefix = "by" if col_type == 'by' else "vs."
    label = get_title_text(col)
    fig_title += f" {title_prefix} {label}"

    return _data, fig_title, label


@check_x
def generate_dist_plot(
        data: pd.DataFrame,
        x: str,
        by: Optional[str] = None,
        distribution: Optional[str] = None,
        bar_mode: Optional[str] = 'overlay',
        cat_limit: Optional[int] = 15
) -> Union[None, go.Figure]:
    """
    Generate a distribution plot using Plotly Express.

    :param data: The DataFrame containing the data.
    :param x: The column name for the x-axis.
    :param by: The column name for grouping, if any.
    :param distribution: The type of distribution plot to generate, if any.
    :param bar_mode: The bar mode for the distribution plot, default 'overlay'.
    :param cat_limit: The maximum number of categories to show, default 15.
    :return: The Plotly Figure object.
    """
    # _data = fill_missing_categories(data, [col for col in [x, by] if col])
    _data = data.copy()
    for col in [x, by]:
        if col:
            if isinstance(data[col].dtype, pd.CategoricalDtype):
                _data[col] = limit_categories(_data[col], cat_limit)

    fig_title, legend_title_text = get_title_text(x), None

    if by:
        _data, fig_title, legend_title_text = prep_variable(_data, by, 'by', fig_title)

    fig_obj = px.histogram(_data, x=x, color=by, marginal=distribution, opacity=0.6)
    fig_obj.update_layout(
        xaxis_title=get_title_text(x),
        yaxis_title='Frequency',
        barmode=bar_mode,
        title_text=fig_title,
        legend_title_text=legend_title_text,
    )
    return fig_obj


@check_x
def generate_box_plot(
        data: pd.DataFrame,
        x: str,
        y: Optional[str] = None,
        by: Optional[str] = None,
        points_mode: Optional[bool] = False,
        cat_limit: Optional[int] = 15
) -> Union[None, go.Figure]:
    """
    Generate a box plot using Plotly Express.

    :param data: The DataFrame containing the data.
    :param x: The column name for the x-axis.
    :param y: The column name for the y-axis, if any.
    :param by: The column name for grouping, if any.
    :param points_mode: Whether to use points mode, default False.
    :param cat_limit: The maximum number of categories to show, default 15.
    :return: The Plotly Figure object.
    """
    _data = data.copy()
    for col in [x, y, by]:
        if col:
            if isinstance(data[col].dtype, pd.CategoricalDtype):
                _data[col] = limit_categories(_data[col], cat_limit)

    fig_title, y_label, legend_title_text = get_title_text(x), None, None

    if y:
        _data, fig_title, y_label = prep_variable(_data, y, 'y', fig_title, x_col=x)

    if by:
        _data, fig_title, legend_title_text = prep_variable(_data, by, 'by', fig_title)

    if points_mode == "only":
        fig_obj = px.strip(_data, x=x, y=y, color=by)
    else:
        fig_obj = px.box(_data, x=x, y=y, color=by, points=points_mode)

    fig_obj.update_layout(
        xaxis_title=get_title_text(x),
        yaxis_title=y_label,
        title_text=fig_title,
        legend_title_text=legend_title_text,
    )
    return fig_obj


@check_x
def generate_violin_plot(
        data: pd.DataFrame, x: str, y: Optional[str] = None, by: Optional[str] = None,
        mode: Optional[str] = 'group', box_mode: Optional[bool] = False,
        points_mode: Optional[str] = None, cat_limit: Optional[int] = 15
) -> Union[None, go.Figure]:
    """
    Generate a violin plot using Plotly Express.

    :param data: DataFrame containing the data.
    :param x: Column name for the x-axis.
    :param y: Column name for the y-axis, if any.
    :param by: Column name for grouping, if any.
    :param mode: Mode for the violin plot, default 'group'.
    :param box_mode: Whether to show a box plot inside the violin plot, default False.
    :param points_mode: Mode for displaying individual points, if any.
    :param cat_limit: Maximum number of categories to show, default 15.
    :return: Plotly Figure object.
    """
    # _data = fill_missing_categories(data, [col for col in [x, y, by] if col])
    _data = data.copy()
    for col in [x, y, by]:
        if col:
            if isinstance(data[col].dtype, pd.CategoricalDtype):
                _data[col] = limit_categories(_data[col], cat_limit)

    fig_title, y_label, legend_title_text = get_title_text(x), None, None

    if y:
        _data, fig_title, y_label = prep_variable(_data, y, 'y', fig_title, x_col=x)

    if by:
        _data, fig_title, legend_title_text = prep_variable(_data, by, 'by', fig_title)

    fig_obj = px.violin(_data, x=x, y=y, color=by, violinmode=mode, box=box_mode, points=points_mode)
    fig_obj.update_layout(
        xaxis_title=get_title_text(x),
        yaxis_title=y_label,
        title_text=fig_title,
        legend_title_text=legend_title_text,
    )
    return fig_obj


@check_x
def generate_pie_plot(
        data: pd.DataFrame,
        x: str,
        by: Optional[str] = None,
        cat_limit: Optional[int] = 15
) -> Union[None, go.Figure]:
    """
    Generate a pie plot using Plotly Express.

    :param data: The DataFrame containing the data.
    :param x: The column name for the x-axis.
    :param by: The column name for grouping, if any.
    :param cat_limit: The maximum number of categories to show, default 15.
    :return: The Plotly Figure object.
    """
    _data = data.copy()

    for col in [x, by]:
        if col:
            if isinstance(data[col].dtype, pd.CategoricalDtype):
                _data[col] = limit_categories(_data[col], cat_limit)

    if pd.api.types.is_numeric_dtype(_data[x]):
        if len(_data[x].unique()) > 10:
            _data = bin_numeric_column(_data, x, bins=10)

    fig_title = get_title_text(x)

    if by:
        _data, fig_title, label = prep_variable(_data, by, 'by', fig_title, bins=6)
        cats = _data[by].value_counts().nlargest(6).index.tolist()
        n_cats = len(cats)

        max_rows, max_cols = 2, 3
        if n_cats in [3, 4]:
            n_rows, n_cols = 2, 2
        else:
            n_cols = min(n_cats, max_cols)
            n_rows = min((n_cats - 1) // max_cols + 1, max_rows)

        specs = [[{'type': 'domain'} for _ in range(n_cols)] for _ in range(n_rows)]
        subtitles = [f"{get_title_text(str(cat))}" for cat in cats]
        fig_obj = make_subplots(rows=n_rows, cols=n_cols, specs=specs, subplot_titles=subtitles)

        for i, cat in enumerate(cats):
            sub_fig = px.pie(_data[_data[by] == cat], x, opacity=0.6, hole=0.3)
            for trace in sub_fig['data']:
                fig_obj.add_trace(trace, row=i // n_cols + 1, col=i % n_cols + 1)
    else:
        fig_obj = px.pie(_data, x, opacity=0.6, hole=0.3)

    fig_obj.update_layout(
        legend_title_text=get_title_text(x),
        title_text=fig_title,
    )
    return fig_obj


def get_plotting_cols(
        df: pd.DataFrame,
        dont_plot_cols: List[str] = None
) -> Tuple[List[str], List[str], List[str], str]:
    """
    Get the columns to use for plotting.

    :param df: The DataFrame.
    :param dont_plot_cols: Columns to exclude from plotting.
    :return: The columns to use for plotting.
    """
    plotting_cols = [col for col in df.columns if "path" not in col and col not in dont_plot_cols]

    x_options = [col for col in df.columns if col in plotting_cols]
    y_options = x_options.copy()
    # by_options = x_options.copy()
    by_options = [col for col in df.columns if col in plotting_cols and isinstance(df[col].dtype, pd.CategoricalDtype)]

    if "age" in x_options:
        default_x = "age"
    elif "sex" in x_options:
        default_x = "sex"
    else:
        default_x = x_options[0]

    return x_options, y_options, by_options, default_x


def make_table_from_df(df: pd.DataFrame) -> dbc.Table:
    """
    Make a dash-bootstrap-components table from a DataFrame.

    :param df: The DataFrame.
    :return: The dash-bootstrap-components Table.
    """
    return dbc.Table.from_dataframe(df, striped=True, hover=True, index=True, responsive=True,)


def generate_plotting_table(
        data: pd.DataFrame,
        x: str = None,
        y: Optional[str] = None,
        by: Optional[str] =None
) -> dbc.Table:
    """
    Generate a table of summary statistics for the plotting page.

    :param data: The DataFrame.
    :param x: The column name for the x-axis.
    :param y: The column name for the y-axis, if any.
    :param by: The column name for grouping, if any.
    :return: The dash-bootstrap-components Table.
    """
    if x is None:
        x = "age" if "age" in data.columns else data.columns[0]
    if by:
        stats = data.groupby(by)[[x]].describe().transpose().reset_index(level=0).drop('level_0', axis=1)
        missing = pd.DataFrame(data.groupby(by)[[x]].apply(lambda g: g[x].isna().sum()), columns=['missing']).transpose()
        missing_percent = pd.DataFrame(
            data.groupby(by)[[x]].apply(lambda g: g[x].isna().sum() / g.shape[0]), columns=['missing %']
        ).transpose()
        out_data = pd.concat([stats, missing, missing_percent])
        out_data = out_data.round(2)
        return make_table_from_df(out_data.rename_axis("Statistic"))
    else:
        stats = data[[x]].describe()
        missing = pd.DataFrame({x: data[x].isna().sum()}, index=['missing'])
        missing_percent = pd.DataFrame({x: data[x].isna().sum() / data.shape[0]}, index=['missing %'])
        out_data = pd.concat([stats, missing, missing_percent])
        out_data = out_data.round(2)
        return make_table_from_df(out_data.rename_axis("Statistic"))


def create_plotting_layout(df: pd.DataFrame, dont_plot_cols: List[str] = None) -> dbc.Container:
    """
    Create the layout for the plotting page.

    :param df: The DataFrame.
    :param dont_plot_cols: The columns to exclude from plotting.
    :return: The layout for the plotting page.
    """
    return dbc.Container([
        dbc.Col([
            dbc.Row([
                dbc.Col([
                    get_controls(*get_plotting_cols(df, dont_plot_cols)),
                    dbc.Card(id='plotting-table', children=[generate_plotting_table(df)], body=True)
                ], width=4, className='h-100 g-0', style={"overflow-y": "auto"}),
                dbc.Col([
                    dbc.Card([
                        dbc.CardHeader(dbc.Tabs([
                            dbc.Tab(label="Dist. Plot", tab_id="plotting_dist-tab", id="plotting_dist-tab"),
                            dbc.Tab(label="Pie", tab_id="plotting_pie-tab", id="plotting_pie-tab"),
                            dbc.Tab(label="Box", tab_id="plotting_box-tab", id="plotting_box-tab"),
                            dbc.Tab(label="Violin", tab_id="plotting_violin-tab", id="plotting_violin-tab"),
                        ], id="plotting-plot-card-tabs", active_tab="plotting_dist-tab", persistence=True,
                            persistence_type="session", )
                        ),
                        dbc.CardBody(dbc.Container(id="plotting-plot-card-content", className="h-100"),
                                     className="h-100", style={"overflow-y": "auto"}),
                    ], className='h-100 g-0'),
                ], width=8, className='h-100 ps-3 g-0'),
            ], className='h-100 g-0'),
        ], className="mt-3 mb-3 g-0", style={"height": "80vh"}),
    ], fluid=True)
