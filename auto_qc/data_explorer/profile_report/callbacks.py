"""
callbacks.py: Callbacks for the profile report tool.
"""

import dash
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
from dash import dcc, no_update
from typing import Tuple
import tempfile
import os
import pandas as pd

from .profile_report import generate_profile_report


@dash.callback(
    [Output('norm_sample_size', 'disabled'),
     Output('norm_seed', 'disabled')],
    [Input('norm_sample_report', 'value')]
)
def activate_sample_buttons(to_sample):
    if to_sample:
        return False, False
    return True, True


@dash.callback(
    [Output('profile-button-container', 'children'),
     Output('norm_settings_dropdown', 'disabled')],
    [Input('generate-report-button', 'n_clicks')]
)
def update_button_color(n_clicks: int):
    """Trigger report generation and update the loading status.

    :param n_clicks: Number of button clicks.
    :return: tuple: Button style and HTML content of the report.
    """
    if n_clicks == 0:
        return dbc.Button('Generate Report', id='generate-report-button', size="lg", n_clicks=0), False
    elif n_clicks == 1:
        # return 'warning', True
        loading_button = dbc.Button([
            " Generating - Please wait... ",
            dbc.Spinner(size="sm"),
        ], id='generate-report-button', color='success', outline=True, size="lg", n_clicks=n_clicks, disabled=True)
        return loading_button, True
    else:
        pass


@dash.callback(
    [Output('generate-report-button', 'style'),
     Output('norm_settings_dropdown', 'style'),
     Output('profile-report', 'srcDoc'),
     Output('save-report-button', 'style')],
    [Input('generate-report-button', 'n_clicks')],
    [State("norm_minimal_report", 'value'),
     State("norm_explorative", 'value'),
     State("norm_focused_report", 'value'),
     State("norm_continuous_var_interactions", 'value'),
     State("norm_sample_report", 'value'),
     State("norm_sample_size", 'value'),
     State("norm_seed", 'value'),
     State("dataframe", 'data'),
     State("dtypes", 'data'),
     State("all_dicom_headers_option", 'data'),
     State("value-maps", 'data'),
     State("data_schema_path", 'data')]
)
def update_output(
        n_clicks: int,
        minimal_report: bool,
        explorative: bool,
        focused_report: bool,
        continuous_var_interactions: bool,
        sample_report: bool,
        sample_size: int,
        seed: int,
        json_data,
        dtypes: dict,
        all_dicom_headers: bool,
        value_maps: dict,
        data_schema_path: str
) -> Tuple:
    """Trigger report generation and update the loading status.

    :param n_clicks: Number of button clicks.
    :param minimal_report: Whether to generate the report with the minimal setting (see ydata_profiling docs).
    :param explorative: Whether to generate the report with the explorative setting (see ydata_profiling docs).
    :param focused_report: Whether to generate the report a more focused subset of columns, as defined in the data schema. This can be used if that is lots of data.
    :param continuous_var_interactions: Whether to generate the report with interactions between continuous variables (see ydata_profiling docs).
    :param sample_report: Whether to generate the report with a sample of the data.
    :param sample_size: Size of the sample to use for the report.
    :param seed: Seed to use for sampling.
    :return: tuple: Button style and HTML content of the report.
    """
    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)

    if n_clicks > 0:
        # Generate the report and get its HTML content
        report_html = generate_profile_report(
            df,
            all_dicom_headers,
            minimal_report,
            explorative,
            focused_report,
            continuous_var_interactions,
            sample_report,
            sample_size,
            seed,
            value_maps,
            data_schema_path
        )
        return {'display': 'none'}, {'display': 'none'}, report_html, {'display': 'block'}

    return {'display': 'block'}, {'display': 'grid'}, None, {'display': 'none'}


@dash.callback(
    Output("download-report", "data"),
    [Input("save-report-button", "n_clicks"), Input('profile-report', 'srcDoc')],
    prevent_initial_call=True,
)
def generate_file(n: int, rpt: str = ""):
    """
    Generate a file and return its path for downloading.

    :param n: Number of button clicks
    :param rpt: Path to the file to download
    :return: A dictionary containing the file path
    """
    if n:
        # Create a temporary file
        with tempfile.NamedTemporaryFile(delete=False, suffix=".html") as temp:
            temp.write(rpt.encode("utf-8"))
            temp_file_path = temp.name

        # Check if the file exists before trying to send it
        if not os.path.exists(temp_file_path):
            return no_update

        return dcc.send_file(temp_file_path)

    else:
        return no_update

