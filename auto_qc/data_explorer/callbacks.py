"""
callbacks.py: Contains all the callbacks for the main app.
"""

import dash
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
from dash import html
import pandas as pd
from typing import List, Any

from .profile_report.profile_report import create_profile_report_layout
from .comparison_report.comparison_report import create_comparison_report_layout
from .dicom_comparison.dicom_comparison import create_qc_dicom_comp_layout
from .plotting.plotting import create_plotting_layout


@dash.callback(
        Output("navbar-collapse", "is_open"),
        [Input("navbar-toggler", "n_clicks")],
        [State("navbar-collapse", "is_open")],
    )
def toggle_navbar_collapse(n: int, is_open: bool) -> bool:
    """
    Callback to toggle the navbar collapse on small screens.

    :param n: Number of clicks on the navbar toggler.
    :param is_open: Whether the navbar is open or not.
    :return: Whether the navbar should be open or not.
    """
    if n:
        return not is_open
    return is_open


@dash.callback(
    Output("page-content", "children"),
    Input("url", "pathname"),
    [State("dataframe", 'data'),
     State("dtypes", 'data'),
     State("value-maps", 'data'),
     State("all_dicom_headers_option", 'data'),
     State("data_schema_path", 'data')]
)
def render_page_content(
        pathname: str,
        json_data: str,
        dtypes: dict,
        value_maps: dict,
        all_dicom_headers: bool,
        data_schema_path: str
) -> Any:
    """
    Callback to render the page content.

    :param pathname: The current pathname.
    :param json_data: The dataframe as a JSON string.
    :param dtypes: The data types of the data.
    :param value_maps: The value maps.
    :param all_dicom_headers: Whether to show all DICOM headers or not according to the data schema.
    :param data_schema_path: The path to the data schema.
    :return: The page content.
    """
    df = pd.read_json(json_data, orient='split')
    df = df.astype(dtypes)

    if pathname in ["/", "/profile"]:
        return create_profile_report_layout()
    elif pathname == "/comparison":
        return create_comparison_report_layout(
            df, value_maps, all_dicom_headers, data_schema_path
        )
    elif pathname == "/qc_dicom_comp":
        return create_qc_dicom_comp_layout()
    elif pathname == "/plotting":
        return create_plotting_layout(df, value_maps['DONT_PLOT_COLS'])
    else:
        return html.Div(
            dbc.Container(
                [
                    html.H1("404: Not found", className="text-danger"),
                    html.P(
                        f"The pathname {pathname} was not recognised...",
                        className="lead",
                    ),
                    html.Hr(className="my-2"),
                    html.P(
                        "Please navigate using the links above.",
                        className="lead",
                    ),
                ],
                fluid=True,
                className="py-3",
            ),
            className="p-3 bg-light rounded-3",
        )


@dash.callback(
    [
        Output("profile-link", "active"),
        Output("plotting-link", "active"),
        Output("comparison-link", "active"),
        Output("qc_dicom_comp-link", "active"),
    ],
    [Input("url", "pathname")]
)
def update_active_navlink(pathname: str) -> List[bool]:
    """
    Callback to update the active navlink.

    :param pathname: The current pathname.
    :return: The active navlink.
    """
    return [
        pathname in ["/", "/profile"],
        pathname == "/plotting",
        pathname == "/comparison",
        pathname == "/qc_dicom_comp",
    ]
