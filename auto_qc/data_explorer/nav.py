"""
nav.py: Generate the navbar for the data explorer app.
"""

from dash import html, get_asset_url
import dash_bootstrap_components as dbc

gitlab_link = 'https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/auto-qc'  # Link to GitLab repo
docs_link = 'https://ias49.uniofcam.dev/auto-qc/user_guide/data_explorer/overview.html'  # Link to documentation


def generate_navbar() -> dbc.Navbar:
    """
    Generate the navbar for the data explorer app.

    :return: The navbar.
    """
    return dbc.Navbar(
        dbc.Container([
            html.A(
                dbc.Row([
                    dbc.Col(html.Img(src=get_asset_url('autoqc_logo.png'), height="30px")),
                    dbc.Col(dbc.NavbarBrand("AutoQC Data Explorer", className="ms-2")),
                ], align="center", className="g-0 flex-nowrap"),
                href=gitlab_link,
                style={"textDecoration": "none"},
            ),
            dbc.NavbarToggler(id="navbar-toggler", n_clicks=0),
            dbc.Collapse([
                dbc.Nav([
                    dbc.NavItem(dbc.NavLink("Profiling Report", href="/profile", id="profile-link")),
                    dbc.NavItem(dbc.NavLink("Comparison Report", href="/comparison", id="comparison-link")),
                    dbc.NavItem(dbc.NavLink("QC vs. DICOM", href="/qc_dicom_comp", id="qc_dicom_comp-link")),
                    dbc.NavItem(dbc.NavLink("Interactive Plotting", href="/plotting", id="plotting-link")),
                    dbc.NavItem(dbc.NavLink("Help", href=docs_link, target="_blank", id="help-link"))
                ], className="g-0 ms-auto flex-nowrap mt-3 mt-md-0", navbar=True,
                    # pills=True
                ),
            ], id="navbar-collapse", is_open=False, navbar=True),
        ]),
        color="dark",
        dark=True,
        sticky="top",
        expand="lg",
    )

