"""
metrics.py: Custom Metrics for the AutoQC Package.

This module contains custom metrics for the AutoQC package. These metrics are
used to evaluate the performance of a model during training and testing.
"""

from .lazytf import K, tf

def recall(y_true, y_pred):
    """
    Compute the recall.

    :param y_true: Tensor of true targets.
    :param y_pred: Tensor of predicted targets.

    :return: Recall score.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    res = true_positives / (possible_positives + K.epsilon())
    return res

def precision(y_true, y_pred):
    """
    Compute the precision.

    :param y_true: Tensor of true targets.
    :param y_pred: Tensor of predicted targets.

    :return: Precision score.
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    res = true_positives / (predicted_positives + K.epsilon())
    return res

def f1(y_true, y_pred):
    """
    Compute the F1 score, also known as balanced F-score or F-measure.

    :param y_true: Tensor of true targets.
    :param y_pred: Tensor of predicted targets.

    :return: F1 score.
    """
    val_precision = precision(y_true, y_pred)
    val_recall = recall(y_true, y_pred)
    return 2*((val_precision*val_recall)/(val_precision+val_recall+K.epsilon()))
    

def angle_difference(x, y):
    """
    Calculate minimum difference between two angles.

    :param x: First angle.
    :param y: Second angle.

    :return: Minimum difference between the two angles.
    """
    return 180 - abs(abs(x - y) - 180)


def angle_error(y_true, y_pred):
    """
    Calculate the mean difference between the true angles
    and the predicted angles. Each angle is represented
    as a binary vector.

    :param y_true: Tensor of true angles.
    :param y_pred: Tensor of predicted angles.

    :return: Mean difference between true and predicted angles.
    """
    diff = angle_difference(K.argmax(y_true), K.argmax(y_pred))
    return K.mean(K.cast(K.abs(diff), K.floatx()))


def focal_loss(gamma=2., alpha=.25):
    def focal_loss_fixed(y_true, y_pred):
        pt_1 = tf.where(tf.equal(y_true, 1), y_pred, tf.ones_like(y_pred))
        pt_0 = tf.where(tf.equal(y_true, 0), y_pred, tf.zeros_like(y_pred))
        return -K.sum(alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1)) - K.sum((1-alpha) * K.pow(pt_0, gamma) * K.log(1. - pt_0))
    return focal_loss_fixed
