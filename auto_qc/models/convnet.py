"""
convnet.py: Convolutional Neural Network (CNN) Model Definition.

This module defines a CNN model using the Keras API. The model is based on the
one defined in the `BaseNet` class in `basenet.py`. The model is defined using
the `keras` API, and is compiled using the `Adam` optimizer and the
`binary_crossentropy` loss function. The model is also configured to use the
`f1` metric for evaluation.
"""

from typing import Tuple

from .basenet import BaseNet
from .lazytf import Adam, K, keras, Model
from .metrics import f1

LOSS = "binary_crossentropy"
ACCURACY = "accuracy"


def ConvNet(
    input_shape: Tuple[int, int, int] = (None, None, 1),
    learning_rate: float = 1e-3,
    loss: str = None,
    accuracy: str = None,
    dropout: bool = True,
    nlayers: int = 2,
    nblocks: int = 3,
    nfilters: int = 20,
    nunits: int = 64,
    augmentation: bool = True,
    crop: bool = True,
    name: str = None,
) -> Model:
    """
    Generates a Convolutional Neural Network (CNN) using the base model defined in BaseNet.

    :param input_shape: Shape of input images, defaults to (None, None, 1)
    :param learning_rate: Learning rate for the Adam optimizer, defaults to 1e-4
    :param loss: The identifier for the loss function to be used, defaults to binary cross entropy loss if not provided
    :param accuracy: The identifier for the accuracy metric to be used, defaults to binary accuracy if not provided
    :param dropout: Whether to add a Dropout layer in the model, defaults to True
    :param nlayers: The number of convolutional layers in each block, defaults to 2
    :param nblocks: The number of blocks in the network, defaults to 3
    :param nfilters: The initial number of filters, defaults to 20
    :param nunits: The number of units in the Dense layer, defaults to 64
    :param augmentation: Whether to include data augmentation layers in the model, defaults to True
    :param crop: Whether to include a crop and resize layer in the model, defaults to True
    :param name: The name of the model, defaults to 'ConvNet' if not provided

    :return: Compiled TensorFlow model
    """
    loss = loss or LOSS
    accuracy = accuracy or ACCURACY

    metrics = [
        keras.metrics.BinaryAccuracy(name="accuracy"),
        keras.metrics.TruePositives(name='tp'),
        keras.metrics.FalsePositives(name='fp'),
        keras.metrics.TrueNegatives(name='tn'),
        keras.metrics.FalseNegatives(name='fn'), 
        keras.metrics.Precision(name='precision'),
        keras.metrics.Recall(name='recall'),
        f1,
        keras.metrics.AUC(name='auc'),
        keras.metrics.AUC(name='prc', curve='PR'), # precision-recall curve
      ]

    K.clear_session()

    model = BaseNet(
        input_shape=input_shape,
        dropout=dropout,
        nlayers=nlayers,
        nblocks=nblocks,
        nfilters=nfilters,
        nunits=nunits,
        nclasses=1,
        augmentation=augmentation,
        crop=crop,
    )

    model._name = name or "ConvNet"
    model.compile(
        optimizer=Adam(learning_rate=learning_rate),
        loss=loss,
        metrics=metrics)
    model.summary()
    return model
