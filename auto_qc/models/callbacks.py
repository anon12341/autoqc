"""
callbacks.py: Callbacks for the AutoQC Package.

This module defines custom callbacks for the TensorFlow model training process.
"""

from .lazytf import tf

class CustomStopper(tf.keras.callbacks.EarlyStopping):
    """
    Custom callback that extends the EarlyStopping callback in TensorFlow.
    This callback will stop training when a monitored metric has stopped improving.
    """
    def __init__(self, **kwargs):
        self.start_epoch = kwargs.pop("start_epoch")
        super().__init__(**kwargs)

    def on_epoch_end(self, epoch, logs=None):
        # if epoch > self.start_epoch:
        super().on_epoch_end(epoch, logs)

class CustomCheckpoint(tf.keras.callbacks.ModelCheckpoint):
    """
    Custom callback that extends the ModelCheckpoint callback in TensorFlow.
    This callback will save the model after each epoch.
    """
    def __init__(self, **kwargs):
        self.start_epoch = kwargs.pop("start_epoch")
        super().__init__(**kwargs)

    def on_epoch_end(self, epoch, logs=None):
        # if epoch > self.start_epoch:
        super().on_epoch_end(epoch, logs)

def get_callbacks(checkpoint=None, early_stopping=False, logfile=None, tuner=False, model_name=None):
    """
    Return a list of TensorFlow callbacks.

    :param checkpoint: path to save the model file, defaults to None
    :param early_stopping: whether to use early stopping, defaults to False
    :param logfile: path to save the log file, defaults to None
    :param tuner: whether the function is used in a hyperparameter tuning scenario, defaults to False
    :return: a list of TensorFlow callbacks
    """
    accuracy = "accuracy" # model.compiled_metrics._metrics[0]
    # loss = model.loss
    callbacks = []

    if model_name=="RotNet":
        accuracy = "angle_error"
        mode = "min"
    else:
        # metric = "auc"
        mode = "max"

    reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(
        monitor='val_loss', factor=0.5, patience=3, min_lr=1e-6)
    callbacks.append(reduce_lr)

    if checkpoint and (not tuner):
        checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
            filepath=checkpoint,
            save_weights_only=False,
            monitor=f"val_{accuracy}",
            mode=mode,
            save_best_only=True,
            start_epoch=6,
        )
        callbacks.append(checkpoint_callback)

    if early_stopping:
        early_callback = CustomStopper(
            monitor="val_loss", mode="min", patience=10, 
            verbose=1, start_epoch=6
        )
        callbacks.append(early_callback)

    if logfile and (not tuner):
        logger = tf.keras.callbacks.CSVLogger(logfile, separator=',', append=False)
        callbacks.append(logger)

    return callbacks
