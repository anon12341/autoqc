"""
basenet.py: BaseNet Model Definition.

Base model definition upon which the ConvNet model is built.
"""

from typing import Tuple

from .lazytf import Conv2D, Dense, Dropout, MaxPooling2D
from .lazytf import Input, Model, ReLU, Add, GlobalAveragePooling2D
from .lazytf import layers
from .lazytf import RandomRotation, RandomZoom, RandomHeight, RandomWidth
from .layers import CropAndResize


def BaseNet(
    input_shape: Tuple[int, int, int] = (None, None, 1),
    dropout: bool = True,
    nlayers: int = 2,
    nclasses: int = 1,
    nfilters: int = 20,
    nunits: int = 64,
    nblocks: int = 3,
    augmentation: bool = True,
    crop: bool = True,
    imgsize: int = 256,
) -> Model:
    """
    Base model used by all models.

    :param input_shape: Shape of input images, default is (None, None, 1)
    :param dropout: If true, dropout layer is added, default is True
    :param nlayers: Number of convolutional layers, default is 2
    :param nclasses: Number of classes to predict, default is 1
    :param nfilters: Number of filters in the convolutional layers, default is 20
    :param nunits: Number of units in the dense layer, default is 64
    :param nblocks: Number of blocks in each convolutional layer, default is 3
    :param augmentation: If true, data augmentation is applied, default is True
    :param crop: If true, cropping is applied to the images, default is True
    :param imgsize: Desired image size after cropping, default is 256

    :return: Compiled TensorFlow model
    """

    print(
        f"BaseNet(nlayers={nlayers}, nfilters={nfilters}, dropout={dropout}, nblocks={nblocks}, nunits={nunits})"
    )

    inputs = Input(shape=input_shape)
    x = inputs

    if augmentation:
        x = RandomRotation(0.1)(x)
        x = RandomWidth(0.1)(x)
        x = RandomHeight(0.1)(x)
        x = RandomZoom((-0.1, 0.1), (-0.1, 0.1))(x)

    if crop:
        x = CropAndResize(imgsize, imgsize)(x)
    else:
        x = layers.Resizing(imgsize, imgsize)(x)

    num_filters = nfilters

    for i in range(nlayers):
        downsample = True
        num_filters = nfilters + 16 * i
        for j in range(nblocks):
            y = Conv2D(
                num_filters,
                strides=1,
                padding="same",
                kernel_size=(3, 3),
                activation="relu",
            )(x)
            if downsample:
                y = MaxPooling2D(pool_size=(2, 2), padding="same")(y)
                x = Conv2D(num_filters, padding="same", strides=2, kernel_size=(3, 3))(
                    x
                )
            else:
                y = Conv2D(num_filters, strides=1, padding="same", kernel_size=(3, 3))(
                    y
                )
            downsample = False
            x = Add()([y, x])
            x = ReLU()(x)

    # x = MaxPooling2D(pool_size=(2, 2))(x)
    # x = Flatten()(x)

    x = GlobalAveragePooling2D()(x)

    if dropout:
        x = Dropout(0.25)(x)

    if nclasses == 1:
        out = "sigmoid"
    else:
        out = "softmax"

    x = Dense(nunits, activation="relu")(x)
    outputs = Dense(nclasses, activation=out)(x)

    model = Model(inputs=inputs, outputs=outputs)
    return model
