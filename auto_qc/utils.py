"""
utils.py: Utility Functions for the AutoQC Package.
"""

import os
from pathlib import Path
import imageio.v2 as iio
import numpy as np
from pydicom.pixel_data_handlers.util import apply_modality_lut, apply_voi_lut
import pydicom
from pydicom import Dataset, FileDataset
import yaml
from skimage import exposure, transform
from scipy.ndimage import rotate
from argparse import ArgumentTypeError
from typing import Optional, Callable
import multiprocessing
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.axes import Axes
import json
from typing import Union, List, Tuple
from PIL import Image
from argparse import Namespace
import cv2
from tf_explain.core.grad_cam import GradCAM
import io

from .models.lazytf import Model, tf


def bytescale(
        arr: np.ndarray,
        low: Optional[float] = None,
        high: Optional[float] = None,
        a: float = 0,
        b: float = 255
) -> np.ndarray:
    """
    Linearly rescale values in an array. By default, it scales the values to the byte range (0-255).

    :param arr: The array to rescale.
    :param low: Lower boundary of the output interval. All values smaller than low are clipped to low.
    :param high: Upper boundary of the output interval. All values larger than high are clipped to high.
    :param a: Lower boundary of the input interval.
    :param b: Upper boundary of the input interval.
    :return: The rescaled array.
    """

    arr = arr.astype(float) # to ensure floating point division

    # Clip to specified high/low values, if any
    if low is not None:
        arr = np.maximum(arr, low)
    if high is not None:
        arr = np.minimum(arr, high)

    min_val, max_val = np.min(arr), np.max(arr)

    if np.isclose(min_val, max_val):  # avoid division by zero
        return np.full_like(arr, a, dtype=np.uint8)

    # Normalize between a and b
    return (((b - a) * (arr - min_val) / (max_val - min_val)) + a).astype(np.uint8)


def batcher(
        obj: object,
        nlines: int
) -> iter:
    """
    Batch generator for an object. Reads nlines lines at a time.

    :param obj: The object to read lines from.
    :param nlines: The number of lines to read at a time.
    :return: A batch of lines.
    """
    while True:
        res = [obj.readline().strip() for _ in range(nlines)]
        res = [r for r in res if r != ""]
        if len(res) == 0:
            break
        yield res


def read_config(config: str, validate=None):
    """
    Read configuration from a file.

    :param config: The path to the configuration file.
    :param validate: A function to validate the configuration. If None, no validation is performed.
    :return: The configuration as a dictionary.
    """
    try:
        conf = yaml.safe_load(config)
    except Exception as e:
        print("ERROR: Unable to read configuration file.")
        raise e
    if validate is not None:
        conf = validate(conf)
    return conf


def apply_luts(arr: np.ndarray, ds: pydicom.dataset.Dataset) -> np.ndarray:
    """
    Apply the modality and VOI LUTs to an array.

    :param arr: The array to apply the LUTs to.
    :param ds: The DICOM dataset containing the LUTs.
    :return: The array with the LUTs applied.
    """

    if "BitsAllocated" in ds:
        bits = ds.BitsAllocated
    elif "BitsStored" in ds:
        bits = ds.BitsStored
    else:
        bits = 16

    if bits == 8:
        arr = apply_modality_lut(arr, ds)
    elif bits == 32:
        arr = apply_voi_lut(arr, ds).astype(np.int32)
    else:
        arr = apply_voi_lut(arr, ds).astype(np.int16)

    return apply_voi_lut(arr, ds, index=0)


def autowindow(img):
    """
    Autowindowing function for chest X-rays using numpy.

    :param img: numpy array representing the image.
    :type np.ndarray
    :return windowed_img: numpy array of the windowed image.
    :rtype np.ndarray
    """
    # Calculate the histogram of the image
    hist_bins = np.max(img) - np.min(img) + 1
    hist, bin_edges = np.histogram(img, bins=hist_bins, range=(np.min(img), np.max(img) + 1))

    # Calculate the cumulative distribution function (CDF)
    cdf = hist.cumsum()
    cdf_normalized = cdf / cdf.max()

    # Set the lower and upper percentiles for windowing
    lower_percentile = 0.05
    upper_percentile = 0.95

    # Find the intensity values corresponding to these percentiles
    lower_value = np.argmax(cdf_normalized > lower_percentile)
    upper_value = np.argmax(cdf_normalized > upper_percentile)

    # Clip the image based on the computed window levels
    windowed_img = np.clip(img, lower_value, upper_value)

    return windowed_img


def preprocess_image(
        im: np.ndarray,
        equalize: bool = True,
        resize: bool = False,
        rescale: bool = True,
) -> np.ndarray:
    """
    Preprocess an image, including equalization, resizing, and rescaling.

    :param im: The image to preprocess.
    :param equalize: Whether to equalize the image histogram.
    :param resize: Whether to resize the image.
    :param rescale: Whether to rescale the image.
    :param rotation: The angle to rotate the image. If None, no rotation is performed.
    :return: The preprocessed image.
    """
    if len(im.shape) > 2:  # get only first channel
        im = im[:, :, 0]
    arr = im.astype("float")
    if equalize:
        arr = exposure.equalize_hist(arr)
    if resize:
        target_shape = 256
        arr = transform.resize(arr, (target_shape, target_shape))
    if rescale:
        target_shape = 256
        scl = min([target_shape/s for s in arr.shape])
        arr=transform.rescale(arr, scl)
        dy, dx = [target_shape-s for s in arr.shape]
        arr = np.pad(arr, ((dy//2,dy-dy//2),(dx//2,dx-dx//2)))
    return arr


def high_intensity_enhancement(image: np.ndarray) -> np.ndarray:
    """
    Enhance high intensity pixels in an image.

    :param image: The image to enhance.
    :return: The enhanced image.
    """
    import cv2

    image = preprocess_image(image)

    if len(image.shape) > 2:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Clahe normalization
    image = cv2.normalize(image, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    clahe = cv2.createCLAHE(clipLimit=4.0, tileGridSize=(8, 8))  # Modify these parameters as needed
    image = clahe.apply(image)
    # Apply sigmoid function
    image = cv2.normalize(image, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_64F)
    sigmoid_image = 1.0 / (1.0 + np.exp(-5 * (image - 0.8)))

    return sigmoid_image

    return image


def imread(imgname: str, high_intensity_preprocessing: bool = False) -> Optional[np.ndarray]:
    """
    Read an image file.

    :param imgname: The name of the image file.
    :return: The image data.
    """
    path = Path(imgname)
    try:
        if path.suffix in [".dcm"]:
            ds = pydicom.read_file(f"{path}")
            arr = ds.pixel_array
        elif path.suffix in [".npy"]:
            arr = np.load(path)
        else:
            arr = iio.imread(path)
        if high_intensity_preprocessing:
            return high_intensity_enhancement(preprocess_image(arr))
        else:
            return preprocess_image(arr)
    except Exception as e:
        print(f"ERROR: Failed to read image at {path}. Reason: {str(e)}")
        return None


def load_model(filename: str) -> Model:
    """
    Load a model from a file.

    :param filename: The name of the file to load the model from.
    :return: The loaded model.
    """
    import tensorflow as tf

    saved_models = Path(os.getenv("AUTOQC_MODELS", "./"))
    tf.keras.backend.clear_session()
    model = tf.keras.models.load_model(f"{saved_models / filename}")
    return model


def error_catcher(func: Callable) -> Callable:
    """
    A decorator that catches exceptions in the decorated function and returns them in a dictionary.

    :param func: The function to decorate.
    :return: The decorated function.
    """
    def wrapper(*args, **kwargs):
        try:
            res = func(*args, **kwargs)
        except Exception as e:
            res = {"error": e, "stop": True}
        return res
    return wrapper


def valid_file(file_path: str) -> str:
    """
    Verifies if a provided path corresponds to an existing file.

    :param file_path: The path to check
    :return: The validated file path
    :raises ArgumentTypeError: If the file doesn't exist
    """
    if not Path(file_path).exists():
        raise ArgumentTypeError(f"File '{file_path}' does not exist.")
    return file_path


def valid_directory(directory: str) -> str:
    """
    Verifies if a provided path corresponds to an existing directory.

    :param directory: The path to check
    :return: The validated directory path
    :raises ArgumentTypeError: If the directory doesn't exist
    """
    if not Path(directory).is_dir():
        raise ArgumentTypeError(f"Directory '{directory}' does not exist.")
    return directory


def ncpus() -> int:
    """
    Get the number of available CPUs.

    :return: The number of available CPUs.
    """
    num_cpus = multiprocessing.cpu_count()
    print("Number of available CPUs:", num_cpus)
    return num_cpus


def check_gpu():
    """
    Prints statements to check if a GPU is available for Tensorflow.
    """
    import tensorflow as tf
    physical_devices = tf.config.list_physical_devices('GPU')
    if len(physical_devices) > 0:
        print(f"Physical GPU devices: {len(physical_devices)}")
    else:
        print("No GPU devices found.")
    print("TensorFlow is using GPU / CUDA working:", tf.test.is_built_with_cuda())
    print()


def save_and_close_fig(fig: Figure, filename: Union[str, Path]):
    """
    Save a figure and close it.

    :param fig: The figure to save.
    :param filename: The name of the file to save the figure to.
    """
    fig.savefig(filename)
    plt.close(fig)


def invert_grayscale(img: np.ndarray) -> np.ndarray:
    """
    Invert a grayscale image.

    :param img: The image to invert.
    :return: The inverted image.
    """
    return np.max(img) + np.min(img) - img


def crop_center(img: np.ndarray, percent: int = 50) -> np.ndarray:
    """
    Crop the center of an image.

    :param img: the image to crop
    :param percent: percent of the image to crop
    :return: the cropped image
    """
    y, x = img.shape
    cropx = int(x * percent / 100)
    cropy = int(y * percent / 100)
    startx = x // 2 - (cropx // 2)
    starty = y // 2 - (cropy // 2)
    return img[starty:starty + cropy, startx:startx + cropx]


def normalize_image(image: np.ndarray) -> np.ndarray:
    """
    Normalize the image to the range [0, 255].

    :param image: Image array to normalize.
    :return: Normalized image array.
    """
    min_val = np.min(image)
    max_val = np.max(image)
    normalized_image = (image - min_val) / (max_val - min_val) * 255
    return normalized_image.astype(np.uint8)


class MyJsonEncoder(json.JSONEncoder):
    """
    A custom JSON encoder that converts numpy types to native Python types.
    """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        else:
            return super().default(obj)


def combine_figures(figs: Union[str, List[str]], out_path: str = "./combined.png", n_cols: int = 3):
    """
    Combine multiple figures into a single figure.

    :param figs: A list of paths to the figures to combine or a path to a directory containing the figures.
    :param out_path: The path to save the combined figure to, defaults to "./combined.png"
    :param n_cols: The number of columns to use for the combined figure, defaults to 3.
    """
    if isinstance(figs, list) and len(figs) > 1:
        pass
    elif isinstance(figs, list) and len(figs) == 1 and os.path.isdir(figs[0]):
        figs = [os.path.join(figs[0], file) for file in os.listdir(figs[0]) if file.endswith(
            (".png", ".jpg", ".jpeg", ".gif", ".bmp", ".tiff", ".tif", ".pdf")
        )]
    elif isinstance(figs, str) and os.path.isdir(figs):
        print(os.listdir(figs))
        figs = [os.path.join(figs, file) for file in os.listdir(figs) if file.endswith(
            (".png", ".jpg", ".jpeg", ".gif", ".bmp", ".tiff", ".tif", ".pdf")
        )]
    else:
        raise ValueError(f"Invalid value for figs: {figs}. Must be a list of paths or a path to a directory.")

    imgs = [Image.open(x) for x in figs]
    n_imgs = len(imgs)

    # Find the max height and width
    max_width = max([img.size[0] for img in imgs])
    max_height = max([img.size[1] for img in imgs])

    # Number of rows for the final combined image
    n_rows = np.ceil(n_imgs / float(n_cols)).astype(int)

    # Create a new image of size (n_rows * max_height, n_cols * max_width)
    combined_img = Image.new('RGB', (n_cols * max_width, n_rows * max_height))

    # Iterate over the images and paste them into the combined image
    for i, img in enumerate(imgs):
        row = i // n_cols
        col = i % n_cols
        combined_img.paste(img, (col * max_width, row * max_height))

    combined_img.save(out_path)


def combine_figures_cl(args: Namespace):
    """
    CLI wrapper for combine_figures.

    :param args: The arguments.
    """
    combine_figures(args.figures, args.outpath, args.ncols)


def correct_rotation(arr: np.ndarray, angle: Union[int, float]) -> np.ndarray:
    """
    Correct the rotation of an array by rotating it by the negative of the given angle. Performs the rotation by
    interpolating the array and padding as necessary with the minimum value of the array.

    :param arr: The array to correct.
    :param angle: The angle to correct the array by.
    :return: The corrected array.
    """
    return rotate(arr, -angle, reshape=True, mode='constant', cval=arr.min())


def correct_90_rotation(image: np.ndarray, angle: Union[int, float]) -> Tuple[np.ndarray, int]:
    """
    Correct the 90-degree rotation of an image, avoiding the need for interpolation when rotating by a non-right angle.

    :param image: The image to correct.
    :param angle: The angle to correct the image by.
    :return: The corrected image and the amount of rotation applied.
    """
    angle %= 360        # Ensure angle is in range [0, 360)
    img = image.copy()
    # Determine the amount of 90-degree rotation to apply and apply it
    if 45 <= angle < 135:
        degrees = 90
        img = np.rot90(img, k=-1)
    elif 135 <= angle < 225:
        degrees = 180
        img = np.rot90(img, k=-2)
    elif 225 <= angle < 315:
        degrees = 270
        img = np.rot90(img, k=-3)
    else:
        degrees = 0
    return img, degrees


def grad_cam(
        input_model: tf.keras.Model,
        img_array: np.ndarray,
        layers_idx: Union[int, float, Tuple[Union[int, float], ...], List[Union[int, float]]],
        weights: Optional[Union[Tuple[Union[int, float], ...], List[Union[int, float]]]] = None
) -> np.ndarray:
    """
    GradCAM method for visualizing input saliency.

    :param input_model: The model to use for GradCAM.
    :param img_array: The image to use for GradCAM.
    :param layers_idx: The indices of the convolutional layers to use for GradCAM, defaults to -1.
    :param weights: The weights to use for each layer, defaults to None.
    :return: The heatmap.
    """
    # Accept either a single layer index or a list of layer indices
    layers_idx = [layers_idx] if isinstance(layers_idx, int) else layers_idx

    explainer = GradCAM()       # GradCAM explainer instance

    # Add weights to layers as needed
    if weights is None:
        weights = [1 / len(layers_idx)] * len(layers_idx)
    weights = [w / sum(weights) for w in weights]

    if len(img_array.shape) == 2:
        img_array = img_array[None, ..., None]

    # GradCAM for each layer
    combined_heatmap = None
    for _idx, weight in zip(layers_idx, weights):
        minimum = 0
        maximum = 0
        idx = _idx
        while minimum == maximum:
            try:
                for use_guided_grads in [True, False]:
                    # Get the name of the layer
                    conv_layer_name = [layer for layer in input_model.layers if isinstance(layer, tf.keras.layers.Conv2D)][idx].name
                    # Generate the heatmap
                    heatmap = explainer.explain((img_array, 1), input_model, class_index=0, layer_name=conv_layer_name,
                                                image_weight=0, use_guided_grads=use_guided_grads)
                    # Convert the heatmap to grayscale to allow for combining multiple heatmaps
                    heatmap_gray = cv2.cvtColor(heatmap, cv2.COLOR_BGR2GRAY)
                    minimum = heatmap_gray.min()
                    maximum = heatmap_gray.max()
                    if minimum != maximum:
                        break
                idx = idx-1 if idx > -2 else -100
            except IndexError:
                heatmap_gray = np.zeros((256, 256), dtype=int)
                break
        # Normalize the heatmap
        heatmap_gray = heatmap_gray / heatmap_gray.max()
        # Combine the heatmaps
        if combined_heatmap is None:
            combined_heatmap = weight * heatmap_gray
        else:
            combined_heatmap += weight * heatmap_gray

    return combined_heatmap


def plot_heatmap(
        img: np.ndarray,
        heatmap: np.ndarray,
        colormap: Optional[str] = 'jet',
        cbar: bool = True,
        alpha: Optional[Union[int, float]] = 0.4,
        figsize: Optional[Tuple[int, int]] = (6, 6),
        ax: Axes = None,
) -> Union[Axes, Tuple[Figure, Axes]]:
    """
    Plot a heatmap overlaid on the image.

    :param img: The CXR.
    :param heatmap: The heatmap.
    :param colormap: The colormap to use for the heatmap, defaults to 'jet'.
    :param cbar: Whether to add a colorbar to the plot, defaults to True.
    :param alpha: The alpha value to use for the heatmap, defaults to 0.4.
    :param figsize: The figure size, defaults to (6, 6).
    :param ax: The matplotlib axes to plot on, defaults to None.
    :return: The matplotlib figure and axes if no axes are provided, otherwise just the axes.
    """
    f, ax = (None, ax) if ax else plt.subplots(figsize=figsize)     # Create a figure and axes if none are provided
    _ = ax.imshow(img, cmap="bone", aspect='equal')                                 # Plot the CXR
    hm = ax.imshow(heatmap, cmap=colormap, alpha=alpha, aspect='equal')             # Plot the heatmap

    if cbar:
        cbar = plt.colorbar(hm, ax=ax, alpha=1)
        # Create a black rectangle behind the colorbar to darken the
        # colours to better match the heatmap overlaid on the CXR
        x_bounds = cbar.ax.get_xbound()
        y_bounds = cbar.ax.get_ybound()
        buffer = (x_bounds[1] - x_bounds[0]) / 5
        black_rect = plt.Rectangle((x_bounds[0], y_bounds[0]), x_bounds[1] - x_bounds[0], y_bounds[1] - y_bounds[0],
                                   facecolor='black', alpha=0.5)
        cbar.ax.add_patch(black_rect).set_zorder(0)
        # Add text to the colorbar
        cbar.set_ticks([])
        cbar.set_label('Feature Importance', rotation=270, labelpad=25)
        _ = cbar.ax.text(x_bounds[1] + buffer, y_bounds[1], 'Higher', va='top', ha='left')
        _ = cbar.ax.text(x_bounds[1] + buffer, y_bounds[0], 'Lower', va='bottom', ha='left')

    ax.axis("off")

    if f:
        return f, ax
    else:
        return ax


def fig_to_pil(fig: plt.Figure) -> Image.Image:
    """
    Convert a matplotlib figure to a PIL image.

    :param fig: The matplotlib figure.
    :return: The PIL image.
    """
    buf = io.BytesIO()
    fig.tight_layout(pad=0)
    fig.savefig(buf, format='png', bbox_inches=0, pad_inches=0)
    buf.seek(0)
    img = Image.open(buf)
    return img


def create_icns(
        png_path: str,
        icns_path: str,
        sizes: Optional[Union[Tuple[int], List[int]]] = (16, 32, 64, 128, 256, 512, 1024)
):
    """
    Create an .icns file from a .png file.

    :param png_path: The path to the .png file.
    :param icns_path: The path to the .icns file to create.
    :param sizes: The sizes to create the .icns file with, defaults to (16, 32, 64, 128, 256, 512, 1024).
    :return: None
    """
    img = Image.open(png_path)
    icon_sizes = []

    for size in sizes:
        # Resize while maintaining aspect ratio (thumbnail method maintains aspect ratio)
        copy = img.copy()
        copy.thumbnail((size, size))

        # Create new image and paste the resized image into it, centering it
        new_image = Image.new("RGBA", (size, size), (0, 0, 0, 0))
        new_image.paste(copy, ((size - copy.width) // 2, (size - copy.height) // 2))

        icon_sizes.append(new_image)

    if icns_path.endswith('.icns'):
        icns_path = icns_path[:-5]

    # Save the images as .icns
    icon_sizes[0].save(f'{icns_path}.icns', format='ICNS', append_images=icon_sizes[1:])
