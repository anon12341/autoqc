"""
preprocess.py - Preprocess a dataset of images using a Dask distributed computation.

This module, preprocess.py, provides functionalities to preprocess a dataset of images. It uses Dask for distributed
computation and imageio for reading and writing images.

Main Components:

- preprocess_run: Preprocesses a single image with optional histogram equalization and rotation, and writes the output to
a specified directory.
- preprocess: Reads a preprocessing configuration from a file and uses Dask to distribute the computation of preprocessing
over a set of images based on this configuration. The resulting images are written to a specified output directory.

This module is particularly useful for large scale image preprocessing tasks, as it enables the use of distributed
computation to significantly speed up the process. The output images can then be used for further analysis or machine
learning tasks.
"""

from argparse import Namespace
from contextlib import closing
from pathlib import Path
import dask
import imageio
from distributed import Client, as_completed
from tqdm import tqdm

from ..conf import schema_preprocess
from ..utils import read_config, preprocess_image, imread, high_intensity_enhancement

@dask.delayed
def preprocess_run(imname: Path, output_dir: Path,
                   enhance_high_intensity: bool = False, equalize: bool=True, rotation: int=None) -> None:
    """
    Preprocess an image with optional histogram equalization and rotation.

    :param imname: The name of the image file to process
    :param output_dir: The directory to write the output file to
    :param enhance_high_intensity: If True, perform high intensity enhancement on the image, defaults to False
    :param equalize: If True, perform histogram equalization on the image, defaults to True
    :param rotation: If specified, rotate the image by this amount, defaults to None
    """
    try:
        im = imread(imname)
    except FileNotFoundError:
        return
    arr = preprocess_image(im)
    if enhance_high_intensity:
        arr = high_intensity_enhancement(arr)
    out = f"{output_dir}/{imname.stem}.tif"
    imageio.imwrite(out, arr)


def preprocess(args: Namespace) -> None:
    """
    Preprocess a dataset of images using a Dask distributed computation. The configuration
    for the preprocessing is read from a file specified in the command line arguments.

    :param args: Command line arguments, should contain 'config'
    """
    config = read_config(args.config, schema_preprocess)
    output_dir = config.output_dir / config.dataset
    high_intensity = config.enhance_high_intensity
    print(f"Dataset {config.dataset!r}")
    for label in config.labels:
        name = label["name"]
        filename = label["file"]
        path = label["path"]
        print(f"Preprocessing {name!r} - {path!r} images")
        out = output_dir / name / path
        out.mkdir(exist_ok=True, parents=True)
        with open(filename, "r") as fh:
            tasks = [preprocess_run(Path(name.strip()), out, high_intensity) for name in fh]
        with closing(Client()) as client:
            futures = client.compute(tasks)
            with tqdm(total=len(futures)) as pbar:
                for _fut in as_completed(futures):
                    pbar.update(1)

        # Special case for making rotated images
        #if name in ["rot0"]:
        #    with open(filename, "r") as fh:
        #        filenames = [Path(name.strip()) for name in fh]
        #    for rot in [90, 180, 270]:
        #        print(f"Preprocessing rot{rot} images")
        #        out = output_dir / f"rot{rot}"
        #        out.mkdir(exist_ok=True, parents=True)
        #        tasks = [
        #            preprocess_run(fname, out, rotation=rot) for fname in filenames
        #        ]
        #        with closing(Client()) as client:
        #            futures = client.compute(tasks)
        #            with tqdm(total=len(futures)) as pbar:
        #                for _fut in as_completed(futures):
        #                    pbar.update(1)
