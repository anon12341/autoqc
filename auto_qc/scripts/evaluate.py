"""
evaluate.py

This module evaluates the AutoQC models on the validation, internal test, and external test sets.

Classes:
- QCModel: A class to manage the model for testing for binary classification tasks in the AutoQC package. The class
    provides methods for loading models, making predictions, calibrating the models, and finding the best threshold that
    maximizes the F1 score.
- QCTrainingEvaluator: A class to evaluate the training process of a model, including plotting training history curves
    and showing metrics for the training, validation and internal test sets.
- QCInferenceEvaluator: A class for evaluating the quality control model's predictions on a particular dataset. It will
    calculate the metrics for the dataset with confidence intervals, plot confusion matrices, plot the ROC and PRC
    curves, and display examples of images for which the model made a mistake.
- QCAspectRatioEvaluator: A class for evaluating the aspect ratio model's predictions. It will
    calculate the metrics for the dataset with confidence intervals, plot histograms and plot confusion matrices.

Functions:
- evaluate: Function to evaluate all AutoQC tools independently and save all outputs.
- evaluate_dl_model: Function to evaluate an AutoQC deep learning model and save all outputs.
- evaluate_aspect_ratio: Function to evaluate the aspect ratio tool and save all outputs.
- make_confusion_matrix: Function to plot the confusion matrix from a 2x2 numpy array.
- calculate_conf_matrix_metrics: Calculate the sensitivity, specificity, positive predictive value, and negative
    predictive value from the confusion matrix.
- compute_metrics: Compute the metrics for a set of predictions and labels.
- exclude_unsuitable_cases: Exclude cases that are unsuitable for use at the relevant stage of the pipeline.
- format_list_to_string: Format a list of numbers to a string with a specified number of decimal places.
- make_results_df: Make a dataframe of the results for a dataset.
"""

# IMPORTS --------------------------------------------------------------------------------------------------------------

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.axes import Axes
import matplotlib as mpl
from mlxtend.plotting import plot_confusion_matrix
import os
from tqdm.auto import tqdm
import concurrent.futures
from sklearn.utils import resample
from sklearn.metrics import roc_auc_score, precision_score, recall_score, f1_score, average_precision_score, \
    precision_recall_curve, brier_score_loss, confusion_matrix, roc_curve, auc
from scipy.ndimage import rotate
from scipy.stats import chi2_contingency
from sklearn.calibration import calibration_curve, CalibratedClassifierCV
import json
import joblib
from argparse import Namespace
import yaml
from typing import Dict, Optional, List, Tuple, Generator, Union, Sequence, Any
from importlib.resources import open_text
from PIL import Image
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from ..models.metrics import f1, angle_error
from ..utils import imread, read_config, check_gpu, save_and_close_fig, preprocess_image, high_intensity_enhancement, \
    grad_cam, plot_heatmap
from ..classification import aspect_ratio
from ..conf import schema_evaluate
from ..models.lazytf import keras, tf
from ..models.layers import CropAndResize
from ..operations.boundary import remove_border
from ..operations.ocr import create_dictionaries, clean_data, find_terms, populate_category_df

mpl.rcParams['figure.max_open_warning'] = 200

# CONSTANTS, DICTS, LISTS ----------------------------------------------------------------------------------------------

# Dictionary of the column ds_names for each label
LABEL_TO_COLNAME_MAP = {
    'inverted': 'inv',
    'lateral': 'lat',
    'projection': 'proj',
    'ocr': 'proj',
    'rotation': 'rot',
    'pacemaker': 'ppm',
    'aspect_ratio': 'fraction',
}

# Dictionary of the stage at which each label is utilised in the AutoQC pipeline
STAGE_DICT = {
    'inverted': 'stage1',
    'lateral': 'stage2',
    'aspect_ratio': 'stage2',
    'rotation': 'stage3',
    'projection': 'stage4',
    'pacemaker': 'stage4',
    'ocr': 'stage4',
}

# Creates dictionary of all labels for images which are unsuitable to include at each stage
unsuitable_stage1 = ['paeds', 'de', 'nchest']
unsuitable_stage2 = []
unsuitable_stage3 = ['lat', 'aspect_ratio']
unsuitable_stage4 = ['rot']
UNSUITABLE_FEATURES_DICT = {
    'stage1': unsuitable_stage1,
    'stage2': unsuitable_stage1 + unsuitable_stage2,
    'stage3': unsuitable_stage1 + unsuitable_stage2 + unsuitable_stage3,
    'stage4': unsuitable_stage1 + unsuitable_stage2 + unsuitable_stage3 + unsuitable_stage4,
}

# List of all external holdout sets
INDIVIDUAL_EXTERNAL_HOLDOUT = ['cuh', 'brixia', 'ricord']

# List of datasets to test model on if "all" is entered
ALL_HOLDOUT = ['internal', 'all_external'] + INDIVIDUAL_EXTERNAL_HOLDOUT

# Converts labels for presentation in tables and figures
PRESENTATION_LABEL_MAP = {
    'internal': 'Internal Test',
    'external': 'All External',
    'all_external': 'All External',
    'dev': 'Development',
    'cuh': 'CUH',
    'brixia': 'BrixIA',
    'ricord': 'RICORD-1c',
    'nccid': 'NCCID',
    'bimcv': 'BIMCV',
    'val': 'Validation',
    'inv': 'Inverted',
    'inverted': 'Inverted',
    'lat': 'Lateral',
    'lateral': 'Lateral',
    'aspect_ratio': 'Aspect Ratio',
    'ppm': 'Pacemaker',
    'pacemaker': 'Pacemaker',
    'rot': 'Rotation',
    'rotation': 'Rotation',
    'proj': 'Projection',
    'projection': 'Projection',
    'ocr': 'OCR Projection',
    'accuracy': 'Accuracy',
    'roc_auc': 'ROC AUC',
    'precision': 'Precision',
    'recall': 'Recall',
    'sensitivity': 'Sensitivity',
    'specificity': 'Specificity',
    'ppv': 'PPV',
    'npv': 'NPV',
    'f1': 'F1 Score',
    'auprc': 'AUPRC',
    'quality': 'Quality',
    'quality_exc_artifacts': 'Quality (excluding artifacts)',
}

LABEL_MAPS = {
    'projection': {"pa": 0, "0": 0, "0.0": 0, 0: 0, "ap": 1, "1": 1, "1.0": 1, 1: 1},
    'ocr': {"pa": 0, "0": 0, "0.0": 0, 0: 0, "ap": 1, "1": 1, "1.0": 1, 1: 1, "PA": 0, "AP": 1},
    'rotation': {"0": 0, "0.0": 0, "1": 1, "1.0": 1, 0: 0, 1: 1},
    'rot': {"0": 0, "0.0": 0, "1": 1, "1.0": 1, 0: 0, 1: 1},
}

LAT_TERMS = ['Lateral R', 'Lateral L', 'Chest lat', 'Torax LAT', 'Tórax LAT', 'Esternon LAT',
             'W034 Tórax Lat.', 'Lateral', 'Lateral izq.', 'Torax Lat', 'TORAX Lat', 'TORAX LAT',
             'W034IZQ Tórax lat', 'bucky LAT', 'Torax  LAT', 'W034 Tórax LAT', 'Supine LAT', 'TORAX Erect LAT',
             'LATERAL', 'W034 Tórax Lat. *', 'tórax LAT', 'W034IZQ Tórax lat *', 'torax Lat', 'Erect LAT',
             'Lateral der.', 'TORAX LATERAL', 'LAT', 'W Thorax lat *', 'potter LAT']
PA_TERMS = ['PA', 'Chest PA', 'W Chest PA', 'W Chest PA  GRID', 'PA Chest', 'PA CHEST', 'Chest PA Grid',
            'W Chest PA GRID', 'CHEST PA', 'Chest pa', 'PA Landscape', '1 view PA PA', 'PA GRID', 'PA NO GRID',
            'PA chest', 'PA (Wallstand)', 'X Chest PA', 'PA Paed Fuji', 'W Chest PA (GRID)', 'W Chest PA.',
            'Chest (male) pa', 'Chest (female) pa', 'W CXR Paed PA10+yrs', 'PA Non Grid', 'HIGH KV PA', 'bucky PA',
            'Torax PA', 'Erect PA', 'W033 Tórax PA', 'TORAX  PA', 'Tórax PA', 'TORAX PA', 'TORAX Erect PA',
            'tórax PA H', 'W033 Tórax pa', 'tórax PA', 'Torax Erect PA', 'TORAX Bipe PA', 'W033 Tórax pa *',
            'T035 Tórax PA', 'torax PA', 'parrilla costal AP OBL', 'W033 Tórax PA *', 'Torax  PA',
            'Parrilla Costal PA', 'tórax bucky PA', 'PA horizontal', 'Torax Pediatrico PA', 'W Thorax pa *',
            'W Thorax pa', '4 W Thorax pa', '4 PA CXR no grid', 'W PA CXR no grid', '4 W Thorax pa *',
            'W Chest PA Bariatric', 'T033 Torace pa', 'potter PA', 'Torace PA',
            ]
AP_TERMS = [
    'Chest AP', 'AP', 'X Chest AP', 'AP Chest', 'CHEST AP', 'AP CHEST', 'AP chest', 'Virtual Grid AP',
    'X Chest AP NG Tube', 'AP Landscape', 'AP Portrait', 'PA Landscape', 'AP NG', 'X035 Chest Free Exp AP',
    '1 YR Chest AP', 'X Chest AP Hi kV with grid', 'X Chest AP Erect', 'X Chest AP High kV', 'X Chest AP (NG TUBE)',
    'AP Wireless', 'CR (AP)', 'Chest AP (M2 C PKG)', 'Trolley Abdomen AP', 'X035 Chest AP', 'pediátrico AP',
    'TORAX Supino AP', 'Torax AP', 'Sup.AP Portable', 'tórax AP', 'Torax  AP', 'torax AP', 'portátil AP',
    'AP horizontal', 'tórax AP PORTATIL', 'Torax AP directo', 'Torax Supine AP', 'Tòrax AP', 'dec. supino AP',
    'Supine AP', 'W033 Tórax AP', 'Directo AP', 't?rax AP', 'Pecho AP', 'Torax ninos directo AP', 'Torax niño AP',
    'Torax Pediatrico AP', 'T035 Tórax AP', 'Erect AP', 'TORAX AP MESA', 'AP DIRECTO', 'Tórax AP',
    'Parrilla Costal AP', 'TORAX AP', 'sedestacion AP', 'Abdomen AP', 'parrilla costal AP OBL', 'AP vertical',
    'tórax AP H', 'TÃ²rax AP', 'Torax Supino AP', 'simple AP', 'TORAX Supine AP', 'bucky ap', 'Torax SEDE AP',
    'PEDS CXR AP', 'W T Spine AP *', 'X Pelvis AP', 'portatile AP', 'T033 Torace ap', 'Torace AP',
    'portatile AP (0)',
]


# CLASSES --------------------------------------------------------------------------------------------------------------

class QCModel:
    """
    A class to manage the model for testing for binary classification tasks in the AutoQC package. The class provides
    methods for loading models, making predictions, calibrating the models, and finding the best threshold that
    maximizes the F1 score.

    :param config: A Namespace object containing the configuration parameters for the model evaluation.
    """

    def __init__(self, config: Namespace):
        """
        Initializes a QCModel instance with the given label and model name.

        :param config: A Namespace object containing the configuration parameters for the model evaluation.
        """
        self.config: Namespace = config
        self.label: str = config.task
        self.true_col: str = LABEL_TO_COLNAME_MAP[config.task]
        self.relevant_cols: List[str] = self._get_relevant_cols(config.task)
        self.checkpoint: str = config.dirs.checkpoint
        self.model: Optional[keras.Model] = None
        self.pred_df: Dict[str, Optional[pd.DataFrame]] = {'val': None, 'internal': None, 'external': None}
        self.df: Dict[str, Optional[pd.DataFrame]] = {'val': None, 'internal': None, 'external': None}
        self.pred_col: str = f'{self.true_col}_pred'
        self.pred_prob_col: str = f'{self.true_col}_pred_prob'
        self.cal_pred_col: str = f'{self.true_col}_cal_pred'
        self.cal_pred_prob_col: str = f'{self.true_col}_cal_pred_prob'
        self.best_threshold: Optional[float] = None
        self.calibrated_clf: Optional[CalibratedClassifierCV] = None
        self.label_df: Dict[str, pd.DataFrame] = {
            'val': self._set_dev_labels('val'),
            'internal': self._set_dev_labels('test'),
            'external': pd.read_csv(config.dirs.test_labels, dtype=str),
        }

    def _set_dev_labels(self, testset) -> pd.DataFrame:
        """
        Sets development labels for the specified test set and label.

        :param testset: The test set to set development labels for ('val', 'test', or 'external').
        :return: A DataFrame with the development labels.
        """
        df = pd.read_csv(
            os.path.join(self.config.dirs.dev_labels, f"{STAGE_DICT[self.label]}_paths+labels.csv"), dtype=str
        )
        return df[df['partition'] == testset]

    def load_model(self, print_summary=True):
        """
        Loads the model from the given file.

        :return: A summary of the model.
        """
        if self.label != 'rotation':
            self.model = keras.models.load_model(
                self.checkpoint, custom_objects={"f1": f1, "CropAndResize": CropAndResize}
            )
        else:
            self.model = keras.models.load_model(
                self.checkpoint, custom_objects={"angle_error": angle_error, "CropAndResize": CropAndResize}
            )
        if print_summary:
            return self.model.summary()
        else:
            return None

    def _create_batches(self, images: Dict) -> Generator[Dict[str, np.ndarray], None, None]:
        """
        Creates a generator that yields batches of images from the given image list.

        :param imglist: A list of image paths.
        :return: A generator that yields batches of image paths.
        """
        import itertools
        for i in range(0, len(images.keys()), self.config.nbatch):
            # yield {images.keys()[i: i + self.config.nbatch]: images.values()[i: i + self.config.nbatch]}
            yield {k: v for k, v in itertools.islice(images.items(), i, i + self.config.nbatch)}

    def _make_prediction(self, image_batch: Dict[str, np.ndarray]) -> Dict[str, Any]:
        """
        Makes predictions on the given image batch.

        :param image_batch: A list of image paths to make predictions on.
        :return: A dictionary containing image paths as keys and predictions as values.
        """
        pred = self.model.predict(np.array(list(image_batch.values()))[..., np.newaxis])

        if self.label != 'rotation':
            return {img: p[0] for img, p in zip(image_batch.keys(), pred)}
        else:
            return self._make_rotation_prediction(image_batch, pred)

    def _make_rotation_prediction(self, image_batch: Dict[str, np.ndarray], pred: np.ndarray) -> Dict[str, Any]:
        """
        Makes predictions on the given image batch for the rotation model.  Provides binary predictions for each
        rotation class and then rotates the images to the predicted angle and makes a second prediction. The final
        predictions are provided in a binary format for rotation by an angle which is a multiple of 90 degrees, and
        in a categorical format for the degree of 90 degree rotation (i.e. 0, 90, 180 or 270).

        :param image_batch: A list of image paths to make predictions on.
        :param pred: The predictions for the first rotation prediction.
        :return: The final predictions for the rotation model.
        """
        p0 = pred.copy()
        image_batch_corrected = {}
        for i, (name, image) in enumerate(image_batch.items()):
            image_batch_corrected[name] = rotate(image, np.argmax(p0[i]), reshape=False)
        p1 = self.model.predict(np.array(list(image_batch_corrected.values()))[..., np.newaxis])
        pred_dict = {}
        for img, prbs, orig_prob in zip(image_batch.keys(), p1, p0):
            # for img, prbs, angle in zip(image_batch.keys(), p1, angles):
            pred_angle = int(np.argmax(orig_prob)) + int(np.argmax(prbs))
            # pred_angle = angle + int(np.argmax(prbs))
            if pred_angle > 360:
                pred_angle -= 360
            if pred_angle < 45 or pred_angle >= 315:
                rot_pred = 0
                # rot_pred_prob = 1 - (np.sum(prbs[0:45]) + np.sum(prbs[315:360]))
                rot90_pred = 0
                # rot90_prob = rot_pred_prob
            elif pred_angle >= 90 - 45 and pred_angle < 90 + 45:
                rot_pred = 1
                # rot_pred_prob = 1 - (np.sum(prbs[0:45]) + np.sum(prbs[315:360]))
                rot90_pred = 90
                # rot90_prob = np.sum(prbs[90 - 45:90 + 45])
            elif pred_angle >= 180 - 45 and pred_angle < 180 + 45:
                rot_pred = 1
                # rot_pred_prob = 1 - (np.sum(prbs[0:45]) + np.sum(prbs[315:360]))
                rot90_pred = 180
                # rot90_prob = np.sum(prbs[180 - 45:180 + 45])
            elif pred_angle >= 270 - 45 and pred_angle < 270 + 45:
                rot_pred = 1
                # rot_pred_prob = 1 - (np.sum(prbs[0:45]) + np.sum(prbs[315:360]))
                rot90_pred = 270
                # rot90_prob = np.sum(prbs[270 - 45:275 + 45])
            else:
                print("Unclassified angle:", pred_angle)
                rot_pred = None
                # rot_pred_prob = None
                rot90_pred = None
                # rot90_prob = None
            pred_dict[img] = [np.argmax(orig_prob), np.argmax(prbs), pred_angle, rot_pred,
                              # rot_pred_prob,
                              rot90_pred,
                              # rot90_prob
                              ]
        return pred_dict

    def _read_image(self, image_path: str) -> Dict[str, np.ndarray]:
        try:
            if self.label == 'rotation':
                return {image_path: preprocess_image(remove_border(imread(image_path)))}
            return {image_path: preprocess_image(imread(image_path))}
        except:
            return {}

    def _read_images(self, imagelist: List) -> Dict[str, np.ndarray]:
        read_results = {}
        if self.config.multi_cpu:
            try:
                with concurrent.futures.ThreadPoolExecutor(max_workers=self.config.nworkers) as executor:
                    futures = [executor.submit(self._read_image, item) for item in imagelist]
                    for future in tqdm(concurrent.futures.as_completed(futures), total=len(imagelist)):
                        read_results.update(future.result())
            except Exception as e:
                print(e)
            finally:
                if executor is not None:
                    executor.shutdown(wait=True)
        else:
            for item in tqdm(imagelist):
                read_results.update(self._read_image(item))
        return read_results

    @staticmethod
    def _add_high_intensity_preprocessing(images) -> Dict[str, np.ndarray]:
        for name, image in tqdm(images.items()):
            images[name] = high_intensity_enhancement(image)
        return images

    def run_inference(self, testset: str = 'external'):
        """
        Runs inference on the specified test set and saves the results.

        :param testset: The test set to run inference on ('val', 'internal', or 'external').
        """
        tf.keras.backend.clear_session()
        self.load_model(print_summary=False)
        results = {}
        npy_file_path = f"{self.config.dirs.output}/preprocessed_npy_files/{testset}.npy"

        if os.path.exists(npy_file_path):
            print("Loading preprocessed images...")
            image_arrays = np.load(npy_file_path)
            imglist = np.load(npy_file_path.replace(".npy", '_imagelist.npy'), allow_pickle=True).tolist()
            images = {img: image_arrays[i] for i, img in enumerate(imglist)}
            images = {k: v for k, v in images.items() if v is not None and v.shape == (256, 256)}
            print("Done reading images.\n")
        else:
            if testset == 'internal' or testset == 'val':
                imglist = self.label_df[testset]['path'].str.replace("~/", os.path.expanduser("~/")).str.replace("/storage/covid/", os.path.expanduser("~/Covid/")).tolist()
            else:
                path_col = f'{STAGE_DICT[self.label]}_path'
                imglist = self.label_df[testset][path_col].str.replace("~/", os.path.expanduser("~/")).str.replace("/storage/covid/", os.path.expanduser("~/Covid/")).tolist()

            print("Reading and preprocessing images...")
            images = self._read_images(imglist)
            images = {k: v for k, v in images.items() if v is not None and v.shape == (256, 256)}
            print("Done reading and preprocessing images.\n")

            stacked_images = np.stack(list(images.values()))
            os.makedirs(os.path.dirname(npy_file_path), exist_ok=True)
            np.save(npy_file_path, stacked_images)
            np.save(npy_file_path.replace(".npy", '_imagelist.npy'), list(images.keys()))

        if self.true_col == 'ppm':
            print("Adding high intensity preprocessing...")
            images = self._add_high_intensity_preprocessing(images)
            print("Done adding high intensity preprocessing.\n")

        batches = self._create_batches(images)

        print("Making predictions...")
        if not self.config.use_gpu and self.config.multi_cpu:
            try:
                with concurrent.futures.ThreadPoolExecutor(max_workers=self.config.nworkers) as executor:
                    futures = [executor.submit(self._make_prediction, batch) for batch in batches]
                    for future in tqdm(concurrent.futures.as_completed(futures),
                                       total=len(imglist) // self.config.nbatch):
                        results.update(future.result())
            except KeyboardInterrupt:
                print('KeyboardInterrupt: Stopping inference...')
            except Exception as e:
                print(e)
            finally:
                if executor is not None:
                    executor.shutdown(wait=True)
        else:
            for batch in tqdm(batches, total=len(imglist) // self.config.nbatch):
                results.update(self._make_prediction(batch))

        if self.label == 'rotation':
            self.pred_df[testset] = pd.DataFrame.from_dict(results, orient='index')
            self.pred_df[testset] = self.pred_df[testset].reset_index()
            self.pred_df[testset].columns = [
                'path', '1st_pred_angle', '2nd_pred_angle', 'pred_angle', 'rot_pred',
                # 'rot_pred_prob',
                'rot90_pred',
                # 'rot90_prob'
            ]
        else:
            self.pred_df[testset] = pd.DataFrame(results.items(), columns=['path', f'{self.true_col}_pred_prob'])
        self.save_predictions(testset)
        print("Done making predictions.")
        print("-" * 80, "\n")

    def get_predictions(self):
        """
        Loads or runs inference to get predictions for all test sets.
        """
        for ds in ['val', 'internal', 'external']:
            pred_path = os.path.join(self.config.dirs.inference, f"{ds}_predictions_{self.label}.csv")

            if not os.path.isfile(pred_path):
                print("-" * 80)
                print(f"No prediction file found for {ds} set...\n Running inference...\n")
                self.run_inference(ds)
            else:
                self.read_predictions(ds)

        self.merge_pred_and_labels()

    def save_predictions(self, testset: str = 'external'):
        """
        Saves predictions to a file for the specified test set.

        :param testset: The test set to save predictions for ('val', 'internal', or 'external').
        """
        os.makedirs(self.config.dirs.inference, exist_ok=True)
        self.pred_df[testset].to_csv(
            os.path.join(self.config.dirs.inference, f"{testset}_predictions_{self.label}.csv"), index=False
        )

    def read_predictions(self, testset: str = 'all'):
        """
        Reads predictions from a file for the specified test set.

        :param testset: The test set to read predictions for ('val', 'internal', 'external', or 'all').
        """
        if testset == 'all':
            dss = ['val', 'internal', 'external']
        else:
            dss = [testset]

        for ds in dss:
            self.pred_df[ds] = pd.read_csv(
                os.path.join(self.config.dirs.inference, f"{ds}_predictions_{self.label}.csv")
            )

    def _get_relevant_cols(
            self,
            label: str,
            unsuitable_features=None,
            stage_dictionary=None,
    ) -> list:
        """
        Returns a list of relevant columns for testing for the given label.

        :param label: The label for which to get the relevant columns.
        :param unsuitable_features: A dictionary containing unsuitable features for each stage.
        :param stage_dictionary: A dictionary containing the stage for each label.
        :return: A list of relevant column ds_names.
        """
        if stage_dictionary is None:
            stage_dictionary = STAGE_DICT
        if unsuitable_features is None:
            unsuitable_features = UNSUITABLE_FEATURES_DICT

        if self.label == 'rotation':
            return ['dataset', 'rot', 'rot_clockwise'] + unsuitable_features[stage_dictionary[label]]
        return ['dataset', self.true_col] + unsuitable_features[stage_dictionary[label]]

    def _perform_merge_pred_and_labels(self, testset: str):
        """
        Merges predictions and labels for the specified test set.

        :param testset: The test set to merge predictions and labels for ('val', 'internal', or 'external').
        """
        if testset == 'internal' or testset == 'val':
            path_col = 'path'
            label_df = self.label_df[testset].copy()
            cols_to_drop = ['name', 'partition']
        else:
            path_col = f'{STAGE_DICT[self.label]}_path'
            label_df = self.label_df[testset][[path_col] + self.relevant_cols].copy()
            cols_to_drop = [path_col]

        label_df[path_col] = label_df[path_col].str.replace(
            "~/Covid", os.path.expanduser("~/Covid")
        ).str.replace(
            "/storage/covid/", os.path.expanduser("~/Covid/")
        )

        for col in label_df.columns:
            if col in self.pred_df[testset].columns and col != "path":
                self.pred_df[testset].drop(col, axis=1, inplace=True)

        self.pred_df[testset].path = self.pred_df[testset].path.str.replace("/storage/covid/", os.path.expanduser("~/Covid/"))
        self.df[testset] = self.pred_df[testset].merge(label_df, how='left', left_on='path', right_on=path_col)
        self.df[testset] = encode_labels(
            self.df[testset], self.true_col, label_map=LABEL_MAPS[self.label]
        ) if self.label in LABEL_MAPS.keys() else self.df[testset]
        self.df[testset][self.true_col] = pd.to_numeric(self.df[testset][self.true_col], errors='coerce')
        self.df[testset] = self.df[testset].dropna(subset=[self.true_col])
        self.df[testset][self.true_col] = self.df[testset][self.true_col].astype(int)
        self.df[testset].drop(cols_to_drop, axis=1, inplace=True)

    def merge_pred_and_labels(self, testset: str = 'all'):
        """
        Merges the predictions and labels for the test sets.

        :param testset: The test set to merge predictions and labels for ('val', 'internal', 'external', or 'all').
        """
        if testset == 'all':
            self._perform_merge_pred_and_labels('val')
            self._perform_merge_pred_and_labels('internal')
            self._perform_merge_pred_and_labels('external')
        else:
            self._perform_merge_pred_and_labels(testset)

    def plot_calibration_curve(
            self,
            testset: str = 'val',
            calibrated: bool = False,
            nbins: int = 5,
    ) -> Tuple[Figure, Axes, float]:
        """
        Plots the calibration curve for the specified test set.

        :param testset: The test set to plot the calibration curve for ('val', 'internal', or 'external').
        :param calibrated: Whether to use calibrated_clf predictions (True) or not (False).
        :param nbins: The number of bins to use for the calibration curve.
        :return: The figure and axes objects for the calibration curve.
        """
        if calibrated:
            pred_prob_col = self.cal_pred_prob_col
        else:
            pred_prob_col = self.pred_prob_col

        y_true = np.array(self.df[testset][self.true_col].astype(int))
        y_pred_prob = np.array(self.df[testset][pred_prob_col])

        brier_loss = brier_score_loss(y_true, y_pred_prob)

        fraction_of_positives, mean_predicted_value = calibration_curve(y_true, y_pred_prob, n_bins=nbins, )

        # plot calibration curve
        fig, ax = plt.subplots(figsize=self.config.figure.figsize)
        ax.plot(mean_predicted_value, fraction_of_positives, "s-", color=self.config.figure.color_dict[testset], )
        ax.plot([0, 1], [0, 1], "k:", label="Perfectly calibrated")
        ax.legend()
        ax.title.set_text(f"Calibration curve ({PRESENTATION_LABEL_MAP[testset]})")
        ax.set_xlabel("Predicted probability")
        ax.set_ylabel("Fraction of positives")
        return fig, ax, brier_loss

    def calibrate_pred(self, testset: str = 'external', nbins: int = 5) -> Tuple[Figure, Axes, float]:
        """
        Calibrates the predictions for the specified test set.

        :param testset: The test set to calibrate predictions for ('val', 'internal', or 'external').
        :param nbins: The number of bins to use for the calibration curve.
        :return: The figure and axes objects for the calibration curve.
        """
        if self.df[testset] is None:
            raise ValueError(f"Please load predictions and labels for {testset} set first.")
        y_pred_prob = np.array(self.df[testset][self.pred_prob_col])
        y_pred_calibrated = self.calibrated_clf.predict_proba(y_pred_prob.reshape(-1, 1))[:, 1]
        self.df[testset][f'{self.true_col}_cal_pred_prob'] = y_pred_calibrated

        fig, ax, brier_loss = self.plot_calibration_curve(testset, True, nbins)
        return fig, ax, brier_loss

    def calibrate_model(self, method: str = 'isotonic', save: bool = True) -> CalibratedClassifierCV:
        """
        Calibrates the model using the specified method.

        :param method: The method to use for calibration ('isotonic' or 'sigmoid'). Default: 'isotonic'.
        :param save: Whether to save the calibrated_clf classifier to a file (True) or not (False).
        :return: The calibrated_clf classifier.
        """
        self._check_val_loaded()
        y_true = np.array(self.df['val'][self.true_col].astype(int))
        y_pred_prob = np.array(self.df['val'][self.pred_prob_col].astype(float))
        self.calibrated_clf = CalibratedClassifierCV(method=method, cv='prefit')
        self.calibrated_clf.fit(y_pred_prob.reshape(-1, 1), y_true)
        if save:
            self.save_calibration()

        return self.calibrated_clf

    def save_calibration(self):
        """
        Saves the calibrated_clf classifier to a file.
        """
        os.makedirs("calibrated_clfs", exist_ok=True)
        with open(os.path.join(f"calibrated_clfs", f"{self.label}_calibrated.pkl"), 'wb') as f:
            joblib.dump(self.calibrated_clf, f)

    def load_calibration(self):
        """
        Loads the calibrated_clf classifier from a file.
        """
        with open(os.path.join(f"calibrated_clfs", f"{self.label}_calibrated.pkl"), 'rb') as f:
            self.calibrated_clf = joblib.load(f)

    def _check_val_loaded(self):
        """
        Checks whether the validation predictions and labels have been loaded.
        """
        if self.df['val'] is None:
            raise ValueError("Please load validation predictions and labels first.")

    def roc_curve(self, calibrated=False) -> Tuple[Figure, Axes]:
        """
        Plot the ROC curve.

        :calibrated: Whether to plot the ROC curve for the calibrated predictions (True) or not (False).
        :return: A tuple containing the matplotlib Figure and Axes objects.
        """
        if calibrated:
            true_col = f'{self.true_col}_cal'
            pred_prob_col = self.cal_pred_prob_col
        else:
            true_col = self.true_col
            pred_prob_col = self.pred_prob_col

        fig, ax = plt.subplots(figsize=self.config.figure.figsize)
        y_true = self.df['val'][true_col]
        if len(np.unique(y_true)) == 1:
            print(f'No positive samples in validation set. Skipping ROC curve.')
        else:
            y_pred = self.df['val'][pred_prob_col]
            fpr, tpr, _ = roc_curve(y_true, y_pred)
            roc_auc = auc(fpr, tpr)

            ax.plot(fpr, tpr,
                    color=self.config.figure.color_dict['val'], alpha=0.7,
                    label=f'{PRESENTATION_LABEL_MAP["val"]} (AUC: {roc_auc:.2f})')

        ax.plot([0, 1], [0, 1], 'k--', label='Random (AUC = 0.50)')
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.05])
        ax.set_xlabel('False Positive Rate')
        ax.set_ylabel('True Positive Rate')
        ax.set_title('Receiver Operating Characteristic (ROC) Curve')
        ax.legend(loc='lower right')
        return fig, ax

    def pr_curve(self, calibrated=False) -> Tuple[Figure, Axes]:
        """
        Plot the precision-recall (PR) curve.

        :calibrated: Whether to plot the PR curve for the calibrated predictions (True) or not (False).
        :return: A tuple containing the matplotlib Figure and Axes objects.
        """
        if calibrated:
            true_col = f'{self.true_col}_cal'
            pred_prob_col = self.cal_pred_prob_col
        else:
            true_col = self.true_col
            pred_prob_col = self.pred_prob_col

        fig, ax = plt.subplots(figsize=self.config.figure.figsize)
        y_true = self.df['val'][true_col]
        if len(np.unique(y_true)) == 1:
            print(f'No positive samples in validation set. Skipping PR curve.')
        else:
            y_pred_prob = np.array(self.df['val'][pred_prob_col])
            precision, recall, thresholds_pr = precision_recall_curve(y_true, y_pred_prob)
            ax.plot(recall, precision, color=self.config.figure.color_dict['val'], alpha=0.7,
                    label=f'{PRESENTATION_LABEL_MAP["val"]} (AUC: %0.2f)' % auc(recall, precision))

        ax.set_xlabel('Recall')
        ax.set_ylabel('Precision')
        ax.set_title('Precision-recall (PR) Curve')
        ax.legend(loc="lower left")
        return fig, ax

    def find_best_threshold(self, calibrated: bool = False) -> Tuple[float, pd.DataFrame]:
        """
        Finds the best threshold that maximizes the F1 score.

        :param calibrated: Whether to use calibrated_clf predictions (True) or not (False).
        :return: Tuple of the best threshold and a dataframe with the F1 score at each threshold tested.
        """
        self._check_val_loaded()
        y_true = self.df['val'][self.true_col]
        if calibrated:
            y_pred_prob = np.array(self.df['val'][self.cal_pred_prob_col])
        else:
            y_pred_prob = np.array(self.df['val'][self.pred_prob_col])

        thresholds = np.arange(0.01, 1, 0.01)
        f1_scores = np.array([f1_score(y_true, y_pred_prob >= t, zero_division=1) for t in thresholds])

        max_f1_score = np.max(f1_scores)
        best_thresholds = thresholds[f1_scores == max_f1_score]
        self.best_threshold = min(best_thresholds, key=lambda x: abs(x - 0.5))

        results_df = pd.DataFrame({'Threshold': thresholds, 'F1 Score': f1_scores})

        print(f"\nBest threshold: {self.best_threshold:.2f}\n")

        print(f"Results at each threshold are saved in 'f1s_by_threshold_{self.label}.csv'.")

        return self.best_threshold, results_df


class OCRModel:
    """
    A class to manage the model for testing for binary classification tasks in the AutoQC package. The class provides
    methods for loading models, making predictions, calibrating the models, and finding the best threshold that
    maximizes the F1 score.

    :param config: A Namespace object containing the configuration parameters for the model evaluation.
    """

    def __init__(self, config: Namespace):
        """
        Initializes a QCModel instance with the given label and model name.

        :param config: A Namespace object containing the configuration parameters for the model evaluation.
        """
        self.config: Namespace = config
        self.label: str = config.task
        self.true_col: str = LABEL_TO_COLNAME_MAP[config.task]
        self.relevant_cols: List[str] = self._get_relevant_cols(config.task)
        self.pred_df: Dict[str, Optional[pd.DataFrame]] = {'internal': None, 'external': None}
        self.df: Dict[str, Optional[pd.DataFrame]] = {'internal': None, 'external': None}
        self.pred_col: str = f'{self.true_col}_pred'
        self.annot_col: str = f'{self.true_col}_annot'
        self.label_df: Dict[str, pd.DataFrame] = {
            'internal': self._set_dev_labels('test'),
            'external': pd.read_csv(config.dirs.test_labels, dtype=str),
        }

    def _set_dev_labels(self, testset=None) -> pd.DataFrame:
        """
        Sets development labels for the specified test set and label.

        :param testset: The test set to set development labels for ('val', 'test', or 'external').
        :return: A DataFrame with the development labels.
        """
        df = pd.read_csv(
            os.path.join(self.config.dirs.dev_labels, f"{STAGE_DICT[self.label]}_paths+labels.csv"), dtype=str
        )
        if testset is not None:
            return df[df['partition'] == testset]
        return df

    def get_predictions(self):
        """
        Loads or runs inference to get predictions for all test sets.
        """
        preds = {}
        for ds in ['nccid', 'bimcv'] + INDIVIDUAL_EXTERNAL_HOLDOUT:
            pred_path = os.path.join(self.config.dirs.inference, f"{ds}_annotations.json")
            self.validate_prediction_file(ds, pred_path)
            data = self.read_predictions(ds)
            preds[ds] = self.extract_predictions(data)

        preds_labels = self.merge_pred_and_labels(preds)
        preds_labels = self.clean_up_inferred_projection(preds_labels)
        preds_labels = self.correct_labels(preds_labels)

        dev = pd.concat([preds_labels['nccid'], preds_labels['bimcv']])
        self.df['internal'] = dev[dev.partition == 'test'].drop(columns=['partition'])

        self.df['external'] = pd.DataFrame()
        for ds in INDIVIDUAL_EXTERNAL_HOLDOUT:
            if 'dataset' not in preds_labels[ds].columns:
                preds_labels[ds]['dataset'] = ds
            self.df[ds] = preds_labels[ds].copy()
            self.df['external'] = pd.concat([self.df['external'], preds_labels[ds]])
        for ds in ['internal', 'external']:
            self.df[ds] = self.df[ds][self.df[ds].proj.notna() & self.df[ds].proj_annot.notna()]

        dev_preds = pd.concat([preds['nccid'], preds['bimcv']])
        self.pred_df['internal'] = dev_preds[dev_preds['name'].isin(self.df['internal']['name'])]
        for ds in INDIVIDUAL_EXTERNAL_HOLDOUT:
            if 'dataset' not in preds_labels[ds].columns:
                preds[ds]['dataset'] = ds
            self.pred_df[ds] = preds[ds].copy()
            self.pred_df['external'] = pd.concat(
                [self.pred_df['external'], preds[ds][preds[ds]['name'].isin(self.df['external']['name'])]]
            )

        self.get_prediction_stats()

    @staticmethod
    def validate_prediction_file(dset, predictions_path):
        if not os.path.isfile(predictions_path):
            raise FileNotFoundError(f"Predictions for {dset} set not found. "
                                    f"Sorry - you must perform ocr inference prior to evaluation for "
                                    f"each dataset.")

    def read_predictions(self, testset: str):
        """
        Reads predictions from a file for the specified test set.

        :param testset: The test set to read predictions for ('val', 'internal', 'external', or 'all').
        """
        annot_path = os.path.join(self.config.dirs.inference, f"{testset}_annotations.json")
        with open(annot_path) as f:
            data = json.load(f)
        return data

    @staticmethod
    def extract_predictions(data: dict) -> pd.DataFrame:

        def infer_projection(df):
            """
            Can infer portable if location given
            Can infer AP if portable, supine or semi-erect
            Can infer PA if prone
            """
            df.loc[df.PORTABLE.notna() & (df.PROJECTION.isna() | (
                    df.PROJECTION.str.contains("PA") & df.PROJECTION.str.contains("AP"))), "PROJECTION"] = "AP"
            df = df.fillna(".")
            df.loc[(df['PATIENT POSITIONING'].str.contains("SEMI-ERECT")) & ((df.PROJECTION == ".") | (
                    df.PROJECTION.str.contains("PA") & df.PROJECTION.str.contains("AP"))), "PROJECTION"] = "AP"
            df.loc[(df['PATIENT POSITIONING'].str.contains("SUPINE")) & ((df.PROJECTION == ".") | (
                    df.PROJECTION.str.contains("PA") & df.PROJECTION.str.contains("AP"))), "PROJECTION"] = "AP"
            df.loc[(df['PATIENT POSITIONING'].str.contains("PRONE")) & ((df.PROJECTION == ".") | (
                    df.PROJECTION.str.contains("PA") & df.PROJECTION.str.contains("AP"))), "PROJECTION"] = "PA"
            df = df.replace(".", np.nan)
            return df

        with open_text('auto_qc', 'dict_table.csv') as f:
            df = pd.read_csv(f, dtype=str)
        for col in df.columns:
            df[col] = df[col].str.upper()
            df[col] = df[col].str.strip()
        combined_dict, eng_dict, spa_dict, ita_dict, port_dict = create_dictionaries(df)

        cleaned = clean_data(data)
        results = find_terms(cleaned, combined_dict)
        results_df = populate_category_df(results)
        results_df = infer_projection(results_df)
        return results_df

    @staticmethod
    def clean_up_inferred_projection(_preds_and_labels: Dict[str, pd.DataFrame]) -> Dict[str, pd.DataFrame]:
        _dfs = _preds_and_labels.copy()
        for name in _dfs.keys():
            _dfs[name].loc[
                (_dfs[name].PROJECTION != 'AP') & (_dfs[name].PROJECTION != 'PA') & \
                (_dfs[name].PROJECTION != 'LATERAL'), "PROJECTION"
            ] = np.nan
            _dfs[name] = _dfs[name][_dfs[name].proj != 'lat']
            _dfs[name].PROJECTION = _dfs[name].PROJECTION.map(LABEL_MAPS['ocr'])
            _dfs[name].proj = _dfs[name].proj.map(LABEL_MAPS['ocr'])
        return _dfs

    @staticmethod
    def correct_labels(_preds_and_labels: Dict[str, pd.DataFrame]) -> Dict[str, pd.DataFrame]:
        _dfs = _preds_and_labels.copy()
        for name in _dfs.keys():
            _dfs[name].loc[_dfs[name].PORTABLE.notna(), 'proj_annot'] = 1
            _dfs[name] = _dfs[name].fillna(".")
            _dfs[name].loc[_dfs[name]['PATIENT POSITIONING'].str.contains("SEMI-ERECT"), 'proj_annot'] = 1
            _dfs[name].loc[_dfs[name]['PATIENT POSITIONING'].str.contains("SUPINE"), 'proj_annot'] = 1
            _dfs[name] = _dfs[name].replace(".", np.nan)
        return _dfs

    def _get_relevant_cols(
            self,
            label: str,
            unsuitable_features=None,
            stage_dictionary=None,
    ) -> list:
        """
        Returns a list of relevant columns for testing for the given label.

        :param label: The label for which to get the relevant columns.
        :param unsuitable_features: A dictionary containing unsuitable features for each stage.
        :param stage_dictionary: A dictionary containing the stage for each label.
        :return: A list of relevant column ds_names.
        """
        if stage_dictionary is None:
            stage_dictionary = STAGE_DICT
        if unsuitable_features is None:
            unsuitable_features = UNSUITABLE_FEATURES_DICT
        return ['dataset', self.true_col, "proj_annot"] + unsuitable_features[stage_dictionary[label]]

    def _perform_merge_pred_and_labels(self, testset: str, preds):
        """
        Merges predictions and labels for the specified test set.

        :param testset: The test set to merge predictions and labels for ('val', 'internal', or 'external').
        """
        if testset == 'nccid' or testset == 'bimcv':
            path_col = 'path'
            label_df = self._set_dev_labels()
        else:
            path_col = f'{STAGE_DICT[self.label]}_path'
            label_df = self.label_df['external'][[path_col] + self.relevant_cols].copy()
            if "name" not in label_df.columns:
                label_df['name'] = label_df[path_col].str.split("/").str[-1]
            label_df = label_df.rename({path_col: 'path'}, axis=1)

        label_df['path'] = label_df['path'].str.replace("~/", os.path.expanduser("~/")).str.replace("/storage/covid/", os.path.expanduser("~/Covid/"))
        df = preds.merge(label_df, how='left', on='name')
        df = encode_labels(
            df, self.true_col, label_map=LABEL_MAPS[self.label]
        ) if self.label in LABEL_MAPS.keys() else df
        df[self.true_col] = pd.to_numeric(df[self.true_col], errors='coerce')
        df = df.dropna(subset=[self.true_col])
        df[self.true_col] = df[self.true_col].astype(int)
        return df

    def merge_pred_and_labels(self, preds):
        """
        Merges the predictions and labels for the test sets.

        :param preds: The test set to merge predictions and labels for ('val', 'internal', 'external', or 'all').
        """
        for ds in preds.keys():
            preds[ds] = self._perform_merge_pred_and_labels(ds, preds[ds])
        return preds

    def get_prediction_stats(self):
        pred_possible = {}
        pred_notpossible = {}

        for name in self.df.keys():
            self.df[name].loc[self.df[name].PROJECTION.isna(), "pred_made"] = 0
            self.df[name].loc[self.df[name].PROJECTION.notna(), "pred_made"] = 1

            pred_possible[name] = self.df[name][self.df[name].proj_annot.astype(float) == 1]
            pred_notpossible[name] = self.df[name][self.df[name].proj_annot.astype(float) != 1]

        with open(os.path.join(self.config.dirs.output, "projection_ocr_stats.txt"), 'w') as f:
            print("Proportion of predictions made where annotation exists...", file=f)
            for name in self.df.keys():
                print(
                    f"{name}: "
                    f"{100 * pred_possible[name].pred_made.sum() / pred_possible[name].proj_annot.astype(float).sum():.2f}%"
                    f" - {int(pred_possible[name].pred_made.sum())}/{int(pred_possible[name].proj_annot.astype(float).sum())}",
                    file=f
                )

            print("-" * 75, file=f)
            print("Proportion of predictions made where annotation does not exist...", file=f)
            for name in self.df.keys():
                print(
                    f"{name}: "
                    f"{100 * pred_notpossible[name].pred_made.sum() / pred_notpossible[name].proj_annot.astype(float).count():.2f}%"
                    f" - {int(pred_notpossible[name].pred_made.sum())}/{int(pred_notpossible[name].proj_annot.astype(float).count())}",
                    file=f
                )


class QCTrainingEvaluator:
    """
    A class to evaluate the training process of a model, including plotting training history curves
    and showing metrics for the training, validation and internal test sets.
    """

    def __init__(self, config: Namespace):
        """
        Initialize the TrainingEvaluator with a given QC label.

        :param config: The config object for the model evaluation.
        """
        self.config: Namespace = config
        self.label: str = config.task
        self.history: pd.DataFrame = pd.read_csv(config.dirs.logfile)
        self.training_eval: dict = self._load_eval()
        self.training_curves: Tuple[plt.Figure, plt.Axes] = self.plot_training_curves()

    def _load_eval(self) -> Dict[str, float]:
        """
        Load the model evaluation results from a JSON file.

        :return: A dictionary containing the model evaluation results.
        """
        with open(self.config.dirs.evalfile) as fh:
            res = json.loads(fh.read())
        return res

    def plot_dl_training_curves(self) -> Tuple[Figure, Axes]:
        """
        Plot the training history curves for the model.

        :return: A tuple containing the figure and axes of the plot.
        """

        fig, axes = plt.subplots(nrows=4, ncols=2,
                                 # sharex='all',
                                 figsize=self.config.figure.large_figsize)

        self.history.plot("epoch", ["loss", "val_loss"], title='Loss', ylabel='Loss', ax=axes[0, 0],
                          color=[self.config.figure.color_dict['train'], self.config.figure.color_dict['val']])
        self.history.plot("epoch", ["accuracy", "val_accuracy"], title='Accuracy', ylabel='Accuracy', ax=axes[0, 1],
                          color=[self.config.figure.color_dict['train'], self.config.figure.color_dict['val']])
        self.history.plot("epoch", ["precision", "val_precision"], title='Precision', ylabel='Precision', ax=axes[1, 0],
                          color=[self.config.figure.color_dict['train'], self.config.figure.color_dict['val']])
        self.history.plot("epoch", ["recall", "val_recall"], title='Recall', ylabel='Recall', ax=axes[1, 1],
                          color=[self.config.figure.color_dict['train'], self.config.figure.color_dict['val']])
        self.history.plot("epoch", ["f1", "val_f1"], title='F1 Score', ylabel='F1 Score', ax=axes[2, 0],
                          color=[self.config.figure.color_dict['train'], self.config.figure.color_dict['val']])
        self.history.plot("epoch", ["tp", "tn", "fp", "fn"], title='TP, TN, FP, FN (Training Set)', ylabel='Count',
                          ax=axes[2, 1], cmap=self.config.cm_cmap, alpha=0.7)
        self.history.plot("epoch", ["auc", "val_auc"], title='AUROC', ylabel='AUROC', ax=axes[3, 0],
                          color=[self.config.figure.color_dict['train'], self.config.figure.color_dict['val']])
        self.history.plot("epoch", ["prc", "val_prc"], title='AUPRC', ylabel='AUPRC', ax=axes[3, 1],
                          color=[self.config.figure.color_dict['train'], self.config.figure.color_dict['val']])

        for i in range(4):
            for j in range(2):
                if i == 2 and j == 1:
                    axes[i, j].legend(["TP", "TN", "FP", "FN"])
                else:
                    axes[i, j].legend(["Training", "Validation"])

        fig.suptitle(f"Training History for {PRESENTATION_LABEL_MAP[self.label]}", fontsize=18, y=0.99)
        fig.tight_layout()

        return fig, axes

    def plot_rotation_training_curves(self) -> Tuple[Figure, Axes]:
        """
        Plot the training history curves for the model.

        :return: A tuple containing the figure and axes of the plot.
        """

        fig, axes = plt.subplots(ncols=2,
                                 # sharex='all',
                                 figsize=(self.config.figure.figsize[0] * 2, self.config.figure.figsize[1]))

        self.history.plot("epoch", ["loss", "val_loss"], title='Loss', ylabel='Loss', ax=axes[0],
                          color=[self.config.figure.color_dict['train'], self.config.figure.color_dict['val']])
        self.history.plot("epoch", ["angle_error", "val_angle_error"],
                          title='Angle Error', ylabel='Angle Error', ax=axes[1],
                          color=[self.config.figure.color_dict['train'], self.config.figure.color_dict['val']])

        for i in range(2):
            axes[i].legend(["Training", "Validation"])

        fig.suptitle(f"Training History for {PRESENTATION_LABEL_MAP[self.label]}", fontsize=18, y=0.99)
        fig.tight_layout()

        return fig, axes

    def plot_training_curves(self) -> Tuple[Figure, Axes]:
        if self.label == 'rotation':
            return self.plot_rotation_training_curves()
        return self.plot_dl_training_curves()

    def internal_metrics(self) -> Tuple[Dict[str, Tuple[Figure, Axes]], pd.DataFrame]:
        """
        Show the metrics for the training, validation, and internal test sets.

        :return: A tuple containing: 1. a dictionary where the keys are 'train', 'val', 'test' and the values are
            tuples of the confusion matrix figure and axes; and 2. a dataframe of all metrics for each of the
            development sets (including the internal hold-out set aka. 'test').
        """
        last_epoch = self.history.iloc[-1]

        cm = {'train': make_confusion_matrix(
            "Training set",
            np.array([[last_epoch.tn, last_epoch.fp],
                      [last_epoch.fn, last_epoch.tp]]),
            self.config.figure.cm_figsize,
            cmap=self.config.figure.confusion_matrix_colors["Training set"],
            font_size=self.config.figure.cm_font_size
        ), 'val': make_confusion_matrix(
            "Validation set",
            np.array([[last_epoch.val_tn, last_epoch.val_fp],
                      [last_epoch.val_fn, last_epoch.val_tp]]),
            self.config.figure.cm_figsize,
            cmap=self.config.figure.confusion_matrix_colors["Validation set"],
            font_size=self.config.figure.cm_font_size
        ), 'test': make_confusion_matrix(
            "Test set",
            np.array([[self.training_eval["tn"], self.training_eval["fp"]],
                      [self.training_eval["fn"], self.training_eval["tp"]]]),
            self.config.figure.cm_figsize,
            cmap=self.config.figure.confusion_matrix_colors["Test set"],
            font_size=self.config.figure.cm_font_size
        )}

        train_sens, train_spec, train_ppv, train_npv = calculate_conf_matrix_metrics(
            np.array([[last_epoch.tn, last_epoch.fp], [last_epoch.fn, last_epoch.tp]])
        )
        val_sens, val_spec, val_ppv, val_npv = calculate_conf_matrix_metrics(
            np.array([[last_epoch.val_tn, last_epoch.val_fp], [last_epoch.val_fn, last_epoch.val_tp]])
        )
        test_sens, test_spec, test_ppv, test_npv = calculate_conf_matrix_metrics(
            np.array([[self.training_eval["tn"], self.training_eval["fp"]],
                      [self.training_eval["fn"], self.training_eval["tp"]]])
        )

        metrics_dict = {
            "Set": ["Training", "Validation", "Test"],
            "Accuracy": [last_epoch.accuracy, last_epoch.val_accuracy, self.training_eval["accuracy"]],
            "AUC": [last_epoch.auc, last_epoch.val_auc, self.training_eval["auc"]],
            "Precision": [last_epoch.precision, last_epoch.val_precision, self.training_eval["precision"]],
            "Recall": [last_epoch.recall, last_epoch.val_recall, self.training_eval["recall"]],
            "Sensitivity": [train_sens, val_sens, test_sens],
            "Specificity": [train_spec, val_spec, test_spec],
            "PPV": [train_ppv, val_ppv, test_ppv],
            "NPV": [train_npv, val_npv, test_npv],
            "F1 Score": [last_epoch.f1, last_epoch.val_f1, self.training_eval["f1"]],
            "PRC": [last_epoch.prc, last_epoch.val_prc, self.training_eval["prc"]]
        }

        return cm, pd.DataFrame(metrics_dict)


class QCInferenceEvaluator:
    """
    A class for evaluating the quality control model's predictions on a particular dataset. It will calculate the
    metrics for the dataset with confidence intervals, plot confusion matrices, plot the ROC and PRC curves, and
    display examples of images for which the model made a mistake.

    :param config: The config object for the model.
    :param calibrated: Whether the model is calibrated or not. Defaults to False.
    :param chosen_threshold: The chosen threshold for binary classification. Defaults to 0.5.
    """

    def __init__(
            self, config: Namespace, datasets: Union[str, List[str]], model: QCModel = None, calibrated: bool = None,
            chosen_threshold: Optional[float] = None
    ) -> None:
        """
        Initialize the InferenceEvaluator class.

        :param config: The config object for the model.
        :param calibrated: Whether the model is calibrated or not. Defaults to False.
        :param chosen_threshold: The chosen threshold for binary classification. Defaults to 0.5.
        """
        self.config: Namespace = config
        self.label: str = config.task
        self.true_col: str = LABEL_TO_COLNAME_MAP[config.task]
        self.model: QCModel = model if model is not None else QCModel(config)
        self.ds_names: List[str] = ALL_HOLDOUT if datasets == 'all_holdout' or datasets == "all" else [
            datasets,
        ] if isinstance(datasets, str) else datasets
        self.calibrated: bool = calibrated
        if config.exclude_unsuitable:
            self.datasets: Dict[str, pd.DataFrame] = exclude_unsuitable_cases(
                config.task, self._get_datasets(), config.bounds
            )
        else:
            self.datasets: Dict[str, pd.DataFrame] = self._get_datasets()

        if self.label in LABEL_MAPS.keys():
            for name, ds in self.datasets.items():
                self.datasets[name] = encode_labels(
                    ds, LABEL_TO_COLNAME_MAP[self.label], label_map=LABEL_MAPS[self.label]
                )
        self.threshold: float = chosen_threshold
        self.metrics: List[str] = [
            'accuracy',
            'roc_auc',
            'precision',
            'recall',
            'sensitivity',
            'specificity',
            'ppv',
            'npv',
            'f1',
            'auprc'
        ]
        self.nbootstrap: int = config.nbootstrap
        if self.threshold is not None:
            self._get_binary_predictions()
        if self.label == "ocr":
            self._get_binary_ocr_predictions(self.config.ocr_only_use_if_pred_made)
        self.bootstrap_scores: Dict[str, Dict[str, List[float]]] = {
            name: self._get_scores(name, self.nbootstrap) for name in self.datasets.keys()
        }
        self.cis: Dict[str, Dict[str, Tuple[float, float]]] = {
            name: self._get_cis(name) for name in self.datasets.keys()
        }
        self.results: Dict[str, Dict[str, float]] = {
            name: self._get_results(name) for name in self.datasets.keys()
        }
        self.label_df: pd.DataFrame = get_label_counts(self.datasets, self.label, self.true_col)

    @property
    def pred_col(self) -> str:
        """
        Get the column name for predicted labels based on whether the model is calibrated or not.

        :return: A string representing the column name for predicted labels.
        """
        return f'{self.true_col}_cal_pred' if self.calibrated else f'{self.true_col}_pred'

    @property
    def pred_prob_col(self) -> str:
        """
        Get the column name for predicted probabilities based on whether the model is calibrated or not.

        :return: A string representing the column name for predicted probabilities.
        """
        return f'{self.true_col}_cal_pred_prob' if self.calibrated else f'{self.true_col}_pred_prob'

    @property
    def results_df(self) -> pd.DataFrame:
        """
        Get a DataFrame containing the evaluation results along with their confidence intervals.  Note that the entries
        are strings to improve formatting, so you may wish to use the raw values in the self.results and self.cis
        attributes for any plots or additional calculations.

        :return: A pandas DataFrame containing the evaluation results and confidence intervals.
        """
        return make_results_df(self.results, self.cis)

    @property
    def failure_dfs(self) -> Dict[str, pd.DataFrame]:
        """
        Get a dictionary containing DataFrames with the failed predictions for each dataset.

        :return: A dictionary with dataset ds_names as keys and DataFrames containing failed predictions as values.
        """
        return {name: self._identify_failures(name) for name in self.datasets.keys()}

    def roc_curve(self) -> Tuple[Figure, Axes]:
        """
        Plot the ROC curve.

        :return: A tuple containing the matplotlib Figure and Axes objects.
        """
        fig, ax = plt.subplots(figsize=self.config.figure.figsize)
        for name, df in self.datasets.items():
            y_true = df[self.true_col]
            if len(np.unique(y_true)) == 1:
                print(f'No positive samples in {name} dataset. Skipping ROC curve.')
            else:
                y_pred = df[self.pred_prob_col]
                fpr, tpr, _ = roc_curve(y_true, y_pred)
                roc_auc = auc(fpr, tpr)

                ax.plot(fpr, tpr,
                        color=self.config.figure.color_dict[name], alpha=0.7,
                        label=f'{PRESENTATION_LABEL_MAP[name]} (AUC: {roc_auc:.2f})')

        ax.plot([0, 1], [0, 1], 'k--', label='Random (AUC = 0.50)')
        ax.set_xlim([0.0, 1.0])
        ax.set_ylim([0.0, 1.05])
        ax.set_xlabel('False Positive Rate')
        ax.set_ylabel('True Positive Rate')
        ax.set_title('Receiver Operating Characteristic (ROC) Curve')
        ax.legend(loc='lower right')
        return fig, ax

    def pr_curve(self) -> Tuple[Figure, Axes]:
        """
        Plot the precision-recall (PR) curve.

        :return: A tuple containing the matplotlib Figure and Axes objects.
        """
        fig, ax = plt.subplots(figsize=self.config.figure.figsize)
        for name, df in self.datasets.items():
            y_true = df[self.true_col]
            if len(np.unique(y_true)) == 1:
                print(f'No positive samples in {name} dataset. Skipping PR curve.')
            else:
                y_pred_prob = np.array(df[self.pred_prob_col])
                precision, recall, thresholds_pr = precision_recall_curve(y_true, y_pred_prob)
                ax.plot(recall, precision, color=self.config.figure.color_dict[name], alpha=0.7,
                        label=f'{PRESENTATION_LABEL_MAP[name]} (AUC: %0.2f)' % auc(recall, precision))

        ax.set_xlabel('Recall')
        ax.set_ylabel('Precision')
        ax.set_title('Precision-recall (PR) Curve')
        ax.legend(loc="lower left")
        return fig, ax

    def confusion_matrices(self) -> Dict[str, Tuple[Figure, Axes]]:
        """
        Get the confusion matrix and plot it.

        :return: A tuple containing the figure and axes of the confusion matrix plot.
        """
        matrices = {}
        for name, df in self.datasets.items():
            y_true = df[self.true_col]
            y_pred = np.array(df[self.pred_col])

            conf_matrix = confusion_matrix(y_true, y_pred, labels=[0, 1])
            fig, ax = make_confusion_matrix(
                PRESENTATION_LABEL_MAP[name],
                conf_matrix,
                figsize=self.config.figure.figsize,
                cmap=self.config.figure.confusion_matrix_colors[PRESENTATION_LABEL_MAP[name]],
                font_size=self.config.figure.cm_font_size
            )
            matrices[name] = (fig, ax)
        return matrices

    def probs_hist(self, bins: int = 20, alpha: float = 0.7, **kwargs) -> Tuple[Figure, Axes]:
        """
        Plot a histogram of prediction probabilities.

        :param bins: The number of bins in the histogram. Defaults to 20.
        :param alpha: The alpha value for the histogram. Defaults to 0.7.
        :param kwargs: Additional keyword arguments to pass to the matplotlib hist function.
        :return: A tuple containing the matplotlib Figure and Axes objects.
        """
        fig, ax = plt.subplots(figsize=self.config.figure.figsize)

        if any(ds in INDIVIDUAL_EXTERNAL_HOLDOUT for ds in
               self.datasets.keys()) and 'all_external' in self.datasets.keys():
            dict_to_plot = {name: df for name, df in self.datasets.items() if name != 'all_external'}
        else:
            dict_to_plot = self.datasets

        labels = [PRESENTATION_LABEL_MAP[name] for name in dict_to_plot.keys()]

        ax.hist(
            [df[self.pred_prob_col] for df in dict_to_plot.values()],
            bins=bins,
            rwidth=0.8,
            stacked=True,
            label=labels,
            alpha=alpha,
            edgecolor=self.config.figure.edgecolor,
            color=[self.config.figure.color_dict[name] for name in dict_to_plot.keys()],
            **kwargs
        )

        ax.legend()
        ax.set_title('Histogram of prediction probabilities')
        ax.set_xlabel('Probability')
        ax.set_ylabel('Count')
        fig.tight_layout()
        return fig, ax

    def prob_info(self) -> str:

        statement_list = []

        if "all_external" in self.datasets.keys():

            _df = self.datasets['all_external'].copy()
            incorrect = _df[_df[self.pred_col].astype(int) != _df[self.true_col].astype(int)]

            overall_failures = f'In all of the external data, {len(incorrect)} predictions were incorrect ' \
                               f'out of {len(_df)} images\n'
            statement_list.append(overall_failures)

            for prob_range in [(0.1, 0.9), (0.2, 0.8), (0.3, 0.7)]:
                mid_range = _df[(_df[self.pred_prob_col] > prob_range[0]) & (_df[self.pred_prob_col] < prob_range[1])]
                statement_list.append(f'There were a total of {len(mid_range)} images with predictions '
                                      f'between {prob_range[0]} and {prob_range[1]}')
                _failure_df = self.failure_dfs['all_external'].copy()
                mid_range_failures = _failure_df[(_failure_df[self.pred_prob_col] > prob_range[0]) & \
                                                 (_failure_df[self.pred_prob_col] < prob_range[1])]
                statement_list.append(f'{len(mid_range_failures)} of these were incorrect\n')

            return '\n'.join(statement_list)

        return 'No external data was used in this experiment.'

    def failure_hist(
            self, bins: int = 20, alpha: float = 0.7, **kwargs
    ) -> Tuple[Figure, Axes]:
        """
        Plot a histogram of prediction probabilities in failures.

        :param bins: The number of bins in the histogram. Defaults to 20.
        :param alpha: The alpha value for the histogram. Defaults to 0.7.
        :param kwargs: Additional keyword arguments to pass to the matplotlib hist function.
        :return: A tuple containing the matplotlib Figure and Axes objects.
        """
        fig, ax = plt.subplots(figsize=self.config.figure.figsize)

        if any(ds in INDIVIDUAL_EXTERNAL_HOLDOUT for ds in
               self.failure_dfs.keys()) and 'all_external' in self.failure_dfs.keys():
            dict_to_plot = {name: df for name, df in self.failure_dfs.items() if name != 'all_external'}
        else:
            dict_to_plot = self.datasets

        labels = [PRESENTATION_LABEL_MAP[name] for name in dict_to_plot.keys()]

        ax.hist(
            [df[self.pred_prob_col] for df in dict_to_plot.values()],
            bins=bins,
            rwidth=0.8,
            stacked=True,
            label=labels,
            alpha=alpha,
            edgecolor=self.config.figure.edgecolor,
            color=[self.config.figure.color_dict[name] for name in dict_to_plot.keys()],
            **kwargs
        )

        ax.legend()
        ax.set_title('Histogram of pred probabilities in failures')
        ax.set_xlabel('Probability')
        ax.set_ylabel('Count')
        return fig, ax

    def failure_grids(self, ncols: int = 3, max_rows: int = 10, with_gradcam=False) -> Dict[str, Tuple[Figure, Axes]]:
        """
        Display the failures in the predictions as an image grid.

        :param ncols: The number of columns in the grid. Defaults to 3.
        :param max_rows: The maximum number of rows in the grid. Defaults to 10.
        :param with_gradcam: Whether to include GradCAM images in the grid. Defaults to False.
        :return: A dictionary containing tuples of the matplotlib Figure and Axes objects for each dataset.
        """
        grids = {}
        for name, df in self.failure_dfs.items():
            max_images = ncols * max_rows
            failures = df['path'].tolist()
            if self.label == 'ocr':
                failures = [im_path.replace("stage2", "stage4").replace(
                    "stage4", "stage2_before_inpainting"
                ) for im_path in failures]
            preds = df[self.pred_col].tolist()
            probs = df[self.pred_prob_col].tolist() if self.pred_prob_col in df.columns else None

            failures = [im_path for im_path in failures if os.path.exists(im_path)]
            if len(failures) == 0:
                continue
            else:
                if len(failures) > max_images:
                    failures = failures[:max_images]
                if self.label == 'rotation' or self.pred_prob_col not in df.columns:
                    fig, axes = self.plot_image_grid(failures, ncols, preds=preds, with_gradcam=with_gradcam)
                else:
                    fig, axes = self.plot_image_grid(failures, ncols, probs=probs, with_gradcam=with_gradcam)
                if with_gradcam:
                    fig.suptitle(f'Failures in {PRESENTATION_LABEL_MAP[name]} with GradCAM', fontsize=self.config.figure.font_size+6)
                else:
                    fig.suptitle(f'Failures in {PRESENTATION_LABEL_MAP[name]}', fontsize=self.config.figure.font_size+6)
                grids[name] = (fig, axes)
        return grids

    def gradcam_examples_grid(self, ncols: int = 3, max_rows: int = 5) -> Dict[str, Tuple[Figure, Axes]]:
        grids = {}
        for name, df in self.datasets.items():
            max_images = ncols * max_rows
            sample_df = df.sample(max_images, random_state=42).copy()
            sample_paths = sample_df['path'].tolist()
            probs = sample_df[self.pred_prob_col].tolist() if self.pred_prob_col in df.columns else None
            preds = sample_df[self.pred_col].tolist() if self.pred_col in df.columns else None
            sample_paths = [im_path for im_path in sample_paths if os.path.exists(im_path)]
            if len(sample_paths) == 0:
                continue
            else:
                if self.label == 'rotation' or self.pred_prob_col not in df.columns:
                    fig, axes = self.plot_image_grid(sample_paths, ncols, preds=preds, with_gradcam=True)
                else:
                    fig, axes = self.plot_image_grid(sample_paths, ncols, probs=probs, with_gradcam=True)
                fig.suptitle(f'GradCAM Examples for {PRESENTATION_LABEL_MAP[name]}',
                             fontsize=self.config.figure.font_size+6)
                grids[name] = (fig, axes)
        return grids

    def _calibrate_predictions(self):
        """
        Calibrate the predictions for each dataset using the calibrated classifier.
        """
        self.model.load_calibration()
        self.model.calibrate_pred('val', nbins=5)
        self.model.calibrate_pred('internal', nbins=5)
        self.model.calibrate_pred('external', nbins=5)

    def _get_datasets(self) -> Dict[str, pd.DataFrame]:
        """
        Load the specified datasets and, if necessary, calibrate the predictions.

        :return: A dictionary containing the dataset DataFrames.
        :raises ValueError: If no datasets were specified or if an invalid dataset name is provided.
        """
        if len(self.ds_names) == 0:
            raise ValueError("No datasets were specified.")
        else:
            datasets = {}
            self.model.get_predictions()
            if self.calibrated:
                self._calibrate_predictions()

            df_ext = self.model.df['external']

            for name in self.ds_names:

                # Remove cases with no ground truth (e.g. additional pacemakers added to validation and internal
                # test sets)

                if name == 'val':
                    datasets['val'] = self.model.df['val'].copy()
                elif name == 'internal':
                    datasets['internal'] = self.model.df['internal'].copy()
                elif name == 'all_external':
                    datasets['all_external'] = df_ext.copy()
                elif name == "cuh":
                    datasets['cuh'] = df_ext[df_ext.dataset == 'cuh'].copy()
                elif name == "brixia":
                    datasets['brixia'] = df_ext[df_ext.dataset == 'brixia'].copy()
                elif name == "ricord":
                    datasets['ricord'] = df_ext[df_ext.dataset == 'ricord'].copy()
                else:
                    raise ValueError(f"Invalid dataset name: {name}")

                datasets[name] = datasets[name].dropna(subset=[self.true_col])
            return datasets

    def _get_binary_predictions(self):
        """
        Generate binary predictions based on the chosen threshold attribute.
        """
        for name in self.datasets.keys():
            self.datasets[name][self.pred_col] = 0
            self.datasets[name].loc[
                self.datasets[name][self.pred_prob_col].astype(float) >= self.threshold, self.pred_col
            ] = 1

    def _get_binary_ocr_predictions(self, only_if_pred_made: bool = True):
        """
        Generate binary predictions based on the chosen threshold attribute.
        """
        for name in self.datasets.keys():
            self.datasets[name] = self.datasets[name][self.datasets[name].proj.notna()] if not only_if_pred_made \
                else self.datasets[name][self.datasets[name].proj.notna() & self.datasets[name].PROJECTION.notna()]
            self.datasets[name][self.pred_col] = 0
            self.datasets[name].loc[
                self.datasets[name].PROJECTION == LABEL_MAPS['ocr']['ap'], self.pred_col
            ] = 1

    def _get_ci_bounds(self, name: str, metric: str) -> Tuple[float, float]:
        """
        Get the confidence interval bounds for a specific metric.

        :param name: The name of the dataset for which to calculate the confidence interval bounds.
        :param metric: The metric for which to calculate the confidence interval bounds.
        :return: A tuple containing the lower and upper bounds of the confidence interval.
        """
        lower_bound = np.nanpercentile(self.bootstrap_scores[name][metric], 2.5)
        upper_bound = np.nanpercentile(self.bootstrap_scores[name][metric], 97.5)
        return lower_bound, upper_bound

    def _get_scores(self, name: str, n_bootstrap: int = 1000) -> Dict[str, Dict[str, List[float]]]:
        """
        Compute the scores for the model using bootstrapping and store the results.

        :param name: The name of the dataset for which to calculate the scores.
        :param n_bootstrap: The number of bootstrap samples to generate. Default is 1000.
        :return: A dictionary containing the computed scores for each metric.
        """
        scores = {metric: [] for metric in self.metrics}

        y_true = np.array(self.datasets[name][self.true_col].astype(int))
        if self.pred_prob_col in self.datasets[name].columns:
            y_pred_prob = np.array(self.datasets[name][self.pred_prob_col])
        else:
            y_pred_prob = None
        y_pred = np.array(self.datasets[name][self.pred_col])

        if len(np.unique(y_true)) == 1:
            print(f"WARNING: Only one class present in {name}. Can only calculate accuracy.")

        for i in range(n_bootstrap):
            if self.pred_prob_col in self.datasets[name].columns:
                y_true_sample, y_pred_sample, y_pred_prob_sample = resample(y_true, y_pred, y_pred_prob)
                metrics_sample = compute_metrics(y_true_sample, y_pred_sample, y_pred_prob_sample)
            else:
                y_true_sample, y_pred_sample = resample(y_true, y_pred)
                metrics_sample = compute_metrics(y_true_sample, y_pred_sample)

            for metric, value in metrics_sample.items():
                scores[metric].append(value)

        return scores

    def _get_results(self, name: str) -> Dict[str, float]:
        """
        Calculate the mean of the bootstrap scores for each metric.

        :param name: The name of the dataset for which to calculate the results.
        :return: A dictionary containing the mean of the bootstrap scores for each metric.
        """
        return {metric: np.nanmean(self.bootstrap_scores[name][metric]) for metric in self.metrics}

    def _get_cis(self, name: str) -> Dict[str, Dict[str, Tuple[float, float]]]:
        """
        Get the confidence intervals for each metric.

        :param name: The name of the dataset for which to calculate the confidence intervals.
        :return: A dictionary containing the confidence intervals.
        """
        return {metric: self._get_ci_bounds(name, metric) for metric in self.metrics}

    def _identify_failures(self, name) -> pd.DataFrame:
        """
        Identify the failures in the predictions.

        :param name: The name of the dataset for which to identify the failures.
        :return: A DataFrame containing the failures.
        """
        df = self.datasets[name].copy()
        df['failure'] = df[self.true_col] != df[self.pred_col]
        return df[df['failure']].reset_index(drop=True)

    def _read_image(self, image_path: str) -> np.ndarray:
        if self.label == 'rotation':
            return preprocess_image(remove_border(imread(image_path)))
        return preprocess_image(imread(image_path))

    def plot_image_grid(
            self,
            images: List[str],
            ncols: int = 5,
            probs: Optional[List[float]] = None,
            preds: Optional[List[int]] = None,
            with_gradcam: bool = False,
    ) -> Tuple[Figure, Axes]:
        """
        Plot an image grid with the specified images.

        :param images: A list of image paths.
        :param ncols: The number of columns in the grid. Defaults to 5.
        :param probs: A list of prediction probabilities corresponding to the images.
        :param preds: A list of predictions corresponding to the images.
        :param with_gradcam: Whether to plot the Grad-CAM heatmap on top of the images. Defaults to False.
        :param w_space: The width of the space between the images. Defaults to 0.01.
        :param h_space: The height of the space between the images. Defaults to 0.2.
        :return: A tuple containing the matplotlib Figure and Axes objects.
        """
        images = [image for image in images if os.path.exists(image)]
        nrows = -(-len(images) // ncols)
        figsize = (
            self.config.figure.image_grid_size_factor * ncols, self.config.figure.image_grid_size_factor * nrows + 1
        )
        fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=figsize, layout='constrained')

        conv_layers = None
        if with_gradcam:
            self.model.load_model(print_summary=False)
            # conv_layers = [18, 30] if self.label in ['projection', 'pacemaker'] else [-1]
            conv_layers = [12, 20] if 'model_projection_before_inpainting.h5' in str(self.model.checkpoint) or 'model_projection_bboxes.h5' in str(self.model.checkpoint) else [18, 30] if self.label in ['projection', 'pacemaker'] else [-1]
            # conv_layers = [12, 20] if 'model_projection_before_inpainting.h5' in str(self.model.checkpoint) else [-1] if self.label in ['projection', 'pacemaker'] else [-1]


        for i, ax in enumerate(axes.flat):
            if i < len(images):
                image = self._read_image(images[i])
                # cbar = True if (i + 1) % ncols == 0 and else False
                cbar = True if (i + 1) == ncols else False
                # cbar = False
                if with_gradcam and self.label == 'pacemaker':
                    hi_image = high_intensity_enhancement(image)   # Preprocess the image for pacemakers
                    heatmap = grad_cam(self.model.model, hi_image, layers_idx=conv_layers)
                    plot_heatmap(image, heatmap, ax=ax, cbar=cbar)
                elif with_gradcam:
                    heatmap = grad_cam(self.model.model, image, layers_idx=conv_layers)
                    plot_heatmap(image, heatmap, ax=ax, cbar=cbar)
                else:
                    ax.imshow(image, cmap='bone')
                if probs is None:
                    if float(preds[i]) == 1:
                        ax.set_title(f"Pred {self.label}") if self.label != 'ocr' else ax.set_title(f"Pred AP")
                    else:
                        ax.set_title(f"Pred no {self.label}") if self.label != 'ocr' else ax.set_title(f"Pred PA")
                else:
                    if probs[i] >= self.threshold:
                        if self.label != 'projection':
                            ax.set_title(f"Pred {self.label} (p = {probs[i]:.3f})")
                        else:
                            ax.set_title(f"Pred AP (p = {probs[i]:.3f})")
                    else:
                        if self.label != 'projection':
                            ax.set_title(f"Pred not {self.label} (p = {probs[i]:.3f})")
                        else:
                            ax.set_title(f"Pred PA (p = {probs[i]:.3f})")
                ax.axis('off')
            else:
                ax.axis('off')

        # if with_gradcam:
        #     # cax = inset_axes(axes.flat[ncols - 1], width="5%", height="100%", loc='right',
        #     #                  bbox_to_anchor=(0.1, 0., 1, 1), bbox_transform=axes.flat[ncols - 1].transAxes,
        #     #                  borderpad=0.1)
        #     #
        #     cax = inset_axes(axes.flat[ncols - 1], width="5%", height="100%", loc="lower left",
        #                      bbox_to_anchor=(1.05, 0., 1, 1), bbox_transform=axes.flat[ncols - 1].transAxes,
        #                      borderpad=0.1)
        #
        #     cbar = fig.colorbar(axes.flat[0].get_images()[1], cax=cax)
        #     x_bounds = cax.get_xbound()
        #     y_bounds = cax.get_ybound()
        #     buffer = (x_bounds[1] - x_bounds[0]) / 5
        #     black_rect = plt.Rectangle((x_bounds[0], y_bounds[0]), x_bounds[1] - x_bounds[0],
        #                                y_bounds[1] - y_bounds[0],
        #                                facecolor='black', alpha=0.5)
        #     cbar.ax.add_patch(black_rect).set_zorder(0)
        #     # Add text to the colorbar
        #     cbar.set_ticks([])
        #     cbar.set_label('Feature Importance', rotation=270, labelpad=25)
        #     _ = cbar.ax.text(x_bounds[1] + buffer, y_bounds[1], 'Higher', va='top', ha='left', fontsize=12)
        #     _ = cbar.ax.text(x_bounds[1] + buffer, y_bounds[0], 'Lower', va='bottom', ha='left', fontsize=12)

        return fig, axes


class QCAspectRatioEvaluator:
    """
    A class for evaluating the aspect ratio model's predictions. It will calculate the metrics for the dataset with
    confidence intervals, plot histograms and plot confusion matrices.
    """

    def __init__(self, config: Namespace):
        """
        Initialize the QCAspectRatioEvaluator class.
        """
        self.config: Namespace = config
        self.ds_names: List[str] = ALL_HOLDOUT if config.datasets == 'all_holdout' or config.datasets == 'all' else [
            config.datasets,
        ] if isinstance(config.datasets, str) else config.datasets
        self.bounds: Sequence[float] = config.bounds
        self.lb: float = config.bounds[0]
        self.ub: float = config.bounds[1]
        if config.exclude_unsuitable:
            self.datasets: Dict[str, pd.DataFrame] = exclude_unsuitable_cases(
                'aspect_ratio', self._load_datasets(config.dirs.inference)
            )
        else:
            self.datasets: Dict[str, pd.DataFrame] = self._load_datasets(config.dirs.inference)
        self.metrics: List[str] = [
            'accuracy',
            'roc_auc',
            'precision',
            'recall',
            'sensitivity',
            'specificity',
            'ppv',
            'npv',
            'f1',
            'auprc'
        ]
        self.nbootstrap: int = config.nbootstrap
        self.bootstrap_scores: Dict[str, Dict[str, List[float]]] = {
            name: self._get_scores(name, self.nbootstrap) for name in self.datasets.keys()
        }
        self.cis: Dict[str, Dict[str, Tuple[float, float]]] = {
            name: self._get_cis(name) for name in self.datasets.keys()
        }
        self.results: Dict[str, Dict[str, float]] = {
            name: self._get_results(name) for name in self.datasets.keys()
        }
        self.label_df: pd.DataFrame = get_label_counts(self.datasets, self.config.task, "gt")

    def _load_datasets(self, shapes_dir: str) -> Dict[str, pd.DataFrame]:
        """
        Load the specified datasets and ground truth labels.

        :param shapes_dir: The directory containing the image shapes with or without the aspect ratios. If no shapes
            file is present, they will be calculated from the png files.
        :return: A dictionary containing the dataset DataFrames.
        :raises ValueError: If no datasets were specified or if an invalid dataset name is provided.
        """
        if len(self.ds_names) == 0:
            raise ValueError("No datasets were specified.")
        else:
            datasets = {}

            df_int = pd.read_csv(
                os.path.join(self.config.dirs.dev_labels, f"{STAGE_DICT[self.config.task]}_paths+labels.csv"), dtype=str
            )
            df_ext = pd.read_csv(self.config.dirs.test_labels, dtype=str)
            df_ext.loc[(df_ext.nchest == 1) | (df_ext.nchest == "1"), 'fraction'] = 'nchest'
            df_int = df_int[(df_int.fraction.notna()) & (df_int.fraction != 'lateral')]
            df_ext = df_ext[(df_ext.fraction.notna()) & (df_ext.fraction != 'lateral')]

            for name in self.ds_names:
                if name == 'val':
                    df = df_int[df_int['partition'] == 'val'].copy()
                    datasets['val'] = aspect_ratio.get_aspect_ratios(df, 'internal_dev', shapes_dir)
                elif name == 'train':
                    df = df_int[df_int['partition'] == 'train'].copy()
                    datasets['train'] = aspect_ratio.get_aspect_ratios(df, 'internal_dev', shapes_dir)
                elif name == 'dev':
                    df = df_int[(df_int['partition'] == 'train') | (df_int['partition'] == 'val')].copy()
                    datasets['dev'] = aspect_ratio.get_aspect_ratios(df, 'internal_dev', shapes_dir)
                elif name == 'internal':
                    df = df_int[df_int['partition'] == 'test'].copy()
                    datasets['internal'] = aspect_ratio.get_aspect_ratios(df, 'internal_test', shapes_dir)
                elif name == 'all_external':
                    df = df_ext.copy()
                    datasets['all_external'] = aspect_ratio.get_aspect_ratios(df, 'external', shapes_dir)
                elif name == "cuh":
                    df = df_ext[df_ext.dataset == 'cuh'].copy()
                    datasets['cuh'] = aspect_ratio.get_aspect_ratios(df, 'external', shapes_dir)
                elif name == "brixia":
                    df = df_ext[df_ext.dataset == 'brixia'].copy()
                    datasets['brixia'] = aspect_ratio.get_aspect_ratios(df, 'external', shapes_dir)
                elif name == "ricord":
                    df = df_ext[df_ext.dataset == 'ricord'].copy()
                    datasets['ricord'] = aspect_ratio.get_aspect_ratios(df, 'external', shapes_dir)
                else:
                    raise ValueError(f"Invalid dataset name: {name}")

            for dataset in self.ds_names:
                # print(f"Dataset: {dataset}")
                # print(aspect_ratio in datasets[dataset].columns)

                datasets[dataset] = aspect_ratio.allocate_gt(datasets[dataset])
                datasets[dataset] = aspect_ratio.classify_images(datasets[dataset], self.lb, self.ub)

            return datasets

    def confusion_matrices(self) -> Dict[str, Tuple[Figure, Axes]]:
        """
        Get the confusion matrix and plot it.

        :return: A tuple containing the figure and axes of the confusion matrix plot.
        """
        matrices = {}

        for name, df in self.datasets.items():
            y_true = df['gt']
            y_pred = np.array(df['pred'])
            conf_matrix = confusion_matrix(y_true, y_pred, labels=[0, 1])
            fig, ax = make_confusion_matrix(
                PRESENTATION_LABEL_MAP[name],
                conf_matrix,
                figsize=self.config.figure.cm_figsize,
                cmap=self.config.figure.confusion_matrix_colors[PRESENTATION_LABEL_MAP[name]],
                font_size=self.config.figure.cm_font_size
            )
            fig.tight_layout()
            matrices[name] = (fig, ax)

        return matrices

    def plot_histograms(self) -> Tuple[Figure, Axes]:
        """
        Plot the histograms of the aspect ratios for each dataset with the specified bounds.

        :return: The figure and axes of the plot.
        """
        max_cols = 2
        if len(self.datasets) >= max_cols:
            ncols = max_cols
            nrows = -(-len(self.datasets) // ncols)
        else:
            ncols = 1
            nrows = 1

        size = (ncols * 5, nrows * 5)
        fig, axs = plt.subplots(nrows, ncols, figsize=size)

        dataset_iter = iter(self.datasets.items())
        for i in range(nrows):
            for j in range(ncols):
                try:
                    dataset, df = next(dataset_iter)
                    aspect_ratio.plot_bounds_histogram(df, self.bounds, ax=axs[i, j])
                    if axs[i, j].legend_ is not None:
                        if i == 0 and j == 1:
                            # axs[i, j].legend_.set_title(PRESENTATION_LABEL_MAP[dataset])
                            # axs[i, j].legend(loc='upper right', title=PRESENTATION_LABEL_MAP[dataset], framealpha=0.9)
                            axs[i, j].legend(loc='upper right', framealpha=0.9)
                            handles, labels = axs[i, j].get_legend_handles_labels()
                            new_labels = ['>66% lung', '<66% lung', 'Lower B.', 'Upper B.']
                            axs[i, j].legend(handles, new_labels)
                        else:
                            # axs[i, j].legend([],[], title=PRESENTATION_LABEL_MAP[dataset], framealpha=0.9, loc='upper right')
                            axs[i, j].legend_.remove()
                    axs[i, j].set_title(PRESENTATION_LABEL_MAP[dataset])
                except StopIteration:
                    break
        fig.tight_layout()
        return fig, axs

    def _get_scores(self, name: str, n_bootstrap: int = 1000) -> Dict[str, Dict[str, List[float]]]:
        """
        Compute the scores for the model using bootstrapping and store the results.

        :param name: The name of the dataset for which to calculate the scores.
        :param n_bootstrap: The number of bootstrap samples to generate. Default is 1000.
        :return: A dictionary containing the computed scores for each metric.
        """
        scores = {metric: [] for metric in self.metrics}

        y_true = np.array(self.datasets[name]['gt'].astype(int))
        y_pred = np.array(self.datasets[name]['pred']).astype(int)

        if len(np.unique(y_true)) == 1:
            print(f"WARNING: Only one class present in {name}. Can only calculate accuracy.")

        for i in range(n_bootstrap):
            y_true_sample, y_pred_sample = resample(y_true, y_pred)
            if len(y_true_sample) == 0 or len(y_pred_sample) == 0:
                print(f"WARNING: Empty sample generated for {name} on bootstrap sample {i}. Skipping.")
            else:
                metrics_sample = compute_metrics(y_true_sample, y_pred_sample)

                for metric, value in metrics_sample.items():
                    scores[metric].append(value)

        return scores

    def _get_results(self, name: str) -> Dict[str, float]:
        """
        Calculate the mean of the bootstrap scores for each metric.

        :param name: The name of the dataset for which to calculate the results.
        :return: A dictionary containing the mean of the bootstrap scores for each metric.
        """
        return {metric: np.nanmean(self.bootstrap_scores[name][metric]) for metric in self.metrics}

    def _get_ci_bounds(self, name: str, metric: str) -> Tuple[float, float]:
        """
        Get the confidence interval bounds for a specific metric.

        :param name: The name of the dataset for which to calculate the confidence interval bounds.
        :param metric: The metric for which to calculate the confidence interval bounds.
        :return: A tuple containing the lower and upper bounds of the confidence interval.
        """
        lower_bound = np.nanpercentile(self.bootstrap_scores[name][metric], 2.5)
        upper_bound = np.nanpercentile(self.bootstrap_scores[name][metric], 97.5)
        return lower_bound, upper_bound

    def _get_cis(self, name: str) -> Dict[str, Dict[str, Tuple[float, float]]]:
        """
        Get the confidence intervals for each metric.

        :param name: The name of the dataset for which to calculate the confidence intervals.
        :return: A dictionary containing the confidence intervals.
        """
        return {metric: self._get_ci_bounds(name, metric) for metric in self.metrics}

    def results_by_missing_lung(self) -> Dict[str, pd.DataFrame]:
        """
        Get the predictions for each dataset by the ground truth categories, i.e. the amount of lung in the radiograph.

        :return: A dictionary of dataframes for each dataset containing the predictions for each ground truth category.
        """
        df_dict = {}
        for dataset, df in self.datasets.items():
            df_dict[dataset] = aspect_ratio.results_by_missing_lung(df)
        return df_dict

    @property
    def results_df(self) -> pd.DataFrame:
        """
        Get a DataFrame containing the evaluation results along with their confidence intervals.  Note that the entries
        are strings to improve formatting, so you may wish to use the raw values in the self.results and self.cis
        attributes for any plots or additional calculations.

        :return: A pandas DataFrame containing the evaluation results and confidence intervals.
        """
        combined_res = make_results_df(self.results, self.cis, precision=3)
        if "ROC AUC" in combined_res.index:
            return combined_res.drop(["ROC AUC", "AUPRC"], axis=0)
        return combined_res.drop(["roc_auc", "auprc"], axis=0)


# FUNCTIONS ------------------------------------------------------------------------------------------------------------

def set_colors(config: Namespace) -> Namespace:
    """
    Set the colors for the plots based on the colormap specified in the config file.

    :param config: The config dictionary.
    """
    plt.set_cmap(config.figure.colormap)
    plt.rcParams['image.cmap'] = config.figure.colormap
    cmap = plt.get_cmap(config.figure.colormap)
    config.figure['edgecolor'] = (0, 0, 0, config.figure.edge_alpha)

    num_colors = cmap.N  # Total number of colors in the colormap
    config.figure['colors'] = cmap(np.arange(num_colors))
    config.figure['colors'][1] = np.array(
        [212 / 255, 175 / 255, 55 / 255, 1])  # Change the second color to darker yellow

    config.figure['color_dict'] = {
        'train': config.figure.colors[9],  # purple
        'internal_train': config.figure.colors[9],
        'dev': config.figure.colors[9],  # purple
        'internal_dev': config.figure.colors[9],
        'val': config.figure.colors[6],  # green
        'internal_val': config.figure.colors[6],
        'internal': config.figure.colors[1],  # dark yellow
        'internal_test': config.figure.colors[1],
        'all_external': config.figure.colors[4],  # blue
        'external': config.figure.colors[4],
        'cuh': config.figure.colors[3],  # red
        'brixia': config.figure.colors[8],  # grey
        'ricord': config.figure.colors[5],  # orange
        'mimic': config.figure.colors[7],  # pink
        '0': config.figure.colors[10],  # brown
        0: config.figure.colors[10],
        '1': config.figure.colors[2],  # light blue
        1: config.figure.colors[2],
        'not_chest': config.figure.colors[0],  # black
    }

    # Make confusion matrix colors correspond to main colours
    config.figure['confusion_matrix_colors'] = {
        'train': 'Purples',  # yellow / brown
        'Training set': 'Purples',
        'internal_train': 'Purples',
        'dev': 'Purples',  # yellow / brown
        'Development set': 'Purples',
        'Development': 'Purples',
        'internal_set': 'Purples',
        'val': 'Greens',  # green
        'Validation set': 'Greens',
        'internal_val': 'Greens',
        'internal': 'YlOrBr',  # purples
        'internal_test': 'YlOrBr',
        'Internal Test': 'YlOrBr',
        'Test set': 'YlOrBr',
        'all_external': 'Greys',  # greys
        'All External': 'Greys',
        'external': 'Greys',
        'cuh': 'Reds',  # red
        'CUH': 'Reds',
        'brixia': 'Blues',  # blue
        'BrixIA': 'Blues',
        'ricord': 'Oranges',  # oranges
        'RICORD-1c': 'Oranges',
        'mimic': 'RdPu',  # pink
        'MIMIC': 'RdPu',
    }

    return config


def make_confusion_matrix(
        title: str,
        conf_matrix: np.ndarray,
        figsize: Tuple[int] = (6, 6),
        cmap: str = 'Set1',
        existing_axis=None,
        font_size: int = 32,
) -> Union[Tuple[Figure, Axes], Axes]:
    """
    Function to plot the confusion matrix.

    :param title: The title for the confusion matrix plot.
    :param conf_matrix: The confusion matrix as a numpy array.
    :param figsize: The size of the plot. Defaults to config.figure.cm_figsize.
    :param cmap: The colormap to use for the confusion matrix.  Defaults to Set1.
    :param existing_axis: An existing axis to plot the confusion matrix on.  If None, a new figure and axis will be
        created.
    :param font_size: The font size to use for the plot.  Defaults to 32.
    :return: A tuple containing the figure and axes of the confusion matrix plot, or the axes if an existing axis was
        provided.
    """
    old_font_size = plt.rcParams['font.size']
    plt.rcParams['font.size'] = font_size

    if existing_axis is None:
        fig, ax = plot_confusion_matrix(
            conf_mat=conf_matrix, figsize=figsize, cmap=cmap
        )
        ax.set_title(title)
        plt.rcParams['font.size'] = old_font_size
        return fig, ax
    else:
        plot_confusion_matrix(
            conf_mat=conf_matrix, figsize=figsize, cmap=cmap, axis=existing_axis
        )
        existing_axis.set_title(title)
        plt.rcParams['font.size'] = old_font_size
        return existing_axis


def calculate_conf_matrix_metrics(conf_matrix: np.ndarray) -> Tuple[float, float, float, float]:
    """
    Calculate the sensitivity, specificity, positive predictive value, and negative predictive value from the
    confusion matrix.

    :param conf_matrix: The confusion matrix as a numpy array.
    :return: A tuple containing the sensitivity, specificity, positive predictive value, and negative predictive
        value.
    """
    if len(conf_matrix.ravel()) == 1:
        return np.nan, np.nan, np.nan, np.nan
    else:
        tn, fp, fn, tp = conf_matrix.ravel()
        sens = tp / (tp + fn) if (tp + fn) != 0 else 0
        spec = tn / (tn + fp) if (tn + fp) != 0 else 0
        ppv = tp / (tp + fp) if (tp + fp) != 0 else 0
        npv = tn / (tn + fn) if (tn + fn) != 0 else 0
        return sens, spec, ppv, npv


def compute_metrics(
        y_true_sample: np.ndarray,
        y_pred_sample: np.ndarray,
        y_pred_prob_sample: np.ndarray = None
) -> Dict[str, float]:
    """
    Compute the metrics and return them as a dictionary.

    :param y_true_sample: The true labels for a bootstrap sample.
    :param y_pred_sample: The predicted labels for a bootstrap sample.
    :param y_pred_prob_sample: The predicted probabilities for a bootstrap sample. Defaults to None, in which case
        the metrics that require predicted probabilities will be set to np.nan (i.e. AUROC and AUPRC).
    :return: A dictionary containing computed metrics.
    """
    if len(y_true_sample) > 0 and len(y_true_sample) == len(y_pred_sample):
        confusion_matrix_sample = confusion_matrix(y_true_sample, y_pred_sample, labels=[0, 1])
        sensitivity_sample, specificity_sample, ppv_sample, npv_sample = calculate_conf_matrix_metrics(
            confusion_matrix_sample
        )
        if len(np.unique(y_true_sample)) == 1:
            return {
                'accuracy': np.mean(y_true_sample == y_pred_sample),
                'sensitivity': np.nan,
                'specificity': specificity_sample,
                'ppv': np.nan,
                'npv': npv_sample,
                'roc_auc': np.nan,
                'auprc': np.nan,
                'precision': np.nan,
                'recall': np.nan,
                'f1': np.nan
            }
        else:
            if y_pred_prob_sample is None or len(y_pred_prob_sample) == 0:
                return {
                    "accuracy": np.mean(y_true_sample == y_pred_sample),
                    "roc_auc": np.nan,
                    "precision": precision_score(y_true_sample, y_pred_sample, zero_division=0),
                    "recall": recall_score(y_true_sample, y_pred_sample),
                    "sensitivity": sensitivity_sample,
                    "specificity": specificity_sample,
                    "ppv": ppv_sample,
                    "npv": npv_sample,
                    "f1": f1_score(y_true_sample, y_pred_sample),
                    "auprc": np.nan,
                }
            else:
                return {
                    "accuracy": np.mean(y_true_sample == y_pred_sample),
                    "roc_auc": roc_auc_score(y_true_sample, y_pred_prob_sample),
                    "precision": precision_score(y_true_sample, y_pred_sample, zero_division=0),
                    "recall": recall_score(y_true_sample, y_pred_sample),
                    "sensitivity": sensitivity_sample,
                    "specificity": specificity_sample,
                    "ppv": ppv_sample,
                    "npv": npv_sample,
                    "f1": f1_score(y_true_sample, y_pred_sample),
                    "auprc": average_precision_score(y_true_sample, y_pred_prob_sample),
                }
    elif len(y_true_sample) > 0:
        print("Error: y_true_sample and y_pred_sample must be the same length.")
    else:
        print("Error: y_true_sample must have at least one element.")


def exclude_unsuitable_cases(
        label: str,
        datasets: Dict[str, pd.DataFrame],
        aspect_ratio_bounds: Tuple[float, float] = (0.85, 1.48),
) -> Dict[str, pd.DataFrame]:
    """
    Exclude unsuitable cases from each dataset based on the label stage and unsuitable features. These features
    were excluded in training to reduce noise, but you may wish to include them in the predictions to see how they
    are handled by the model as you may not be able to exclude them in practice.

    This method iterates through all datasets and removes cases with unsuitable features according to the
    label stage. The updated datasets are then assigned back to the datasets dictionary.

    :param label: The QC label, e.g. 'inverted' or 'aspect_ratio'.
    :param datasets: A dictionary containing the dataframes to be filtered.
    :param aspect_ratio_bounds: The lower and upper bounds for the aspect ratio.
    :return: A dictionary containing the filtered dataframes.
    """
    for name, df in datasets.items():
        unsuitable = UNSUITABLE_FEATURES_DICT[STAGE_DICT[label]].copy()
        if label == 'aspect_ratio':
            unsuitable = [feature for feature in unsuitable if feature != 'nchest']
        for feature in unsuitable:
            if feature in df.columns and feature != 'aspect_ratio':
                df = df.loc[(df[feature] == '0') | (df[feature] == 0)]
            elif feature == 'aspect_ratio' and 'aspect_ratio' in df.columns:
                df = df.loc[
                    (df['aspect_ratio'].astype(float) >= aspect_ratio_bounds[0]) &
                    (df['aspect_ratio'].astype(float) <= aspect_ratio_bounds[1])
                    ]
        datasets[name] = df.reset_index(drop=True)
    return datasets


def format_list_to_string(lst: List[float], dcml_places: int = 3) -> str:
    """
    Format a list of two floats into a string representation.

    :param lst: List of floats to be formatted.
    :param dcml_places: The number of decimal places to round to. Defaults to 3.
    :return: A string representation of the list.
    """
    return f"({lst[0]:.{dcml_places}f}, {lst[1]:.{dcml_places}f})"


def make_results_df(results: Dict, cis: Dict, precision=3, ci_precision=None) -> pd.DataFrame:
    """
    Get a DataFrame containing the evaluation results along with their confidence intervals.  Note that the entries
    are strings to improve formatting, so you may wish to use the raw values in the self.results and self.cis
    attributes for any plots or additional calculations.

    :param results: A dictionary containing the evaluation results.
    :param cis: A dictionary containing the confidence intervals.
    :param precision: The number of decimal places to round to. Defaults to 3.
    :param ci_precision: The number of decimal places to round the confidence intervals to. Defaults to the same
        value as precision.
    :return: A pandas DataFrame containing the evaluation results and confidence intervals.
    """
    if ci_precision is None:
        ci_precision = precision

    results_df = pd.DataFrame(results)
    cis = pd.DataFrame(cis)
    combined_cols_df = results_df.copy()

    for col in combined_cols_df.columns:
        results_df[col] = results_df[col].apply(lambda x: f"{x:.{precision}f}")
        cis[col] = cis[col].apply(format_list_to_string, args=(ci_precision,))
        combined_cols_df[col] = results_df[col] + " " + cis[col]

    combined_cols_df = combined_cols_df.apply(lambda x: x.replace('nan', ''))
    combined_cols_df = combined_cols_df.apply(lambda x: x.replace('[]', ''))

    label_map = {col: PRESENTATION_LABEL_MAP[col] for col in combined_cols_df.columns}
    combined_cols_df = combined_cols_df.rename(label_map, axis=1)

    combined_cols_df = combined_cols_df.replace("0.000 (0.000, 0.000)", "- (-, -)")
    combined_cols_df = combined_cols_df.replace("0.00 (0.00, 0.00)", "- (-, -)")
    combined_cols_df = combined_cols_df.replace("0.000 (0.00, 0.00)", "- (-, -)")
    combined_cols_df = combined_cols_df.replace("nan (nan, nan)", "- (-, -)")

    index_map = {col: PRESENTATION_LABEL_MAP[col] for col in combined_cols_df.index}
    combined_cols_df = combined_cols_df.rename(index=index_map)
    return combined_cols_df


def encode_labels(df: pd.DataFrame, col: str, label_map: dict) -> pd.DataFrame:
    """
    Encode the labels for each dataset.

    :param df: The dataframe to be encoded.
    :param col: The column containing the labels to be encoded.
    :param label_map: A dictionary containing the label mapping.
    :return: A dictionary containing the encoded dataframes.
    """
    df[col] = df[col].astype(str).str.lower().map(label_map)
    return df.dropna(subset=col).reset_index(drop=True)


def demographic_evaluation(config: Namespace):
    def get_legend_handles_labels(config: Namespace) -> List[str]:
        if config.task in ['inverted', 'lateral']:
            return [f'Not {PRESENTATION_LABEL_MAP[config.task]}', f'{PRESENTATION_LABEL_MAP[config.task]}']
        elif config.task in ['rotation', 'pacemaker']:
            return [f'No {PRESENTATION_LABEL_MAP[config.task]}', f'{PRESENTATION_LABEL_MAP[config.task]}']
        elif config.task in ['projection', 'ocr']:
            return [f'PA', f'AP']
        elif config.task == 'aspect_ratio':
            return [f'>66% lung', f'<66% lung']
        else:
            return [f'0', f'1']

    def demogs_by_dataset(config: Namespace, d: pd.DataFrame, true_col: str):
        df_grouped = d.groupby(['dataset', true_col]).size().unstack()
        fig, ax = plt.subplots(figsize=(6, 4))
        edgecolor = (0, 0, 0, 1)
        df_grouped.plot(kind='bar', ax=ax, width=0.8, alpha=0.7, edgecolor=edgecolor, color=config.figure.color_dict)
        if config.task in ['rotation', 'pacemaker']:
            ax.set_yscale('log')
            ax.set_ylabel("log(Number of Images)")
        else:
            ax.set_ylabel("Number of Images")
        # Set the labels
        ax.set_xlabel("Dataset")
        # ax.set_title("Number of Inverted and Non-inverted Images per Dataset")
        legend_labels = get_legend_handles_labels(config)
        ax.legend(legend_labels[0])
        current_xticklabels = [label.get_text() for label in ax.get_xticklabels()]
        new_labels = [PRESENTATION_LABEL_MAP[label] for label in current_xticklabels]
        ax.set_xticklabels(new_labels, rotation=0)
        ax.legend_.remove()
        fig.tight_layout()
        fig.savefig(os.path.join(config.dirs.output, 'demogs', 'by_dataset.png'))

    def demogs_by_covid19(config: Namespace, d: pd.DataFrame, true_col: str):
        df_grouped = d.groupby(['final_covid_cohort', true_col]).size().unstack()
        df_grouped = df_grouped.div(df_grouped.sum(axis=1), axis=0)
        fig, ax = plt.subplots(figsize=(6, 4))
        edgecolor = (0, 0, 0, 1)
        df_grouped.plot(kind='bar', ax=ax, width=0.8, alpha=0.7, edgecolor=edgecolor, color=config.figure.color_dict)
        if config.task in ['rotation', 'pacemaker']:
            ax.set_yscale('log')
            ax.set_ylabel("log(Number of Images)")
        else:
            ax.set_ylabel("Number of Images")
        # Set the labels
        ax.set_xlabel("COVID-19 Status")
        # ax.set_title("Number of Inverted and Non-inverted Images per Dataset")
        legend_labels = get_legend_handles_labels(config)
        ax.legend(legend_labels[0])
        new_labels = ["Negative", "Positive"]
        ax.set_xticklabels(new_labels, rotation=0)
        ax.legend_.remove()
        fig.tight_layout()
        fig.savefig(os.path.join(config.dirs.output, 'demogs', 'by_covid_status.png'))

    def demogs_by_manufacturer(config: Namespace, d: pd.DataFrame, true_col: str):
        d.loc[d.Manufacturer == '"GE Healthcare"', "Manufacturer"] = 'GE Healthcare'
        d.loc[d.Manufacturer == 'CARESTREAM', "Manufacturer"] = 'Carestream'
        d.loc[d.Manufacturer == 'CARESTREAM HEALTH', "Manufacturer"] = 'Carestream'
        d.loc[d.Manufacturer == 'Carestream Health', "Manufacturer"] = 'Carestream'
        d.loc[d.Manufacturer == 'Philips Medical Systems', "Manufacturer"] = 'Philips'
        d.loc[d.Manufacturer == 'E-COM Technology Limited.', "Manufacturer"] = 'E-COM Technology'
        d.loc[d.Manufacturer == 'Agfa-Gevaert', "Manufacturer"] = 'Agfa'
        d.Manufacturer = d.Manufacturer.str.replace(" DR", "")

        df_grouped = d.groupby(['Manufacturer', true_col]).size().unstack()
        fig, ax = plt.subplots(figsize=(12, 6))
        edgecolor = (0, 0, 0, 1)
        df_grouped.plot(kind='bar', ax=ax, width=0.8, alpha=0.7, edgecolor=edgecolor, color=config.figure.color_dict)
        if config.task in ['rotation', 'pacemaker']:
            ax.set_yscale('log')
            ax.set_ylabel("log(Number of Images)")
        else:
            ax.set_ylabel("Number of Images")
        # Set the labels
        ax.set_xlabel("Manufacturer")
        # ax.set_title("Number of Inverted and Non-inverted Images per Dataset")
        legend_labels = get_legend_handles_labels(config)
        ax.legend(legend_labels)
        labels = [item.get_text() for item in ax.get_xticklabels()]
        ax.set_xticklabels(labels, rotation=45, ha='right')
        fig.tight_layout()
        fig.savefig(os.path.join(config.dirs.output, 'demogs', 'by_manufacturer.png'))

    def combine_figs(config: Namespace):
        images = [Image.open(x) for x in [
            os.path.join(config.dirs.output, 'demogs', 'by_dataset.png'),
            os.path.join(config.dirs.output, 'demogs', 'by_covid_status.png'),
            os.path.join(config.dirs.output, 'demogs', 'by_manufacturer.png'),
        ]]
        small_image_width, small_image_height = images[0].size
        large_image_width, large_image_height = images[2].size
        combined = Image.new('RGBA', (
            large_image_width,
            small_image_height + large_image_height
        ))
        combined.paste(images[0], (0, 0))
        combined.paste(images[1], (small_image_width, 0))
        combined.paste(images[2], (0, small_image_height))
        # Save the combined image
        combined.save(os.path.join(config.dirs.output, 'demogs', 'combined_eda_histograms.png'))

    def get_p_values(config: Namespace, d: pd.DataFrame, true_col: str):
        p_vals = [
            f"P-values for chi-squared tests between {PRESENTATION_LABEL_MAP[config.task]} and demographic:\n",
        ]
        for col in ['final_covid_cohort', 'Manufacturer', 'dataset']:
            df_for_corr = d[d.inv.notna() & d.final_covid_cohort.notna()]

            contingency_table = pd.crosstab(df_for_corr[true_col].astype(str),
                                            df_for_corr[col].astype(str))
            chi2, p, dof, expected = chi2_contingency(contingency_table)
            p_vals.append(f"{col}: {p}")
        return p_vals

    df_int = pd.read_csv(
        os.path.join(config.dirs.dev_labels, f"{STAGE_DICT[config.task]}_paths+labels.csv"), dtype=str
    )
    int_metadata = pd.concat([pd.read_csv("/storage/covid/datasets/nccid/nccid_qc_meta_29112022.csv", dtype=str),
                              pd.read_csv("/storage/covid/datasets/bimcv/bimcv_qc_meta_29112022.csv", dtype=str)])
    df_int = df_int[['name', 'dataset', 'partition']].merge(int_metadata, on='name', how='left')

    df_ext = pd.read_csv(config.dirs.test_labels, dtype=str)
    if 'final_covid_status' in df_ext.columns:
        df_ext = df_ext.rename(columns={'final_covid_status': 'final_covid_cohort'})
    covid_status_map = {
        'Positive': 'Positive', 'Negative': 'Negative', 'positive': 'Positive', 'negative': 'Negative', 0: 'Negative',
        1: 'Positive', '0': 'Negative', '1': 'Positive'
    }
    df_ext.final_covid_cohort = df_ext.final_covid_cohort.map(covid_status_map)
    df_int.final_covid_cohort = df_int.final_covid_cohort.map(covid_status_map)

    true_col = LABEL_TO_COLNAME_MAP[config.task]
    df = pd.concat([df_int, df_ext], ignore_index=True).dropna(subset=true_col)
    if config.task == 'aspect_ratio':
        df = aspect_ratio.allocate_gt(df)
        true_col = 'gt'
    elif config.task in ['projection', 'rotation', 'ocr']:
        df[true_col] = df[true_col].map(LABEL_MAPS[config.task])

    df.loc[(df.dataset == 'ricord1c') | (df.dataset == 'brixia'), "final_covid_cohort"] = 'Positive'

    os.makedirs(os.path.join(config.dirs.output, 'demogs'), exist_ok=True)

    demogs_by_dataset(config, df, true_col)
    demogs_by_covid19(config, df, true_col)
    demogs_by_manufacturer(config, df, true_col)
    combine_figs(config)

    p_vals = get_p_values(config, df, true_col)
    with open(os.path.join(config.dirs.output, 'demogs', 'p_values.txt'), 'w') as f:
        f.writelines([line + '\n' for line in p_vals])

    if config.task in ['inverted', 'lateral', 'projection']:
        check_for_metadata_errors(config, df)


def check_for_metadata_errors(config: Namespace, df: pd.DataFrame):
    """
    Check for errors in metadata of the hold-out set.

    :param config: The evaluation config
    :param df: The dataframe of the hold-out set
    """
    # proj and lateral will have same methodology, inversion is easier
    if config.task == 'inverted':
        gt_col = 'inv'
        header_cols = ["PhotometricInterpretation"]
        header_map = {"monochrome2": 0, "monochrome1": 1}
    elif config.task == 'projection':
        gt_col = 'proj'
        header_cols = ["dcm_projection", "SeriesDescription"]
        header_map = {'ap': 1, 'pa': 0}
    elif config.task == 'lateral':
        gt_col = 'lat'
        header_cols = ["dcm_projection", "SeriesDescription"]
        header_map = {'ap': 0, 'pa': 0, 'lateral': 1, "lat": 1}
    else:
        print(f"ERROR - Unable to check for metadata accuracy for {config.task}")
        return None

    output_text = ["DICOM Header Accuracy\n"]
    output_text.append(f"Please note that the metadata may not be missing, just uninterpretable.")
    output_text.append(f"For example, it might say AP and Lateral in series description, in which case we cannot tell "
                       f"which projection the image is.\n")

    for header_col in header_cols:
        _df = df.copy()
        _df = _df[_df[gt_col].notna()]
        _df[gt_col] = _df[gt_col].astype(int)

        output_text.append("*"*100)
        output_text.append(f"Checking {header_col} for {config.task}\n")

        if header_col == 'SeriesDescription' and config.task == 'lateral':
            _df.loc[_df[header_col].isin(LAT_TERMS), header_col] = 1
            _df.loc[_df[header_col].isin(AP_TERMS), header_col] = 0
            _df.loc[_df[header_col].isin(PA_TERMS), header_col] = 0
            # _df.loc[_df[header_col].isna(), header_col] = np.nan
            # _df.loc[_df[header_col] == "", header_col] = np.nan
            _df.loc[_df[header_col].notna() & (_df[header_col] != 1) & (_df[header_col] != 0), header_col] = np.nan
        elif header_col == 'SeriesDescription' and config.task == 'projection':
            _df.loc[_df[header_col].isin(AP_TERMS), header_col] = 1
            _df.loc[_df[header_col].isin(PA_TERMS), header_col] = 0
            # _df.loc[_df[header_col].isna(), header_col] = np.nan
            # _df.loc[_df[header_col] == "", header_col] = np.nan
            _df.loc[_df[header_col].notna() & (_df[header_col] != 1) & (_df[header_col] != 0), header_col] = np.nan
        else:
            _df[header_col] = _df[header_col].str.lower().map(header_map)

        _df.loc[_df[header_col].notna() & (_df[header_col] == _df[gt_col]), 'incorrect_header'] = 0
        _df.loc[_df[header_col].notna() & (_df[header_col] != _df[gt_col]), "incorrect_header"] = 1

        header_completed = _df[header_col].notna().sum()
        output_text.append(f"Metadata completed: {int(header_completed)} ({100*header_completed / len(_df):.3f}%)")
        header_missing = _df[header_col].isna().sum()
        output_text.append(f"Metadata missing: {int(header_missing)} ({100*header_missing / len(_df):.3f}%)\n")

        header_correct = _df.loc[_df.incorrect_header == 0, 'incorrect_header'].count()
        output_text.append(f"Metadata correct: {int(header_correct)} ({100*header_correct / header_completed:.3f}%)")
        header_incorrect = _df.incorrect_header.sum()
        output_text.append(f"Metadata incorrect: {int(header_incorrect)} ({100*header_incorrect / header_completed:.3f}%)")

        if config.task == 'projection':
            confict_with_annotations = len(_df[(_df.incorrect_header == 1) & (_df.proj_annot.astype(str) == "1")])
            output_text.append(f"Metadata in conflict with text annotation: "
                               f"{int(confict_with_annotations)} ({100*confict_with_annotations / header_completed:.3f}%)")

        output_text.append(f"\nMetadata missing by dataset:")
        missing_by_dataset = _df.loc[_df[header_col].isna(), 'dataset'].value_counts()
        percentages_missing_by_dataset = _df.loc[
                                             _df[header_col].isna(), 'dataset'
                                         ].value_counts(normalize=True).mul(100).round(2)
        for key in missing_by_dataset.keys():
            output_text.append(f"{key}: {missing_by_dataset[key]} ({percentages_missing_by_dataset[key]}%)")

        output_text.append(f"\nMetadata incorrect by dataset:")
        incorrect_by_dataset = _df.loc[_df.incorrect_header == 1, 'dataset'].value_counts()
        percentages_incorrect_by_dataset = _df.loc[_df.incorrect_header == 1, 'dataset'
                                           ].value_counts(normalize=True).mul(100).round(2)
        for key in incorrect_by_dataset.keys():
            output_text.append(f"{key}: {incorrect_by_dataset[key]} ({percentages_incorrect_by_dataset[key]}%)")

        output_text.append(f"\nMetadata missing by manufacturer:")
        missing_by_manufacturer = _df.loc[_df[header_col].isna(), 'Manufacturer'].value_counts()
        percentages_missing_by_manufacturer = _df.loc[
                                                  _df[header_col].isna(), 'Manufacturer'
                                              ].value_counts(normalize=True).mul(100).round(2)
        for key in missing_by_manufacturer.keys():
            output_text.append(f"{key}: {missing_by_manufacturer[key]} ({percentages_missing_by_manufacturer[key]}%)")

        output_text.append(f"\nMetadata incorrect by manufacturer:")
        incorrect_by_manufacturer = _df.loc[_df.incorrect_header == 1, 'Manufacturer'].value_counts()
        percentages_incorrect_by_manufacturer = _df.loc[
                                                    _df.incorrect_header == 1, 'Manufacturer'
                                                ].value_counts(normalize=True).mul(100).round(2)
        for key in incorrect_by_manufacturer.keys():
            output_text.append(f"{key}: {incorrect_by_manufacturer[key]} ({percentages_incorrect_by_manufacturer[key]}%)")

        output_text.append(f"\nMetadata missing by covid cohort:")
        missing_by_covid = _df.loc[_df[header_col].isna(), 'final_covid_cohort'].value_counts()
        percentages_missing_by_covid = _df.loc[
                                           _df[header_col].isna(), 'final_covid_cohort'
                                       ].value_counts(normalize=True).mul(100).round(2)
        for key in missing_by_covid.keys():
            output_text.append(f"{key}: {missing_by_covid[key]} ({percentages_missing_by_covid[key]}%)")

        output_text.append(f"\nMetadata incorrect by covid cohort:")
        incorrect_by_covid = _df.loc[_df.incorrect_header == 1, 'final_covid_cohort'].value_counts()
        percentages_incorrect_by_covid = _df.loc[
                                             _df.incorrect_header == 1, 'final_covid_cohort'
                                         ].value_counts(normalize=True).mul(100).round(2)
        for key in incorrect_by_covid.keys():
            output_text.append(f"{key}: {incorrect_by_covid[key]} ({percentages_incorrect_by_covid[key]}%)")

    with open(os.path.join(config.dirs.output, 'demogs', 'metadata_accuracy.txt'), 'w') as f:
        f.writelines([line + '\n' for line in output_text])

    return None


def get_label_counts(dfs: Dict[str, pd.DataFrame], label: str, true_col: str) -> pd.DataFrame:
    """
    Get counts of labels in each dataset.

    :param dfs: The dataframes to count labels in.
    :param label: The label to count.
    :param true_col: The column containing the true labels.
    :return: The counts of labels in each dataset.
    """
    counts_dict = {name: df[true_col].value_counts() for name, df in dfs.items()}
    counts_df = pd.DataFrame(counts_dict).transpose().fillna(0).astype(int)
    counts_df['Total'] = counts_df.sum(axis=1)
    counts_df = counts_df.rename(index=PRESENTATION_LABEL_MAP)
    if label == 'projection' or label == 'ocr':
        return counts_df.rename(columns={0: 'PA', 1: 'AP'})
    elif label == 'aspect_ratio':
        return counts_df.rename(columns={0: '>66% lung', 1: '<66% lung'})
    elif label in ['inverted', 'lateral']:
        return counts_df.rename(columns={0: f'Not {label.capitalize()}', 1: f'{label.capitalize()}'})
    else:
        return counts_df.rename(columns={0: f'No {label.capitalize()}', 1: f'{label.capitalize()}'})


def evaluate_dl_model(config: Namespace):
    """
    Evaluate an AutoQC deep learning model and save outputs.

    :param config: The config object for the model to be evaluated.
    """

    def _training_evaluation(_config: Namespace):
        training_eval = QCTrainingEvaluator(_config)
        train_fig, _ = training_eval.plot_training_curves()
        save_and_close_fig(train_fig, os.path.join(_config.dirs.output, f'training_curves_{_config.task}.png'))
        if _config.task != 'rotation':
            cms, training_results = training_eval.internal_metrics()
            training_results.to_csv(os.path.join(_config.dirs.output, f'training_results_{_config.task}.csv'))
            os.makedirs(os.path.join(_config.dirs.output, 'confusion_matrices'), exist_ok=True)
            for dataset, cm in cms.items():
                os.makedirs(os.path.join(_config.dirs.output, 'training_confusion_matrices'), exist_ok=True)
                cm[0].tight_layout()
                save_and_close_fig(cm[0], os.path.join(
                    _config.dirs.output, 'training_confusion_matrices', f'training_cm_{dataset}_{_config.task}.png'
                ))

    def _validation_evaluation(_config: Namespace, _model: QCModel, calibrated: bool = True, thres: float = 0.5) -> float:
        if calibrated:
            _file_name_stem = 'calib'
        else:
            _file_name_stem = 'notcalib'

        # Uncalibrated validation results and calibration curve
        val = QCInferenceEvaluator(_config, 'val', _model, calibrated=calibrated, chosen_threshold=thres)
        val_results = val.results_df
        val_results.to_csv(os.path.join(_config.dirs.output, f'val_{_file_name_stem}_results_{_config.task}.csv'))

        calib_curve, _, brier_loss = _model.plot_calibration_curve('val', calibrated=calibrated, nbins=5)
        save_and_close_fig(calib_curve, os.path.join(
            _config.dirs.output, f'{_file_name_stem}_calib_curve_{_config.task}.png'
        ))

        val_roc_curve, _ = _model.roc_curve(calibrated=_config.calibrated)
        save_and_close_fig(val_roc_curve, os.path.join(_config.dirs.output, f'val_roc_curve_{_config.task}.png'))
        val_pr_curve, _ = _model.pr_curve(calibrated=_config.calibrated)
        save_and_close_fig(val_pr_curve, os.path.join(_config.dirs.output, f'val_pr_curve_{_config.task}.png'))

        return brier_loss

    def _perform_calibration(_model: QCModel):
        _model.calibrate_model()
        _model.save_calibration()
        _model.calibrate_pred('val', nbins=5)
        # calib_calib_curve, _, _calib_brier_loss = _model.calibrate_pred('val', nbins=5)
        # calib_calib_curve.savefig(os.path.join(_out_dir, f'calib_calib_curve_{label}.png'))
        # return _calib_brier_loss

    def _select_threshold(_config: Namespace, _model: QCModel) -> float:
        _selected_threshold, threshold_f1s = _model.find_best_threshold(calibrated=True)
        threshold_f1s.to_csv(os.path.join(_config.dirs.output, f'f1s_by_threshold_{_config.task}.csv'))
        return _selected_threshold

    def _save_brier_loss_and_threshold(
            _config: Namespace,
            _notcalib_brier_loss: float,
            _calib_brier_loss: float,
            _selected_threshold: float
    ):
        with open(os.path.join(_config.dirs.output, f'calibration_threshold_results_{_config.task}.txt'), 'w') as f:
            f.write(f'{_config.task} model calibration results\n')
            f.write(f'Not calibrated Brier loss: {_notcalib_brier_loss:.3f}\n')
            f.write(f'Calibrated Brier loss: {_calib_brier_loss:.3f}\n')
            f.write(f'Selected threshold: {_selected_threshold:.3f}\n')

    def _holdout_evaluation(
            _config: Namespace, _model: Union[QCModel, OCRModel], _selected_threshold: Optional[float] = 0.5
    ):
        # Testing on Holdout Sets
        holdout = QCInferenceEvaluator(
            _config,
            'all_holdout',
            _model,
            calibrated=_config.calibrated,
            chosen_threshold=_selected_threshold,
        )
        os.makedirs(_config.dirs.output, exist_ok=True)
        # Label counts
        holdout_label_df = holdout.label_df
        holdout_label_df.to_csv(os.path.join(_config.dirs.output, f'holdout_label_count_{_config.task}.csv'))
        # Results
        holdout_results = holdout.results_df
        holdout_results.to_csv(os.path.join(_config.dirs.output, f'holdout_results_{_config.task}.csv'))
        # ROC and PR curves
        if _config.task not in ['rotation', 'ocr']:
            roc_curve, _ = holdout.roc_curve()
            save_and_close_fig(roc_curve, os.path.join(_config.dirs.output, f'roc_curve_{_config.task}.png'))
            pr_curve, _ = holdout.pr_curve()
            save_and_close_fig(pr_curve, os.path.join(_config.dirs.output, f'pr_curve_{_config.task}.png'))
        # Confusion matrices
        for name, cm in holdout.confusion_matrices().items():
            os.makedirs(os.path.join(_config.dirs.output, 'confusion_matrices'), exist_ok=True)
            cm[0].tight_layout()
            save_and_close_fig(
                cm[0], os.path.join(_config.dirs.output, 'confusion_matrices', f'holdout_cm_{name}_{_config.task}.png')
            )
        if 'all_external' in holdout.confusion_matrices():
            old_font_size = plt.rcParams['font.size']
            plt.rcParams['font.size'] = config.figure.cm_font_size
            cm = holdout.confusion_matrices()['all_external']
            cm[1].set_title(PRESENTATION_LABEL_MAP[_config.task])
            cm[0].tight_layout()
            save_and_close_fig(
                cm[0], os.path.join(_config.dirs.output, 'confusion_matrices', f'holdout_cm_main_{_config.task}.png')
            )
            plt.rcParams['font.size'] = old_font_size
        # Produce probability histograms
        if _config.task not in ['rotation', 'ocr']:
            probs_hist, _ = holdout.probs_hist()
            probs_hist.tight_layout()
            save_and_close_fig(probs_hist, os.path.join(_config.dirs.output, f'probs_hist_{_config.task}.png'))
            prob_info = holdout.prob_info()
            with open(os.path.join(_config.dirs.output, f'failure_probability_info.txt'), 'w') as f:
                f.write(prob_info)
            failure_hist, _ = holdout.failure_hist()
            failure_hist.tight_layout()
            save_and_close_fig(failure_hist, os.path.join(_config.dirs.output, f'failure_hist_{_config.task}.png'))
        # Produce failure grids
        os.makedirs(os.path.join(_config.dirs.output, 'failure_grids'), exist_ok=True)
        for name, grid in holdout.failure_grids().items():
            save_and_close_fig(grid[0],
                               os.path.join(_config.dirs.output, 'failure_grids',
                                            f'failure_grid_{name}_{_config.task}.png')
                               )
        # Produce GradCAM examples and GradCAM failure grids
        print("Generating GradCAMs...")
        if _config.task not in ['ocr']:
            os.makedirs(os.path.join(_config.dirs.output, 'gradcam'), exist_ok=True)
            for name, grid in holdout.gradcam_examples_grid().items():
                save_and_close_fig(grid[0],
                                   os.path.join(_config.dirs.output, 'gradcam',
                                                f'gradcam_expls_{name}_{_config.task}.png')
                                   )
            for name, grid in holdout.failure_grids(with_gradcam=True).items():
                save_and_close_fig(grid[0],
                                   os.path.join(_config.dirs.output, 'gradcam',
                                                f'gradcam_failures_{name}_{_config.task}.png')
                                   )

    def _print_status(status: str):
        print(f"{'*' * 80}\n{status}")

    _print_status(f'Evaluating {config.task} model...')
    os.makedirs(config.dirs.output, exist_ok=True)

    model = QCModel(config) if config.task != 'ocr' else OCRModel(config)

    if config.run_demogs:
        _print_status(f'Performing demographic evaluation...')
        demographic_evaluation(config)

    notcalib_brier_loss = None
    if config.run_training_evaluator:
        _print_status(f'Performing training and validation evaluation...')
        _training_evaluation(config)
        notcalib_brier_loss = _validation_evaluation(
            config, model, calibrated=False, thres=0.5
        ) if config.task != 'rotation' else None

    if config.calibrate:
        _print_status(f'Performing calibration and threshold selection...')
        _perform_calibration(model)
        selected_threshold = _select_threshold(config, model)
        calib_brier_loss = _validation_evaluation(config, model, calibrated=True, thres=selected_threshold)
    else:
        selected_threshold = None if config.task in ['rotation', 'ocr'] else 0.5
        calib_brier_loss = None

    if config.task not in ['rotation', 'ocr']:
        _save_brier_loss_and_threshold(config, notcalib_brier_loss, calib_brier_loss, selected_threshold)

    if config.run_holdout_evaluator:
        _print_status(f'Performing holdout evaluation...')
        _holdout_evaluation(config, model, selected_threshold)

    _print_status(f'{config.task} model evaluation complete! Outputs saved to {config.dirs.output}.')


def evaluate_aspect_ratio(config: Namespace):
    """
    Evaluates the aspect ratio tool on the holdout set.
    """

    print(f'Evaluating aspect ratio...')

    out_dir = os.path.join(config.dirs.output)
    os.makedirs(out_dir, exist_ok=True)
    os.makedirs(os.path.join(out_dir, 'confusion_matrices'), exist_ok=True)

    # Initiate the evaluator instance
    model = QCAspectRatioEvaluator(config)

    if config.run_demogs:
        print(f'Performing demographic evaluation...')
        demographic_evaluation(config)

    holdout_label_df = model.label_df
    holdout_label_df.to_csv(os.path.join(out_dir, f'holdout_label_count_aspect_ratio.csv'))

    # Get the numeric results
    results_df = model.results_df
    results_df.to_csv(os.path.join(out_dir, 'holdout_results_aspect_ratio.csv'))

    # Plot confusion matrices
    for dataset, cm in model.confusion_matrices().items():
        cm[0].tight_layout()
        save_and_close_fig(cm[0], os.path.join(out_dir, 'confusion_matrices', f'holdout_cm_{dataset}_aspect_ratio.png'))
    if "all_external" in model.confusion_matrices():
        old_font_size = plt.rcParams['font.size']
        plt.rcParams['font.size'] = config.figure.cm_font_size
        cm = model.confusion_matrices()["all_external"]
        cm[1].set_title("Aspect Ratio")
        cm[0].tight_layout()
        save_and_close_fig(cm[0], os.path.join(out_dir, 'confusion_matrices', f'holdout_cm_main_aspect_ratio.png'))
        plt.rcParams['font.size'] = old_font_size

    # Plot histograms
    ar_hists, _ = model.plot_histograms()
    ar_hists.tight_layout()
    save_and_close_fig(ar_hists, os.path.join(out_dir, 'histograms_aspect_ratio.png'))

    # Plot predictions by ground truth category
    for dataset, df in model.results_by_missing_lung().items():
        df.to_csv(os.path.join(out_dir, f'predictions_by_gt_category_{dataset}_aspect_ratio.csv'))

    print(f'Aspect ratio evaluation complete.')


def evaluate_using_dict(config_dict: dict) -> None:
    """
    Evaluate using configuration from a dictionary. This function is used to allow the evaluate function to be
    customised and called more easily from the within python or a notebook.

    :param config_dict: The configuration dictionary.
    :return: The result of the evaluate function.
    """
    # Convert the dictionary to a YAML string.
    config_str = yaml.dump(config_dict)

    # Use the existing read_config function to parse the YAML string.
    config = read_config(config_str, schema_evaluate)

    # Convert the dictionary back into a Namespace object.
    args = Namespace(**config)

    # Call the original evaluate function with the Namespace object.
    return evaluate(args)


def evaluate(args: Namespace):
    config = read_config(args.config, schema_evaluate)

    plt.rcParams.update({'font.size': config.figure.font_size})
    config = set_colors(config)

    if config.task == 'aspect_ratio':
        evaluate_aspect_ratio(config)

    else:
        if config.use_gpu:
            check_gpu()
        else:
            os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

        evaluate_dl_model(config)


