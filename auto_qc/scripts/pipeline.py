"""
pipeline.py: Pipeline for Sending an Image to a Server Over ZeroMQ, Receiving a Response, and Saving the Response Data
to Files.

The pipeline.py module provides a function for running an image processing pipeline. It reads an image from a file,
sends the image to a server using ZeroMQ, receives the processed image and related information from the server, and
then saves this data to disk.

Main Components:

- pipeline: Reads an image from a file specified in the command line arguments, sends it to a server over ZeroMQ, receives
a response containing processed image data and a log, and saves the processed image and the log to files. The saved
files include a .log file with processing information, a .hdr file with header information, and a .tiff file with the
processed image.

This module is useful for setting up a client-server architecture for image processing tasks. It allows for efficient
communication and data transfer between the client and server using ZeroMQ, and facilitates the storage of processing
results and logs.
"""

import json
from argparse import Namespace
from pathlib import Path

import imageio
import yaml
import zmq


def pipeline(args: Namespace) -> None:
    """
    Pipeline for sending an image to a server over ZeroMQ, receiving a response,
    and saving the response data to files.

    :param args: Command line arguments, should contain 'image'
    """
    port = 5560
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect(f"tcp://localhost:{port}")

    name = Path(args.image.name)
    bytes = args.image.read()
    # load an image
    # img = imageio.imread(filename)

    # request model predition
    # socket.send_pyobj(img)
    socket.send_pyobj({"image": bytes})
    response = socket.recv_pyobj()
    with open(f"{name.stem}.log", "w") as fh:
        # fh.write(json.dumps(response["log"], indent=4))
        yaml.dump(response["log"], fh)
        print(f"{name.stem}.log saved")

    with open(f"{name.stem}.hdr", "w") as fh:
        yaml.dump(response["hdr"], fh)
        print(f"{name.stem}.hdr saved")

    imageio.imsave(f"{name.stem}.tiff", response["array"])
    print(f"{name.stem}.tiff saved")
