"""
train.py: Deep Learning Model Training

This module provides a Trainer class and a train function for training various models. It reads configuration data,
prepares data, performs hyperparameter tuning, trains and evaluates models. It allows for model specification, data
augmentation, early stopping, and several other customizable features. The module is designed to work with different
model architectures and a structured dataset.

Classes:
    Trainer: A class that manages the entire training process.

Functions:
    train: Reads configuration, prepares the model, data and starts the training process.

Note:
    This module assumes the availability of a suitable GPU for training. Please ensure that an appropriate
    computational resource is available and properly configured for TensorFlow.
"""

from argparse import Namespace
from pathlib import Path
import json
import numpy as np
from tqdm import tqdm
from imblearn.keras import BalancedBatchGenerator
import os
import random
from typing import Tuple, List, Union, Optional
import math
import keras_tuner as kt
from keras_tuner import Objective, RandomSearch, HyperParameters, Hyperband, BayesianOptimization
from tensorflow.keras.preprocessing.image import ImageDataGenerator, Iterator, NumpyArrayIterator

from ..conf import schema_train
from .. import models
from ..models import get_callbacks
from ..utils import imread, read_config, check_gpu
from ..models.lazytf import tf, Model, Callback, to_categorical, array_to_img
from ..models.layers import CropAndResize


class BalancedDataGenerator(BalancedBatchGenerator):
    """A class to generate data with balanced classes for training models."""

    def __init__(self, x, y, **kwargs):
        """
        Initializes the BalancedDataGenerator object.

        :param x: Input data.
        :param y: Labels corresponding to the input data.
        :param kwargs: Variable length argument list.
        """
        self.shape = x.shape[1:]
        X = x.reshape(x.shape[0], -1)
        super().__init__(X, y, **kwargs)

    def __getitem__(self, index: int) -> Tuple[np.ndarray, np.ndarray]:
        """
        Retrieves the data at the given index.

        :param index: Index of the item.
        :return: Tuple of numpy arrays for x and y data.
        """
        if self.sample_weight is None:
            x, y = super().__getitem__(index)
            x_res = x.reshape(-1, *self.shape)
            res = x_res, y
        else:
            x, y, w = super().__getitem__(index)
            x_res = x.reshape(-1, *self.shape)
            res = x_res, y, w
        return res


class RotationNumpyArrayIterator(NumpyArrayIterator):
    """An iterator class for numpy arrays, with rotation transformations applied."""

    def _get_batches_of_transformed_samples(self, index_array: np.ndarray) -> Union[np.ndarray, Tuple[np.ndarray, ...]]:
        """
        Retrieves batches of transformed samples based on the index array.

        :param index_array: Array of indexes.
        :return: Batches of transformed samples.
        """
        batch_x = np.zeros(
            tuple([len(index_array)] + list(self.x.shape)[1:]), dtype=self.dtype
        )
        if self.y is not None:
            # batch_y = np.zeros((len(batch_x), 360))
            batch_y = [0] * len(batch_x)
        for i, j in enumerate(index_array):
            x = self.x[j]
            params = self.image_data_generator.get_random_transform(x.shape)
            self.image_data_generator.fill_mode = np.random.choice(["constant", "nearest", "wrap", "reflect"])
            self.image_data_generator.cval = 0
            x = self.image_data_generator.apply_transform(x.astype(self.dtype), params)
            x = self.image_data_generator.standardize(x)
            batch_x[i] = x
            if self.y is not None:
                theta = params["theta"]
                if theta is None:
                    theta = 0
                else:
                    if theta < 0:
                        theta = 360 + theta
                batch_y[i] = to_categorical(int(theta), 360)

        if self.save_to_dir:
            for i, j in enumerate(index_array):
                img = array_to_img(batch_x[i], self.data_format, scale=True)
                fname = "{prefix}_{index}_{hash}.{format}".format(
                    prefix=self.save_prefix,
                    index=j,
                    hash=np.random.randint(1e4),
                    format=self.save_format,
                )
                img.save(os.path.join(self.save_to_dir, fname))
        batch_x_miscs = [xx[index_array] for xx in self.x_misc]
        output = (batch_x if batch_x_miscs == [] else [batch_x] + batch_x_miscs,)
        if self.y is None:
            return output[0]
        output += (np.array(batch_y),)
        if self.sample_weight is not None:
            output += (self.sample_weight[index_array],)
        return output


class RotationDataGenerator(ImageDataGenerator):
    """A data generator class that creates batches of tensor image data with real-time data augmentation,
    which is used in this package for rotation."""

    def flow(self, x: np.ndarray, y: Optional[np.ndarray] = None, batch_size: Optional[int] = 64,
             shuffle: Optional[bool] = True, sample_weight: Optional[np.ndarray] = None, seed: Optional[int] = None,
             save_to_dir: Optional[bool] = None, save_prefix: Optional[str] = "", save_format: Optional[str] = "png",
             subset: Optional[str] = None, **kwargs) -> RotationNumpyArrayIterator:
        """
        Retrieves batches of transformed samples based on the index array.

        :param x: Input data.
        :param y: Labels for input data. Default is None.
        :param batch_size: Size of the batches. Default is 64.
        :param shuffle: Whether to shuffle the data. Default is True.
        :param sample_weight: Weights for each sample. Default is None.
        :param seed: Random seed. Default is None.
        :param save_to_dir: Where to save the output. Default is None.
        :param save_prefix: Prefix for the saved data. Default is "".
        :param save_format: Format of the saved data. Default is "png".
        :param subset: Subset of the data to flow. Default is None.
        :return: Iterator for the data.
        """
        return RotationNumpyArrayIterator(
            x=x,
            y=y,
            image_data_generator=self,
            batch_size=batch_size,
            shuffle=shuffle,
            sample_weight=sample_weight,
            seed=seed,
            data_format=self.data_format,
            save_to_dir=save_to_dir,
            save_prefix=save_prefix,
            save_format=save_format,
            subset=subset,
        )


def read_dataset(input_dir: str, label: str) -> np.array:
    """
    Reads the dataset from the provided input directory.

    :param input_dir: The directory where the dataset is located.
    :param label: The label of the dataset.
    :return: The loaded dataset.
    """
    try:
        xx = np.load(input_dir / f"{label}.npy")
    except FileNotFoundError:
        xx = []
        for item in tqdm([*(input_dir / label).glob("*")], desc=label):
            try:
                xx.append(imread(item))
            except (ValueError, FileNotFoundError, OSError) as e:
                print(f"Failed to read file: {item} -> {e}")
        xx = np.stack(xx)
        np.save(input_dir / f"{label}.npy", xx)
    print(f"Read {len(xx)} images with label {label!r}")
    return xx


def split_train_val(
        xx: np.ndarray,
        label: int,
        percentage: Optional[float] = 0.9
) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """
    Splits the provided data into training and validation sets.

    :param xx: The input data to be split.
    :param label: The label of the data.
    :param percentage: The percentage of data to be used for training. Default is 0.9.
    :return: The split data sets for training and validation inputs and labels.
    """
    indx = np.random.choice(range(len(xx)), len(xx))
    n = int(len(indx) * percentage)
    x_train = xx[indx[:n]]
    y_train = np.zeros(n, "int") + label
    x_val = xx[indx[n:]]
    y_val = np.zeros(len(indx) - n, "int") + label
    return x_train, y_train, x_val, y_val


def shuffle_dataset(xx: np.ndarray, yy: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """
    Shuffles the provided data and labels.

    :param xx: The input data to be shuffled.
    :param yy: The labels corresponding to the input data.
    :return: The shuffled data and labels.
    """
    indx = np.random.choice(range(len(xx)), len(xx))
    return xx[indx], yy[indx]


class Trainer:
    def __init__(
            self, model: Model, tuner: Optional[Union[RandomSearch, Hyperband, BayesianOptimization]] = RandomSearch,
            augmentation: Optional[bool] = False, crop: Optional[bool] = True, dropout: Optional[bool] = None,
            nlayers: Optional[int] = 4,
            nfilters: Optional[int] = 64, nblocks: Optional[int] = 3, nunits: Optional[int] = 128,
            objective: str = "val_accuracy", max_trials: Optional[int] = 10, executions_per_trial: Optional[int] = 2,
            max_epochs: Optional[int] = 100, hyperband_iterations: Optional[int] = 2, factor: Optional[int] = 3,
            project_name: Optional[str] = None,
            batch_size: Optional[int] = 32, input_dir: Optional[str] = None, multi_gpu: bool = False,
            tuning_complete: bool = False, verbose: int = 2,
    ):
        """
        Initializes the Trainer class.

        :param model: The Keras model to be trained.
        :param tuner: The keras tuner to be used for hyperparameter tuning. Default is RandomSearch.
        :param augmentation: Whether to use data augmentation. Default is False.
        :param crop: Whether to use cropping. Default is True.
        :param dropout: Dropout rate to use, if any. Default is None.
        :param nlayers: Number of layers in the model. Default is 4.
        :param nfilters: Number of filters in the model. Default is 64.
        :param nblocks: Number of blocks in the model. Default is 3.
        :param objective: Objective to be used for hyperparameter tuning. Default is "val_auc".
        :param max_trials: Maximum number of trials for hyperparameter tuning. Default is 10.
        :param executions_per_trial: Number of executions per trial for hyperparameter tuning. Default is 2.
        :param max_epochs: Maximum number of epochs for hyperband tuning. Default is 100.
        :param hyperband_iterations: Number of iterations for hyperband tuning. Default is 2.
        :param factor: Factor for hyperband tuning. Default is 3.
        :param project_name: Name of the project. Default is None.
        :param batch_size: Batch size for training. Default is 32.
        :param input_dir: Directory for the input data. Default is None.
        :param multi_gpu: Whether to use multiple GPUs (one worker). Default is False.
        """
        self.model = model
        self.tuner = tuner
        self.nlayers = nlayers
        self.nblocks = nblocks
        self.nfilters = nfilters
        self.nunits = nunits
        self.augmentation = augmentation or {}
        self.crop = crop
        self.dropout = dropout
        self.max_trials = max_trials
        self.executions_per_trial = executions_per_trial
        self.project_name = project_name
        self.batch_size = batch_size
        self.max_epochs = max_epochs
        self.factor = factor
        self.hyperband_iterations = hyperband_iterations
        if self.model.__name__ == 'RotNet':
            self.objective = Objective("val_angle_error", direction="min")
        else:
            self.objective = Objective(objective, direction="max")
        self.steps_per_epoch = None
        self.validation_steps = None
        self.input_dir = input_dir
        self.tuning_complete = tuning_complete
        self.verbose = verbose
        self.multi_gpu = multi_gpu
        if self.multi_gpu:
            self.strategy = tf.distribute.MirroredStrategy()
            self.batch_size = self.batch_size * self.strategy.num_replicas_in_sync

    def summary(self):
        """Provides a summary of the model."""
        self.model.summary()

    def _get_train_val_set(self, input_dir: str, labels: List[str], samesize: bool = False):
        """
        Reads data from the specified directory and labels, prepares and shuffles training and validation sets.
        Old method and no longer used, as we specify the training and validation sets in the config file.

        :param input_dir: Directory for the input data.
        :param labels: The labels for the dataset.
        :param samesize: If True, all classes in the dataset will have the same size.
        """
        # Read labels
        xx = [read_dataset(input_dir, label) for label in labels]

        if samesize:
            n = min([x.shape[0] for x in xx])
            xx = [x[:n] for x in xx]

        # Create training and validation
        val = 0
        xx_train = []
        yy_train = []
        xx_val = []
        yy_val = []
        for x in xx:
            x_train, y_train, x_val, y_val = split_train_val(x, val)
            xx_train.append(x_train)
            yy_train.append(y_train)
            xx_val.append(x_val)
            yy_val.append(y_val)
            val = val + 1

        xx_train = np.vstack(xx_train)
        yy_train = np.hstack(yy_train)
        xx_val = np.vstack(xx_val)
        yy_val = np.hstack(yy_val)

        # Shuffle
        xx_train, yy_train = shuffle_dataset(xx_train, yy_train)
        xx_val, yy_val = shuffle_dataset(xx_val, yy_val)

        # Add extra dimension
        self.x_train, self.y_train = xx_train[..., None], yy_train
        self.x_val, self.y_val = xx_val[..., None], yy_val

    def read_data_old(self, input_dir: str, labels: List[str]):
        """
        Reads training images from the specified directory, labels, and prepares training, validation and test sets.
        This is an older version of reading data.

        :param input_dir: Directory for the input data.
        :param labels: The labels for the dataset.
        """
        input_dir = Path(input_dir) / "images"
        if not input_dir.exists():
            raise FileNotFoundError(f"{input_dir}")
        print(f"Reading training images from {input_dir}")
        print(f"Labels: {labels}")

        self._get_train_val_set(input_dir, labels, samesize=True)

        print("Train X :", self.x_train.shape)
        print("Train Y :", self.y_train.shape)
        print("Val X   :", self.x_val.shape)
        print("Val Y   :", self.y_val.shape)
        print("Test X  :", self.x_test.shape)
        print("Test Y  :", self.y_test.shape)
        print("")

    def read_data(self, input_dir: str, dataset: str, labels: List[str]):
        """
        Reads training images from the specified directory and dataset, prepares training, validation and test sets.
        The dataset directory should be structured in a train/val/test fashion for each label.

        :param input_dir: Directory for the input data.
        :param dataset: Name of the dataset to read.
        :param labels: List of labels in the dataset.
        """
        input_dir = Path(input_dir) / dataset
        if not input_dir.exists():
            raise FileNotFoundError(f"{input_dir}")
        print(f"Reading training images from {input_dir}")
        print(f"Labels: {labels}")

        # training
        xx = [read_dataset(input_dir, f"{label}/train") for label in labels]
        x_train = np.vstack(xx)
        if len(labels) == 1:
            y_train = np.zeros(len(x_train))
        else:
            y_train = np.hstack([np.zeros(len(xx[0])), np.ones(len(xx[1]))])
        self.x_train, self.y_train = shuffle_dataset(x_train, y_train)

        # val
        xx = [read_dataset(input_dir, f"{label}/val") for label in labels]
        x_val = np.vstack(xx)
        if len(labels) == 1:
            y_val = np.zeros(len(x_val))
        else:
            y_val = np.hstack([np.zeros(len(xx[0])), np.ones(len(xx[1]))])
        self.x_val, self.y_val = shuffle_dataset(x_val, y_val)

        # test
        xx = [read_dataset(input_dir, f"{label}/test") for label in labels]
        x_test = np.vstack(xx)
        if len(labels) == 1:
            y_test = np.zeros(len(x_test))
        else:
            y_test = np.hstack([np.zeros(len(xx[0])), np.ones(len(xx[1]))])
        self.x_test, self.y_test = shuffle_dataset(x_test, y_test)

    def rotation_generator(self, x: np.ndarray, y: np.ndarray, batch_size: int = 1,
                           seed: Optional[int] = None) -> Iterator:
        """
        Data generator that performs image augmentation by rotating the images.

        :param x: The input data.
        :param y: The labels for the input data.
        :param batch_size: Batch size for the generator.
        :param seed: Seed for random number generator.
        """
        augmentation = self.augmentation
        image_gen = RotationDataGenerator(**augmentation)

        seed = random.randint(1, 1000) if seed is None else seed

        image_generator = image_gen.flow(
            x[..., None],
            y[..., None],
            batch_size=batch_size,
            seed=seed,
            # save_to_dir="./imgs",
        )
        for res in image_generator:
            yield res

    def build_model(self, hp: HyperParameters) -> Model:
        """
        Builds the model for hyperparameter tuning.

        :param hp: The hyperparameters for the model.
        :return: The constructed model.
        """
        # Hyperparameters
        nfilters = self.nfilters if self.nfilters else hp.Choice("nfilters",
                                                                 values=[32, 64, 96, 128])
                                                                 # values=[64, 128, 256, 512])
        nunits = self.nunits if self.nunits else hp.Choice("nunits",
                                                           values=[32, 48, 64, 80, 96, 112, 128])
                                                           # values=[64, 80, 96, 112, 128, 256, 512])
        lr = hp.Choice("lr", values=[
            1e-2,
            1e-3,
            1e-4
        ])
        dropout = hp.Boolean("dropout") if self.dropout is None else self.dropout

        # Model-specific parameters
        is_rotnet = self.model.__name__ == 'RotNet'
        augmentation = None if is_rotnet else self.augmentation if self.augmentation is not None else hp.Boolean(
            "augmentation")
        crop = None if is_rotnet else self.crop if self.crop is not None else hp.Boolean("crop")
        nblocks = None if is_rotnet else self.nblocks if self.nblocks else hp.Int(
            "nblocks", min_value=1, max_value=3, step=1
            # "nblocks", min_value=2, max_value=4, step=1
        )
        nlayers = self.nlayers if self.nlayers else hp.Choice(
            "nlayers", values=[1, 2]
        ) if is_rotnet else hp.Choice(
            "nlayers", values=[1, 2, 3, 4, 6]
            # "nlayers", values=[4, 5, 6]
        )

        model_params = {
            "dropout": dropout,
            "nlayers": nlayers,
            "nfilters": nfilters,
            "nunits": nunits,
            "learning_rate": lr,
            "nblocks": nblocks,
            "augmentation": augmentation,
            "crop": crop
        }
        # Filter out None values for RotNet
        model_params = {k: v for k, v in model_params.items() if v is not None}

        if self.multi_gpu == 'mirrored':
            model_params['learning_rate'] = lr / self.strategy.num_replicas_in_sync
            with self.strategy.scope():
                model = self.model(**model_params)
        else:
            model = self.model(**model_params)

        return model

    def create_tuner(
            self, checkpoint: str, early_stopping: bool, logfile: str, overwrite: bool = True
    ) -> Tuple[RandomSearch, List[Callback]]:
        """
        Creates a tuner for hyperparameter tuning and returns callbacks for training.

        :param checkpoint: The file path to save the model.
        :param early_stopping: If true, stop training when a monitored metric has stopped improving.
        :param logfile: The file path to save the log.
        :param overwrite: If true, the previous tuner will be overwritten.
        :return: The tuner and the callbacks.
        """
        tuner_params = {
            "hypermodel": self.build_model,
            "objective": self.objective,
            "overwrite": overwrite,
            "directory": "tuner",
            "project_name": self.project_name,
        }

        if self.tuner.__name__ == "RandomSearch" or self.tuner.__name__ == "BayesianOptimization":
            tuner_params.update(
                {
                    "max_trials": self.max_trials,
                    "executions_per_trial": self.executions_per_trial,
                }
            )
            tuner = RandomSearch(**tuner_params)

        elif self.tuner.__name__ == "Hyperband":
            tuner_params.update(
                {
                    "max_epochs": self.max_epochs,
                    "factor": self.factor,
                    "hyperband_iterations": self.hyperband_iterations,
                }
            )
            tuner = Hyperband(**tuner_params)

        else:
            raise ValueError(f"Tuner name provided ({self.tuner.__name__}) is not supported. Please choose from: "
                             f"RandomSearch, BayesianOptimization, or Hyperband.")

        callbacks = get_callbacks(checkpoint, early_stopping, logfile, tuner=True, model_name=self.model.__name__)
        return tuner, callbacks

    def create_generators(self) -> Tuple:
        """
        Creates generators for training and validation data. If the model used is for rotation (RotNet), a specific
        generator is used.

        :return: Training generator and validation data.
        """
        if self.model.__name__ == 'RotNet':
            self.augmentation = {"rotation_range": 180}
            training_generator = self.rotation_generator(
                self.x_train, self.y_train, batch_size=self.batch_size, seed=42
            )
            validation_data = self.rotation_generator(
                self.x_val, self.y_val, batch_size=self.batch_size, seed=42
            )
        else:
            training_generator = BalancedDataGenerator(
                self.x_train, self.y_train, batch_size=self.batch_size, random_state=42
            )
            validation_data = (self.x_val, self.y_val)
        return training_generator, validation_data

    def set_rotation_tuner_params(self):
        self.steps_per_epoch = len(self.x_train) // self.batch_size
        self.validation_steps = len(self.x_val) // self.batch_size

    def train_model(
            self, tuner: RandomSearch, callbacks: List[Callback], training_generator: Union[BalancedDataGenerator,
            Iterator], validation_data: Union[np.ndarray, Tuple[np.ndarray]], checkpoint: str,
            early_stopping: bool, logfile: str, epochs: int
    ):
        """
        Performs hyperparameter tuning on the model and trains the best model from the tuning.

        :param tuner: The hyperparameter tuner.
        :param callbacks: List of callbacks to apply during training.
        :param training_generator: Generator for training data.
        :param validation_data: Validation data.
        :param checkpoint: The file path to save the model.
        :param early_stopping: If true, stop training when a monitored metric has stopped improving.
        :param logfile: The file path to save the log.
        :param epochs: The number of epochs to train the model.
        """
        import tensorflow as tf

        if self.model.__name__ == 'RotNet':
            self.set_rotation_tuner_params()

        common_params = {
            "epochs": epochs,
            "validation_data": validation_data,
            "verbose": self.verbose,
            "steps_per_epoch": self.steps_per_epoch,
            "validation_steps": self.validation_steps,
        }

        if not self.tuning_complete:
            tuner.search(training_generator, callbacks=callbacks, **common_params)

        best_hp = tuner.get_best_hyperparameters()[0]

        tf.keras.backend.clear_session()

        model = self.build_model(best_hp)
        model.summary()

        callbacks = get_callbacks(checkpoint, early_stopping, logfile, model_name=self.model.__name__)
        model.fit(training_generator, callbacks=callbacks, **common_params)
        del model

    def run(self, checkpoint: str, early_stopping: bool, logfile: str, epochs: int):
        """
        Performs the training process: creates data generators, sets rotation settings if necessary, creates a
        tuner, and trains the model.  If the tuning has previously been completed, the best hyperparameters from
        the previous tuning will be used if 'tuning_complete' is set to 'true' in the config file.

        :param checkpoint: The file path to save the model.
        :param early_stopping: If true, stop training when a monitored metric has stopped improving.
        :param logfile: The file path to save the log.
        :param epochs: The number of epochs to train the model.
        """
        es = {True: "", False: "out"}[early_stopping]
        print(f"Training for {epochs} epochs with{es} early stopping.")

        training_generator, validation_data = self.create_generators()

        if self.tuning_complete:
            print("Loading tuner...")
            try:
                tuner, callbacks = self.create_tuner(checkpoint, early_stopping, logfile, overwrite=False)
                print("Previous tuner loaded! Using best hyperparameters from previous tuning...")
            except Exception as e:
                raise Exception("Failed to load tuner. Please run the tuner first.") from e
        else:
            tuner, callbacks = self.create_tuner(checkpoint, early_stopping, logfile)

        self.train_model(
            tuner, callbacks, training_generator, validation_data, checkpoint, early_stopping, logfile, epochs
        )

        self.evaluate_model(checkpoint)

    def evaluate_model(self, checkpoint: str):
        """
        Loads the best model from training and evaluates it on the test set, saving the results to a JSON file.

        :param checkpoint: The file path to load the model.
        """
        import tensorflow as tf
        from auto_qc.models.metrics import f1, angle_error
        tf.keras.backend.clear_session()

        if self.model.__name__ == 'RotNet':
            model = tf.keras.models.load_model(checkpoint, custom_objects={"angle_error": angle_error})
            test_generator = self.rotation_generator(self.x_test, self.y_test, seed=42, batch_size=self.batch_size)
            score = model.evaluate(test_generator, steps=math.ceil(len(self.x_test) / self.batch_size))
        else:
            model = tf.keras.models.load_model(checkpoint, custom_objects={"f1": f1, 'CropAndResize': CropAndResize})
            score = model.evaluate(self.x_test, self.y_test)
        res = {k: v for k, v in zip(model.metrics_names, score)}
        out = checkpoint.replace('.h5', '_eval.json')
        with open(out, "w") as fh:
            json.dump(res, fh)


def train(args: Namespace) -> dict:
    """
    Reads the configuration file, checks the GPU availability, prepares the model, reads the data, and starts the
    training process.

    :param args: The arguments passed to the training function.
    :return: The training history.
    """

    check_gpu()

    config = read_config(args.config, schema_train)

    trainer = Trainer(
        getattr(models, config.model.name),
        tuner=getattr(kt, config.tuner.name),
        nlayers=config.model.nlayers,
        nfilters=config.model.nfilters,
        nblocks=config.model.nblocks,
        nunits=config.model.nunits,
        augmentation=config.model.augmentation,
        crop=config.model.crop,
        dropout=config.model.dropout,
        objective=config.tuner.objective,
        max_trials=config.tuner.max_trials,
        executions_per_trial=config.tuner.executions_per_trial,
        batch_size=config.model.batch_size,
        factor=config.tuner.factor,
        max_epochs=config.tuner.max_epochs,
        hyperband_iterations=config.tuner.iterations,
        project_name=f"{config.model.name}_{config.checkpoint.replace('.h5', '')}",
        multi_gpu=config.model.multi_gpu,
        tuning_complete=config.tuner.tuning_complete,
        verbose=config.verbose,
    )
    trainer.read_data(config.data_dir, config.dataset, config.labels)

    history = trainer.run(config.checkpoint, config.early_stopping, config.logfile, config.epochs, )

    return history
