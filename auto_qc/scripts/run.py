"""
run.py: Run the full AutoQC pipeline.

This script runs the full AutoQC pipeline, including preprocessing, classification, and quality control.
The pipeline can be run on a single image or a directory of images. It can be run on a single CPU or multiple CPUs,
and can use a GPU for model inference if available. The options can be specified in a config file or on the command
line. The config file allows for slightly greater customisation.

To run directly:
    python -m auto_qc.run_pipeline --config /path/to/config.yaml

Command line examples:
    autoqc run --config /path/to/config.yaml
    autoqc run --img_path /path/to/images/* --outpath /path/to/output/directory --cpu_workers 5 --gpu true
        --batch_size 32

Jupyter notebook:
    from argparse import Namespace
    from run import run_pipeline

    args = Namespace(config_path="/path/to/config.yaml")
    run_pipeline(args)

The config file is a YAML file with the following structure:

- `quality`: Specifies image quality metrics and their respective defaults.
    - e.g., brisque: ["low", "med", "high", "none"]
- `checkpoints`: Points to model checkpoint files and directories.
    - e.g., checkpoint_dir: Path("./models"), inversion: "model_inversion.h5"
- `thresholds`: Specifies various model thresholds.
    - e.g., inversion: 0.48, lateral: 0.36
- `exclude_or_review`: Decides the action to take based on specific criteria.
    - e.g., lateral: ["exclude", "review"], quality: ["exclude", "review"]
- `ocr`: Configurations related to Optical Character Recognition (OCR).
    - e.g., lang: ["eng", "spa", "ita", "por"], method: ["combined", "python", "subprocess"]
- `options`: Miscellaneous options for pipeline behavior.
    - e.g., autowindow_instead_of_luts: bool, correct_inversion: bool
- `run`: Overall structure encapsulating all other schemas to configure the pipeline's behavior.
    - Includes dataset specification, paths, GPU usage, and others.

For a complete specification, please refer to the provided schemas.

Variables:
    - SCRIPT_DIR: Path to the directory containing this script.
    - QUALITY_METRICS: List of quality metrics to use.
    - QUALITY_BOUNDS: Dictionary of quality metric bounds.
    - QUALITY_CONFIG: Dictionary of quality metric defaults for the partitions to remove and
        sensitivities for each metric.
"""

from argparse import Namespace
import shutil
import gc
import inspect
import os
from pathlib import Path
from importlib.resources import open_text
from typing import Optional, Union, Tuple, List, Dict, Generator, Any, Callable, Iterable, Literal
import logging
import json
import numpy as np
import math
import pandas as pd
import itertools
import pydicom
from pydicom.dataset import Dataset, FileDataset
import imageio.v2 as iio
from scipy.ndimage import rotate
from tqdm.auto import tqdm
import concurrent.futures
from functools import partial
import pytesseract
import multiprocessing as mp

from ..conf import schema_run
from ..classification.quality import quality
from ..models.metrics import f1, angle_error
from ..models.layers import CropAndResize
from ..models.lazytf import tf, keras, K, Model
from ..operations.boundary import remove_border
from ..operations.inpaint import inpaint_cxr
from ..operations.ocr import clean_data, create_dictionaries, find_terms, python_ocr_pipeline, \
    ocr_subprocess, populate_category_df, OUTPUT_CATEGORIES
from ..utils import preprocess_image, read_config, high_intensity_enhancement, correct_90_rotation, \
    invert_grayscale, autowindow, apply_luts

# Get the path to the directory containing this script for use in finding OCR dictionaries
SCRIPT_DIR = Path(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))

# Quality metrics to be calculated
QUALITY_METRICS: List[str] = [
    'niqe',
    'niqe_cutout50',
    'brisque',
    'brisque_cutout50',
    'contrast_ratio',
    'contrast_ratio_cutout50',
    'entropy',
    'entropy_cutout50',
    'mean_intensity',
    'median_intensity',
    'std_intensity',
    'var_intensity',
    'fft_contrast',
    'sharpness',
    'sharpness_cutout50',
    'resolution',
    'noise_estimate',
    'skewness',
    'skewness_cutout50',
]

# Literal value lists for type hinting for quality dictionaries
SensitivityLevels = Literal['high', 'med', 'low', 'none']
Partitions = Literal['upper', 'lower', 'both']

# Default quality bounds for each metric and sensitivity level
QUALITY_BOUNDS: Dict[str, Dict[SensitivityLevels, Tuple[float, float]]] = {
    'niqe_combined': {
        'high': (2.7952769369877233, 4.463382299808445),
        'med': (2.428293757167164, 4.830365479629004),
        'low': (2.1280347918594344, 5.130624444936734)
    },
    'brisque_combined': {
        'high': (0.5589338743392728, 0.952255657442611),
        'med': (0.46879763237809113, 1.0423918994037926),
        'low': (0.4032440018608681, 1.1079455299210157)
    },
    'contrast_ratio_combined': {
        'high': (1.069019607843137, 1.7623529411764705),
        'med': (0.9223529411764706, 1.909019607843137),
        'low': (0.8023529411764705, 2.029019607843137)
    },
    'entropy_combined': {
        'high': (3.5905073404131613, 5.572967419029041),
        'med': (3.1940153246899854, 5.969459434752217),
        'low': (2.916470913683762, 6.2470038457584405)
    },
    'sharpness_combined': {
        'high': (-240.4414090785629, 662.0548691626659),
        'med': (-398.98805255337334, 820.6015126374764),
        'low': (-398.98805255337334, 820.6015126374764)
    },
    'skewness_combined': {
        'high': (-1.9476208198886957, 0.31522769749469326),
        'med': (-2.382783996308578, 0.7503908739145757),
        'low': (-2.6873982198024957, 1.0550050974084932)
    }
}

# Default quality config dictionary containing which values to remove, e.g. want to remove low entropy
QUALITY_CONFIG: Dict[str, Dict[str, Union[Partitions, SensitivityLevels]]] = {
    'niqe_combined': {'partition': 'upper'},
    'brisque_combined': {'partition': 'upper'},
    'contrast_ratio_combined': {'partition': 'both'},
    'entropy_combined': {'partition': 'lower'},
    'sharpness_combined': {'partition': 'both'},
    'skewness_combined': {'partition': 'upper'},
}


def disable_gpu():
    """
    Disables GPU usage in TensorFlow by setting CUDA_VISIBLE_DEVICES to an empty value.

    :return: None
    """
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'


def enable_gpu():
    """
    Enables GPU usage in TensorFlow by resetting CUDA_VISIBLE_DEVICES.

    :return: None
    """
    os.environ['CUDA_VISIBLE_DEVICES'] = ''
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        # Assuming that you want to use the first GPU if multiple GPUs are available
        tf.config.experimental.set_visible_devices(gpus[0], 'GPU')


def execute_on_device(
        function: Callable[..., Dict[str, Any]],
        items: Union[Dict[str, Any], Iterable[Any]],
        config: Dict[str, Any],
        description: str = "",
        total: Optional[int] = None,
        gpu: bool = False,
        multithread: bool = False,
        *args: Any,
        **kwargs: Any
) -> Dict[str, Any]:
    """
    Executes a function on a list of items, either on the CPU or GPU depending on the config.

    :param function: The function to execute on each item
    :param items: The items to execute the function on
    :param config: The config dictionary
    :param description: The description of the items being processed for the progress bar, defaults to ""
    :param total: The total number of items being processed for the progress bar, defaults to None
    :param gpu: Whether to use the GPU or not, defaults to False
    :param multithread: Whether to use multithreading or multiprocessing, defaults to False
    :param args: The arguments to pass to the function
    :param kwargs: The keyword arguments to pass to the function
    :return: The results of the function
    """
    results_dict = {}
    if total is None and isinstance(items, (list, tuple, dict)):
        total = len(items)

    # if config['use_gpu'] or config['cpu_workers'] == 1:
    # if (config['use_gpu'] and gpu is True) or config['cpu_workers'] == 1:
    if gpu is True:
        enable_gpu()
        for item in tqdm(items, desc=f"Processing {description}".strip(" "), unit="item", total=total,
                         bar_format='{l_bar}{bar}|'):
            results_dict.update(function(item, *args, **kwargs))
    # elif config['cpu_workers'] > 1:
    else:
        disable_gpu()
        try:
            if multithread:
                with concurrent.futures.ThreadPoolExecutor(max_workers=config['cpu_workers']) as executor:
                    func_partial = partial(function, *args, **kwargs)
                    futures = [executor.submit(func_partial, item) for item in items]
                    for future in tqdm(concurrent.futures.as_completed(futures), total=total, unit="item",
                                       desc=f"Processing {description}".strip(" "), bar_format='{l_bar}{bar}|'):
                        results_dict.update(future.result())
            else:
                with concurrent.futures.ProcessPoolExecutor(
                        max_workers=config['cpu_workers'],
                        mp_context=mp.get_context('spawn'), initializer=disable_gpu # prevents CUDA initialization error when using process pool
                    # otherwise there appears to be a bug that even when CUDA_VISIBLE_DEVICES is set to -1, the GPU is still used.
                    # Alternatively you can set multithread=True, but this is slower.
                ) as executor:
                    func_partial = partial(function, *args, **kwargs)
                    futures = [executor.submit(func_partial, item) for item in items]
                    for future in tqdm(concurrent.futures.as_completed(futures), total=total, unit="item",
                                       desc=f"Processing {description}".strip(" "), bar_format='{l_bar}{bar}|'):
                        results_dict.update(future.result())
        except KeyboardInterrupt:
            logging.info('KeyboardInterrupt: Stopping...')
        except Exception as e:
            logging.error(f"Error during multiprocessing: {str(e)}")
        finally:
            if executor is not None:
                executor.shutdown(wait=True)
    # else:
    #     logging.error("Invalid device_type specified")
    #     raise ValueError("Invalid device_type specified")
    return results_dict


def check_gpu():
    """
    Prints statements to check if a GPU is available for Tensorflow.
    """
    physical_devices = tf.config.list_physical_devices('GPU')
    if len(physical_devices) > 0:
        logging.info(f"Physical GPU devices: {len(physical_devices)}")
    else:
        logging.info("No GPU devices found.")
    logging.info(f"TensorFlow is using GPU / CUDA working: {tf.test.is_built_with_cuda()}")


def read_image_and_metadata(
        image_path: str, json_dir: Optional[str] = None
) -> Tuple[np.ndarray, Optional[Union[Dataset, FileDataset]]]:
    """
    Reads an image and metadata from a file.

    :param image_path: The path to the image
    :param json_dir: The path to the directory containing the json files with the metadata (optional - only required if
        the image is not a DICOM file). Defaults to None.
    :return: The image and metadata
    """
    image_path = Path(image_path)
    if image_path.suffix == '.dcm':
        try:
            ds = pydicom.dcmread(image_path)
            return ds.pixel_array, ds
        except pydicom.errors.InvalidDicomError:
            logging.error(f"Could not read DICOM file {image_path}")
            try:
                return iio.imread(image_path), None
            except FileNotFoundError:
                logging.error(f"Could not find file {image_path}")
                raise FileNotFoundError(f"Could not find file {image_path}")
    elif json_dir:
        try:
            json_path = Path(json_dir) / f'{image_path.stem}.json'
            with open(json_path, 'r') as f:
                json_data = json.load(f)
        except FileNotFoundError:
            logging.error(f"Could not find json file for {image_path}")
            return iio.imread(image_path), None
        try:
            return iio.imread(image_path), Dataset().from_json(json_data)
        except Exception as e:
            logging.error(f"Could not convert json into dicom dataset: {str(e)}")
            return iio.imread(image_path), None
    else:
        try:
            return iio.imread(image_path), None
        except FileNotFoundError:
            logging.error(f"Could not find file {image_path}")
            raise FileNotFoundError(f"Could not find file {image_path}")


def pixel_intensity_transforms(
        image: np.ndarray, ds: Optional[Union[Dataset, FileDataset]], luts_applied: bool
) -> np.ndarray:
    """
    Apply pixel intensity transforms to an image.

    :param image: The image to apply pixel intensity transforms to.
    :param luts_applied: Whether the LUTs / windowing have already been applied to the image. Defaults to True.
    :param ds: The DICOM dataset associated with the image. Defaults to None.
    :return: The image with pixel intensity transforms applied.
    """
    if ds:
        pit_img = apply_luts(image, ds) if not luts_applied else image
        if "PhotometricInterpretation" in ds:
            pit_img = invert_grayscale(pit_img) if ds.PhotometricInterpretation == 'MONOCHROME1' else pit_img
    else:
        pit_img = autowindow(image) if not luts_applied else image
    return pit_img


def apply_pixel_intensity_transforms(
        image_path: str, luts_applied: bool, preprocess: bool, json_dir: Optional[str] = None
) -> Dict[str, np.ndarray]:
    """
    Apply the pixel intensity transforms to an image.

    :param image_path: The path to the image.
    :param luts_applied: Whether the LUTs / windowing have already been applied to the image.
    :param preprocess: Preprocess the image.
    :param json_dir: The path to the directory containing the json files with the metadata (optional - only required if
        the image is not a DICOM file). Defaults to None.
    :return: A dictionary containing the image path (key) and the output image (value).
    """
    img, ds = read_image_and_metadata(image_path, json_dir)
    pit_img = pixel_intensity_transforms(img, ds, luts_applied)
    return {image_path: preprocess_image(pit_img) if preprocess else pit_img}


def prep_image_for_inpainting(
        image_path: str, config: Dict[str, Any], inv_predictions: Dict[str, List[Union[float, str]]]
) -> Tuple[np.ndarray, np.ndarray]:
    """
    Prepares an image for inpainting by applying the pixel intensity transforms and inverting the image if necessary.

    :param image_path: The path to the image.
    :param config: The config dictionary.
    :param inv_predictions: The dictionary containing the predictions for whether the image is inverted or not.
    :return: The image with and without pixel intensity transforms applied, with both inverted if necessary.
    """
    no_luts_img, ds = read_image_and_metadata(image_path, config['json_dir'])
    pit_img = pixel_intensity_transforms(no_luts_img, ds, config['luts_applied'])
    no_luts_img = invert_grayscale(no_luts_img) if inv_predictions[image_path][1] == 'inverted' else no_luts_img
    pit_img = invert_grayscale(pit_img) if inv_predictions[image_path][1] == 'inverted' else pit_img
    return no_luts_img, pit_img


def remove_annotations(
        image_path: str, config: Dict[str, Any], inv_predictions: Dict[str, List[Union[float, str]]]
) -> np.ndarray:
    """
    Removes annotations from an image.

    :param image_path: The path to the image.
    :param config: The config dictionary.
    :param inv_predictions: The dictionary containing the predictions for whether the image is inverted or not.
    :return: The image with annotations removed.
    """
    no_luts_img, pit_img = prep_image_for_inpainting(image_path, config, inv_predictions)
    if config['options']['text_removal_method'] == 'none' or not config['options']['text_removal_method']:
        return pit_img
    method = 'bbox' if config['options']['text_removal_method'] == 'bbox' else 'inpaint_cxr'
    ns = True if config['options']['text_removal_method'] == 'ns' else False
    return inpaint_cxr(pit_img, no_luts_img, method, ns, config['options']['ll_mask'])


def remove_annotations_and_preprocess(
        image_path: str,
        conf: Dict[str, Any],
        inv_predictions: Dict[str, List[Union[float, str]]]
) -> Dict[str, np.ndarray]:
    """
    Removes annotations from an image, applies the pixel intensity transforms, removes the border and preprocesses the
    image.

    :param image_path: The path to the image.
    :param conf: The config dictionary.
    :param inv_predictions: The dictionary containing the predictions for whether the image is inverted or not.
    :return: A dictionary with the image path (key) and the preprocessed image with annotations removed (np.ndarray).
    """
    annots_removed = remove_annotations(image_path, conf, inv_predictions)
    border_removed = remove_border(annots_removed)
    # save to avoid having to run again at the end of the pipeline
    if conf['options']['save_stage2_intermediate']:
        st2_path = Path(conf['image_review_dir']) / 'stage2' / Path(image_path).with_suffix('.png').name
        st2_path.parent.mkdir(parents=True, exist_ok=True)
        try:
            iio.imsave(st2_path, border_removed.astype(np.uint16))
        except Exception as e:
            logging.error(f'Error saving intermediate image at {st2_path}: {e}')
    return {image_path: preprocess_image(border_removed)}


def calculate_aspect_ratio(
        image_path: str, image_dict: Dict[str, np.ndarray], conf: Dict[str, Any]
) -> Dict[str, List[Union[float, str]]]:
    """
    Calculates the aspect ratio of an image.

    :param image_path: The path to the image.
    :param image_dict: A dictionary containing the image path (key) and the image (value).
    :param conf: The config dictionary.
    :return: A dictionary containing the image path (key) and a list (value) containing the aspect ratio [0] and the
        string outcome [1] ('keep' or 'remove').
    """
    image_padding_removed = remove_border(image_dict[image_path])
    ar = image_padding_removed.shape[1] / image_padding_removed.shape[0]
    if conf['aspect_ratio_bounds'][0] <= ar <= conf['aspect_ratio_bounds'][1]:
        return {image_path: [ar, 'keep']}
    else:
        return {image_path: [ar, 'remove']}


def correct_rotation(
        image_path: str,
        image_dict: Dict[str, np.ndarray],
        rotation_predictions: Dict[str, List[Union[int, Optional[str], Optional[int]]]],
) -> Dict[str, np.ndarray]:
    """
    Corrects the 90-degree rotation of an image.

    :param image_path: The path to the image.
    :param image_dict: A dictionary containing the image path (key) and the image (value).
    :param rotation_predictions: A dictionary containing the image path (key) and a list (value) containing the
        predicted rotation angle in step 1 of the tool [0], predicted rotation angle in step 2 of the tool [1],
        the final predicted rotation angle [2], the binary outcome ('rotated' or 'not rotated') [3] and the 90-degree
        rotation angle, i.e. 0, 90, 180, 270 [4].
    :return: A dictionary containing the image path (key) and the corrected image (value).
    """
    if rotation_predictions[image_path][3] == 'rotated':
        return {image_path: correct_90_rotation(image_dict[image_path], rotation_predictions[image_path][2])[0]}
    else:
        return {image_path: image_dict[image_path]}


def create_batches(
        images: Dict[str, np.ndarray], nbatch: Optional[int] = 8
) -> Generator[Dict[str, np.ndarray], None, None]:
    """
    Creates batches of images for prediction.

    :param images: The dictionary containing the images.
    :param nbatch: The number of images per batch.
    :return: A generator of dictionaries containing the image path (key) and the image (value).
    """
    for i in range(0, len(images.keys()), nbatch):
        yield {k: v for k, v in itertools.islice(images.items(), i, i + nbatch)}


def load_model(checkpoint: Union[str, Path]) -> Model:
    """
    Loads a model from a checkpoint.

    :param checkpoint: The path to the checkpoint.
    :return: The loaded model.
    """
    K.clear_session()
    try:
        if 'rotation' not in checkpoint:
            return keras.models.load_model(
                checkpoint, custom_objects={"f1": f1, "CropAndResize": CropAndResize}
            )
        else:
            return keras.models.load_model(
                checkpoint, custom_objects={"angle_error": angle_error, "CropAndResize": CropAndResize}
            )
    except Exception as e:
        logging.error(f'Error loading model from {checkpoint}: {e}')
        raise e


def make_rotation_prediction(
        image_batch: Dict[str, np.ndarray], model: Model, pred: np.ndarray
) -> Dict[str, List[Union[int, Optional[str], Optional[int]]]]:
    """
    Makes predictions on the given image batch for the rotation model.  Provides binary predictions for each
    rotation class and then rotates the images to the predicted angle and makes a second prediction. The final
    predictions are provided in a binary format for rotation by an angle which is a multiple of 90 degrees, and
    in a categorical format for the degree of 90 degree rotation (i.e. 0, 90, 180 or 270).

    :param image_batch: A list of image paths to make predictions on.
    :param model: The rotation model.
    :param pred: The predictions for the first rotation prediction.
    :return: The final predictions for the rotation model.
    """
    p0 = pred.copy()
    image_batch_corrected = {}
    for i, (name, image) in enumerate(image_batch.items()):
        image_batch_corrected[name] = rotate(image, float(np.argmax(p0[i])), reshape=False)
    p1 = model.predict(np.array(list(image_batch_corrected.values()))[..., np.newaxis])
    pred_dict = {}
    for img, prbs, orig_prob in zip(image_batch.keys(), p1, p0):
        pred_angle = int(np.argmax(orig_prob)) + int(np.argmax(prbs))
        if pred_angle > 360:
            pred_angle -= 360
        if pred_angle < 45 or pred_angle >= 315:
            rot_pred = 'not_rotated'
            rot90_pred = 0
        elif 90 - 45 <= pred_angle < 90 + 45:
            rot_pred = 'rotated'
            rot90_pred = 90
        elif 180 - 45 <= pred_angle < 180 + 45:
            rot_pred = 'rotated'
            rot90_pred = 180
        elif 270 - 45 <= pred_angle < 270 + 45:
            rot_pred = 'rotated'
            rot90_pred = 270
        else:
            logging.error(f"Unclassified angle for {img}: {pred_angle}")
            rot_pred = None
            rot90_pred = None
        pred_dict[img] = [np.argmax(orig_prob), np.argmax(prbs), pred_angle, rot_pred, rot90_pred]
    return pred_dict


def run_inference(
        image_batch: Dict[str, np.ndarray], model: Model, rotation: bool = False
) -> Union[Dict[str, float], Dict[str, List[Union[int, Optional[str], Optional[int]]]]]:
    """
    Runs inference on a batch of images.

    :param image_batch: The batch of images to run inference on in the form of a dictionary.
    :param model: The model to use for inference.
    :param rotation: Whether or not to run step 2 for rotation inference, defaults to False.
    :return:
    :rtype:
    """
    try:
        pred = model.predict(np.array(list(image_batch.values()))[..., np.newaxis])
    except Exception as e:
        logging.error(f'Error running inference on {image_batch.keys()}: {e}')
        raise e
    if rotation:
        return make_rotation_prediction(image_batch, model, pred)
    else:
        return {img: p[0] for img, p in zip(image_batch.keys(), pred)}


def run_pipeline_ocr(image_path: str, config: Dict[str, Any], text_mask: np.ndarray) -> Dict[str, List[str]]:
    """
    Runs the OCR pipeline on a single image.

    :param image_path: The path to the image.
    :param config: The configuration dictionary.
    :param text_mask: The text mask for the image.
    :return: The OCR predictions for the image.
    """
    im_path = Path(image_path)
    lang = config['ocr']['lang']
    method = config['ocr']['method']
    words_file = config['ocr']['words_file']
    patterns_file = config['ocr']['patterns_file']

    if method == 'python':
        return {im_path.name: list(python_ocr_pipeline(im_path, lang, words_file, patterns_file, arr=text_mask))}
    elif method == 'subprocess':
        return {im_path.name: list(
            ocr_subprocess(im_path, lang=lang, words_file=words_file, patterns_file=patterns_file, arr=text_mask))}
    else:
        python_anotations = python_ocr_pipeline(im_path, lang, words_file, patterns_file, text_mask)
        subprocess_annotations = ocr_subprocess(im_path, lang=lang, words_file=words_file,
                                                patterns_file=patterns_file, arr=text_mask)
        return {im_path.name: list(python_anotations.union(subprocess_annotations))}


def read_annotations(
        image_path: str, conf: Dict[str, Any],
        inv_predictions: Dict[str, List[Union[float, str]]]
) -> Dict[str, List[str]]:
    """
    Reads the annotations for a single image.

    :param image_path: The path to the image.
    :param conf: The configuration dictionary.
    :param inv_predictions: The grayscale inversion predictions for the image.
    :return: The OCR annotations for the image.
    """
    image, no_luts_image = prep_image_for_inpainting(image_path, conf, inv_predictions)
    mask = inpaint_cxr(image, no_luts_image, method='extract_text')
    return run_pipeline_ocr(image_path, conf, mask)


def select_dictionary(config: Dict[str, Any]) -> Dict[str, Any]:
    """
    Selects the dictionary to use for OCR.

    :param config: The configuration dictionary.
    :return: The dictionary to use for OCR.
    """
    try:
        with open_text('auto_qc', 'dict_table.csv') as f:
            dict_df = pd.read_csv(f, dtype=str)
    except Exception as e:
        logging.error(f'Error reading dictionary table: {e}')
        raise e
    combined_dict, eng_dict, spa_dict, ita_dict, port_dict = create_dictionaries(dict_df)
    if config['ocr']['lang'] == 'eng':
        return eng_dict
    elif config['ocr']['lang'] == 'spa':
        return spa_dict
    elif config['ocr']['lang'] == 'ita':
        return ita_dict
    elif config['ocr']['lang'] == 'port':
        return port_dict
    else:
        return combined_dict


def add_high_intensity_preprocessing(
        image_path: str, image_dict: Dict[str, np.ndarray]
) -> Dict[str, np.ndarray]:
    """
    Adds preprocessing to the image to highlight the high intensity objects, e.g. pacemakers, using a sigmoidal filter.

    :param image_path: The path to the image.
    :param image_dict: The dictionary of images.
    :return: A dictionary of images with the preprocessing applied.
    """
    image = image_dict[image_path]
    return {image_path: high_intensity_enhancement(preprocess_image(remove_border(image)))}


def infer_projection(df: pd.DataFrame) -> pd.DataFrame:
    """
    Infers the projection from the patient positioning column of the OCR dataframe. It assumes we can infer portable
    if location given, infer AP if portable, supine or semi-erect, and infer PA if prone.

    :param df: The OCR dataframe.
    :return: The OCR dataframe with the projection column inferred.
    """
    df.loc[df.PORTABLE.notna() & (df.PROJECTION.isna() | (
            df.PROJECTION.str.contains("PA") & df.PROJECTION.str.contains("AP"))), "PROJECTION"] = "AP"
    df = df.fillna(".")
    df.loc[(df['PATIENT POSITIONING'].str.contains("SEMI-ERECT")) & ((df.PROJECTION == ".") | (
            df.PROJECTION.str.contains("PA") & df.PROJECTION.str.contains("AP"))), "PROJECTION"] = "AP"
    df.loc[(df['PATIENT POSITIONING'].str.contains("SUPINE")) & ((df.PROJECTION == ".") | (
            df.PROJECTION.str.contains("PA") & df.PROJECTION.str.contains("AP"))), "PROJECTION"] = "AP"
    df.loc[(df['PATIENT POSITIONING'].str.contains("PRONE")) & ((df.PROJECTION == ".") | (
            df.PROJECTION.str.contains("PA") & df.PROJECTION.str.contains("AP"))), "PROJECTION"] = "PA"
    df = df.replace(".", np.nan)
    return df


def calculate_quality(
        image_path: str, image: np.ndarray, quality_config: Dict[str, Any] = None
):
    """
    Calculates the quality metrics for an image.

    :param image_path: Image identifier, in this case the path to the image.
    :param image: The image array.
    :param quality_config: The quality configuration dictionary.
    :return: A dictionary of quality metrics for the image.
    """
    if quality_config is None:
        quality_config = QUALITY_CONFIG
    quality_metrics = list(quality(image_path, metrics_to_calculate=QUALITY_METRICS, image_array=image).values())[0]
    quality_metrics["outlier_score"] = 0
    for metric in ['niqe', 'brisque', 'contrast_ratio', 'entropy', 'sharpness', 'skewness']:
        metric_sensitivity = quality_config[f"{metric}_combined"]['sensitivity']
        if metric_sensitivity and metric_sensitivity != "none":
            result = quality_metrics[f"{metric}"] + quality_metrics[f"{metric}_cutout50"]
            quality_metrics[f"{metric}_combined"] = result
            bounds = QUALITY_BOUNDS[f"{metric}_combined"][metric_sensitivity]
            partition_to_remove = quality_config[f"{metric}_combined"]['partition']
            if partition_to_remove == "both" and (result < bounds[0] or result > bounds[1]):
                quality_metrics["outlier_score"] += 1
            elif partition_to_remove == "upper" and result > bounds[1]:
                quality_metrics["outlier_score"] += 1
            elif partition_to_remove == "lower" and result < bounds[0]:
                quality_metrics["outlier_score"] += 1
    return quality_metrics


def create_ocr_dataframe(ocr_predictions) -> pd.DataFrame:
    """
    Creates a dataframe from the OCR predictions.

    :param ocr_predictions: The OCR predictions.
    :return: The OCR dataframe.
    """
    results_df = populate_category_df(ocr_predictions, OUTPUT_CATEGORIES)
    results_df = infer_projection(results_df)
    results_df = results_df.reset_index().rename(columns={'name': 'input_path'})
    return results_df


def check_ocr_words_and_patterns(config: Dict[str, Any]) -> Dict[str, Any]:
    """
    Checks the OCR words and patterns files are present, and if not, uses the default files.

    :param config: The configuration dictionary.
    :return: The configuration dictionary with the OCR words and patterns files.
    """
    for ocr_file in ['words_file', 'patterns_file']:
        if not config['ocr'][ocr_file]:
            config['ocr'][ocr_file] = os.path.join(
                os.path.dirname(SCRIPT_DIR), "operations", 'ocr_files',
                f'{config["ocr"]["lang"]}.user-{ocr_file.split("_")[0]}'
            )
    return config


def create_predictions_dataframe(
        inv_predictions: Dict[str, List[Union[float, str]]],
        lat_predictions: Dict[str, List[Union[float, str]]],
        ar_predictions: Dict[str, List[Union[float, str]]],
        rot_predictions: Dict[str, List[Union[int, Optional[str], Optional[int]]]],
        proj_predictions: Dict[str, List[Union[float, str]]],
        ppm_predictions: Dict[str, List[Union[float, str]]],
        ocr_df: pd.DataFrame,
        quality_df: pd.DataFrame,
        outcome_res: Dict[str, str],
) -> pd.DataFrame:
    """
    Creates a dataframe from the predictions.

    :param inv_predictions: The grayscale inversion predictions.
    :param lat_predictions: The lateral projection predictions.
    :param ar_predictions: The aspect ratios.
    :param rot_predictions: The rotation predictions.
    :param proj_predictions: The frontal projection predictions.
    :param ppm_predictions: The pacemaker predictions.
    :param ocr_df: The OCR dataframe.
    :param quality_df: The quality dataframe.
    :param outcome_res: The outcome results.
    :return: The predictions dataframe.
    """

    # Helper function to split dictionary values into separate columns
    def _split_dict_values(predictions, col_names):
        df = pd.DataFrame(predictions.items(), columns=['input_path', 'values'])
        df[col_names] = pd.DataFrame(df['values'].tolist(), index=df.index)
        df.drop(columns='values', inplace=True)
        return df

    inv_df = _split_dict_values(inv_predictions, ['inv_prob', 'inv_pred'])
    lat_df = _split_dict_values(lat_predictions, ['lat_prob', 'lat_pred'])
    ar_df = _split_dict_values(ar_predictions, ['ar_prob', 'ar_pred'])
    proj_df = _split_dict_values(proj_predictions, ['proj_dl_prob', 'proj_dl_pred'])
    ppm_df = _split_dict_values(ppm_predictions, ['ppm_prob', 'ppm_pred'])
    rot_df = _split_dict_values(rot_predictions,
                                ['orig_prob_argmax', 'prbs_argmax', 'pred_angle', 'rot_pred', 'rot90_pred'])

    # Merging all dataframes on the image_filename column
    merged_df = pd.merge(inv_df, lat_df, on='input_path', how='outer')
    merged_df = pd.merge(merged_df, ar_df, on='input_path', how='outer')
    merged_df = pd.merge(merged_df, rot_df, on='input_path', how='outer')
    merged_df = pd.merge(merged_df, proj_df, on='input_path', how='outer')
    merged_df = pd.merge(merged_df, ppm_df, on='input_path', how='outer')

    # Merge quality and OCR projection predictions
    merged_df = pd.merge(merged_df, quality_df[['input_path', 'outlier_score']], on='input_path', how='left')
    if "PROJECTION" in ocr_df.columns:
        merged_df = pd.merge(merged_df, ocr_df[['input_path', 'PROJECTION']], on='input_path', how='left')
    final_df = merged_df.rename(columns={'PROJECTION': 'proj_ocr_pred', 'outlier_score': 'quality_outlier_score'})

    final_df['outcome'] = final_df.input_path.map(outcome_res)

    return final_df


def make_directories(config: Dict[str, Any]):
    """
    Creates the output directories if they do not exist.

    :param config: The configuration dictionary.
    """
    try:
        for directory in ['image_output_dir', 'image_review_dir']:
            config[directory].mkdir(exist_ok=True, parents=True)
        config['label_output_file'].parent.mkdir(exist_ok=True, parents=True)
    except PermissionError:
        logging.error('Permission denied to create output directories. Please check your permissions and try again.')
        raise PermissionError('Permission denied to create output directories. '
                              'Please check your permissions and try again.')


def get_full_checkpoint_directories(config: Dict[str, Any]) -> Dict[str, Any]:
    """
    Returns the full checkpoint directories.

    :param config: The configuration dictionary.
    :return: The configuration dictionary with the full checkpoint directories.
    """
    if config['checkpoints']['checkpoint_dir']:
        for key, value in config['checkpoints'].items():
            config['checkpoints'][key] = os.path.join(
                config['checkpoints']['checkpoint_dir'], value
            ) if key != 'checkpoint_dir' else value
    return config


def get_quality_config(config) -> Dict[str, Dict[str, str]]:
    """
    Returns the quality configuration dictionary with the quality tool sensitivities added using the
    default QUALITY_CONFIG.

    :param config: The configuration dictionary.
    :return: The quality configuration dictionary.
    """
    for metric in QUALITY_CONFIG:
        QUALITY_CONFIG[metric]['sensitivity'] = config['quality'][metric.replace("_combined", "")]
    return QUALITY_CONFIG


def assign_prediction_string(
        tool: str, preds: Dict[str, Union[float, List[Union[float, str]]]], config: Dict[str, Any]
) -> Dict[str, List[Union[float, str]]]:
    """
    Assigns the prediction string to the predictions dictionary.

    :param tool: The tool (e.g. inversion).
    :param preds: The predictions dictionary from model inference.
    :param config: The configuration dictionary.
    :return: The predictions dictionary with the prediction string.
    """
    _label_map = {
        "inversion": ["inverted", "not_inverted"],
        "lateral": ["lateral", "not_lateral"],
        "projection": ["ap", "pa"],
        "pacemaker": ["pacemaker", "no_pacemaker"],
    }
    for key, value in preds.items():
        positive = True if value >= config['thresholds'][tool] else False
        preds[key] = [value, _label_map[tool][0]] if positive else [value, _label_map[tool][1]]
    return preds


def run_stage1(config: Dict[str, Any]) -> Dict[str, List[Union[float, str]]]:
    """
    Runs the first stage of the AutoQC pipeline, which includes the following steps:
    1. Apply pixel intensity transforms to the images.
    2. Run inference on batches of images using the grayscale inversion model.

    :param config: A configuration dictionary containing:
        - image_list: A list of image paths to run inference on.
        - batch_size: The number of images to run inference on at a time.
        - checkpoint: A dictionary with the key = 'inversion' and value = the path to the grayscale inversion model
            checkpoint.
        - luts_applied: A boolean indicating whether or not LUTs have already been applied to the images.
        - json_dir: The path to the directory containing the JSON files for the images (optional).
        - threshold: A dictionary with the key = 'inversion' and value = the model threshold (float).
    :return: The preprocessed images and the grayscale inversion predictions.
    """
    logging.info("Beginning stage 1 of AutoQC pipeline...")
    pp_images = execute_on_device(
        apply_pixel_intensity_transforms, config['image_list'], config, "pixel intensity transforms",
        gpu=False, luts_applied=config['luts_applied'], preprocess=True, json_dir=config['json_dir'], )

    batches = create_batches(pp_images, config['batch_size'])
    num_batches = math.ceil(len(pp_images) / config['batch_size'])

    model = load_model(config['checkpoints']['inversion'])
    inv_predictions = execute_on_device(run_inference, batches, config, "inversion inference",
                                        total=num_batches, model=model, gpu=True)
    inv_predictions = assign_prediction_string('inversion', inv_predictions, config)

    logging.info("Completed stage 1.")

    return inv_predictions


def run_stage2(
        config: Dict[str, Any], inv_predictions: Dict[str, List[Union[float, str]]]
) -> Tuple[Dict[str, np.ndarray], Dict[str, List[Union[float, str]]], Dict[str, List[Union[float, str]]]]:
    """
    Runs the second stage of the AutoQC pipeline, which includes the following steps:
    1. Remove annotations.
    2. Remove the image border / padding.
    3. Run inference on batches of images using the lateral projection model.
    4. Calculate the aspect ratio and compare to the set bounds.

    :param config: The configuration dictionary.
    :param inv_predictions: The grayscale inversion predictions.
    :return: The preprocessed images, the lateral projection predictions, and the aspect ratio outcomes.
    """

    logging.info("Beginning stage 2...")

    # Remove annotations and border then preprocess
    pp_images = execute_on_device(remove_annotations_and_preprocess, config['image_list'], config,
                                  "to remove annotations and border", inv_predictions=inv_predictions,
                                  conf=config, gpu=False)

    # Create batches
    batches = create_batches(pp_images, config['batch_size'])
    num_batches = math.ceil(len(pp_images) / config['batch_size'])

    # Lateral projection inference
    model = load_model(config['checkpoints']['lateral'])
    lat_predictions = execute_on_device(run_inference, batches, config, "lateral projection inference",
                                        total=num_batches, model=model, gpu=True)
    lat_predictions = assign_prediction_string('lateral', lat_predictions, config)

    # Aspect ratio calculation
    ar_predictions = execute_on_device(calculate_aspect_ratio, list(pp_images.keys()), config,
                                       "aspect ratio calculation", image_dict=pp_images, conf=config,
                                       gpu=False)

    logging.info("Completed stage 2.")

    return pp_images, lat_predictions, ar_predictions


def run_stage3(
        config: Dict[str, Any], pp_images: Dict[str, np.ndarray]
) -> Tuple[Dict[str, np.ndarray], Dict[str, List[Union[int, Optional[str], Optional[int]]]]]:
    """
    Runs the third stage of the AutoQC pipeline, which includes the following steps:
    1. Apply high intensity preprocessing to the images.
    2. Run inference on batches of images using the rotation model.
    3. Correct the rotation of the preprocessed images.

    :param config: The configuration dictionary.
    :param pp_images: The preprocessed images.
    :return: The preprocessed images and the rotation predictions.
    """

    logging.info("Beginning stage 3...")

    high_int = execute_on_device(add_high_intensity_preprocessing, list(pp_images.keys()), config,
                                 "high itensity preprocessing", gpu=False, image_dict=pp_images)

    # Create batches
    batches = create_batches(high_int, config['batch_size'])
    num_batches = math.ceil(len(high_int) / config['batch_size'])

    # Rotation inference
    model = load_model(config['checkpoints']['rotation'])
    rot_predictions = execute_on_device(run_inference, batches, config, "rotation inference",
                                        total=num_batches, gpu=True, model=model, rotation=True)
    del high_int
    gc.collect()

    # Correct rotation
    pp_images = execute_on_device(correct_rotation, list(pp_images.keys()), config, "rotation correction",
                                  gpu=False, image_dict=pp_images, rotation_predictions=rot_predictions)

    logging.info("Completed stage 3.")

    return pp_images, rot_predictions


def run_stage4(
        config: Dict[str, Any], pp_images: Dict[str, np.ndarray], inv_predictions: Dict[str, List[Union[float, str]]]
) -> Tuple[Dict[str, List[Union[float, str]]], Dict[str, List[Union[float, str]]], Dict[str, List[Union[float, str]]]]:
    """
    Runs the fourth stage of the AutoQC pipeline, which includes the following steps:
    1. Run inference on batches of images using the frontal projection model.
    2. Run inference on batches of images using the pacemaker model.
    3. Run the OCR tool on the images.
    N.B. As quality is calculated using the original image, this is performed in the next stage to reduce the number of
        times the original image is loaded.

    :param config: The configuration dictionary.
    :param pp_images: The preprocessed images.
    :param inv_predictions: The grayscale inversion predictions.
    :return: The frontal projection predictions, the pacemaker predictions, and the OCR outputs.
        Dict[str, List[Union[float, str]]]]
    """

    logging.info("Beginning stage 4...")

    num_batches = math.ceil(len(pp_images) / config['batch_size'])

    # Projection inference
    batches = create_batches(pp_images, config['batch_size'])
    model = load_model(config['checkpoints']['projection'])
    proj_predictions = execute_on_device(run_inference, batches, config, "projection inference",
                                         total=num_batches, model=model, gpu=True)
    proj_predictions = assign_prediction_string('projection', proj_predictions, config)

    # Pacemaker inference
    batches = create_batches(pp_images, config['batch_size'])
    model = load_model(config['checkpoints']['pacemaker'])
    ppm_predictions = execute_on_device(run_inference, batches, config, "pacemaker inference",
                                        total=num_batches, model=model, gpu=True)
    ppm_predictions = assign_prediction_string('pacemaker', ppm_predictions, config)

    # Run OCR and clean outputs
    try:
        config = check_ocr_words_and_patterns(config)
        ocr_dict = execute_on_device(read_annotations, list(pp_images.keys()), config, "OCR",
                                     inv_predictions=inv_predictions, conf=config, gpu=False, multithread=False)
        selected_dict = select_dictionary(config)
        cleaned_ocr = clean_data(ocr_dict)
        ocr_out = find_terms(cleaned_ocr, selected_dict)
    except pytesseract.pytesseract.TesseractNotFoundError:
        logging.error("Tesseract not found, skipping OCR...")
        ocr_out = {}
    except pytesseract.pytesseract.TesseractError as e:
        logging.error("Tesseract not failed, skipping OCR. Error: %s", e)
        ocr_out = {}

    logging.info("Completed stage 4.")

    return proj_predictions, ppm_predictions, ocr_out


def handle_prediction(
        image_path: str,
        final_image: np.ndarray,
        prediction: str,
        prediction_type: str,
        config: Dict[str, Any],
        image_outcome: str
) -> str:
    """
    Handles adverse outcomes for an image, either by saving it to the review directory or by excluding it.

    :param image_path: The path to the original image.
    :param final_image: The final, output image.
    :param prediction: The tool output/prediction string.
    :param prediction_type: The type of prediction, i.e. 'aspect_ratio', 'lateral' or 'quality'.
    :param config: The configuration dictionary.
    :param image_outcome: The current outcome for the image.
    :return: The updated outcome for the image.
    """
    if prediction == prediction_type and config['exclude_or_review'][prediction_type] == 'review':
        outpath = Path(config['image_review_dir']) / prediction_type / Path(image_path).with_suffix('.png').name
        outpath.parent.mkdir(parents=True, exist_ok=True)
        try:
            iio.imsave(outpath, final_image.astype(np.uint16))
        except Exception as e:
            logging.error(f"Failed to save image {image_path} to review directory:", e)
        image_outcome = 'review'
    elif prediction == prediction_type and config['exclude_or_review'][prediction_type] == 'exclude':
        image_outcome = 'excluded'
    return image_outcome


def process_image_and_get_quality(
        image_path: str,
        conf: Dict[str, Any],
        quality_config: Dict[str, Dict[str, str]],
        inv_predictions: Dict[str, List[Union[float, str]]],
        lat_predictions: Dict[str, List[Union[float, str]]],
        ar_predictions: Dict[str, List[Union[float, str]]],
        rot_predictions: Dict[str, List[Union[int, Optional[str], Optional[int]]]]
) -> Dict[str, Tuple[str, Dict[str, List[Union[float, str]]]]]:
    """
    Processes the final/output image using the pipeline outputs and returns the quality score.

    :param image_path: The path to the original image.
    :param conf: The configuration dictionary.
    :param quality_config: The quality configuration dictionary.
    :param inv_predictions: The grayscale inversion predictions.
    :param lat_predictions: The lateral projection predictions.
    :param ar_predictions: The aspect ratio predictions.
    :param rot_predictions: The rotation predictions.
    :return: A dictionary containing the quality score and the image outcome.
    """
    image_outcome = 'included'

    if conf['options']['save_stage2_intermediate']:
        intermed_path = Path(conf['image_review_dir']) / 'stage2' / Path(image_path).with_suffix('.png').name
        inpainted_img = iio.imread(intermed_path)
        if not conf['options']['keep_stage2_intermediate']:
            intermed_path.unlink()
    else:
        annots_removed = remove_annotations(image_path, conf, inv_predictions)
        inpainted_img = remove_border(annots_removed)

    final_image = list(correct_rotation(image_path, {image_path: inpainted_img}, rot_predictions).values())[0]

    quality_results = calculate_quality(image_path, final_image, quality_config)

    image_outcome = handle_prediction(image_path, final_image, lat_predictions[image_path][1], 'lateral',
                                      conf, image_outcome)
    if image_outcome == 'excluded':
        return {image_path: (image_outcome, quality_results)}

    ar_outcome = 'aspect_ratio' if ar_predictions[image_path][1] == 'remove' else 'keep'
    image_outcome = handle_prediction(image_path, final_image, ar_outcome, 'aspect_ratio',
                                      conf, image_outcome)
    if image_outcome == 'excluded':
        return {image_path: (image_outcome, quality_results)}

    quality_outcome = 'quality' if quality_results['outlier_score'] > 0 else 'keep'
    image_outcome = handle_prediction(image_path, final_image, quality_outcome, 'quality',
                                      conf, image_outcome)
    if image_outcome == 'excluded':
        return {image_path: (image_outcome, quality_results)}

    if image_outcome == 'included':
        outpath = Path(conf['image_output_dir']) / Path(image_path).with_suffix('.png').name
        outpath.parent.mkdir(parents=True, exist_ok=True)
        iio.imsave(outpath, final_image.astype(np.uint16))

    return {image_path: (image_outcome, quality_results)}


def process_original_images_and_run_quality(
        config, quality_config, inv_predictions, lat_predictions, ar_predictions, rot_predictions
) -> Tuple[Dict[str, str], Dict[str, Dict[str, Union[int, float]]]]:
    """
    Processes the original images using the pipeline outputs and runs the quality pipeline.

    :param config: The configuration dictionary.
    :param quality_config: The quality configuration dictionary.
    :param inv_predictions: The grayscale inversion predictions.
    :param lat_predictions: The lateral projection predictions.
    :param ar_predictions: The aspect ratio predictions.
    :param rot_predictions: The rotation predictions.
    :return: The image outcomes and the quality results.
    """

    outcome_quality_dict = execute_on_device(process_image_and_get_quality, config['image_list'], config,
                                             "final images", conf=config, gpu=False, multithread=False,
                                             quality_config=quality_config,
                                             inv_predictions=inv_predictions, lat_predictions=lat_predictions,
                                             ar_predictions=ar_predictions, rot_predictions=rot_predictions)
    outcomes = {}
    quality_results = {}
    for key, value in outcome_quality_dict.items():
        outcomes[key] = value[0]
        quality_results[key] = value[1]
    return outcomes, quality_results


def remove_intermediate_directory(config: Dict[str, Any]):
    """
    Removes the intermediate stage 2 directory if the user has selected not to keep it.

    :param config: The configuration dictionary.
    """
    if not config['options']['keep_stage2_intermediate'] and (Path(config['image_review_dir']) / 'stage2').exists():
        try:
            shutil.rmtree(Path(config['image_review_dir']) / 'stage2')
        except FileNotFoundError:
            logging.warning("Stage 2 directory not found, skipping removal.")
        except PermissionError:
            logging.error("Permission denied while removing stage 2 directory.")


def create_and_save_dataframes(
        config: Dict[str, Any],
        inv_predictions: Dict[str, List[Union[float, str]]],
        lat_predictions: Dict[str, List[Union[float, str]]],
        ar_predictions: Dict[str, List[Union[float, str]]],
        rot_predictions: Dict[str, List[Union[int, Optional[str], Optional[int]]]],
        proj_predictions: Dict[str, List[Union[float, str]]],
        ppm_predictions: Dict[str, List[Union[float, str]]],
        ocr_predictions: Dict[str, Any],
        outcome_results: Dict[str, str],
        quality_results: Dict[str, Dict[str, Union[int, float]]]
):
    """
    Creates and saves the dataframes for the predictions and outcomes.

    :param config: The configuration dictionary.
    :param inv_predictions: The grayscale inversion predictions.
    :param lat_predictions: The lateral projection predictions.
    :param ar_predictions: The aspect ratio predictions.
    :param rot_predictions: The rotation predictions.
    :param proj_predictions: The frontal projection predictions.
    :param ppm_predictions: The pacemaker predictions.
    :param ocr_predictions: The OCR predictions.
    :param outcome_results: The image outcomes.
    :param quality_results: The quality results.
    """
    logging.info("Creating and saving output dataframes...")

    quality_df = pd.DataFrame.from_dict(quality_results, orient='index')
    quality_df = quality_df.reset_index().rename(columns={'index': 'input_path'})
    try:
        quality_df.to_csv(
            config['label_output_file'].parent / ("quality_metrics_" + str(config['label_output_file'].name)),
            index=False
        )
    except Exception as e:
        logging.error(f"Failed to save quality metrics dataframe: {str(e)}")

    if ocr_predictions:
        ocr_df = create_ocr_dataframe(ocr_predictions)
        try:
            ocr_df.to_csv(
                config['label_output_file'].parent / ("ocr_" + str(config['label_output_file'].name)), index=False
            )
        except Exception as e:
            logging.error(f"Failed to save ocr dataframe: {str(e)}")
    else:
        ocr_df = pd.DataFrame()

    preds_df = create_predictions_dataframe(
        inv_predictions, lat_predictions, ar_predictions, rot_predictions,
        proj_predictions, ppm_predictions, ocr_df, quality_df, outcome_results
    )
    try:
        preds_df.to_csv(config['label_output_file'], index=False)
    except Exception as e:
        logging.error(f"Failed to save QC output dataframe: {str(e)}")


def convert_args_to_config(
        args: Namespace
) -> Dict[str, Union[str, float, int, bool, Path, List[Any], Tuple[Any], Dict[str, Any]]]:
    """
    Converts the command line arguments to a configuration dictionary.

    :param args: The command line arguments.
    :return: The configuration dictionary.
    """
    try:
        return {
            'dataset': args.dataset,
            'image_list': args.img_path if isinstance(args.img_path, list) else [args.img_path],
            'json_dir': args.json_dir,
            'label_output_file': args.outpath / 'labels.csv', 'image_output_dir': args.outpath / 'qc_out',
            'image_review_dir': args.outpath / 'qc_review', 'use_gpu': args.gpu, 'cpu_workers': args.cpu_workers,
            'batch_size': args.batch_size, 'luts_applied': args.luts_applied, 'aspect_ratio_bounds': (0.85, 1.48),
            'quality': {
                'brisque': 'low',
                'niqe': 'none',
                'contrast_ratio': 'high',
                'entropy': 'high',
                'sharpness': 'med',
                'skewness': 'med',
            }, 'checkpoints': {
                "checkpoint_dir": args.checkpoint_dir,
                "inversion": "checkpoint_inversion.h5",
                "lateral": "checkpoint_lateral.h5",
                "rotation": "checkpoint_rotation.h5",
                "projection": "checkpoint_projection.h5",
                "pacemaker": "checkpoint_pacemaker.h5",
            }, 'thresholds': {
                "inversion": 0.48,
                "lateral": 0.36,
                "projection": 0.46,
                "pacemaker": 0.51,
            }, 'exclude_or_review': {
                'lateral': args.exclude_or_review,
                'aspect_ratio': args.exclude_or_review,
                'quality': args.exclude_or_review,
            }, "ocr": {
                'lang': 'eng',
                'method': 'combined',
                'words_file': None,
                'patterns_file': None,
            }, "options": {
                'autowindow_instead_of_luts': args.autowindow_instead_of_luts,
                'autowindow_if_no_luts': args.autowindow_if_no_luts,
                'correct_inversion': args.correct_inversion,
                'correct_rotation': args.correct_rotation,
                'text_removal_method': 'telea',
                'll_mask': True,
                'save_stage2_intermediate': args.save_stage2,
                'keep_stage2_intermediate': args.keep_stage2,
            }, "logging": {
                'log_file': args.log_file,
                'level': args.log_level,
            }
        }
    except Exception as e:  # Replace with actual exceptions that might occur
        raise e


def qc_pipeline(
        config: Dict[str, Any]
) -> Tuple[
    Dict[str, Any], Dict[str, Any], Dict[str, Any], Dict[str, Any], Dict[str, Any], Dict[str, Any], Dict[str, Any]
]:
    """
    Runs the entire QC pipeline.

    :param config: The configuration dictionary.
    :return: The predictions from each stage of the pipeline.
        Dict[str, Any], Dict[str, Any]]
    """
    try:
        inv_predictions = run_stage1(config)
        pp_images, lat_predictions, ar_predictions = run_stage2(config, inv_predictions)
        pp_images, rot_predictions = run_stage3(config, pp_images)
        proj_predictions, ppm_predictions, ocr_predictions = run_stage4(config, pp_images, inv_predictions)
        return inv_predictions, lat_predictions, ar_predictions, rot_predictions, \
            proj_predictions, ppm_predictions, ocr_predictions
    except Exception as e:
        logging.error(f"An error occurred in the QC pipeline: {str(e)}")
        raise e


def setup_config(
        args: Namespace
) -> Dict[str, Union[str, float, int, bool, Path, List[Any], Tuple[Any], Dict[str, Any]]]:
    """
    Sets up the configuration dictionary.

    :param args: The command line arguments.
    :return: The configuration dictionary.
    """
    if args.config:
        # config = vars(read_config(args.config, schema_run))
        config = read_config(args.config, schema_run)
        config['image_list'] = [
            os.path.join(config['data_dir'], f) for f in os.listdir(config['data_dir']) if f.endswith(
                ('.png', '.jpg', '.jpeg', '.gif', '.bmp', '.tiff', '.tif', '.dcm', '.dicom')
            )
        ]
    else:
        config = convert_args_to_config(args)
    return config


def setup_logging(config: Dict[str, Any]):
    """
    Sets up the logger based on the configuration.

    :param config: The configuration dictionary.
    """
    log_level = getattr(logging, config['logging']['level'].upper(), logging.INFO)
    logging.basicConfig(level=log_level, format='%(asctime)s - %(levelname)s - %(message)s')
    logger = logging.getLogger()

    log_file_path = config['logging'].get('file_path')
    if log_file_path:
        file_handler = logging.FileHandler(log_file_path)
        logger.addHandler(file_handler)

    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    for handler in logger.handlers[:]:
        handler.setFormatter(formatter)


def run_pipeline(args: Namespace):
    """
    Runs the entire pipeline using the command line arguments.

    :param args: The command line arguments.
    """
    try:
        config = setup_config(args)
        setup_logging(config)
        make_directories(config)
        config = get_full_checkpoint_directories(config)
        quality_config = get_quality_config(config)

        if config['use_gpu']:
            check_gpu()
        else:
            os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

        inv_preds, lat_preds, ar_preds, rot_preds, proj_preds, ppm_preds, ocr_preds = qc_pipeline(config)
        outcome_res, quality_res = process_original_images_and_run_quality(config, quality_config,
                                                                           inv_preds, lat_preds, ar_preds, rot_preds)
        remove_intermediate_directory(config)
        create_and_save_dataframes(config, inv_preds, lat_preds, ar_preds, rot_preds,
                                   proj_preds, ppm_preds, ocr_preds, outcome_res, quality_res)

        logging.info("Completed QC pipeline!")
    except Exception as e:
        logging.error(f"An unexpected error occurred: {str(e)}")
