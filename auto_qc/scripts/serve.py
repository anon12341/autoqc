"""
serve.py: Server for Loading and Serving Keras Models.

This module, serve.py, provides functionalities to create a server for loading and serving keras models in a
multiprocessing manner. It also defines a pipeline for processing messages using a series of plugins and establishes
a queue for managing message routing between multiple clients and servers.

Main Components:

- load_model: Loads a Keras model from a .h5 file.
- pipeline: Processes a message through a series of plugins and
returns the processed result.
- Server: A server class that processes messages in a separate process using ZeroMQ for
message communication.
- Queue: A queue class that manages message routing between multiple clients and servers,
also using ZeroMQ.
- serve_model: Starts a queue and multiple server processes to serve a model. This module heavily
relies on ZeroMQ for communication between processes and Yapsy for plugin management. The loaded model and pipeline
of plugins can be used to process data sent by clients, which is then returned back to the client. The server can be
run on a specified port and its functioning can be monitored and controlled separately.
"""

import os
import pathlib
from argparse import Namespace
from multiprocessing import Process

import zmq
from yapsy.PluginManager import PluginManager

from ..utils import preprocess_image

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"


def load_model(filename: str):
    """
    Load a keras model from a .h5 file.

    :param filename: The path to the .h5 file of the model
    :returns: The loaded keras model
    """
    import tensorflow as tf

    tf.keras.backend.clear_session()
    model = tf.keras.models.load_model(filename)
    print(f"Model {model.name!r} loaded from {filename!r}")
    return model


def pipeline(message: dict) -> dict:
    """
    Process a message through a series of plugins. The order of processing is determined by
    the "Order" attribute of the plugins. The process stops if a plugin returns a message
    with 'stop' set to True.

    :param message: The initial message to process
    :returns: A dictionary with the final message and a log of the processing
    """
    path = pathlib.Path(__file__).parent.resolve() / "../plugins"

    manager = PluginManager()
    manager.setPluginPlaces([f"{path}"])
    manager.collectPlugins()

    plugins = manager.getAllPlugins()
    plugins_sorted = sorted(
        plugins, key=lambda plugin: int(plugin.details["Core"]["Order"])
    )

    log = []
    res = message
    for plugin in plugins_sorted:
        name = plugin.details["Core"]["Name"]
        res = plugin.plugin_object.run(res, plugin.details)
        log.append({name: {k: v for k, v in res.items() if k not in ["hdr", "array"]}})
        if res["stop"]:
            break
    return {"log": log, "hdr": res["hdr"], "array": res["array"]}


class Server(Process):
    """
    Server class for processing messages in a separate process.

    :param port: Port number for the server to listen on
    :param name: Name for the server process, defaults to ""
    """
    def __init__(self, port: int, name: str = "") -> None:
        super().__init__()
        self.port = port
        # self.model = load_model(modelname)
        self.name = name

    def set_socket(self) -> zmq.Socket:
        """
        Setup a REP (reply) socket for the server to receive messages and send replies.

        :returns: The initialized zmq socket
        """
        socket = self.ctx.socket(zmq.REP)
        socket.connect(f"tcp://localhost:{self.port}")
        return socket

    def process(self, message: dict) -> dict:
        """
        Process a message using the pipeline of plugins.

        :param message: The message to process
        :returns: The processed message
        """
        print("-" * 80)
        res = pipeline(message)
        print("Done")
        return res
        # img = preprocess_image(message)
        # pred = self.model.predict(img)
        # return pred

    def run(self) -> None:
        """
        Main loop for the server. Receives messages from the socket, processes them, and sends the results back.
        """
        print(f"Starting server {self.name}")
        self.ctx = zmq.Context()
        self.socket = self.set_socket()
        while True:
            message = self.socket.recv_pyobj()
            try:
                res = self.process(message)
            except Exception as e:
                raise
                res = -99  # ERROR
            self.socket.send_pyobj(res)


class Queue(Process):
    """
    Queue class for managing message routing between multiple clients and servers.

    :param port_frontend: Port number for the frontend socket to listen on
    :param port_backend: Port number for the backend socket to listen on
    """
    def __init__(self, port_frontend: int, port_backend: int) -> None:
        super().__init__()
        self.port_frontend = port_frontend
        self.port_backend = port_backend

    def set_sockets(self) -> (zmq.Socket, zmq.Socket):
        """
        Set up frontend and backend sockets for the queue.

        :returns: A tuple with the frontend and backend zmq sockets
        """
        # Clients
        frontend = self.ctx.socket(zmq.XREP)
        frontend.bind(f"tcp://*:{self.port_frontend}")
        # Service
        backend = self.ctx.socket(zmq.XREQ)
        backend.bind(f"tcp://*:{self.port_backend}")
        return frontend, backend

    def run(self) -> None:
        """
        Main loop for the queue. Sets up sockets and starts the zmq device.
        """
        print("Starting queue")
        self.ctx = zmq.Context()
        self.frontend, self.backend = self.set_sockets()
        zmq.device(zmq.QUEUE, self.frontend, self.backend)


def serve_model(args: Namespace) -> None:
    """
    Start a queue and multiple server processes to serve a model.

    :param args: Command line arguments, should contain 'port' and 'nservers'
    """
    queue = Queue(args.port, args.port + 1)
    queue.start()
    for n in range(args.nservers):
        server = Server(args.port + 1, name=f":{n+1}")
        server.start()
