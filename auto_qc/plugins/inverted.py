import os
import time
from pathlib import Path

from auto_qc.utils import error_catcher, load_model
from tqdm import tqdm
from yapsy.IPlugin import IPlugin


class Inverted(IPlugin):
    @error_catcher
    def run(self, message, details):
        classname = self.__class__.__name__

        res = {}
        with tqdm(total=100) as pbar:
            pbar.set_description(f"{classname} - loading model")
            pbar.update(10)
            model_filename = details["Core"]["Model"]
            try:
                model = load_model(model_filename)
            except Exception as e:
                print(e)

            pbar.set_description(f"{classname} - running prediction")
            time.sleep(2)
            pbar.update(50)

            pbar.set_description(f"{classname} - prediction done")
            res.update({"model_prediction": "Not_Inverted"})
            pbar.update(100)
            time.sleep(1)

        res.update({"stop": False})
        res.update({"array": message["array"], "hdr": message["hdr"]})
        return res
