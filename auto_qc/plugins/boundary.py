from auto_qc.classification.boundary import classify_boundary
from auto_qc.operations.boundary import remove_border
from auto_qc.utils import error_catcher
from tqdm import tqdm
from yapsy.IPlugin import IPlugin


class Boundary(IPlugin):
    @error_catcher
    def run(self, message, details):
        classname = self.__class__.__name__

        res = {}
        with tqdm(total=100) as pbar:
            pbar.set_description(f"{classname} - calculating image boundary")
            pbar.update(10)
            array = message["array"]
            q = classify_boundary(array)
            pbar.update(50)
            if q > 0.2:
                pbar.set_description(f"{classname} - cropping image")
                pbar.update(60)
                array = message["array"]
                cropped = remove_border(array)
                message["array"] = cropped
            pbar.update(100)

        res.update({"stop": False, "image_boundary": float(q)})
        res.update({"array": message["array"], "hdr": message["hdr"]})
        return res
