import io
import time

import pydicom
from auto_qc.utils import error_catcher
from tqdm import tqdm
from yapsy.IPlugin import IPlugin


class PreProcess(IPlugin):
    @error_catcher
    def run(self, message, details):
        classname = self.__class__.__name__

        with tqdm(total=100) as pbar:
            # Step 1: Read image
            pbar.set_description(f"{classname} - read input")
            image = io.BytesIO(message["image"])
            pbar.update(10)
            # Step 2: Read image headers and array
            pbar.set_description(f"{classname} - decode input")
            ds = pydicom.read_file(image)
            pbar.update(30)
            # Step 3: Prepare image
            pbar.set_description(f"{classname} - prepare image")
            arr = ds.pixel_array
            hdr = {}
            for attr in dir(ds):
                value = getattr(ds, attr)
                if type(value) in [pydicom.multival.MultiValue]:
                    hdr[attr] = list(value)
                elif type(value) in [pydicom.valuerep.DSfloat]:
                    hdr[attr] = float(value)
                elif type(value) in [str, int, float, dict]:
                    hdr[attr] = value
            pbar.update(100)

        return {"array": arr, "hdr": hdr, "status": "OK", "stop": False}
