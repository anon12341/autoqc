"""
quality.py: Image Quality Module for auto_qc

This module contains functions for calculating image quality metrics.
"""

from typing import List, Union, Optional, Tuple, Dict
import imageio.v2 as imageio
import numpy as np
from skimage.util.dtype import dtype_limits
from skimage.util import view_as_blocks
import SimpleITK as sitk
from radiomics import featureextractor
import logging
from argparse import Namespace
from tqdm.auto import tqdm
from concurrent.futures import ProcessPoolExecutor, as_completed
import os
import gc
import json
import cv2
from pywt import wavedec2
from scipy import stats


from ..utils import bytescale, crop_center, MyJsonEncoder
from .live_qa import brisque as brisque_live
from .live_qa import niqe as niqe_live
from ..operations.boundary import remove_border

radiomics_logger = logging.getLogger('radiomics')
radiomics_logger.setLevel(logging.ERROR)


def contrast_ratio(
    img: np.ndarray,
    cutout: bool = False,
    cutout_percent: int = 50,
    lower_percentile: int = 2,
    upper_percentile: int = 98,
) -> float:
    """
    Calculate the image contrast quality.

    :param img: Image array to classify.
    :param cutout: Flag to determine if the center cutout of the image should be used.
    :param cutout_percent: Percent of the image to use for the cutout.
    :param lower_percentile: Lower percentile for contrast stretching.
    :param upper_percentile: Upper percentile for contrast stretching.

    :return: Image quality statistic.
    """
    if cutout:
        img = crop_center(img, percent=cutout_percent)

    if np.max(img) > 255 or np.max(img) <= 1:
        bimg = bytescale(img).astype("uint8")
    else:
        bimg = img.astype("uint8")
    dlimits = dtype_limits(bimg, clip_negative=False)
    limits = np.percentile(bimg, [lower_percentile, upper_percentile])
    ratio = (limits[1] - limits[0]) / (dlimits[1] - dlimits[0])
    return ratio


def brisque(
        img: np.ndarray,
        cutout: bool = False,
        cutout_percent: int = 50,
) -> float:
    """
    Calculate the BRISQUE image quality score using the LIVE python implementation.

    :param img: Image array to classify.
    :param cutout: Flag to determine if the center cutout of the image should be used.
    :param cutout_percent: Percent of the image to use for the cutout.
    :return: Image quality statistic.
    """
    if cutout:
        img = crop_center(img, percent=cutout_percent)
    return brisque_live(img).mean()


def niqe(
    img: np.ndarray,
    cutout: bool = False,
    cutout_percent: int = 50,
) -> float:
    """
    Calculate the NIQE image quality score using the LIVE python implementation.  This utilises the parameters tuned
    for chest x-rays.

    :param img: Image array to classify.
    :param cutout: Flag to determine if the center cutout of the image should be used.
    :param cutout_percent: Percent of the image to use for the cutout.
    :return: Image quality statistic.
    """
    if cutout:
        img = crop_center(img, percent=cutout_percent)
    return niqe_live(img).mean()


def entropy(image: np.ndarray, cutout: bool = False, cutout_percent: Optional[int] = 50) -> float:
    """
    Calculate the entropy of the image.

    :param image: The image to calculate the entropy for.
    :param cutout: Whether to use a cutout of the image.
    :param cutout_percent: The percent of the image to use for the cutout, if cutout is True.
    :return: The entropy of the image.
    """
    params = {'binWidth': 25, 'resampledPixelSpacing': None}

    # Instantiate the extractor
    extractor = featureextractor.RadiomicsFeatureExtractor(**params)

    # Disable all classes
    extractor.disableAllFeatures()

    # Enable desired feature
    extractor.enableFeaturesByName(firstorder=['Entropy'])

    if cutout:
        image = crop_center(image, percent=cutout_percent)

    sitk_image = sitk.GetImageFromArray(image)
    # For the mask, consider the entire image as the ROI
    mask = sitk.GetImageFromArray(np.ones(image.shape, dtype=np.int8))

    # Calculate features
    featureVector = extractor.execute(sitk_image, mask)

    # Access specific features
    entropy = featureVector['original_firstorder_Entropy']

    return entropy


def block_entropy(
        image: np.ndarray, block_size: Tuple[int, int] = (128,128),
        cutout: bool = False, cutout_percent: Optional[int] = 50
) -> float:
    """
    Calculate the average entropy of blocks of the image.  This is performed as entropy is dependent on the size of
    the image and this aims to remove that bias.

    :param image: The image to calculate the entropy for.
    :param block_size: The size of the blocks to divide the image into, default is (128, 128).
    :param cutout: Whether to use a cutout of the image.
    :param cutout_percent: The percent of the image to use for the cutout, if cutout is True.
    :return: The average entropy of the image blocks.
    """
    if cutout:
        image = crop_center(image, percent=cutout_percent)

    # Calculate new dimensions that are multiples of the block size
    new_dims = (image.shape[0] // block_size[0] * block_size[0],
                image.shape[1] // block_size[1] * block_size[1])

    # Crop image to new dimensions
    image = image[:new_dims[0], :new_dims[1]]

    # Divide the image into blocks
    blocks = view_as_blocks(image, block_size)

    # # Flatten the block array into a 2D array where each row is a block
    # blocks = blocks.reshape(blocks.shape[0]*blocks.shape[1], -1)

    # Calculate the entropy for each block
    block_entropies = [entropy(block.reshape(-1, block.shape[-1])) for block in blocks]

    # # Calculate the entropy for each block
    # block_entropies = [entropy(block) for block in blocks]

    # Calculate and return the average entropy
    return float(np.mean(block_entropies))


def fft_contrast(image: np.ndarray, radius: int = 30, high_intensity_threshold: Optional[int] = None):
    """
    Assess the contrast of an image using the Fast Fourier Transform (FFT).

    :param image: The image to assess
    :param radius: The radius of the circle to use for the FFT
    :param high_intensity_threshold: The threshold to use for high-intensity pixels
    :return: The fft contrast of the image
    """
    if high_intensity_threshold is not None:
        # Create a mask of high-intensity pixels
        high_intensity_mask = image > high_intensity_threshold
        low_intensity_mask = image <= 1

        # Exclude high-intensity pixels
        image = np.where(high_intensity_mask, np.median(image), image)
        image = np.where(low_intensity_mask, np.median(image), image)

    # Compute the 2-dimensional discrete Fourier Transform
    f = np.fft.fft2(image)

    # Shift the zero-frequency component to the center of the spectrum
    fshift = np.fft.fftshift(f)

    # Compute the power spectrum (magnitude squared)
    magnitude_spectrum = np.abs(fshift) ** 2

    # Compute total power
    total_power = np.sum(magnitude_spectrum)

    # Create a mask to define the "low frequency" region
    num_rows, num_cols = image.shape
    crow, ccol = num_rows // 2, num_cols // 2
    mask = np.zeros((num_rows, num_cols))
    mask[crow - radius: crow + radius, ccol - radius: ccol + radius] = 1

    # Compute low frequency power
    low_freq_power = np.sum(magnitude_spectrum * mask)

    # Compute high frequency power
    high_freq_power = total_power - low_freq_power

    # Compute the ratio of high-frequency power to total power
    fft_contrast = high_freq_power / total_power

    return fft_contrast


def mean_intensity(image: np.ndarray) -> float:
    """
    Calculate the mean intensity of the image.

    :param image: Image array to calculate the mean intensity of.
    :return: The mean intensity of the image.
    """
    return image.mean()


def std_intensity(image: np.ndarray) -> float:
    """
    Calculate the standard deviation of the image.

    :param image: Image array to calculate the standard deviation of.
    :return: The standard deviation of the image.
    """
    return image.std()


def var_intensity(image: np.ndarray) -> float:
    """
    Calculate the variance of the image.

    :param image: Image array to calculate the variance of.
    :return: The variance of the image.
    """
    return image.var()


def median_intensity(image: np.ndarray) -> float:
    """
    Calculate the median intensity of the image.

    :param image: Image array to calculate the median intensity of.
    :return: The median of the image.
    """
    return float(np.median(image))


def ptp_intensity(image: np.ndarray) -> float:
    """
    Calculate the peak-to-peak intensity of the image.

    :param image: The image to calculate the peak-to-peak intensity of.
    :return: The peak-to-peak intensity of the image.
    """
    return image.ptp()


def sharpness(image: np.ndarray, cutout: bool = False, cutout_percent: int = 50) -> float:
    """
    Estimate the sharpness of an image.

    :param image: The image to estimate the sharpness of.
    :param cutout: Whether to cut out the center of the image before calculating sharpness. Defaults to False.
    :param cutout_percent: The percentage of the image to cut out. Defaults to 50.
    :return: The sharpness of the image.
    """
    if cutout:
        image = crop_center(image, percent=cutout_percent)
    sharpness_measure = cv2.Laplacian(image, cv2.CV_64F).var()
    return sharpness_measure.item()


def noise_estimate(image: np.ndarray, wavelet: str = "haar") -> float:
    """
    Estimate the noise in an image by calculating the median absolute deviation of the detail coefficients of the
    image's wavelet transform.

    :param image: The image to estimate the noise in.
    :param wavelet: The wavelet to use for the noise estimation. Defaults to "haar".
    :return: The noise estimate of the image.
    """
    coeffs = wavedec2(image, wavelet, level=1)
    detail_coeffs = np.concatenate([c.ravel() for c in coeffs[1]])
    noise_std_dev = stats.median_abs_deviation(detail_coeffs)
    return noise_std_dev


def skewness(image: np.ndarray, cutout: bool = False, cutout_percent: int = 50):
    """
    Calculate the skewness of the histogram of an image.

    :param image: The image to calculate the skewness of.
    :param cutout: Whether to cut out the center of the image before calculating skewness. Defaults to False.
    :param cutout_percent: The percentage of the image to cut out. Defaults to 50.
    :return: The skewness of the image.
    """
    if cutout:
        image = crop_center(image, percent=cutout_percent)
    return stats.skew(image.flatten())


def quality(
        image_path: str, metrics_to_calculate: List[str], image_array: np.ndarray = None
) -> Dict[str, Dict[str, float]]:
    """
    Calculate the quality of an image.

    :param image_path: The path to the image to calculate the quality of.
    :param metrics_to_calculate: The metrics to calculate.
    :return: The quality metrics of the image.
    """

    image = imageio.imread(image_path) if image_array is None else image_array
    image = remove_border(image)
    image = bytescale(image)
    quality_metrics = {
        'niqe': niqe(image) if 'niqe' in metrics_to_calculate else None,
        'niqe_cutout50': niqe(
            image, cutout=True, cutout_percent=50
        ) if 'niqe_cutout50' in metrics_to_calculate else None,
        'brisque': brisque(image) if 'brisque' in metrics_to_calculate else None,
        'brisque_cutout50': brisque(
            image, cutout=True, cutout_percent=50
        ) if 'brisque_cutout50' in metrics_to_calculate else None,
        'contrast_ratio': contrast_ratio(image) if 'contrast_ratio' in metrics_to_calculate else None,
        'contrast_ratio_cutout50': contrast_ratio(
            image, cutout=True, cutout_percent=50
        ) if 'contrast_ratio_cutout50' in metrics_to_calculate else None,
        'entropy': block_entropy(image) if 'entropy' in metrics_to_calculate else None,
        'entropy_cutout50': block_entropy(
            image, cutout=True, cutout_percent=50
        ) if 'entropy_cutout50' in metrics_to_calculate else None,
        'mean_intensity': mean_intensity(image) if 'mean_intensity' in metrics_to_calculate else None,
        'median_intensity': median_intensity(image) if 'median_intensity' in metrics_to_calculate else None,
        'std_intensity': std_intensity(image) if 'std_intensity' in metrics_to_calculate else None,
        'var_intensity': var_intensity(image) if 'var_intensity' in metrics_to_calculate else None,
        'fft_contrast': fft_contrast(image) if 'fft_contrast' in metrics_to_calculate else None,
        'sharpness': sharpness(image) if 'sharpness' in metrics_to_calculate else None,
        'sharpness_cutout50': sharpness(
            image, cutout=True, cutout_percent=50
        ) if 'sharpness_cutout50' in metrics_to_calculate else None,
        'x_resolution': image.shape[1] if 'resolution' in metrics_to_calculate else None,
        'y_resolution': image.shape[0] if 'resolution' in metrics_to_calculate else None,
        'noise_estimate': noise_estimate(image) if 'noise_estimate' in metrics_to_calculate else None,
        'skewness': skewness(image) if 'skewness' in metrics_to_calculate else None,
        'skewness_cutout50': skewness(
            image, cutout=True, cutout_percent=50
        ) if 'skewness_cutout50' in metrics_to_calculate else None,
    }
    del image
    gc.collect()
    return {image_path: quality_metrics}


def calculate_quality(args: Namespace):
    """
    Calculate the quality of an image and save the results to a JSON file.
    """
    image_path = args.img_path
    out = args.outpath
    metrics = args.metrics
    nworkers = args.nworkers

    if metrics == "all" or "all" in metrics:
        metrics = [
            'niqe',
            'niqe_cutout50',
            'brisque',
            'brisque_cutout50',
            'contrast_ratio',
            'contrast_ratio_cutout50',
            'entropy',
            'entropy_cutout50',
            'mean_intensity',
            'median_intensity',
            'std_intensity',
            'var_intensity',
            'fft_contrast',
            'sharpness',
            'sharpness_cutout50',
            'resolution',
            'noise_estimate',
            'skewness',
            'skewness_cutout50',
        ]

    results = {}
    executor = None

    if nworkers > 1:
        try:
            with ProcessPoolExecutor(max_workers=nworkers) as executor:
                futures = [
                    executor.submit(
                        quality, file, metrics
                    ) for file in image_path
                ]
                for future in tqdm(as_completed(futures), total=len(futures)):
                    results.update(future.result())
        except KeyboardInterrupt:
            print('KeyboardInterrupt: Stopping quality assessment...')
        except Exception as e:
            print(e)
        finally:
            if executor is not None:
                executor.shutdown(wait=True)

    else:
        for file in image_path:
            print(f'Processing {file}')
            results.update(quality(file, metrics))

    # Save results to JSON
    with open(os.path.join(out, 'quality_metrics.json'), 'w') as outfile:
        json.dump(results, outfile, cls=MyJsonEncoder)

    print(f"Done! Output saved in '{os.path.join(out, 'quality_metrics.json')}'")
