"""
ocr.py: Functions for performing optical character recognition (OCR) on masks of annotations produced from inpaint.py.

The functions in this file are used to perform OCR on masks of annotations produced from inpaint.py. The masks are
converted to images, and then the images are passed to pytesseract for OCR. The OCR results are then saved to a JSON
file.
"""

import numpy as np
import pytesseract
from PIL import Image
import os
from skimage import measure
import subprocess
import tempfile
import imageio.v2 as iio
from concurrent.futures import ThreadPoolExecutor, as_completed
import gc
from argparse import Namespace
from pathlib import Path
from tqdm.auto import tqdm
import json
from typing import Union, List, Dict, Optional, Set, Tuple
import inspect
import warnings
import pandas as pd

warnings.filterwarnings("ignore", category=UserWarning, module="imageio.plugins.pillow")


SCRIPT_DIR = Path(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))

PROJ_REMOVALS = {
    "PA": ["ap", "aP", "Ap"],
    "AP": ["pa", "Pa", "pA"]
}

TO_STRIP = [" ", "[", "]", "{", "}", "'", '"', "`", "~", ",", "_", "£", "$", "&", "^", ".", " "]

CLEAN_DICT = {
    "EREC": "ERECT",
    "AP-ERECT": "AP ERECT",
    "APERECT": "AP ERECT",
    "PAERECT": "PA ERECT",
    "PA-ERECT": "PA ERECT",
    "PAPRONE": "PA PRONE",
    "APSUPINE": "AP SUPINE",
    "APSEMI-ERECT": "AP SEMIERECT",
    "APSEMIERECT": "AP SEMIERECT",
    "AOBILE": "MOBILE",
    "AP-ER": "AP ERECT",
    "SEMLERECE": "SEMIERECT",
    "E REC": "ERECT",
    "RA": "PA",
    "AR": "AP",
    "RPA": "R PA",
    "LPA": "L PA",
    "RAP": "R AP",
    "LAP": "L AP",
    "LERECT": "L ERECT",
    "RERECT": "R ERECT",
    "ERECTL": "ERECT L",
    "ERECTR": "ERECT R",
    "ERECTTT": "ERECT",
    "ERECTT": "ERECT",
    "ERECTTTT": "ERECT",
    "ERECTTTTT": "ERECT",
    "SEMIERECTTT": "SEMI-ERECT",
    "SEMIERECTT": "SEMI-ERECT",
    "SEMIERECTTTT": "SEMI-ERECT",
    "SEMIERECTTTTT": "SEMI-ERECT",
    "SEMI-ERECTTT": "SEMI-ERECT",
    "SEMI-ERECTT": "SEMI-ERECT",
}
clean_dict_lower = {}
for key, value in CLEAN_DICT.items():
    clean_dict_lower[key.lower()] = value
CLEAN_DICT.update(clean_dict_lower)

OUTPUT_CATEGORIES = {
    'DATE OF BIRTH': 'PATIENT INFO',
    'SAU': 'LOCATION',
    'SHOULDER': 'ANATOMY',
    'STRETCHER': 'PATIENT POSITIONING',
    'BEHIND GLASS': 'ISOLATION',
    'RED DOT': 'ABNORMAL FLAG',
    'PAEDIATRIC': 'PATIENT INFO',
    'NEURO ICU': 'LOCATION',
    'CICU': 'LOCATION',
    'PORTABLE': 'PORTABLE',
    'SEMI-SUPINE': 'PATIENT POSITIONING',
    'ANTERIOR': 'ANATOMY',
    'PACU': 'LOCATION',
    'KV': 'TECHNICAL',
    'EXPOSURE': 'TECHNICAL',
    'ABDOMEN': 'ANATOMY',
    'MICU': 'LOCATION',
    'SX': 'ADDITIONAL INFO',
    'ISOLATION': 'ISOLATION',
    'INSPIRATORY': 'TECHNICAL',
    'CHAIR': 'PATIENT POSITIONING',
    'ITU': 'LOCATION',
    'NICU': 'LOCATION',
    'PICU': 'LOCATION',
    'BASES': 'ANATOMY',
    'RIGHT': 'SIDE',
    'PA': 'PROJECTION',
    'BED': 'PATIENT POSITIONING',
    'CSRU': 'LOCATION',
    'PRE-PROCEDURE': 'DATE/TIME',
    'RED FLAG': 'ABNORMAL FLAG',
    'RESUS': 'LOCATION',
    'HDU': 'LOCATION',
    'THORAX': 'ANATOMY',
    'APICAL': 'VIEW',
    'NAME': 'PATIENT INFO',
    'EXPIRATORY': 'TECHNICAL',
    'PRONE': 'PATIENT POSITIONING',
    'SEMI-ERECT': 'PATIENT POSITIONING',
    'NCCU': 'LOCATION',
    'ARTEFACT': 'ABNORMAL FLAG',
    'ED': 'LOCATION',
    'DATE': 'TIME/DATE',
    'WARD': 'LOCATION',
    'ERECT': 'PATIENT POSITIONING',
    'AP': 'PROJECTION',
    'POSTERIOR': 'ANATOMY',
    'VICU': 'LOCATION',
    'POST-PROCEDURE': 'DATE/TIME',
    'THEATRES': 'LOCATION',
    'SN': 'ADDITIONAL INFO',
    'ACU': 'LOCATION',
    'MAS': 'TECHNICAL',
    'ICU': 'LOCATION',
    'LAO': 'VIEW',
    'SICU': 'LOCATION',
    'RAO': 'VIEW',
    'OUTPATIENTS': 'LOCATION',
    'OBLIQUE': 'VIEW',
    'LEFT': 'SIDE',
    'RECOVERY': 'LOCATION',
    'SUPINE': 'PATIENT POSITIONING',
    'CCU': 'LOCATION',
    'STUDY TIME': 'TIME/DATE',
    'REJECT': 'REJECT',
    'IDU': 'LOCATION',
    'RPO': 'LOCATION',
    'IMCU': 'LOCATION',
    'LATERAL': 'PROJECTION',
    'NON COOPERATIVE': 'ADDITIONAL INFO',
    'CR': 'TECHNICAL',
    'TSIU': 'LOCATION',
    'LPO': 'LOCATION',
    'CATH LAB': 'LOCATION',
    'ICCU': 'LOCATION',
    'KVP': 'TECHNICAL',
    'TCU': 'LOCATION',
    'DO NOT USE': 'REJECT',
    'COPY': 'REPEAT/COPY',
    'STUDYTIME': 'TIME/DATE',
    'REPEAT': 'REPEAT/COPY',
    'DX': 'TECHNICAL',
    'PROTOCOL': 'TECHNICAL',
    'HOSPITAL': 'LOCATION'
}


def add_json_extension(outpath: str) -> str:
    """
    Add a .json extension to the outpath if it doesn't already have one.

    :param outpath: the output path
    :return: the output path with a .json extension
    """
    extension = Path(outpath).suffix
    if extension.lower() != ".json":
        outpath = f"{outpath}.json"
    return outpath


def resize_for_ocr(image: Image) -> Image:
    """
    Resize an image to a maximum of 1024 pixels in the longest dimension.

    :param image: The image to resize.
    :return: The resized image.
    """
    length_x, width_y = image.size
    factor = min(1, np.float16(1024 / length_x))
    size = int(factor * length_x), int(factor * width_y)
    image_resize = image.resize(size, Image.LANCZOS)
    return image_resize


def set_image_dpi(image: Image, dpi: int) -> Image:
    """
    Rescaling image to a specific dpi without resizing

    :param image: The image
    :param dpi: The desired dpi
    :return: A rescaled image
    """
    width, height = image.size
    new_width = int(width * dpi / 72)  # 72 is the default dpi for PIL
    new_height = int(height * dpi / 72)
    resized_image = image.resize((new_width, new_height), Image.LANCZOS)
    return resized_image


def add_padding(image: np.ndarray, pad_width: int = 10):
    """
    Add padding to an image.

    :param image: The image to add padding to.
    :param pad_width: The number of pixels to add to the border of the image.
    :return: The padded image.
    """
    return np.pad(image, pad_width=pad_width, mode='constant', constant_values=0)


def find_contour_areas(image: np.ndarray) -> Union[np.ndarray, None]:
    """
    Find the areas of the contours in an image.

    :param image: The image to find the contours in.
    :return: The concatenated contours.
    """
    contours = measure.find_contours(image, ((np.amin(image) + np.amax(image)) / 4))
    if not contours:
        return None
    all_contours = np.empty((0, 2))
    for contour in contours:
        all_contours = np.concatenate((all_contours, contour))
    return all_contours


def trim_to_contours(image: np.ndarray, contour_values: np.ndarray, border: int = 5):
    """
    Trim an image to the bounding box of the contours.

    :param image: The image to be trimmed.
    :param contour_values: The values of the contours.
    :param border: The number of pixels to add to the border of the bounding box.
    :return: The trimmed image.
    """
    if contour_values is None:
        return image
    x0 = int(np.amin(contour_values[:, 0]))
    x0 = x0 - border if x0 - border > 0 else 0
    y0 = int(np.amin(contour_values[:, 1]))
    y0 = y0 - border if y0 - border > 0 else 0
    x1 = int(np.amax(contour_values[:, 0]))
    x1 = x1 + border if x1 + border < image.shape[0] else image.shape[0]
    y1 = int(np.amax(contour_values[:, 1]))
    y1 = y1 + border if y1 + border < image.shape[1] else image.shape[1]
    return image[x0:x1, y0:y1]


def ocr_python(
        image: Image, psm: Union[int, str] = 6, lang: str = 'eng',
        words_file: Union[int, str] = os.path.join(SCRIPT_DIR, 'ocr_files', 'eng.user-words'),
        patterns_file: Union[int, str] = os.path.join(SCRIPT_DIR, 'ocr_files', 'eng.user-patterns'),
        dpi: Optional[Union[int, str]] = None
) -> List[str]:
    """
    Run tesseract OCR on an image using a python methodology, utilising pytesseract.

    :param image: The image to be processed (mask of annotations).
    :param psm: The tessedit page segmentation mode. Default is 6 (Assume a single uniform block of text).
    :param lang: The language to use for OCR. Default is 'eng'.
    :param words_file: The path to the user words file. Default is 'path/to/this/script/eng.user-words'.
    :param patterns_file: The path to the user patterns file. Default is 'path/to/this/script/eng.user-patterns'.
    :param dpi: The desired dpi to set the image to for OCR. Default is None. 300 is recommended by pytesseract.
    :return: List of annotations.
    """
    if dpi is not None:
        rescaled_image = set_image_dpi(image, int(dpi))
    else:
        rescaled_image = resize_for_ocr(image)

    custom_config = f'--psm {psm} --user-words {words_file} --user-patterns {patterns_file} -c load_system_dawg=F ' \
                    f'-c load_freq_dawg=F -c language_model_penalty_non_dict_word=1 -c language_model_penalty_case=0 ' \
                    f'-c language_model_penalty_non_freq_dict_word=0'

    annotations = pytesseract.image_to_string(
        rescaled_image,
        lang=lang,
        config=custom_config
    )

    del image, rescaled_image
    gc.collect()
    return annotations.split('\n')


def ocr_subprocess(
        img_path:  Union[str, Path], cwd:  Union[str, Path] = Path("."), psm:  Union[str, int] = 12,
        lang: str = 'eng', words_file: Union[str, Path] = os.path.join(SCRIPT_DIR, 'ocr_files', 'eng.user-words'),
        patterns_file: Union[str, Path] = os.path.join(SCRIPT_DIR, 'ocr_files', 'eng.user-patterns'),
        arr: np.ndarray = None
) -> Set[str]:
    """
    Run tesseract OCR on an image using a subprocess methodology.  For some reason, this produces different results.

    :param img_path: Path to the image to be processed (mask of annotations).
    :param cwd: The current working directory.
    :param psm: The tessedit page segmentation mode. Default is 12 (Sparse text with OSD).
    :param lang: The language to use for OCR. Default is 'eng'.
    :param words_file: The path to the user words file. Default is 'path/to/this/script/eng.user-words'.
    :param patterns_file: The path to the user patterns file. Default is 'path/to/this/script/eng.user-patterns'.
    :return: Set of annotations.
    """
    image = Image.fromarray(arr) if arr is not None else Image.open(img_path)

    if image.mode != "RGB":
        image = image.convert("RGB")

    with tempfile.NamedTemporaryFile(suffix='.png', delete=False) as temp:
        image.save(temp.name, dpi=(300, 300))

    cmd = [
        'tesseract.exe' if os.name == 'nt' else 'tesseract',
        temp.name,
        "stdout",
        '-l', lang,
        '--psm', str(psm),
        '--user-words', words_file,
        '--user-patterns', patterns_file,
        '-c', 'load_system_dawg=F load_freq_dawg=F language_model_penalty_non_dict_word=0 '
        'language_model_penalty_case=0 language_model_penalty_non_freq_dict_word=0'
    ]

    proc = subprocess.Popen(cmd, cwd=cwd, startupinfo=None, creationflags=0,
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    with proc:
        output, errors = proc.communicate()
        proc.wait()

    output_string = output.decode('utf-8')
    # error_string = errors.decode('utf-8')

    try:
        os.remove(temp.name)
    except OSError:
        pass

    annotations = [item for item in output_string.split("\n") if item]
    return set(annotations)


def python_ocr_pipeline(
        img_path: Union[str, Path], lang: str = 'eng',
        words_file: Union[str, Path] = os.path.join(SCRIPT_DIR, 'ocr_files', 'eng.user-words'),
        patterns_file: Union[str, Path] = os.path.join(SCRIPT_DIR, 'ocr_files', 'eng.user-patterns'),
        arr: np.ndarray = None
) -> Set[str]:
    """
    Pipeline for OCR using pytesseract. This pipeline is used for images with a white background and black text. It
    first finds the contours of the text, then trims the image to the contours, and then performs OCR on the trimmed
    image. The pipeline combines the results of two different page segmentation modes (PSM) of pytesseract with
    different dpi's.

    :param img_path: The path to the image to be processed (mask of annotations)
    :param lang: Tesseract language/s to use for OCR.
    :param words_file: The user words file to use for Tesseract, if any. Defaults to
        'path/to/this/script/eng.user-words'.
    :param patterns_file: The user patterns file to use for Tesseract, if any. Defaults to
        'path/to/this/script/eng.user-patterns'.
    :return: Set of annotations found in the image.
    """
    arr = arr if arr is not None else iio.imread(img_path)
    arr_pad = add_padding(arr)
    contour_areas = find_contour_areas(arr_pad)
    arr_for_ocr = trim_to_contours(arr_pad, contour_areas, border=5)
    img = Image.fromarray(arr_for_ocr.astype(np.uint8))

    results1 = ocr_python(img, psm=6, lang=lang, words_file=words_file, patterns_file=patterns_file, dpi=None)
    results2 = ocr_python(img, psm=12, lang=lang, words_file=words_file, patterns_file=patterns_file, dpi=300)
    annotations = [x for x in results1 + results2 if x]

    del arr, arr_pad, contour_areas, arr_for_ocr, img, results1, results2
    gc.collect()
    return set(annotations)


def run_ocr(
        img_path: Union[str, Path], method: str = 'combined', lang: str = 'eng',
        words_file: Union[str, Path] = os.path.join(SCRIPT_DIR, 'ocr_files', 'eng.user-words'),
        patterns_file: Union[str, Path] = os.path.join(SCRIPT_DIR, 'ocr_files', 'eng.user-patterns')
) -> Dict[str, List[str]]:
    """
    Runs the OCR pipeline on an image.

    :param img_path: Path to the image to be processed.
    :param method: Method to use for OCR. Can be 'combined', 'python', or 'subprocess'.
    :param lang: Tesseract language/s to use for OCR.
    :param words_file: The user words file to use for Tesseract, if any. Defaults to
        'path/to/this/script/eng.user-words'.
    :param patterns_file: The user patterns file to use for Tesseract, if any. Defaults to
        'path/to/this/script/eng.user-patterns'.
    :return: Dictionary of annotations.
    """
    if method == 'combined':
        python_anotations = python_ocr_pipeline(img_path, lang, words_file, patterns_file)
        subprocess_annotations = ocr_subprocess(img_path, lang=lang, words_file=words_file, patterns_file=patterns_file)
        return {img_path.name: list(python_anotations.union(subprocess_annotations))}
    elif method == 'python':
        return {img_path.name: list(python_ocr_pipeline(img_path, lang, words_file, patterns_file))}
    elif method == 'subprocess':
        return {img_path.name: list(
            ocr_subprocess(img_path, lang=lang, words_file=words_file, patterns_file=patterns_file))}
    else:
        raise ValueError(f'Invalid method: {method}')


def read_masks(args: Namespace):
    """
    Run OCR on CXR masks and save annotations to a json file.

    :param args: Arguments
    """
    img_path = args.img_path
    outpath = args.outpath
    method = args.method
    lang = args.lang
    words_file = args.words_file
    patterns_file = args.patterns_file
    nworkers = args.nworkers
    result_dict = {}

    if not words_file:
        words_file = os.path.join(SCRIPT_DIR, 'ocr_files', f'{lang}.user-words')
        if not os.path.isfile(words_file):
            print(f'No words file found for {lang}. {words_file} doesn\'t exist.')
            words_file = None
    if not patterns_file:
        patterns_file = os.path.join(SCRIPT_DIR, 'ocr_files', f'{lang}.user-patterns')
        if not os.path.isfile(patterns_file):
            print(f'No patterns file found for {lang}. {patterns_file} doesn\'t exist.')
            patterns_file = None

    if nworkers > 1:
        try:
            with ThreadPoolExecutor(max_workers=nworkers) as executor:
                futures = [
                    executor.submit(
                        run_ocr, Path(file), method, lang, words_file, patterns_file
                    ) for file in img_path
                ]
                for future in tqdm(as_completed(futures), total=len(futures)):
                    result = future.result()
                    result_dict.update(result)
        except KeyboardInterrupt:
            print('KeyboardInterrupt: Stopping OCR...')
        except Exception as e:
            print(e)
        finally:
            if executor is not None:
                executor.shutdown(wait=True)

    else:
        for file in img_path:
            print(f'Processing {file}')
            result = run_ocr(Path(file), method, lang, words_file, patterns_file)
            result_dict.update(result)

    out_json = add_json_extension(outpath)
    with open(out_json, 'w') as outfile:
        json.dump(result_dict, outfile)

    print("Done!")
    print(f'OCR results saved to {outpath}')


def clean_data(
        ds: Dict[str, List[str]],
        clean_dict: Optional[Dict[str, str]] = None,
        to_strip: Optional[str] = None,
        proj_removals: Optional[Dict[str, List[str]]] = None
) -> Dict[str, List[str]]:
    """
    Cleans the data in the input dictionary by applying various string operations such as stripping certain characters,
    replacing phrases, and removing certain items based on the project removals dictionary. The cleaned data is then
    transformed to uppercase and filtered by clean_dict before being returned.

    :param ds: The input dictionary containing the data to be cleaned. The dictionary structure should be: {file: [items]}
    :param clean_dict: A dictionary containing the mappings used for replacements during the cleaning process.
                       Defaults to CLEAN_DICT if not provided.
    :param to_strip: A string containing characters to be stripped from the items during the cleaning process.
                     Defaults to TO_STRIP if not provided.
    :param proj_removals: A dictionary containing items to be removed during the cleaning process. The dictionary structure
                          should be: {upper_item: [lower_items]}. Defaults to PROJ_REMOVALS if not provided.
    :return: The cleaned data dictionary.
    """
    if clean_dict is None:
        clean_dict = CLEAN_DICT
    if to_strip is None:
        to_strip = TO_STRIP
    if proj_removals is None:
        proj_removals = PROJ_REMOVALS

    for file, items in ds.items():
        new_items = []
        for item in items:
            for character in to_strip:
                item = item.strip(character)
            item.replace("_", " ")
            item = item.replace("SEMI ", "SEMI-")
            item = item.replace("semi ", "semi-")
            item = item.replace("ANTERO POSTERIOR", "ANTERO-POSTERIOR")
            item = item.replace("POSTERO ANTERIOR", "POSTERO-ANTERIOR")
            for key in clean_dict.keys():
                if key in item:
                    item = item.replace(key, clean_dict[key])
            split_items = item.split(" ")
            for split_item in split_items:
                for character in to_strip:
                    split_item = split_item.strip(character)
                for key in clean_dict.keys():
                    if key in split_item:
                        split_item = split_item.replace(key, clean_dict[key])
                new_items.append(split_item)
        ds[file] = new_items

    for file, items in ds.items():
        for proj_upper, proj_lower_vals in proj_removals.items():
            for proj_lower in proj_lower_vals:
                if proj_upper in items and proj_lower in items:
                    ds[file].remove(proj_lower)
        if ("L" in items or "R" in items) and "l" in items:
            ds[file].remove("l")
        if ("L" in items or "R" in items) and "r" in items:
            ds[file].remove("r")
        if ("S" in items or "D" in items) and "s" in items:
            ds[file].remove("s")
        if ("S" in items or "D" in items) and "d" in items:
            ds[file].remove("d")
        if ("I" in items or "D" in items) and "d" in items:
            ds[file].remove("d")
        if ("I" in items or "D" in items) and "i" in items:
            ds[file].remove("i")
        if ("E" in items or "D" in items) and "e" in items:
            ds[file].remove("e")
        if ("E" in items or "D" in items) and "d" in items:
            ds[file].remove("d")
        if "cr" in items:
            ds[file].remove("cr")
        if "dx" in items:
            ds[file].remove("dx")

    for file, items in ds.items():
        new_items = []
        for item in items:
            item = item.upper()
            if item in clean_dict.keys():
                item = clean_dict[item]
            new_items.append(item)
        ds[file] = list(set(new_items))
        if len(ds[file]) > 20:
            ds[file] = []
    return ds


def create_dictionaries(terms_df: pd.DataFrame) -> Tuple[
    Dict[str, str], Dict[str, str], Dict[str, str], Dict[str, str], Dict[str, str]]:
    """
    Create dictionaries for mapping terms in different languages to English and standardising them.

    :param terms_df: A data frame containing terms in various languages and their respective mappings.
    :return: A tuple containing five dictionaries: combined_dict, eng_dict, spa_dict, ita_dict, and port_dict.
    """
    eng_dict = terms_df.set_index('English')['EnglishOutput'].to_dict()
    spa_dict = terms_df.set_index('Spanish')['EnglishOutput'].to_dict()
    ita_dict = terms_df.set_index('Italian')['EnglishOutput'].to_dict()
    port_dict = terms_df.set_index('Portuguese')['EnglishOutput'].to_dict()

    df2 = terms_df.copy()
    df2 = df2.drop(["ItalianOutput", "SpanishOutput", "PortugueseOutput"], axis=1)
    df2 = df2.melt(['EnglishOutput',])
    combined_dict = df2.set_index('value')['EnglishOutput'].to_dict()
    return combined_dict, eng_dict, spa_dict, ita_dict, port_dict


def find_terms(ds: Dict[str, List[str]], dictionary: Dict[str, str]) -> Dict[str, List[str]]:
    """
    Find and replace terms in the dataset using the specified dictionary.

    :param ds: A dictionary with file names as keys and lists of terms as values.
    :param dictionary: A dictionary for mapping terms.
    :return: A dictionary with the same keys as ds, where terms have been replaced according to the dictionary.
    """
    res = {}
    for file, items in ds.items():
        new_items = []
        for item in items:
            if item in dictionary.keys():
                new_items.append(dictionary[item])
            new_items = list(set(new_items))
        res[file] = new_items
    return res


def populate_category_df(res: Dict[str, List[str]], cats: Dict[str, str] = None,
                         sep: str = ",") -> pd.DataFrame:
    """
    Populate a DataFrame with categories based on the res dictionary and cats mapping.

    :param res: A dictionary with scan names as keys and lists of values as values.
    :param cats: A dictionary with category mappings.
    :param sep: A string used to separate multiple values in the same cell.
    :return: A populated DataFrame with categories.
    """
    if cats is None:
        cats = OUTPUT_CATEGORIES

    def _blank_results_df(res, cols):
        df = pd.DataFrame(columns=cols)
        df['name'] = list(res.keys())
        return df.set_index('name')

    cols = ['name'] + list(set(cats.values()))
    df = _blank_results_df(res, cols)
    for scan, values in res.items():
        for value in values:
            if value.upper() in cats:
                category = cats[value.upper()]
                if pd.isnull(df.loc[scan, category]):  # if the cell is empty
                    df.loc[scan, category] = value.upper()
                else:  # if the cell already contains some values
                    df.loc[scan, category] += sep + value.upper()
    return df
