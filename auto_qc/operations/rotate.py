"""
rotate.py: Image Rotation Module

This module provides functionality for rotating an image by a specified angle,
with a particular emphasis on rotations that are multiples of 90 degrees.
The module provides two primary functions, `correct_90_rotation()` and `correct_rotation()`.

`correct_90_rotation()` takes an image and an angle as input and rotates the
image by the nearest 90 degrees to the given angle.

`correct_rotation()` also takes an image and an angle as input and rotates the image.
It also takes an optional `full_rotation` boolean parameter. If `full_rotation` is True,
the image is rotated by the given angle, otherwise, it is rotated by the nearest 90-degree angle.

The module also provides a `main()` function, which parses command-line arguments for an image file,
an optional output directory, an optional rotation angle, and an optional `full_rotation` flag.
It then loads the image, performs the appropriate rotation, saves the rotated image, and prints a success message.

This module can be run as a standalone script to perform image rotation from the command line.

Examples
--------
Command line usage:
    python rotate.py image.png --angle 45 --full_rotation --out ./rotated_images
"""


from scipy.ndimage import rotate
import numpy as np
from typing import Union, Tuple
from argparse import ArgumentParser
import sys
from pathlib import Path
import imageio.v3 as iio

from ..utils import valid_file, valid_directory


def correct_90_rotation(
        img_array: np.ndarray,
        angle: Union[int, float]
) -> Tuple[np.ndarray, int]:
    """
    Rotates the image by the nearest 90-degree angle to the provided one.

    :param img_array: The image to rotate as a numpy array
    :param angle: The angle to rotate the image by
    :return: The rotated image as a numpy array and the degree of rotation
    """
    # Ensure angle is in range [0, 360)
    angle %= 360
    # Determine the amount of 90 degree rotation
    if 45 <= angle < 135:
        degrees = 90
    elif 135 <= angle < 225:
        degrees = 180
    elif 225 <= angle < 315:
        degrees = 270
    else:
        degrees = 0
    # Rotate the image
    new_arr = rotate(img_array, degrees, mode='constant', cval=0)
    return new_arr, degrees


def correct_rotation(
        img_array: np.ndarray,
        angle: Union[int, float],
        full_rotation: bool = False
) -> Tuple[np.ndarray, int]:
    """
    Corrects the rotation of an image by rotating it by the given angle. If
    full_rotation is True, the image is rotated by the given angle. If it is
    False, the image is rotated by the nearest 90-degree angle.

    :param img_array: The image to rotate as a numpy array
    :param angle: The angle to rotate the image by
    :param full_rotation: Whether to rotate the image by the given angle or
        the nearest 90-degree angle
    :return: The rotated image as a numpy array
    """
    angle %= 360
    if full_rotation:
        return rotate(img_array, angle, mode='constant', cval=0), angle
    else:
        return correct_90_rotation(img_array, angle)


def main():
    """
    The main function of the script. It parses the command-line arguments, loads the image,
    performs the rotation, saves the rotated image, and prints a success message.
    """
    parser = ArgumentParser(description="This script rotates an image by a given angle or by the "
                                        "nearest 90-degree angle.")
    parser.add_argument("image", type=valid_file, help="The image file to rotate")
    parser.add_argument("--out", required=False, type=valid_directory, default=".",
                        help="The directory to save the output image")
    parser.add_argument("--angle", required=False, type=float, default=0.0,
                        help="The angle to rotate the image by")
    parser.add_argument("--full_rotation", required=False, action='store_true',
                        help="Whether to rotate the image by the given angle or the nearest 90-degree angle")

    args = parser.parse_args((sys.argv[1:]))
    filename = Path(args.image)
    img = iio.imread(filename)
    rotated, degrees = correct_rotation(img, args.angle, args.full_rotation)
    print(f"Rotated the image by {degrees} degrees")
    original_extension = filename.suffix
    out = Path(args.out) / f"{filename.stem}_rotated{original_extension}"
    iio.imwrite(out, rotated)
    print(f"Saved to: {out}")
    print("Done!")


if __name__ == "__main__":
    main()
