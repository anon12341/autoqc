"""
boundary.py: Functions for Removing the Padding / Border of an Image.

The boundary.py module provides functions to remove the borders of an image. It's especially useful for cropping the
excess space (or padding) around the main content of an image.

Main Components:

- remove_border: Removes the border of the input image. It performs this by detecting labels in the blank borders /
padding and removing them to return a cropped image.
- remove_border2: Similar to remove_border, it removes the border of the input image. This function employs a different
method and is capable of dealing with writings in the blank borders.
- main: The main function of the script. It parses command-line arguments, loads the image, performs cropping using
remove_border, and saves the cropped image. This function is intended to be used when this module is run as a script.

This module is helpful in image processing pipelines where unnecessary borders or padding need to be removed from
images. The script can be run from the command line to process an individual image file.
"""

import sys
from argparse import ArgumentParser, Namespace
from pathlib import Path
import imageio.v2 as iio
import cv2
import numpy as np
from concurrent.futures import as_completed, ProcessPoolExecutor
from tqdm.auto import tqdm
from typing import Union, Optional

from ..utils import bytescale, imread, valid_file, valid_directory


def _remove_border(img: np.ndarray) -> np.ndarray:
    """
    Removes the border of the input image.

    The function detects labels in the blank borders / padding, removes them,
    and returns the cropped image.

    :param img: The input image as a 2D numpy array
    :return: The cropped image with borders removed
    """
    img_min = img.min()
    img = img - img_min
    val = np.median(img, axis=1).min() * 0
    x = (np.median(img, axis=1, keepdims=True) > val) * 1
    x = np.resize(x, (img.shape[1], img.shape[0])).T

    val = np.median(img, axis=0).min() * 0
    y = (np.median(img, axis=0, keepdims=True) > val) * 1
    y = np.resize(y, (img.shape[0], img.shape[1]))

    nimg = img * x * y
    cimg = (img * 0 + 100) * x * y
    
    bimg = bytescale(cimg).astype("uint8")
    
    # bimg = ndi.gaussian_filter(nimg, 3)
    # bimg = bytescale(bimg).astype("uint8")

    try:
        _, thresh = cv2.threshold(bimg, 1, 255, cv2.THRESH_BINARY)
        contours, hierarchy = cv2.findContours(
            thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
        )
        cnt = contours[0]
        x, y, w, h = cv2.boundingRect(cnt)
        crop = nimg[y + 3 : y + h - 3, x + 3 : x + w - 3]
    except:
        crop = img + img_min

    r = min(crop.shape[0]/img.shape[0], crop.shape[1]/img.shape[1])
    if r < 0.3:
        crop = img + img_min

    return crop


def remove_border(img: np.ndarray) -> np.ndarray:
    """
    Removes the border of the input image. We call the _remove_border function twice to ensure that the border is
    removed, as sometimes the image may have a border within the border and we found that this best tackled
    collimation.

    :param img: The input image as a 2D numpy array
    :return: The cropped image with borders removed
    """
    return _remove_border(_remove_border(img))


def remove_border2(img: np.ndarray) -> np.ndarray:
    """
    Removes the border of the input image, but this function employs a
    different multi_gpu to remove_border.

    This function is capable of dealing with writings in the blank borders.
    It returns the cropped image.

    :param img: The input image as a 2D numpy array
    :return: The cropped image with borders removed
    """
    img_min = img.min()
    img = img - img_min

    bimg = bytescale(img).astype("uint8")

    # threshold
    thresh = cv2.threshold(bimg, 1, 255, cv2.THRESH_BINARY)[1]

    # apply open morphology
    #kernel = np.ones((5,5), np.uint8)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (15,15))
    morph = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)

    # get bounding box coordinates from largest external contour
    contours = cv2.findContours(morph, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[0] if len(contours) == 2 else contours[1]
    big_contour = max(contours, key=cv2.contourArea)
    x,y,w,h = cv2.boundingRect(big_contour)

    # crop image contour image
    result = img.copy() + img_min
    result = result[y:y+h, x:x+w]

    return result


def _remove_padding(im_path: Path, bits: Optional[int], out_path: Optional[Union[str, Path]] = None):
    """
    Helper function to remove the padding from an image.

    :param im_path: The path to the image to be cropped
    :param bits: The bit depth of the image, defaults to None. If None, the bit depth is inferred from the array
        following the behaviour of imageio.imwrite.
    :param out_path: The path to save the cropped image to, defaults to None. If None, the cropped image overwrites
        the original image.
    """
    img = iio.imread(im_path)
    cropped = remove_border(img)
    if out_path:
        out = Path(out_path) / im_path.name
        out.parent.mkdir(parents=True, exist_ok=True)
    else:
        out = im_path

    if int(bits) == 16:
        iio.imwrite(out, cropped.astype(np.uint16))
    elif int(bits) == 8:
        iio.imwrite(out, cropped.astype(np.uint8))
    else:
        iio.imwrite(out, cropped)


def remove_padding(args: Namespace):
    """
    Function to manage the cropping of the padding from the command line.

    :param args: The command line arguments
    """
    image_list = args.image_path if isinstance(args.image_path, list) else [args.image_path]
    image_list = [Path(file) for file in image_list if file.endswith((
        '.tif', '.tiff', '.png', '.jpg', '.jpeg', '.bmp', '.npy'
    ))]
    bit_depth = args.bit_depth
    outpath = args.outpath
    nworkers = args.nworkers

    if nworkers > 1:
        try:
            with ProcessPoolExecutor(max_workers=nworkers) as executor:
                futures = [
                    executor.submit(
                        _remove_padding, file, bit_depth, outpath
                    ) for file in image_list
                ]
                for future in tqdm(as_completed(futures), total=len(futures)):
                    future.result()
        except KeyboardInterrupt:
            print('KeyboardInterrupt: Stopping cropping...')
        except Exception as e:
            print(e)
        finally:
            if executor is not None:
                executor.shutdown(wait=True)
    else:
        for img_path in image_list:
            _remove_padding(img_path, bit_depth, outpath)


def main():
    """
    The main function of the script. It parses the command-line arguments, loads the image,
    performs the cropping, and saves the cropped image.
    """
    parser = ArgumentParser(description="This script removes padding from an image.")
    parser.add_argument("image", type=valid_file, nargs='+', help="The image file from which to remove padding")
    parser.add_argument("--out", required=False, type=valid_directory, default=".",
                        help="The directory to save the output image")

    args = parser.parse_args((sys.argv[1:]))
    filename = Path(args.image)
    img = imread(str(filename))
    original_extension = filename.suffix
    cropped = remove_border(img)
    out = Path(args.out) / f"{filename.stem}_cropped{original_extension}"
    iio.imwrite(out, cropped)
    print(f"Done! Saved to: {out}")


if __name__ == "__main__":
    main()
