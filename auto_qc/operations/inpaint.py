"""
inpaint.py: Inpainting of Annotations on Chest X-Rays

Module to remove or extract annotations/text from a chest x-ray. Annotations/text may be 
removed by inpainting or using a bounding box of zeros. Additional functions return the mask 
applied to the original to obtain the final image, and plot the original and final image  
side-by-side to facilitate visual comparison.

Please note, the inpaint_cxr function:
    - handles and returns 16-bit image arrays.
    - was developed on images where the the modality and voi lookup tables (LUTs), and photometric 
    interpretation had previously been applied - if not been applied, performance may be variable.
"""

from argparse import Namespace
from pathlib import Path
import imageio.v2 as imageio
import pydicom
from pydicom.pixel_data_handlers.util import apply_voi_lut, apply_modality_lut
import numpy as np
import pandas as pd
import cv2
from skimage.morphology import reconstruction
import scipy.signal
import matplotlib.figure
import matplotlib.pyplot as plt
from numpy.typing import ArrayLike
from typing import Tuple, Optional, Union
from tqdm.auto import tqdm
from concurrent.futures import ThreadPoolExecutor, as_completed
import gc


def plot_comparison(
        original: np.ndarray,
        inpainted: np.ndarray,
        fig_title: Optional[str] = None,
        fig_size: Tuple[int, int] = (30, 10),
        title_size: int = 25
) -> matplotlib.figure.Figure:
    """
    Creates a figure of the original image (left) versus the inpainted version (middle) with the mask (right)
    for visual comparison.

    :param original: Original image
    :param inpainted: Inpainted image
    :param fig_title: Desired title of figure, e.g. filename.
    :param fig_size: Figure size to pass to matplotlib, defaults to (20,10)
    :param title_size: Size of the plot title, defaults to 25
    :return: matplotlib figure object
    """

    aspect_ratio = original.shape[1] / original.shape[0]

    # Calculate the width and height of the figure dynamically
    fig_width = fig_size[0] * aspect_ratio
    fig_height = fig_size[1]

    fig, (ax1, ax2, ax3) = plt.subplots(ncols=3, figsize=(fig_width, fig_height))
    ax1.imshow(original, cmap="bone")
    ax1.set_title('Original', fontsize=title_size)
    ax1.axis('off')

    ax2.imshow(inpainted, cmap="bone")
    ax2.set_title('Inpainted', fontsize=title_size)
    ax2.axis('off')

    mask = inpainted - original
    ax3.imshow(mask, cmap="bone")
    ax3.set_title('Mask', fontsize=title_size)
    ax3.axis('off')

    if fig_title != None:
        fig.suptitle(fig_title, fontsize=title_size)

    fig.tight_layout()

    return fig


def create_comparison(
        original_path: Union[str, Path],
        inpainted_path: Union[str, Path],
        out_dir: Union[str, Path],
        fig_title: Optional[str] = None,
        fig_size: Tuple[int, int] = (30, 10),
        title_size: int = 25
):
    """
    Creates and saves a figure of the original image (left) versus the inpainted version (middle) with the mask (right)
    for visual comparison.

    :param original: Original image
    :param inpainted: Inpainted image
    :param outpath: path to save figure
    :param fig_title: figure title
    :param fig_size: figure size, defaults to (30, 10)
    :param title_size: figure title size, defaults to 25
    """
    original = imageio.imread(original_path)
    inpainted = imageio.imread(inpainted_path)
    fig = plot_comparison(original, inpainted, fig_title, fig_size, title_size)

    Path(out_dir).mkdir(parents=True, exist_ok=True)
    outpath = Path(out_dir) / Path(inpainted_path).name
    fig.savefig(outpath)
    plt.close(fig)
    del original, inpainted, fig
    gc.collect()


def get_applied_mask(
        original: np.ndarray,
        inpainted: np.ndarray
) -> np.ndarray:
    """
    Get the mask used for inpainting / bbox.  Note this is after radius applied, resulting in dilation of the inpainted
    area.

    :param original: Original image
    :param inpainted: Output image from inpaint_cxr
    :return: Mask
    """
    return (np.abs(original - inpainted) > 0).astype('uint8')


def inpaint_cxr(
        image: np.ndarray,
        no_lut_image: Optional[np.ndarray] = None,
        method: str = 'inpaint_cxr',
        use_ns_inpainting: bool = False,
        ll_mask: bool = True
) -> np.ndarray:
    """
    Remove or extract a mask of chest x-ray annotations/text.

    :param image: Input image in the form of an array.
    :param no_lut_image: Input image in the form of an array without the LUTs applied.
    :param method: removal method or extract annotations/text ('inpaint_cxr', 'bbox', or 'extract_text').
            'inpaint_cxr': use inpainting.
            'bbox': use a bounding box of zeros.
            'extract_text': extract annotations/text in the form of a mask (e.g. for OCR).
    :param use_ns_inpainting: False: use the 'Fast Marching Method' (cv.INPAINT_TELEA) for inpainting.
            True: use Navier-Stokes method (cv.INPAINT_NS) for inpainting.
    :param ll_mask: Include low-level text in the mask.
    :return: Output image
    """

    def roi_filter(
            im: np.ndarray,
            ceiling_factor: float,
            apply_medfilt: bool,
            med_filter_ks: int
    ) -> np.ndarray:
        """
        Filter with ones to create ROI and apply filters to identify the ROIs.

        :param im: input image
        :param ceiling_factor: the factor to multiply the image size by to create the kernel size
        :param apply_medfilt: whether to apply the first median filter
        :param med_filter_ks: kernel size for the second median filter
        :return: output mask
        """
        # Filter with ones to create ROI
        ks = np.ceil(ceiling_factor * im.size).astype('int')
        Kernel = np.ones((ks, ks))
        if apply_medfilt:
            msk = cv2.filter2D(src=scipy.signal.medfilt2d(im, 3), kernel=Kernel, ddepth=-1)
        else:
            msk = cv2.filter2D(src=im, kernel=Kernel, ddepth=-1)  # filled
        [s, msk] = cv2.threshold(msk, 20, 255, cv2.THRESH_BINARY)
        if msk.max() != 0:
            msk = scipy.signal.medfilt2d(msk / msk.max(), med_filter_ks)
        return msk

    def fill_rings(
            orig_msk: np.ndarray
    ) -> np.ndarray:
        """
        Fill rings in mask to not leave a black circle

        :param orig_msk: input mask
        :return: output mask
        """
        # Fill ring to not leave a black circle - can be left out if we do not care about it
        seed = np.copy(orig_msk)
        seed[1:-1, 1:-1] = orig_msk.max()
        rings_filled = reconstruction(seed, orig_msk, method='erosion')
        rings_filled = removeCc(rings_filled.astype('uint8'), 0.00025 * rings_filled.size)
        # ignore if contains less than 25% of pixelamount
        rings_filled = removeCc(rings_filled.astype('uint8'), 0.1 * rings_filled.size, cc='large')
        return rings_filled

    def inpaint_image(
            im1: np.ndarray,
            msk: np.ndarray,
            use_ns: bool,
            r: int = 12
    ) -> np.ndarray:
        """
        Inpaint image using OpenCV.

        :param im1: image to inpaint
        :param msk: the mask to use for inpainting
        :param use_ns: whether to use Navier-Stokes method (cv.INPAINT_NS) or Fast Marching Method (cv.INPAINT_TELEA)
        :param r: radius to use for inpainting
        :return: output image
        """
        if im1.dtype == np.int64:
            # im1 = ((im1 / np.max(im1)) * 65535).astype(np.uint16)
            im1 = im1.astype(np.uint16)
        if use_ns:
            im2 = cv2.inpaint(im1, msk.astype('uint8'), r - 1, cv2.INPAINT_NS)
        else:
            im2 = cv2.inpaint(im1, msk.astype('uint8'), r - 1, cv2.INPAINT_TELEA)
        # set to min max
        im2 = np.clip(im2, im1.min(), im1.max())
        # just use inpainted area
        im2 = (msk == 255) * im2 + (msk == 0) * im1
        return im2

    def rectangular_contours(
            image: np.ndarray
    ) -> Optional[np.ndarray]:
        """
        Find rectangular contours in an image.

        :param image: image to find contours in
        :return: output image with contours drawn
        """
        rec_im = None
        contours, hierarchy = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        for i in range(len(contours)):
            temp = contours[i].flatten()
            x_values = temp[::2]
            y_values = temp[1::2]
            x_min = np.min(x_values)
            x_max = np.max(x_values)
            y_min = np.min(y_values)
            y_max = np.max(y_values)
            rec_im = cv2.rectangle(image, (x_min, y_min), (x_max, y_max), (255, 0, 0), -1)
        return rec_im

    def detect_edges(
            im: np.ndarray
    ) -> np.ndarray:
        """
        Detect edges in an image.

        :param im: input image
        :return: output mask
        """
        im_uint8 = cv2.normalize(im, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)
        # Sobel Edge Detection to find regions with letters
        gX = cv2.Sobel(src=im_uint8, ddepth=cv2.CV_64F, dx=1, dy=0, ksize=7)
        gY = cv2.Sobel(src=im_uint8, ddepth=cv2.CV_64F, dx=0, dy=1, ksize=7)  # kernelsize ? 3 or 7 work well
        # compute gradient magnitude
        mag = np.sqrt((gX ** 2) + (gY ** 2))
        max_mag = mag.max()
        if max_mag != 0:
            mag = mag / max_mag
        else:
            mag = np.zeros_like(mag)
        msk = mag > 10 * np.mean(mag)  # 0.35 #0.37?
        # Need uint8 for inpainting
        msk = scipy.signal.medfilt2d(msk.astype('uint8'), 3)  # or 3?
        return msk

    def fill_letters(
            msk1: np.ndarray
    ) -> np.ndarray:
        """
        Fill letters to not leave outlines.

        :param msk1: input mask
        :return: output mask
        """
        seed = np.copy(msk1)
        seed[1:-1, 1:-1] = msk1.max()
        msk2 = scipy.signal.medfilt2d(reconstruction(seed, msk1, method='erosion'),
                                      19)  # cut bounding box ?medfilt ?9?13?21?
        return msk2

    def removeCc(
            image: np.ndarray,
            threshold: int,
            cc: str = 'small'
    ) -> np.ndarray:
        """
        Removal of too small or too large connected components.

        :param image: input image
        :param threshold: threshold for connected components
        :param cc: 'small' or 'large'
        :return: output image
        """
        # find all connected components
        nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(image, connectivity=8)
        sizes = stats[1:, -1];
        nb_components = nb_components - 1

        img = np.zeros((output.shape), dtype=np.uint8)
        # keep just components in the image that are above threshold
        for i in range(0, nb_components):
            if cc == 'small':
                if sizes[i] >= threshold:
                    img[output == i + 1] = 255
            else:
                if sizes[i] < threshold:
                    img[output == i + 1] = 255
        return img

    def high_level_mask(
            full_image: np.ndarray, img: np.ndarray, dst: np.ndarray, no_luts_image=None, method: str = "inpaint",
            use_ns_inpainting: bool = False, radius: int = 12
    ) -> Tuple[np.ndarray, np.ndarray]:
        """
        Creates a mask for the high level text (at the top of the histogram).

        :param img: the original image
        :param dst: the output image
        :param method: method to use for removing the text, any of 'extract_text', 'bbox' or 'inpaint'
        :param use_ns_inpainting: whether to use the Navier-Stokes inpainting algorithm, defaults to False
        :param radius: radius of the inpainting algorithm, defaults to 12
        :return: the mask for the high level text and the output image
        """
        if no_luts_image is not None:
            hl_img = no_luts_image
        else:
            hl_img = img

        # STEP 1 - Create Mask1 for high level text
        histogram, bin_edges = np.histogram(hl_img, bins=256, range=(0, 1))
        # Select images with text via histogram
        if histogram[255] > 10 * histogram[254]:
            [_, imgt] = cv2.threshold(hl_img, 0.96, 255, cv2.THRESH_BINARY)  # threshold original image
            if method != 'extract_text':
                roi = roi_filter(imgt, 0.000005, True, 25)
                roi = removeCc(roi.astype('uint8'), 0.0025 * roi.size)
                ks = np.ceil(0.000005 * roi.size).astype('int')
                kernel_dilation = np.ones((ks, ks), np.uint8)
                roi = cv2.dilate(roi.astype(np.uint8), kernel_dilation, iterations=1)

                filled = fill_rings(roi)

                # print(full_image.dtype)
                if method == 'inpaint_cxr':
                    dst = inpaint_image(full_image, filled, use_ns_inpainting, r=radius)
                elif method == 'bbox':
                    roirem = rectangular_contours(filled.astype('uint8'))
                    if roirem is not None:
                        dst = full_image * (roirem == 0)
        else:
            imgt = np.zeros(img.shape)
        return imgt, dst

    def low_level_mask(
            dst: np.ndarray, method: str, use_ns_inpainting: bool = False, radius: int = 12
    ) -> Tuple[np.ndarray, np.ndarray]:
        """
        Create Mask2 for low level text and tags.

        :param dst: image
        :param method: how to remove annotations, either 'inpaint_cxr', 'bbox' or 'extract_text'
        :param use_ns_inpainting: Whether to use Navier-Stokes inpainting, defaults to False
        :param radius: radius of inpainting, defaults to 12
        :return: the mask and the output image
        """

        inpaint_mask = detect_edges(dst)
        if method != 'extract_text':
            filled = fill_letters(inpaint_mask)
            filled = removeCc(filled.astype('uint8'), 0.0001 * filled.size)
            # Only continue if mask is not just black...
            if filled.max() != 0:
                # roi = roi_filter(filled, 0.00003, False, 9)
                roi = roi_filter(filled, 0.00002, False, 9)
                # ignore if contains less than 0.015% of total no of pixels
                roirem = removeCc(roi.astype('uint8'), 0.0015 * roi.size)

                # ignore if contains more than 5% of total no of pixels
                roirem = rectangular_contours(roirem.astype('uint8'))
                roirem = removeCc(roirem, 0.05 * roirem.size, cc='large')
                if roirem.max() != 0:
                    if method == 'inpaint_cxr':
                        dst = inpaint_image(dst, roirem, use_ns_inpainting, r=radius)
                    elif method == 'bbox':
                        dst = dst * (roirem == 0)
        return inpaint_mask, dst

    if image.dtype == np.uint64 or image.dtype == np.uint32:
        image = image.astype(np.unint16)
    elif image.dtype == np.float64:
        image = image.astype(np.float32)
    # Initiate variables
    dst = image.copy()  # output
    img = image / image.max()
    if no_lut_image is not None:
        no_lut_image = no_lut_image / no_lut_image.max()
        imgt, dst = high_level_mask(image, img, dst, no_luts_image=no_lut_image, method=method,
                                    use_ns_inpainting=use_ns_inpainting)
    else:
        imgt, dst = high_level_mask(image, img, dst, no_luts_image=None, method=method,
                                    use_ns_inpainting=use_ns_inpainting)

    # STEP 2 - Create mask for low-level text e.g. tags, metal laterality marker, machine specific labels, etc
    if ll_mask:
        inpaint_mask, dst = low_level_mask(dst, method, use_ns_inpainting)
    else:
        inpaint_mask = np.zeros(img.shape)

    if method == 'extract_text':
        return imgt + inpaint_mask
    else:
        return dst.astype('uint16')


def invert_grayscale(img):
    return np.max(img) + np.min(img) - img


def read_dicom(
        impath: Path, inversion_csv: Optional[Path] = None, name_col: str = 'name', inversion_col: str = 'inv'
) -> Tuple[np.ndarray, np.ndarray, pydicom.Dataset]:
    """
    Read dicom image and invert if necessary.

    :param impath: path to dicom image
    :param inversion_csv: path to csv file with inversion information, defaults to None
    :param name_col: column name of image, defaults to 'name'
    :param inversion_col: column name of inversion information, defaults to 'inv'
    :return: image as an array and the dicom dataset
    """
    ds = pydicom.dcmread(impath)
    original_img = ds.pixel_array
    lut_original = apply_modality_lut(original_img.astype('uint16'), ds)
    lut_original = apply_voi_lut(lut_original.astype('uint16'), ds, index=0)
    if inversion_csv:
        inversion_df = pd.read_csv(inversion_csv)
        png_name = Path(impath).with_suffix(".png").name
        if png_name in inversion_df[name_col].values:
            if inversion_df[inversion_df[name_col] == png_name][inversion_col].astype(float).astype(int).values == 1:
                original_img = invert_grayscale(original_img)
                lut_original = invert_grayscale(lut_original)
        # for brixia dicoms, due to inconsistency in naming convention have to find them the inversion csv
        # using this method:
        elif png_name in [x.split("_")[-1] for x in inversion_df[name_col].values]:
            if inversion_df[
                inversion_df[name_col].str.split("_").str[-1] == png_name
            ][inversion_col].astype(float).astype(int).values == 1:
                original_img = invert_grayscale(original_img)
                lut_original = invert_grayscale(lut_original)
        else:
            if hasattr(ds, 'PatientName'):
                if ds.PhotometricInterpretation == 'MONOCHROME1':
                    original_img = invert_grayscale(original_img)
                    lut_original = invert_grayscale(lut_original)
    else:
        if hasattr(ds, 'PatientName'):
            if ds.PhotometricInterpretation == 'MONOCHROME1':
                original_img = invert_grayscale(original_img)
                lut_original = invert_grayscale(lut_original)
    return lut_original, original_img, ds


def inpaint_run(
        impath: Path, outpath: Path, method, use_ns: bool = False, ll_mask: bool = True, inversion_csv: Path = None,
        name_col: str = 'name', inversion_col: str = 'inv'
):
    """
    Run inpainting on a single image, saving the image to the output directory.

    :param impath: path to image
    :param outpath: path to output directory
    :param method: inpainting method, either 'inpaint_cxr', 'bbox' or 'extract_text'
    :param use_ns: use Navier-Stokes inpainting, defaults to False
    :param ll_mask: use low-level mask, defaults to True
    :param inversion_csv: path to csv file with inversion information, defaults to None
    :param name_col: column name of image, defaults to 'name'
    :param inversion_col: column name of inversion information, defaults to 'inv'
    """
    try:
        if impath.suffix == '.dcm':
            luts_img, raw_img, ds = read_dicom(impath, inversion_csv, name_col, inversion_col)
        else:
            luts_img = imageio.imread(impath)
            if inversion_csv:
                inversion_df = pd.read_csv(inversion_csv)
                if Path(impath).with_suffix(".png").name in inversion_df[name_col].values or Path(impath).with_suffix(
                        ".png").name in [x.split("_")[-1] for x in inversion_df[name_col].values]:
                    if inversion_df[inversion_df[name_col] == impath.name][inversion_col].astype(float).astype(
                            int).values[0] == 1:
                        luts_img = invert_grayscale(luts_img)
            raw_img = None
    except FileNotFoundError:
        return
    dst_img = inpaint_cxr(luts_img, raw_img, method=method, use_ns_inpainting=use_ns, ll_mask=ll_mask)
    out = Path(outpath) / (impath.stem + '.png')

    imageio.imsave(out, dst_img.astype('uint16'))

    del luts_img, raw_img, dst_img
    gc.collect()


def inpaint(args: Namespace):
    """
    Run inpainting on a list of images to remove annotations and saves output. Can remove annotations using inpainting,
    using a bounding box, or extract text.

    :param args: Arguments
    """
    img_path = args.img_path
    outpath = args.outpath
    removal_method = args.method
    use_ns = args.use_ns
    ll_mask = args.ll_mask
    nworkers = args.nworkers
    inversion_csv = args.inversion_csv
    name_col = args.name_col
    inversion_col = args.inversion_col

    if nworkers > 1:
        try:
            with ThreadPoolExecutor(max_workers=nworkers) as executor:
                futures = [
                    executor.submit(
                        inpaint_run, Path(file), Path(outpath), removal_method, use_ns, ll_mask,
                        inversion_csv, name_col, inversion_col
                    ) for file in img_path
                ]
                for future in tqdm(as_completed(futures), total=len(futures)):
                    future.result()
        except KeyboardInterrupt:
            print('KeyboardInterrupt: Stopping inpainting...')
        except Exception as e:
            print(e)
        finally:
            if executor is not None:
                executor.shutdown(wait=True)

    else:
        for file in img_path:
            inpaint_run(Path(file), Path(outpath), removal_method, use_ns, ll_mask, inversion_csv, name_col,
                        inversion_col)

    print(f"Done! Output saved in '{outpath}'")


def create_comparison_images(args: Namespace):
    """
    Create comparison images between the original and inpainted images.

    :param args: Arguments
    """
    inpainted_path = args.inpainted_path
    original_dir = args.original_dir
    out_dir = args.out_dir
    fig_size = args.fig_size
    fig_title = args.fig_title
    title_size = args.title_size
    nworkers = args.nworkers

    matplotlib.use('Agg')

    if nworkers > 1:
        import matplotlib as mpl
        mpl.rcParams['figure.max_open_warning'] = nworkers * 2
        try:
            with ThreadPoolExecutor(max_workers=nworkers) as executor:
                futures = [
                    executor.submit(
                        create_comparison, Path(original_dir) / Path(file).name, Path(file), Path(out_dir),
                        fig_title, fig_size, title_size
                    ) for file in inpainted_path
                ]
                for future in tqdm(as_completed(futures), total=len(futures)):
                    future.result()
        except KeyboardInterrupt:
            print('KeyboardInterrupt: Stopping inpainting...')
        except Exception as e:
            print(e)
        finally:
            if executor is not None:
                executor.shutdown(wait=True)

    else:
        for file in inpainted_path:
            print(f'Processing {file}')
            create_comparison(Path(original_dir) / Path(file).name, Path(file), Path(out_dir),
                              fig_title, fig_size, title_size)

    print(f"Done! Output saved in '{out_dir}'")
