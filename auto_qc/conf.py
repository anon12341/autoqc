"""
conf.py: Configuration Schemas and Attribute Accessible Dictionaries

This module defines various schemas for validating configuration dictionaries
using the `voluptuous` library. These schemas are used to ensure the
correctness and completeness of configuration data before it is used elsewhere
in the application.

This module also defines `AttrDict` and `AttrSchema` classes, which are
extensions of Python's built-in `dict` and `voluptuous.Schema` respectively,
allowing attribute-style access to dictionary items.

The configuration schemas defined in this module include:

- schema_preprocess_labels: Ensures a dictionary has "name", "file", and "path" keys, all with string values.
- schema_preprocess: Checks for the presence of "dataset", "labels", and "output_dir" keys.
- schema_model: Validates a dictionary for keys associated with a model configuration.
- schema_train: Validates a dictionary for keys needed for training a model.
- schema_run: Validates a dictionary for keys needed for running a model.
"""

from pathlib import Path
import voluptuous as vo
import os

class AttrDict(dict):
    """
    Attribute dictionary.

    Allows to access dictionary items by attribute.

    Examples
    --------
    >>> d = {'a': 1, 'b': 2}
    >>> ad = AttrDict(d)
    >>> ad.a
    ... 1
    """

    def __getattr__(self, item: str) -> any:
        """
        Returns the value of a dictionary key as an attribute.

        :param item: The name of the attribute to get.
        :return: The value of the item.
        """
        return self.get(item, None)

    def __getstate__(self) -> dict:
        """
        Returns the current state of the dictionary for pickling.

        :return: The current state.
        """
        return self

    def __setstate__(self, state: dict):
        """
        Updates the dictionary with a state dictionary.

        :param state: The state dictionary to update from.
        """
        self.update(state)


class AttrSchema(vo.Schema):
    """
    A voluptuous schema subclass that returns the validated data as an AttrDict.

    This allows attribute-style access on the validated data.
    """

    def __call__(self, d: dict) -> AttrDict:
        """
        Validates the input dictionary and returns an AttrDict.

        :param d: The input dictionary to validate.
        :return: The validated dictionary as an AttrDict.
        """
        return AttrDict(super().__call__(d))


schema_preprocess_labels = AttrSchema(
    vo.All(
        {
            vo.Required("name"): str,
            vo.Required("file"): str,
            vo.Required("path"): str})
)

schema_preprocess = AttrSchema(
    vo.All(
        {
            vo.Required("dataset"): str,
            vo.Required("labels"): vo.All([schema_preprocess_labels]),
            vo.Required("output_dir"): vo.Coerce(Path),
            vo.Optional("enhance_high_intensity", default=False): bool,
        }
    )
)

schema_model = AttrSchema(
    vo.All(
        {
            vo.Required("name"): vo.In(["ConvNet", "RotNet"]),
            vo.Optional("augmentation", default=False): bool,
            vo.Optional("crop", default=True): bool,
            vo.Optional("nlayers", default=0): vo.All(int, vo.Range(min=0)),
            vo.Optional("nblocks", default=0): vo.All(int, vo.Range(min=0)),
            vo.Optional("nfilters", default=0): vo.All(int, vo.Range(min=0)),
            vo.Optional("nunits", default=0): vo.All(int, vo.Range(min=0)),
            vo.Optional("batch_size", default=32): vo.All(int, vo.Range(min=1)),
            vo.Optional("dropout"): bool,
        }
    )
)

schema_tuner = AttrSchema(
    vo.All(
        {
            vo.Optional("name", default="RandomSearch"): vo.In(["RandomSearch", "Hyperband", "BayesianOptimization"]),
            vo.Optional("max_trials", default=10): vo.All(int, vo.Range(min=1)),
            vo.Optional("executions_per_trial", default=2): vo.All(int, vo.Range(min=1)),
            vo.Optional("max_epochs", default=100): vo.All(int, vo.Range(min=5)),
            vo.Optional("factor", default=3): vo.All(int, vo.Range(min=1)),
            vo.Optional("iterations", default=2): vo.All(int, vo.Range(min=1)),
            vo.Optional("objective", default='val_accuracy'): str,
            vo.Optional("tuning_complete", default=False): bool,
        }
    )
)
default_tuner = {
    "name": "RandomSearch",
    "max_trials": 10,
    "executions_per_trial": 2,
    "max_epochs": 100,
    "factor": 3,
    "iterations": 2,
    "objective": "val_accuracy",
    "tuning_complete": False,
}

schema_train = AttrSchema(
    vo.All(
        {
            vo.Required("dataset"): str,
            vo.Required("data_dir"): vo.Coerce(Path),
            vo.Required("model"): schema_model,
            vo.Optional("tuner", default=default_tuner): schema_tuner,
            vo.Required("labels"): vo.All([str]),
            vo.Required("epochs"): int,
            vo.Optional("early_stopping", default=False): bool,
            vo.Optional("checkpoint", default=""): str,
            vo.Optional("logfile", default=""): str,
            vo.Optional("metric", default="val_auc"): vo.In([
                "loss", "val_loss", "auc", "val_auc", "accuracy", "val_accuracy", "f1", "val_f1"
            ]),
            vo.Optional("multi_gpu", default=False): bool,
            vo.Optional("verbose", default=2): int,
        }
    )
)

schema_quality = AttrSchema(
    vo.All(
        {
            vo.Optional("brisque", default="low"): vo.In(["low", "med", "high", "none"]),
            vo.Optional("niqe", default="none"): vo.In(["low", "med", "high", "none"]),
            vo.Optional("contrast_ratio", default="high"): vo.In(["low", "med", "high", "none"]),
            vo.Optional("entropy", default="high"): vo.In(["low", "med", "high", "none"]),
            vo.Optional("sharpness", default="med"): vo.In(["low", "med", "high", "none"]),
            vo.Optional("skewness", default="med"): vo.In(["low", "med", "high", "none"]),
        }
    )
)
default_quality = {
    "brisque": "low",
    "niqe": "none",
    "contrast_ratio": "high",
    "entropy": "high",
    "sharpness": "med",
    "skewness": "med",
}

schema_thresholds = AttrSchema(
    vo.All(
        {
            vo.Optional("inversion", default=0.48): float,
            vo.Optional("lateral", default=0.36): float,
            vo.Optional("projection", default=0.46): float,
            vo.Optional("pacemaker", default=0.51): float,
        }
    )
)
default_thresholds = {
    "inversion": 0.48,
    "lateral": 0.36,
    "projection": 0.46,
    "pacemaker": 0.51,
}

schema_checkpoints = AttrSchema(
    vo.All(
        {
            vo.Optional("checkpoint_dir", default=Path("./models")): vo.Coerce(Path),
            vo.Optional("inversion", default="model_inversion.h5"): str,
            vo.Optional("lateral", default="model_lateral.h5"): str,
            vo.Optional("rotation", default="model_rotation.h5"): str,
            vo.Optional("projection", default="model_projection.h5"): str,
            vo.Optional("pacemaker", default="model_pacemaker.h5"): str,
        }
    )
)

schema_exclude_or_review = AttrSchema(
    vo.All(
        {
            vo.Optional("lateral", default="review"): vo.In(["exclude", "review"]),
            vo.Optional("aspect_ratio", default="review"): vo.In(["exclude", "review"]),
            vo.Optional("quality", default="review"): vo.In(["exclude", "review"]),
        }
    )
)
default_exclude = {
    "lateral": "review",
    "aspect_ratio": "review",
    "quality": "review",
}

schema_pipeline_ocr = AttrSchema(
    vo.All(
        {
            vo.Optional("lang", default='eng'): vo.In(["eng", "spa", "ita", "por"]),
            vo.Optional("method", default='combined'): vo.In(["combined", "python", "subprocess"]),
            vo.Optional("words_file", default=None): vo.In([vo.Coerce(Path), None]),
            vo.Optional("patterns_file", default=None): vo.In([vo.Coerce(Path), None]),
        }
    )
)
default_pipeline_ocr = {
    "lang": "eng",
    "method": "combined",
    "words_file": None,
    "patterns_file": None,
}

schema_options = AttrSchema(
    vo.All(
        {
            vo.Optional("autowindow_instead_of_luts", default=False): bool,
            vo.Optional("autowindow_if_no_luts", default=True): bool,
            vo.Optional("correct_inversion", default=True): bool,
            vo.Optional("correct_rotation", default=True): bool,
            vo.Optional("text_removal_method", default="telea"): vo.In(["telea", "ns", "bbox", "none"]),
            vo.Optional("ll_mask", default=True): bool,
            vo.Optional("save_stage2_intermediate", default=True): bool,
            vo.Optional("keep_stage2_intermediate", default=False): bool,
        }
    )
)
default_options = {
    "autowindow_instead_of_luts": False,
    "autowindow_if_no_luts": True,
    "correct_inversion": True,
    "correct_rotation": True,
    "text_removal_method": "telea",
    "ll_mask": True,
    "save_stage2_intermediate": True,
    "keep_stage2_intermediate": False,
}

schema_logging = AttrSchema(
    vo.All(
        {
            vo.Optional("level", default='info'): vo.In(["debug", "info", "warning", "error", "critical"]),
            vo.Optional("log_file", default=None): vo.In([vo.Coerce(Path), None]),
        }
    )
)
default_logging = {
    "level": "info",
    "log_file": None,
}

schema_run = AttrSchema(
    vo.All(
        {
            vo.Required("dataset"): str,
            vo.Required("data_dir"): vo.Coerce(Path),
            vo.Required("checkpoints"): schema_checkpoints,
            vo.Optional("json_dir", default=None): vo.In([vo.Coerce(Path), None]),
            vo.Optional("label_output_file", default="qc_data.csv"): vo.Coerce(Path),
            vo.Optional("image_output_dir", default="./qc_out"): vo.Coerce(Path),
            vo.Optional("image_review_dir", default="./qc_review"): vo.Coerce(Path),
            vo.Optional("use_gpu", default=False): bool,
            vo.Optional("cpu_workers", default=os.cpu_count()): vo.All(int, vo.Range(min=1)),
            vo.Optional("batch_size", default=16): vo.All(int, vo.Range(min=1)),
            vo.Optional("luts_applied", default=False): bool,
            vo.Optional("aspect_ratio_bounds", default=[0.85, 1.48]): vo.Schema([float, float]),
            vo.Optional("quality", default=default_quality): schema_quality,
            vo.Optional("thresholds", default=default_thresholds): schema_thresholds,
            vo.Optional("exclude_or_review", default=default_exclude): schema_exclude_or_review,
            vo.Optional("ocr", default=default_pipeline_ocr): schema_pipeline_ocr,
            vo.Optional("options", default=default_options): schema_options,
            vo.Optional("logging", default=default_logging): schema_logging,
        }
    )
)


def default_figure(v):
    if v is None:
        return {
            "figsize": (6, 6),
            "small_figsize": (6, 6),
            "large_figsize": (14, 21),
            "cm_figsize": (6, 6),
            "image_grid_size_factor": 5,
            "font_size": 14,
            "colormap": "Set3",
            "cm_cmap": "Set1",
            "edge_alpha": 1.0
        }
    return v


schema_figure = AttrSchema(
    vo.All(
        {
            vo.Optional("figsize", default=(6, 6)): vo.Schema((int, int)),
            vo.Optional("small_figsize", default=(6, 6)): vo.Schema((int, int)),
            vo.Optional("large_figsize", default=(14, 21)): vo.Schema((int, int)),
            vo.Optional("cm_figsize", default=(6, 6)): vo.Schema((int, int)),
            vo.Optional("cm_font_size", default=32): int,
            vo.Optional("image_grid_size_factor", default=5): vo.All(int, vo.Range(min=1)),
            vo.Optional("font_size", default=14): vo.All(int, vo.Range(min=1)),
            vo.Optional("colormap", default="Set3"): str,
            vo.Optional("cm_cmap", default="Set1"): str,
            vo.Optional("edge_alpha", default=1.0): float,
        }
    )
)

schema_dirs = AttrSchema(
    vo.All(
        {
            vo.Optional("checkpoint"): vo.Coerce(Path),
            vo.Optional("logfile"): vo.Coerce(Path),
            vo.Optional("evalfile"): vo.Coerce(Path),
            vo.Required("dev_labels"): vo.Coerce(Path),
            vo.Optional("test_labels"): vo.Coerce(Path),
            vo.Optional("inference", default=Path("./inference")): vo.Coerce(Path),
            vo.Optional("output", default=Path("./eval_output")): vo.Coerce(Path),
            vo.Optional("dict_table", default=Path("./dict_table.csv")): vo.Coerce(Path),
        }
    )
)

schema_evaluate = AttrSchema(
    vo.All(
        {
            vo.Required("task"): str,
            # vo.Required("labels"): vo.All([str]),
            vo.Required("datasets"): vo.Any(
                vo.In(["all", "all_holdout"]),
                vo.Schema(vo.All([
                    vo.In(["internal", "rsna", "brixia", "cuh"]),
                ], vo.Length(min=1), vo.Unique()))
            ),
            vo.Required("dirs"): schema_dirs,
            vo.Optional("figure", default=None): vo.All(default_figure, schema_figure),
            vo.Optional("nbatch", default=8): vo.All(int, vo.Range(min=1)),
            vo.Optional("use_gpu", default=False): bool,
            vo.Optional("multi_cpu", default=False): bool,
            vo.Optional("nworkers", default=5): vo.All(int, vo.Range(min=1)),
            vo.Optional("run_training_evaluator", default=False): bool,
            vo.Optional("calibrate", default=False): bool,
            vo.Optional("run_holdout_evaluator", default=True): bool,
            vo.Optional("exclude_unsuitable", default=True): bool,
            vo.Optional("nbootstrap", default=1000): vo.All(int, vo.Range(min=1)),
            vo.Optional("bounds", default=[0.85, 1.48]): vo.Schema([float, float]),
            vo.Optional("ocr_only_use_if_pred_made", default=True): bool,
            vo.Optional("run_demogs", default=False): bool,
        }
    )
)

