# AutoQC Notebooks

This directory contains Jupyter notebooks used in development of the AutoQC package 
and to generate tables and figures for the paper.

The included notebooks are:

| Notebook                               | Description                                                                                               |
|----------------------------------------|-----------------------------------------------------------------------------------------------------------|
| `select_one_image_per_patient.ipynb`   | Selects one image per patient from a dataset at random.                                                   |
| `parition_steps_1-2.ipynb`             | Creates the train, val, test splits and selects images for development of Stages 1 and 2 of the pipeline. |
| `partition_step_3-4.ipynb`             | Selects images for development of Stages 3 and 4 of the pipeline.                                         |
| `create_preprocessing_commands.ipynb`  | Creates the bash commands to run the preprocessing pipeline for each label/tool.                          |
| `quality_dev_eval.ipynb`               | Review the quality metrics on the development set.                                                        |
| `quality_test_eval.ipynb`              | Evaluates the quality metrics on the test sets.                                                           |
| `inpainting_checker.ipynb`             | Used to visualise saved comparison images and label the inpainting results to evaluate text removal.      |
| `inpainting_eval.ipynb`                | Evaluates the inpainting results.                                                                         |
| `inpainting_cropping_quant_eval.ipynb` | A quantitative evaluation inpainting and cropping.                                                        |
| `collate_external_testsets.ipynb`      | Collates the external testset paths for each step and labels into a single file                           |
| `create_main_figures.ipynb`            | Creates the main figures and tables for the paper.                                                        |
| `tuner_hp_table.ipynb`                 | Creates the table of hyperparameters for the tuner.                                                       |
| `make_demogs_table.ipynb`              | Makes a table of demographics for the data for inclusion in paper.                                        |
| `image_viz.ipynb`                      | Allows easy visualisation of images in a directory or listed in a text file.                              |
| `gradcam_viz.ipynb`                    | Allows easy GradCAM visualisation.                                                                        |
