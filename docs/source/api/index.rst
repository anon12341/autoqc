.. _sect-api:

API Reference
=============

The following pages provide a reference to the modules and functions used in the pipeline to allow for 
further development and customisation.

.. toctree::
   :maxdepth: 4

   auto_qc.indices

.. toctree::
   :maxdepth: 4
   :caption: Subpackages

   auto_qc.classification
   auto_qc.models
   auto_qc.operations
   auto_qc.scripts
   auto_qc.data_explorer

.. toctree::
   :maxdepth: 4
   :caption: Modules

   auto_qc.cli
   auto_qc.conf
   auto_qc.lazyloader
   auto_qc.utils
