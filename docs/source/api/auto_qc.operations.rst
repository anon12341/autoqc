auto\_qc.operations
===================

auto\_qc.operations.boundary
----------------------------

.. automodule:: auto_qc.operations.boundary
   :members:
   :undoc-members:
   :show-inheritance:

auto\_qc.operations.inpaint
---------------------------

.. automodule:: auto_qc.operations.inpaint
   :members:
   :undoc-members:
   :show-inheritance:

auto\_qc.operations.ocr
-----------------------

.. automodule:: auto_qc.operations.ocr
   :members:
   :undoc-members:
   :show-inheritance:

auto\_qc.operations.rotate
--------------------------

.. automodule:: auto_qc.operations.rotate
   :members:
   :undoc-members:
   :show-inheritance:
