auto\_qc.classification.live\_qa
================================

auto\_qc.classification.live\_qa.brisque
----------------------------------------

.. automodule:: auto_qc.classification.live_qa.brisque
   :members:
   :undoc-members:
   :show-inheritance:

auto\_qc.classification.live\_qa.niqe
-------------------------------------

.. automodule:: auto_qc.classification.live_qa.niqe
   :members:
   :undoc-members:
   :show-inheritance:

auto\_qc.classification.live\_qa.utils
--------------------------------------

.. automodule:: auto_qc.classification.live_qa.utils
   :members:
   :undoc-members:
   :show-inheritance:
