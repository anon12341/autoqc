document.addEventListener("DOMContentLoaded", function () {
  // Side tab
  var typeformDiv = document.createElement("div");
  typeformDiv.setAttribute("data-tf-sidetab", "VADVHaTq");
  typeformDiv.setAttribute("data-tf-custom-icon", "https://images.typeform.com/images/jQFUEpyUpq4U");
  typeformDiv.setAttribute("data-tf-opacity", "90");
  typeformDiv.setAttribute("data-tf-iframe-props", "title=NCCIDxClean Pre-print Request");
  typeformDiv.setAttribute("data-tf-auto-close", "10000");
  typeformDiv.setAttribute("data-tf-transitive-search-params", "");
  typeformDiv.setAttribute("data-tf-button-color", "#39ACCF");
  typeformDiv.setAttribute("data-tf-button-text", "Request pre-print!");
  typeformDiv.setAttribute("data-tf-medium", "snippet");
  typeformDiv.setAttribute("style", "all:unset;");

  document.body.appendChild(typeformDiv);

  // Chat icon
  var chatDiv = document.createElement("div");
  chatDiv.setAttribute("data-tf-popover", "VADVHaTq");
  chatDiv.setAttribute("data-tf-custom-icon", "https://images.typeform.com/images/FpeRmNzBSMS3");
  chatDiv.setAttribute("data-tf-opacity", "90");
  chatDiv.setAttribute("data-tf-iframe-props", "title=NCCIDxClean Pre-print Request");
  chatDiv.setAttribute("data-tf-chat", "");
  chatDiv.setAttribute("data-tf-auto-close", "10000");
  chatDiv.setAttribute("data-tf-transitive-search-params", "");
  chatDiv.setAttribute("data-tf-button-color", "#39ACCF");
  chatDiv.setAttribute("data-tf-medium", "snippet");
  chatDiv.setAttribute("style", "all:unset;");

  document.body.appendChild(chatDiv);

  var script = document.createElement("script");
  script.setAttribute("src", "//embed.typeform.com/next/embed.js");
  document.body.appendChild(script);
});
