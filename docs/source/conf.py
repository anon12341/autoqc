# Configuration file for the Sphinx documentation builder.

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import os
import sys
sys.path.insert(0, os.path.abspath('../../../auto-qc'))


# -- Project information -----------------------------------------------------

project = 'AutoQC'
copyright = '- Details removed for peer review anonymization - please see LICENSE at https://gitlab.com/anon12341/autoqc/-/blob/master/LICENSE.md'
author = 'Anonymized'

import auto_qc

version = release = auto_qc.__version__

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'myst_parser',
    'sphinx.ext.intersphinx',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.mathjax',
    'sphinx.ext.viewcode',
    "numpydoc",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.todo",
    "sphinx_markdown_tables",
    "sphinx_copybutton",
    "sphinx_search.extension",
    'sphinx_autodoc_typehints',
    # "sphinxext.rediraffe",
    "sphinx_design",
    "jupyter_sphinx",
    "matplotlib.sphinxext.plot_directive",
    # "myst_nb",
    "sphinx_togglebutton",
    # "sphinx_favicon",
    'sphinx_tabs.tabs',
]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", "**.ipynb_checkpoints"]

# -- Options for autodoc ----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#configuration

# Automatically extract typehints when specified and place them in
# descriptions of the relevant function/method.
autodoc_typehints = "description"

# Don't show class signature with the class' name.
autodoc_class_signature = "separated"

# -- Sitemap -----------------------------------------------------------------

# ReadTheDocs has its own way of generating sitemaps, etc.
if not os.environ.get("READTHEDOCS"):
    extensions += ["sphinx_sitemap"]

    html_baseurl = os.environ.get("SITEMAP_URL_BASE", "http://127.0.0.1:8000/")
    sitemap_locales = [None]
    sitemap_url_scheme = "{link}"

# -- autosummary -------------------------------------------------------------

autosummary_generate = True

# -- Internationalization ----------------------------------------------------

# specifying the natural language populates some key tags
language = "en"

# -- MyST options ------------------------------------------------------------

# This allows us to use ::: to denote directives, useful for admonitions
myst_enable_extensions = ["colon_fence", "linkify", "substitution"]
myst_heading_anchors = 2
myst_substitutions = {"rtd": "[Read the Docs](https://readthedocs.org/)"}

# -- Options for HTML output -------------------------------------------------

html_theme = "pydata_sphinx_theme"
html_logo = "_static/autoqc_logo.png"
html_favicon = "_static/autoqc_logo.ico"
html_sourcelink_suffix = ""
html_sidebars = {
    "**": ["sidebar-nav-bs.html", "sidebar-ethical-ads.html"]
}

html_theme_options = {
    "external_links": [
        {
            "url": "",
            "name": "Request Paper",
            "target": "_blank",
            "rel": "noopener"
        },
        {
            "url": "https://gitlab.com/anon12341/speedyqc",
            "name": "SpeedyQC"
        },
        {
            "url": "",
            "name": "Collab Website"
        },
    ],
    "gitlab_url": "https://gitlab.com/anon12341/autoqc",
    "header_links_before_dropdown": 5,
    "icon_links": [
        # {
        #     "name": "PyPI",
        #     "url": "https://pypi.org/project/pydata-sphinx-theme",
        #     "icon": "fa-solid fa-box",
        # },
        {
            "name": "SpeedyQC",
            "url": "https://gitlab.com/anon12341/speedyqc",
            "icon": "_static/speedy_qc_white_panel.svg",
            "type": "local",
        },
    ],
    "logo": {
        "text": "AutoQC",
        "image_dark": "_static/autoqc_logo.png",
        "alt_text": "AutoQC",
    },
    # "use_edit_page_button": True,
    "show_toc_level": 2,
    "navbar_align": "left",  # [left, content, right] For testing that the navbar items align properly
    "navbar_center": ["navbar-nav"],
    "announcement": """WARNING! This is an anonymized version of the repository for academic peer review, please do not use.""",
    "show_nav_level": 1,
    # "navbar_start": ["navbar-logo"],
    # "navbar_end": ["theme-switcher", "navbar-icon-links"],
    # "navbar_persistent": ["search-button"],
    # "primary_sidebar_end": ["sidebar-ethical-ads.html"],
    # "article_footer_items": ["prev-next.html", "test.html", "test.html"],
    # "content_footer_items": ["prev-next.html", "test.html", "test.html"],
    # "footer_start": ["test.html", "test.html"],
    # "secondary_sidebar_items": ["page-toc.html"],  # Remove the source buttons
}

html_context = {
    "gitlab_user": "anon12341",
    "gitlab_repo": "auto-qc",
    "gitlab_version": "main",
    "doc_path": "docs",
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]
todo_include_todos = True


# -- autodoc output location -------------------------------------------------------

# Path to the directory containing the API modules
apidir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'api'))

# Configure autodoc to search for modules in the API directory
# autodoc_mock_imports = []
# for filename in os.listdir(apidir):
#     if filename.endswith('.rst'):
#         module = os.path.splitext(filename)[0]
#         autodoc_mock_imports.append(module)

# Generate an index for the API user_guide
master_doc = 'index'


# -- application setup -------------------------------------------------------

# Include the Typeform side tab JavaScript file
# html_js_files = [
#     "custom.js",
# ]

def setup_to_main(app, pagename, templatename, context, doctree):
    def to_main(link: str) -> str:
        """Transform "edit on github" links and make sure they always point to the main branch
        Args:
            link: the link to the github edit interface
        Returns:
            the link to the tip of the main branch for the same file
        """
        links = link.split("/")
        idx = links.index("edit")
        return "/".join(links[: idx + 1]) + "/main/" + "/".join(links[idx + 2 :])

    context["to_main"] = to_main


def setup(app):
    app.connect("html-page-context", setup_to_main)
