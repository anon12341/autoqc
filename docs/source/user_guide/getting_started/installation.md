---
myst:
  html_meta:
    "description lang=en": |
      Installation instructions for AutoQC.
---

# Installation

Installation is performed in two steps: installing the package and downloading the models.

## Installing the Package

The package and dependencies may be installed using pip:
```console
pip install git+https://gitlab.com/anon12341/autoqc/
```

Alternatively, the git repository may be cloned and installed locally:
```console
git clone git+https://gitlab.com/anon12341/autoqc/
cd auto-qc
pip install .
```

```{hint}
It is advised to install the package in a virtual environment (conda or venv) with python 3.7 or higher.
```

## Downloading the Models

The models are stored on a separate data hosting service. They can be downloaded from the command line
or within python:

::::{tab-set}
:::{tab-item} Console
```console
autoqc download-models
```
or 
```console
python -m auto_qc.download_models
```
:::
:::{tab-item} Python
```python
import auto_qc

auto_qc.download_models()
```
:::
:::{tab-item} Manual Download
They can be downloaded from the web browser using 
this [link (removed for anonymization)]().
:::
::::

```{note}
Two sets of models are included: 

1. The models used in our paper to allow for replication of the results.
   - Datasets: NCCID and BIMCV
   - Nomenclature: `model_<tool_name>_paper.h5`
   
2. The models retrained on all of our datasets (i.e. the development and test sets), which should demonstrate superior 
performance for use in practice.
   - Datasets: NCCID, BIMCV, UH, BrixIA, and MIDRC-RICORD-1c
   - Nomenclature: `model_<tool_name>.h5`
```

```{warning}
AutoQC remains under development. Use at your own risk and please report any issues on our 
[GitLab repository]().
```
