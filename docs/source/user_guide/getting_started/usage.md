---
myst:
  html_meta:
    "description lang=en": |
      Usage instructions for the AutoQC pipeline.
---

# Usage

## Running the AutoQC Pipeline

The package may be run from the command line or within python and can be run on a single image or a directory of images. 
The user can choose to use a single CPU (default) or multiple CPUs, and can utilise a GPU for model inference if 
available.

::::{tab-set}
:::{tab-item} Console
With default settings:
  ```console
  autoqc run --img_path /path/to/images/*
  ```
With command line arguments:
  ```console
  autoqc run --img_path /path/to/images/* --outpath /path/to/output/directory --cpu_workers 5 --gpu true --batch_size 32
  ```
With a config file:
  ```console
  autoqc run --config /path/to/config.yaml
  ```
Using python:
  ```console
  python -m auto_qc.run_pipeline --config /path/to/config.yaml
  ```
:::

:::{tab-item} Python / Jupyter
```python
from argparse import Namespace
from auto_qc.scripts.run import run_pipeline

args = Namespace(config_path="/path/to/config.yaml")
run_pipeline(args)
```
:::
::::

```{hint}
Using a config file allows for slightly greater customisation.
```

### Configuration

To **view the options for command line configuration:**
```console
autoqc run -h
```

The **config file** is a YAML file with the following structure:

- `quality`: Specifies image quality metrics and their respective defaults.
    - e.g., brisque: ["low", "med", "high", "none"]
- `checkpoints`: Points to model checkpoint files and directories.
    - e.g., checkpoint_dir: Path("./models"), inversion: "model_inversion.h5"
- `thresholds`: Specifies various model thresholds.
    - e.g., inversion: 0.48, lateral: 0.36
- `exclude_or_review`: Decides the action to take based on specific criteria.
    - e.g., lateral: ["exclude", "review"], quality: ["exclude", "review"]
- `ocr`: Configurations related to Optical Character Recognition (OCR).
    - e.g., lang: ["eng", "spa", "ita", "por"], method: ["combined", "python", "subprocess"]
- `options`: Miscellaneous options for pipeline behavior.
    - e.g., autowindow_instead_of_luts: bool, correct_inversion: bool
- `run`: Overall structure encapsulating all other schemas to configure the pipeline's behavior.
    - Includes dataset specification, paths, GPU usage, and others.

````{dropdown} Config file example...
:color: gray
:icon: tools

  ```yaml
  # config.yaml
  
  # dataset name
  dataset: dataset_name
  
  # input path
  data_dir: /path/to/images
  
  # output paths
  label_output_file: /path/to/labels.csv
  image_output_dir: /path/to/output/images
  image_review_dir: /path/to/directory/for/review/images
  
  # whether to use a gpu or multiple cpus. leave cpu_workers=1 for no multiprocessing.
  use_gpu: true
  cpu_workers: 5
  batch_size: 16
  
  # have the look-up tables already been applied?
  luts_applied: false
  
  # filenames of the model checkpoints in checkpoint_dir
  checkpoints:
      checkpoint_dir: /path/to/checkpoints   # common directory of the models. if none, put full directory for each checkpoint.
      inversion: model_inverted.h5
      lateral: model_lateral.h5
      rotation: model_rotation.h5
      projection: model_projection.h5
      pacemaker: model_pacemaker.h5
  
  # classification model thresholds
  thresholds:
      inversion: 0.48                         # default = 0.48
      lateral: 0.36                           # default = 0.36
      projection: 0.46                        # default = 0.46
      pacemaker: 0.51                         # default = 0.51
  
  aspect_ratio_bounds: [0.85, 1.48]           # (lower bound, upper bound) to use, default = (0.85, 1.48)
  
  # sensitivities of the quality scoring tools
  quality:
      brisque: low                            # default = low
      niqe: none                              # default = none
      contrast_ratio: high                    # default = high
      entropy: high                           # default = high
      sharpness: med                          # default = med
      skewness: med                           # default = med
  
  # whether to exclude, review or leave radiographs identified in these tools.
  #   - exclude: the images will not be included in the output folder.
  #   - review: the outputs will be stored in a separate folder for review.
  #   - leave: the images will be
  exclude_or_review:
      lateral: exclude
      aspect_ratio: exclude
      quality: review
      
  # options for the optical character recognition
  ocr:
      method: combined                        # options are 'combined', 'python', or 'subprocess'
      lang: eng                               # options are 'eng', 'spa', 'por', 'ita'
  
  # additional options...
  options:
      autowindow_instead_of_luts: false       # use autowindowing instead of luts
      autowindow_if_no_luts: true             # apply autowindowing if no luts in metadata
      correct_inversion: true                 # whether to correct grayscale inversion
      correct_rotation: true                  # whether to correct any 90-degree rotation
      text_removal_method: telea              # annotation / text removal method, options: telea (default), ns, bbox, none
      ll_mask: true                           # use lower level mask in text removal
      save_stage2_intermediate: true          # save stage 2 intermediate to avoid running text removal twice
      keep_stage2_intermediate: true          # whether to keep the stage 2 image
  ```
````

### Outputs

**Images**: The preprocessed images are stored as 16-bit grayscale .png files in the output directory specified by the user. If 
files are excluded they are not stored, whilst if they are for review, they are stored in a separate directory.

**Labels**: The output labels are stored as a `.csv` file in the output directory specified by the user. This includes the 
radiographic projection inferred from the optical character recognition (OCR).

**OCR Outputs**: The raw outputs of the optical character recognition (OCR) are stored in a separate `.csv`  file specified by the user.
This includes addtional terms found in the image, such as patient positioning (e.g. "ERECT", "SUPINE") and whether the 
radiograph was portable.

**DICOM Headers**: The DICOM header information is stored in a separate `.csv` file specified by the user.

## Using the AutoQC Package to Develop New Models

The AutoQC package can be used to develop new models. The package is designed to be modular, and so new models can be
added to the pipeline with relative ease. Furthermore, the user can train their own models with ease from the command 
line or python. Basic information on how to do this is provided in the [Deep Learning Models section](sect-train), with 
additional information provided in the [API documentation](sect-api).
