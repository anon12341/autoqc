---
myst:
  html_meta:
    "description lang=en": |
      User guide for AutoQC.
---

# Image Quality

:::{button-ref} buttons
:color: primary
:outline:
AutoQC Stage 4
:::

Image quality, in terms of its suitability for use in machine learning, is difficult to 
define and can be subjective. The task is made more difficult as it is a no-reference problem, 
i.e. we are not comparing the image to another version of itself as is done in PSNR, SSIM, etc. 

To provide an objective measure of quality, we have included a number of metrics which
are calculated for each image:

- Contrast Ratio is the ratio of the range of values between the 2nd and 98th percentile pixel 
intensities to that in the full image.
- Entropy is influenced by image size. We calculate this metric from the mean of equally sized 
patches to mitigate this. 
- Sharpness: We estimate sharpness by calculating the variance of its Laplacian-filtered image.
- [NIQE](https://live.ece.utexas.edu/research/quality/niqe_spl.pdf) and 
[BRISQUE](https://live.ece.utexas.edu/publications/2012/TIP%20BRISQUE.pdf) are no-reference quality metrics for natural images. NIQE was 
fine-tuned on 185 ‘pristine’ radiographs from the development set using the [code provided 
by the authors](https://github.com/utlive/niqe).

All these metrics were calculated for the entire image and after cropping the central 
50% of pixels to capture the values for the most crucial area in the radiograph, with the 
final value being the sum of the two. We identify outliers as images with metrics falling 
outside the range set using the mean absolute difference on the development set. 

Although not used in the AutoQC pipeline, the outputs also include:
- Pixel resolution
- Noise estimate
- Mean pixel intensity
- Median pixel intensity
- Standard deviation of pixel intensity
- Variance of pixel intensity
- Fast Fourier Transform (FFT) contrast of the image

## Example Usage

### Obtaining Metrics

::::{tab-set}
:::{tab-item} Console
```console
autoqc quality_scores <path to image>
```

to run on a directory of images, specify the output directory and run on multiple cpu workers:

```console
autoqc quality_scores <path to image directory>/* --output_dir <path to output directory> --nworkers <number of workers>
```

The output is a csv file with a range of quality metrics for each image.
:::
:::{tab-item} Python

To run on a single image:
```python
from auto_qc.classification.quality import quality

metrics = [
    'niqe',
    'niqe_cutout50',
    'brisque',
    'brisque_cutout50',
    'contrast_ratio',
    'contrast_ratio_cutout50',
    'entropy',
    'entropy_cutout50',
    'mean_intensity',
    'median_intensity',
    'std_intensity',
    'var_intensity',
    'fft_contrast',
    'sharpness',
    'sharpness_cutout50',
    'resolution',
    'noise_estimate',
    'skewness',
    'skewness_cutout50',
]

quality_scores = quality("/path/to/image", metrics)
```
or to run on an image array:

```python
from auto_qc.classification.quality import quality
import imageio.v2 as iio

image = iio.imread("/path/to/image")

metrics = [
    '... as above ...',
]

quality_scores = quality("img_id", metrics, image_array=image)
```

In both cases, the output is a dictionary of quality metrics for the image.
:::
::::

### Obtaining Metrics and Outlier Detection

Outlier detection is performed in the AutoQC pipeline, but otherwise must be done in python:

```python
from auto_qc.scripts.run import calculate_quality
import imageio.v2 as iio

image = iio.imread("/path/to/image")

quality_metrics = calculate_quality('image_identifier', image)
```

The dictionary key `outlier_score` will equal the count of the number of metrics the image is an outlier for.

To customise the thresholds for outlier detection, pass the `quality_config` argument to `calculate_quality`:

```python
from auto_qc.scripts.run import calculate_quality
import imageio.v2 as iio

image = iio.imread("/path/to/image")
quality_config = {
    'niqe_combined': {'partition': 'upper', 'sensitivity': 'low'},
    'brisque_combined': {'partition': 'upper', 'sensitivity': 'low'},
    'contrast_ratio_combined': {'partition': 'both', 'sensitivity': 'high'},
    'entropy_combined': {'partition': 'lower', 'sensitivity': 'high'},
    'sharpness_combined': {'partition': 'both', 'sensitivity': 'med'},
    'skewness_combined': {'partition': 'upper', 'sensitivity': 'med'},
}

quality_metrics = calculate_quality('image_identifier', image, quality_config)
```

## Developing your own Quality Metrics

### Determining Outlier Sensitivities and Metrics to Utilise

To determine the optimal outlier sensitivities and metrics to use, we use the `quality_tuner` script
after obtaining the quality metrics for the development set as above:

::::{tab-set}
:::{tab-item} Console
```console
autoqc quality_tuner <path to quality metrics json> --outpath <path to output directory> --nworkers <number of workers if using multiple cpus>
```
:::
:::{tab-item} Python
```python
from auto_qc.scripts.quality_tuner import run_quality_tuner
from argparse import Namespace

args = Namespace(
    quality_metrics_path="/path/to/quality/metrics.json",
    output_dir="/path/to/output/directory",
    nworkers=4,
)

run_quality_tuner(args)
```
:::
::::

This script aims to find the optimal constant for an objective function that minimizes 
both the standard deviation of a particular quality metric in a dataset and the rate of data removal. 
The objective function utilises the Median Absolute Deviation (MAD) for outlier detection and calculates
the MAD for a range of constants (1-5 at intervals of 0.1). The removal rate and standard deviation are
then calculated for each constant and the optimal constant is the one that minimises the objective function:

```math
objective = std_dev + lambda * removal_rate
```

where $lambda_ = sensitivity * initial_std_dev$

`lambda_` serves as a regularisation parameter acting as the 'sensitivity' of the outlier detection. We
calculate the optimal constant and the corresponding bounds for each metric and outlier sensitivity (low: 
`sensitivity` = 3.5; medium: `sensitivity` = 2.5; and high: `sensitivity` = 1.5).

The output is a json file with the optimal constants and bounds for each metric and outlier sensitivity,
along with a correlation matrix of the quality metrics and examples of the images removed for each metric
at each sensitivity and examples with high, medium and low values.

### Evaluation of the Tuning

The [`quality_dev_eval.ipynb`]() is used to review and analyse the results of the tuning, along with
the output figures. You then select the metrics to use and the outlier sensitivities for each metric.

### Testing on the Test Sets

After obtaining the quality metrics for each test set, we can evaluate the performance of the
quality metrics and outlier detection on the test sets using the [`quality_test_eval.ipynb`]() notebook.
