---
myst:
  html_meta:
    "description lang=en": |
      User guide for AutoQC.
---

# Aspect Ratio

:::{button-ref} buttons
:color: primary
:outline:
AutoQC Stage 2
:::

A simple statistical method of removing images which are unlikely to contain at least two thirds of
the lungs. Calculated after cropping and the upper and lower bounds were determined by 
on our development data and then externally validated.

## Example Usage

To calculate the aspect ratio of an image in python, you can use:

```python
from auto_qc.classification.aspect_ratio import calc_aspect_ratio
import imageio.v2 as iio 

image = iio.imread("path/to/image")
aspect_ratio = calc_aspect_ratio(image.shape[0], image.shape[1])
```

or for a dataframe of images:

```python
from auto_qc.classification.aspect_ratio import calc_aspect_ratio
import pandas as pd

df = pd.read_csv("path/to/dataframe.csv")
df["aspect_ratio"] = calc_aspect_ratio(df["height"], df["width"])
```

The bounds identified in the development data are provided in [our paper, which can be
requested](https://rrw71r81hp8.typeform.com/to/ifo65jAj).

## Get your own bounds

To get your own bounds, you can follow the code used in the [example notebook](), 
although you will need labelled data as specified in that notebook.

The evaluate.py script can be used to evaluate the aspect ratio on a dataset using a config file
as for the deep learning models:

```yaml
# eval_aspect_ratio.yaml

task: aspect_ratio

datasets: all

dirs:
  dev_labels: path_to_dev_labels_directory
  test_labels: path_to_test_labels_csv
  inference: path_to_directory_to_store_shapes
  output: path_to_directory_to_store_output

bounds: [0.85, 1.48]

run_demogs: true

exclude_unsuitable: true
nbootstrap: 1000
nbatch: 8

use_gpu: false
multi_cpu: true
nworkers: 60
```

Then run the evaluation with:

::::{tab-set}
:::{tab-item} Console
```console
autoqc evaluate --config path/to/config.yaml
```
:::
:::{tab-item} Python
```python
from argparse import Namespace
from auto_qc.scripts.evaluate import evaluate

args = Namespace(config_path="/path/to/config.yaml")
evaluate(args)
```
:::
::::
