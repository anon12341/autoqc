---
myst:
  html_meta:
    "description lang=en": |
      User guide for AutoQC.
---

# Text / Annotation Removal

:::{button-ref} buttons
:color: primary
:outline:
AutoQC Stage 2
:::

Image annotations are identified using two steps:

1. **High-level text**: text is identified if there is a spike of over ten times
the number of pixels with a value of 255 in an image converted to 8-bit image
compared to 254. It then creates a mask using a combination of filters and
thresholding.
2. **Low-level text**: text is identified using a Sobel edge detection,
median filters, and contouring. 

Telea’s method is used for inpainting by default, but the code allows for the user
to use the Navier-Stokes method or simply draw a rectangle of zero values around the 
areas identified.

## Example Usage

::::{tab-set}
:::{tab-item} Console
:sync: console
```console
autoqc inpaint <path to image>
```

To specify the method of text removal and the output directory, use the following:
```console
autoqc inpaint <path to image> --method <method> --use-ns <boolean> --output-dir <path to output directory>
```
where the available methods are `inpaint_cxr` and `bbox`, and the --use-ns flag is a boolean determining
whether Navier-Stokes or Telea inpainting is used if method = inpaint_cxr (default = `False`).

Help can be accessed using: `autoqc inpaint --help`
:::
:::{tab-item} Python
:sync: python
```python
import imageio.v2 as iio
from auto_qc.operations.inpaint import inpaint_cxr

img = iio.imread("path/to/image")
inpainted = inpaint_cxr(img)
```

Help can be accessed using: `help(inpaint_cxr)`
:::
::::

## Comparison Images

Comparison images can be generated using the following:

::::{tab-set}
:::{tab-item} Console
:sync: console
```console
autoqc inpaint_comparison <path to image> --original_dir <path to directory containing original images>
```
:::
:::{tab-item} Python
:sync: python
```python
from auto_qc.operations.inpaint import create_comparison

comparison_img = create_comparison("path/to/original", "path/to/inpainted", "path/to/output/dir")
```
:::
::::

:::{figure} ../../_static/inpaint_comparison_example.png
:alt: Inpainting comparison example
:align: center
Example of inpainting comparison output.
:::

## Review / Output Labelling Notebook

The comparison images were reviewed and labelled using widgets in Jupyter notebook 
([`inpainting_checker.ipynb`]()).  The results were then evaluated in 
[`inpainting_eval.ipynb`]().
