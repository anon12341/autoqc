---
myst:
  html_meta:
    "description lang=en": |
      User guide for AutoQC.
---

# Cropping of Padding / Borders

:::{button-ref} buttons
:color: primary
:outline:
AutoQC Stage 2
:::

The tool will remove padding or a border from the image, even if there is text extending into it.

## Example Usage

::::{tab-set}
:::{tab-item} Console
```console
autoqc remove_padding <path to image> --outpath <output directory> --bit_depth <bit depth>
```
:::
:::{tab-item} Python
```python
import imageio.v2 as iio
from auto_qc.operations.boundary import remove_border

img = iio.imread("path/to/image")
new_img = remove_border(img)
```
:::
::::

## Example Output

:::{figure} ../../_static/cropping_example.png
:fig-width: 50%
:alt: Cropped image example
:align: center
A real example of cropping of padding from a radiograph.
:::

