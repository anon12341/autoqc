---
myst:
  html_meta:
    "description lang=en": |
      User guide for AutoQC.
---

# Pixel Intensity Transformations

:::{button-ref} buttons
:color: primary
:outline:
AutoQC Stage 1
:::

[Modality look-up tables](https://dicom.innolitics.com/ciods/cr-image/modality-lut) (LUTs) and 
windowing or [volume of interest LUTs](https://dicom.innolitics.com/ciods/cr-image/voi-lut) 
are applied as specified in the metadata. If no LUT is specified,
auto-windowing is applied. Given that there are no standardised pixel units
as there are in CT, these LUTs allow for a degree of image standardisation
between manufacturers and models regardless of the acquisition parameters
or scanner characteristics. 

:::{figure} ../../_static/luts_example.png
:alt: LUTs example
:align: center
Examples of chest radiographs before and after application of the default LUTs.
:::

If available, the 
[Photometric Interpretation (0028,0004)](https://dicom.innolitics.com/ciods/cr-image/image-pixel/00280004) 
DICOM tag is used to determine whether the image is stored in an inverted state. If necessary, the 
image is inverted.

## Example Usage

To apply the transforms to an image outside of the AutoQC pipeline, the following code can be used in
python:

```python
from auto_qc.scripts.run import read_image_and_metadata, pixel_intensity_transforms

image_path = "path/to/image.dcm"

img, ds = read_image_and_metadata(image_path)
pit_img = pixel_intensity_transforms(img, ds, luts_applied=False)
```
