---
myst:
  html_meta:
    "description lang=en": |
      User guide for AutoQC Data Explorer.
---

# Overview

The data explorer is a tool for visualising the outputs of the AutoQC pipeline. It is built using 
[Dash](https://dash.plotly.com/) and allows for:
- statistical reports to be automatically generated using [YData Profiling](https://docs.profiling.ydata.ai/) 
(formerly pandas-profiling), visualised in a web browser and saved. 
- comparison of the outputs of the AutoQC pipeline with the DICOM headers.
- interactive plotting of the AutoQC outputs, DICOM headers and patient demographics.

![Screenshots of the AutoQC Data Explorer](../../_static/data_explorer_screenshots.png)

It can be run from the command line using:

```console
autoqc data_explorer --data /path/to/data/*.csv
```

You can also use multiple .csv files, incase you have separate files with the AutoQC, DICOM headers and patient 
demographics.
  
```console
autoqc data_explorer --data /path/to/autoqc/data/*.csv /path/to/other/data1/*.csv /path/to/other/data2/*.csv
```

In this case, the data explorer will merge the data from the different files into a single table using the 'name' column
by default. If there is no name column, it will try to find a path column and will use the filename with the extension 
removed. You can specify a different column to merge on using the `--join_on` argument.

You can then view the data explorer in your browser at `http://localhost:5000/` or `http://127.0.0.1:5000` by default,
but you can specify a different address and port using the `--host` and `--port` arguments.

## Settings

| Argument              | Description                                                     | Default                                        |
|-----------------------|-----------------------------------------------------------------|------------------------------------------------|
| `--data`              | Path to the data to be visualised.                              | None                                           |
| `--join_on`           | Column to join the data on.                                     | 'name'                                         |
| `--host`              | Host address to run the data explorer on.                       | '127.0.0.1'                                    |
| `--port`              | Port to run the data explorer on.                               | 5000                                           |
| `--data_schema`       | Path to the data schema.                                        | 'auto_qc/data_explorer/assets/data_schema.yml' |
| `--value_maps`        | Path to the value maps.                                         | 'auto_qc/data_explorer/assets/value_maps.yml'  |
| `--cat_limit`         | Maximum number of categories to plot for categorical variables. | 15                                             |
| `--all_dicom_headers` | Whether to include all DICOM headers in the data schema.        | False                                          |
| `--debug`             | Whether to run the data explorer in debug mode.                 | False                                          |

### Further Details

**The Data Schema:**
The data schema utilised is defined in the [data schema](auto_qc/data_explorer/assets/data_schema.yml). This can be
edited as needed to your specific use case. The `--data_schema` argument can be used to specify the new schema file.

**Value Maps:** 
The value maps utilised is defined in the [value_maps](auto_qc/data_explorer/assets/value_maps.yml) file. This can be
edited as needed to your specific use case. The `--value_maps` argument can be used to specify the new schema file.

**Category Limits:**
The maximum number of categories to use for plotting categorical variables can be specified using the `--cat_limit`
argument. The default is 15.

**DICOM Headers:**
The `--all_dicom_headers` argument can be used to specify whether to utilise all DICOM headers within the schema. By 
default, only headers that may be useful in analysing X-ray datasets prior to model development are included.

**Debug Mode:**
The `--debug` argument can be used to run the data explorer in debug mode, which will allow you to edit the code and
see the changes in the browser without having to restart the server.
