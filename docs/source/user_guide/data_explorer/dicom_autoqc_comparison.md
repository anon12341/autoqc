---
myst:
  html_meta:
    "description lang=en": |
      User guide for AutoQC Data Explorer.
---

# DICOM - AutoQC Comparison

This page allows users to compare the DICOM header information with the AutoQC output for the same images.

## Grayscale Inversion

The Photometric Interpretation (0028,0004) tag is used to determine whether the image has inverted grayscale values.
If available, this is applied during the AutoQC pipeline, prior to analysis by the grayscale inversion detection tool.
For comparison purposes, the Data Explorer extrapolates the AutoQC grayscale inversion prediction back to the
a prediction for the original image, prior to utilising the Photometric Interpretation.

## Radiographic Projection (View)

The AutoQC output may be compared to a number of DICOM headers which can be selected using the buttons above the chart:
- The View Headers option combines the information from the View Position (0018,5100) and View Code Sequence (0054,0220).
- The Combined Headers option first uses the information from View Headers, but if that is missing it first tries to 
extract it from the Series Description (0008,103E) and if that is missing it tries the Study Description (0008,1030).

The AutoQC prediction is made using the following logic:
- If the image is predicted to be lateral by the lateral projection detection tool, it is labelled as lateral.
- If the image is predicted to be non-lateral by the frontal projection detection tool, the output of the OCR tool is
  used to determine whether the image is AP or PA.
- If the image is predicted to be non-lateral by the frontal projection detection tool, and the OCR tool is unable to
  determine the view, the prediction of the projection deep learning tool is used.

## Body Part Examined / Anatomy

The AutoQC output may be compared to a number of DICOM headers which can be selected using the buttons above the chart:
- The Anatomy Headers option combines the information from the Body Part Examined (0018,0015) and Anatomic Region Sequence
  (0008,2218).
- The Combined Headers option first uses the information from Anatomy Headers, but if that is missing it first tries to 
extract it from the Series Description (0008,103E) and if that is missing it tries the Study Description (0008,1030).

```{note}
N.B. The AutoQC tool used (Aspect Ratio) is not really designed to predict the anatomy, but is rather a 
straightforward tool to determine whether the image would benefit from manual review. As such, the output of the tool 
is not directly comparable to the DICOM header information.
