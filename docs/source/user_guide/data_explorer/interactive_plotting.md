---
myst:
  html_meta:
    "description lang=en": |
      User guide for AutoQC Data Explorer.
---

# Interactive Plotting

The AutoQC Data Explorer provides a number of interactive plotting tools to allow users to explore the data in more 
detail. These plots can be customised to show additional information using the button groups above the plot. The table 
to the left of the plot shows some basic statistics for the selected data.

| Plot Type                       | Example                                                                                    |
|---------------------------------|--------------------------------------------------------------------------------------------|
| Histograms / Distribution Plots | ![Distribution plot example of age versus COVID-19 status](../../_static/hist_example.png) |
| Pie Charts                      | ![Pie chart example of Modality versus COVID-19 status](../../_static/pie_example.png)     |
| Box Plots                       | ![Box plot example of age versus COVID-19 status](../../_static/box_example.png)           |
| Violin Plots                    | ![Violin plot example of age versus COVID-19 status](../../_static/violin_example.png)     |
