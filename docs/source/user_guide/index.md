---
myst:
  html_meta:
    "description lang=en": |
      Introduction to the user guide for AutoQC.
---

# User Guide

This guide provides a user guide to the AutoQC package, the models utilised and the rationale behind the 
functionality included.

:::{note}
:class: sidebar note
The AutoQC package can be used to develop new models from the command line or python with ease. 

Basic information on is provided in the [Deep Learning Models section](sect-train), with 
additional guidance in the [API documentation](sect-api).
:::

```{toctree}
:caption: Getting Started
:maxdepth: 1

getting_started/installation
getting_started/usage
```

```{toctree}
:caption: Data Explorer
:maxdepth: 1

data_explorer/overview
data_explorer/reports
data_explorer/dicom_autoqc_comparison
data_explorer/interactive_plotting
```

```{toctree}
:caption: Development of AutoQC
:maxdepth: 1

development/workflow
development/data
development/labelling
development/additional_details
```

```{toctree}
:caption: Deep Learning Models
:maxdepth: 1

dl_models/preprocessing
dl_models/themodels
dl_models/tuners
dl_models/training
dl_models/prediction
dl_models/evaluation
dl_models/serving
```

```{toctree}
:caption: Operations
:maxdepth: 1

tools/pixel_standardisation
tools/text_removal
tools/cropping
tools/ocr
```

```{toctree}
:caption: Image Classification
:maxdepth: 1

tools/aspect_ratio
tools/quality
```
