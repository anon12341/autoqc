---
myst:
  html_meta:
    "description lang=en": |
      AutoQC development.
---

# Datasets and Evaluation

Over 65,000 CXRs were curated from five datasets covering 39 institutions
in 7 countries. The datasets used were:
- [National Covid-19 Chest Imaging Database (NCCID)](https://nhsx.github.io/covid-chest-imaging-database/)
- [BIMCV-COVID19](https://bimcv.cipf.es/bimcv-projects/bimcv-covid19/)
- [University Hospital (link removed for anonymization)]()
- [BrixIA](https://brixia.github.io/)
- [International COVID-19 Open Radiology Database Release 1c (MIDRC-RICORD-1c)](https://wiki.cancerimagingarchive.net/pages/viewpage.action?pageId=70230281)

The following table summarises the datasets used at the time of development:

| Dataset         | Partition   | Centers / Countries             | No. of radiographs (patients) | Male            | Female         | Mean Age (IQR) |
|:----------------|:------------|:--------------------------------|:------------------------------|:----------------|:---------------|:---------------|
| NCCID           | Development | 22 / UK                         | 19,812 (5,330)                | 12,148 (61.3%)  | 7,664 (38.7%)  | 67.6 (57-81)   |
| BIMCV-COVID-19  | Development | 11 / Spain	                     | 25,447 (9,072)                | 4,549 (50.1%)   | 4,522 (49.9%)  | 62.0 (49-78)   |
| BrixIA COVID-19 | Test        | 1 / Italy                       | 4,689 (2,347)                 | 3,267 (69.7%)   | 1,422 (30.3%)  | 64.4 (57-72)   |
| UH              | Test        | 1 / UK                          | 13,945 (5,298)                | 7,701 (55.3%)   | 6,220 (44.7%)  | 65.3 (53-80)   |
| RICORD-1c       | Test        | 4 / USA, Brazil, Canada, Turkey | 1,256 (361)                   | 822 (65.4%)     | 4,34 (34.6%)   | 57.6 (47-69)   |
|                 |             |                                 |                               |                 |                |                |
| Total           |             |                                 | 65,149 (22,408)	              | 28,487 (58.4%)	 | 20,262 (41.6%) | 65.3 (54-79)   |

The NCCID and BIMCV datasets were used for development, whilst the UH, BrixIA and RICORD-1c 
datasets were used for external testing. The NCCID and BIMCV datasets were used for development 
as they are the largest, containing a wide range of contributing hospitals and scanner types 
offering a diverse range of patients to improve generalisability.

For development of AutoQC, only one image per patient was utilised to avoid
any patient-level bias. The images were first split into development (80\%) and test sets (20\%),
with the development set further split into training (80\%) and validation sets (20\%). The 
test set was held-out for internal evaluation.

```{note}
Full details are detailed in our paper, which is 
[avaliable on request](https://rrw71r81hp8.typeform.com/to/ifo65jAj) having been submitted for 
publication.
```
