---
file_format: mystnb
kernelspec:
  name: python3
  display_name: Python 3
mystnb:
  execution_mode: cache
---

# Additional Details

## Input Data

### File Name Conventions

If you develop tools using the AutoQC package, you may hit some bugs if the filenames do not follow the 
convention:  \<dataset_name\>\_\<patient_id\>_.....  

If only running the pipeline, the name does not matter.

Image files were named using the following conventions during development:

| Dataset   | Naming Convention                                             |
|-----------|---------------------------------------------------------------|
| NCCID     | nccid\_<patient\_id\>\_<sopinstanceuid\>.dcm                  |
| BIMCV     | bimcv\_<patient\_id\>\_<bimcv\_file\_path\>.dcm               |
| UH        | uh\_<patient\_id\>\_<sopinstanceuid\>.dcm                    |
| BrixIA    | brix\_<patient\_id\>\_<study\_date\>\_<brixia\_filename\>.dcm |
| RICORD-1c | ricord1c\_<patient\_id\>\_<sopinstanceuid\>.dcm               |

### Data File Examples

Alongside the images, for development we had csv files containing the QC labels, demographics, some clinical data, and
some important image metadata from the DICOM headers.

Here are two example csv files containing synthetic data:

#### Example CXR list file:
:::{csv-table} 
:file: ../../_static/example_csvs/example_cxr_list.csv
:header-rows: 1
:align: left
:::

#### Example QC label and metadata file:
:::{csv-table} 
:file: ../../_static/example_csvs/example_qc_meta_file.csv
:header-rows: 1
:align: left
:::

## Our Paper and Supplementary Materials

Full details of the pipeline and the individual tools are available in our paper and its supplementary materials.
This includes:
- Rationale for the pipeline and inclusion of each tool
- Best model hyperparameters identified
- Model training and calibration results, including ROC and calibration curves
- Evaluation results, including confusion matrices
- Failure analysis
- GradCam heatmap examples
- Examples of the two-stage rotation model inference
- Examples of image quality metrics
- Ethical approval
- Funding and conflicts of interest
- Acknowledgements

Our paper is [avaliable on request (link removed for anonymization)]() having been submitted for 
publication.
