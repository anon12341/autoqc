---
myst:
  html_meta:
    "description lang=en": |
        AutoQC development.
---

# Image Labelling

The images were labelled for development and testing of AutoQC using [SpeedyQC](https://gitlab.com/anon12341/speedyqc), 
a bespoke, freely available image labelling tool developed by our group.  The tool allows for:

:::{figure} ../../_static/speedy_qc_white_panel.svg
:alt: SpeedyQC Logo
:figwidth: 20%
:align: right
:::

- Viewing of DICOM and other images
- Windowing, inversion and rotation of images
- Labelling of images, which is fully customisable and can be configured for tick-boxes and radio buttons
- Drawing of bounding boxes

:::{figure} ../../_static/speedyqc_screenshot.png
:alt: SpeedyQC Screenshot
:align: center
:auto-fig-width: true
A screenshot of SpeedyQC showing the labelling of a radiograph with bounding boxes.
:::
