.. _sect-train:

Model Training
==============

Imbalanced datasets
-------------------

Most of the datasets we are dealing with are imbalanced. For example,
there are many more images without pacemakers than with pacemakers. To deal with
this issue we sample the images from the larger class and use the same number of images
for each class in each epoch. The sampling process is performed before each
epoch in order to include the maximum number of images from the larger class
in the training process. Only the training set (not the validation set) is
sampled.

Outputs
-------

* Checkpoint (.h5 file) of the best model trained after selecting the best hyperparameters.
* A .csv file with the training history, containing the values of the loss and several metrics (accuracy, recall, precision, f1, etc) at each epoch.

.. tip::
    Plots of the training history can be made automatically using the evaluate script.

Training models
---------------

Once data has been preprocessed we can train models.

Training is performed using the following:

.. tab-set::

    .. tab-item:: Console

        .. code-block:: console

            autoqc train --config <path to config file>

    .. tab-item:: Python

        To run with a configuration file (see below):

        .. code-block:: python

            from autoqc.scripts.train import train

            args = Namespace(config_path="/path/to/config/train.yaml")
            history = train(args)

        To run using the Trainer class directly with the default configuration:

        .. code-block:: python

            from autoqc.scripts.train import Trainer

            trainer = Trainer()
            trainer.read_data(data_dir, dataset_name, path_to_labels)
            history = trainer.run(config.checkpoint, config.early_stopping, config.logfile, config.epochs, )

        (N.B. trainer add /<dataset>/train and /<dataset>/val to data_dir automatically)

        The parameters to customise the Trainer are detailed in the
        :ref:`API documentation <sect-api>` and can be viewed using ``help(Trainer)`` in python.

Configuration files
-------------------

.. code-block:: yaml

    # train.yaml

    # Dataset name
    dataset: dev

    # Location of prepared images
    # (N.B. will add /<dataset>/train and /<dataset>/val to path automatically)
    data_dir: /path/to/preprocessed/

    # Model to train
    model:
      name: ConvNet
      augmentation: false
      batch_size: 32

    tuner:
      name: Hyperband
      tuning_complete: false
      max_epochs: 50
      factor: 3
      iterations: 3

    # Labels (given when preprocessing)
    labels:
      # This will be label = 0 in prediction
      - pa
      # This will be label = 1 in prediction
      - ap

    # Number of epochs to run
    epochs: 50

    # Early stopping
    early_stopping: true

    # Ouput file to save model on checkpoint
    checkpoint: model_projection.h5
    logfile: history_projection.csv

This example will use a Hyperband tuner to find the best hyperparameters for the model with a batch size of 32. The
final model will train for a maximum of 50 epochs using early stopping based on the loss,
saving the best model as ``model_projection.h5`` with the training history in ``history_projection.csv``.

The model to train can currently have the values: ConvNet or RotNet.


Example console output
----------------------

.. code-block:: bash

    Training for 50 epochs with early stopping.
    BaseNet(nlayers=1, nfilters=32, dropout=False, nblocks=1, nunits=32)
    Model: "ConvNet"
    __________________________________________________________________________________________________
     Layer (type)                   Output Shape         Param #     Connected to
    ==================================================================================================
     input_1 (InputLayer)           [(None, None, None,  0           []
                                     1)]

     crop_and_resize (CropAndResize  (None, 256, 256, 1)  0          ['input_1[0][0]']
     )

     conv2d (Conv2D)                (None, 256, 256, 32  320         ['crop_and_resize[0][0]']
                                    )

     max_pooling2d (MaxPooling2D)   (None, 128, 128, 32  0           ['conv2d[0][0]']
                                    )

     conv2d_1 (Conv2D)              (None, 128, 128, 32  320         ['crop_and_resize[0][0]']
                                    )

     add (Add)                      (None, 128, 128, 32  0           ['max_pooling2d[0][0]',
                                    )                                 'conv2d_1[0][0]']

     re_lu (ReLU)                   (None, 128, 128, 32  0           ['add[0][0]']
                                    )

     global_average_pooling2d (Glob  (None, 32)          0           ['re_lu[0][0]']
     alAveragePooling2D)

     dense (Dense)                  (None, 32)           1056        ['global_average_pooling2d[0][0]'
                                                                     ]

     dense_1 (Dense)                (None, 1)            33          ['dense[0][0]']

    ==================================================================================================
    Total params: 1,729
    Trainable params: 1,729
    Non-trainable params: 0
    __________________________________________________________________________________________________

    Reading training images from /path/to/images
    Labels: ['normal', 'inverted']

    Train X: (1573, 256, 256, 1)
    Train Y: (1573,)
    Val X: (175, 256, 256, 1)
    Val Y: (175,)

    Training for 50 epochs with early stopping.
    Epoch 1/50
    50/50 [========] - 50s 987ms/step - loss: 0.2058 - accuracy: 0.9310 - val_loss: 0.0261 - val_accuracy: 0.9943


