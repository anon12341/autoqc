Serving Models
==============

Models are served using a client-server architecture using ZeroMQ. 
The following command will start one server with a Queue device 
listening in port 5560 that will redirect the requests made by a client
to the server:

.. tab-set::

    .. tab-item:: Console

        .. code-block:: console

            autoqc serve <path to model checkpoint> --port 5560 --nservers 1


    .. tab-item:: Python

        .. code-block:: python

            import imageio
            import zmq

            port = 5560
            context = zmq.Context()
            socket = context.socket(zmq.REQ)
            socket.connect(f"tcp://localhost:{port}")

            # load an image
            img = imageio.imread(filename)

            # request model predition
            socket.send_pyobj(img)
            response = socket.recv_pyobj()
