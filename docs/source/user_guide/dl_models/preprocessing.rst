Preprocessing
=============

The first step to train a deep learning model using the AutoQC package is to preprocess the images for training.
This requires files containing the list of original images for each image type / label (e.g. inverted and non-inverted,
or ap and pa for frontal projection). This will:

* read the original images
* perform histogram equalization,
* transform (reshape) to 256x256 dimensions, whilst maintaining the aspect ratio by padding either side of the image as necessary.

These images are the inputs to all the models.

We prepare the data with the following command

.. code-block:: bash

    autoqc preprocess --config <path to config file>

where the configuration file contains the following values:

.. code-block:: yaml

    # prepare_inverted.yaml

    # Dataset name
    dataset: dataset_name

    # File lists
    labels:
      # Inverted images
      - name: inverted
        file: train_inverted.txt
        path: train
      - name: inverted
        file: val_inverted.txt
        path: val
      - name: inverted
        file: test_inverted.txt
        path: test
      # Non inverted images
      - name: noninverted
        file: train_noninverted.txt
        path: train
      - name: noninverted
        file: val_noninverted.txt
        path: val
      - name: noninverted
        file: test_noninverted.txt
        path: test

    # Output directory, i.e. where images for training will be created
    output_dir: /path/to/output_dir

    # Add high intensity enhancement to the images (optional, default: false)
    enhance_high_intensity: false

The labels are arbitrary names ('inverted' and 'noninverted' in the example). Images are read from the correspoding
files in the ``.txt`` file, preprocessed and saved in ``<output_dir>/<dataset>/<label_name>/<path>``.

'High intensity enhancement' is an optional step that can be applied to the images. This is a simple sigmoid function
that increases the contrast of the images for high intensity pixels. This is useful for detecting high intensity
objects such as pacemakers, lines and tubes.

.. note::

    These images are only used for model training and inference to generate QC labels. They are not the outputs of
    the full pipeline; the labels are used to inform any processing steps performed on the original image.
