# Tuners

In order to select the best model, we use [KerasTuner](https://keras.io/api/keras_tuner/) to run 
a hyperparameter search to select the best-performing model based on the accuracy on the 
validation set. We use a [Random Search](https://keras.io/api/keras_tuner/tuners/random/) with a narrower 
hyperparameter search field for the less complex problems (inversion and lateral projection) and 
a [Hyperband](https://keras.io/api/keras_tuner/tuners/hyperband/) algorithm for those which are 
more complex (rotation, projection and pacemaker).

```{note}
When training a model with the AutoQC package, the user may specify the tuner to be used in the 
config file. The default is a 'RandomSearch'.

The tuner settings may also be customised. Please see the API documentation for more information.
```
